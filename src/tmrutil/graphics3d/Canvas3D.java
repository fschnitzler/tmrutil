package tmrutil.graphics3d;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;
import tmrutil.math.geometry.Point3D;
import tmrutil.stats.Random;
import tmrutil.util.ThreeTuple;

/**
 * Canvas3D is a canvas for rendering 3-dimensional objects.
 * @author Timothy A. Mann
 *
 */
public class Canvas3D implements Drawable, KeyListener
{
	private List<Shape3D> _shapes;
	private Camera3D _camera;
	
	/**
	 * Constructs an empty, 3-dimensional canvas with a camera.
	 * @param camera a camera determining the viewing position
	 */
	public Canvas3D(Camera3D camera)
	{
		_camera = camera;
		_shapes = new ArrayList<Shape3D>();
	}
	
	/**
	 * Adds a 3-dimensional shape to be rendered.
	 * @param shape a 3-dimensional shape
	 */
	public void add(Shape3D shape)
	{
		_shapes.add(shape);
	}
	
	/**
	 * Removes a 3-dimensional shape to be rendered.
	 * @param shape a 3-dimensional shape
	 * @return true if the shape was successfully removed; otherwise false
	 */
	public boolean remove(Shape3D shape)
	{
		return _shapes.remove(shape);
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		
		AffineTransform xform = new AffineTransform();
		xform.scale(width/2.0, -height/2.0);
		xform.translate(1, -1);
		g2.transform(xform);
		g2.setStroke(new BasicStroke((float)(1.0/Math.max(width, height))));
		
		for(Shape3D shape : _shapes){
			shape.draw(g2, _camera);
		}
		
		g2.dispose();
	}
	
	@Override
	public void keyPressed(KeyEvent e)
	{
		int kcode = e.getKeyCode();
		switch(kcode){
		case KeyEvent.VK_Z:
			_camera.incrementOrientation(-0.02, 0, 0);
			break;
		case KeyEvent.VK_X:
			_camera.incrementOrientation(0.02, 0, 0);
			break;
		case KeyEvent.VK_C:
			_camera.incrementOrientation(0, -0.02, 0);
			break;
		case KeyEvent.VK_V:
			_camera.incrementOrientation(0, 0.02, 0);
			break;
		case KeyEvent.VK_B:
			_camera.incrementOrientation(0, 0, -0.02);
			break;
		case KeyEvent.VK_N:
			_camera.incrementOrientation(0, 0, 0.02);
			break;
		case KeyEvent.VK_LEFT:
			_camera.incrementPosition(-0.1, 0, 0);
			break;
		case KeyEvent.VK_RIGHT:
			_camera.incrementPosition(0.1, 0, 0);
			break;
		case KeyEvent.VK_DOWN:
			_camera.incrementPosition(0, 0, -0.1);
			break;
		case KeyEvent.VK_UP:
			_camera.incrementPosition(0, 0, 0.1);
			break;
		case KeyEvent.VK_L:
			_camera.incrementPosition(0, -0.1, 0);
			break;
		case KeyEvent.VK_O:
			_camera.incrementPosition(0, 0.1, 0);
			break;
		case KeyEvent.VK_SPACE:
			_camera.setPosition(new Point3D(0, 0, 0));
			_camera.setOrientation(new ThreeTuple<Double,Double,Double>(0.0, 0.0, 0.0));
			break;
		default:
			break;
		}
		e.getComponent().repaint();
		ThreeTuple<Double,Double,Double> orientation = _camera.getOrientation();
		System.out.println("Orientation : (" + orientation.getA() + ", " + orientation.getB() + ", " + orientation.getC() + ")");
		//e.consume();
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		//System.out.println("Key Released!");
		//e.consume();
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		//System.out.println("Key Typed!");
		//e.consume();
	}
	
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("Canvas3D [Test]");
		frame.setSize(640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Camera3D camera = new Camera3D(new Point3D(0,0,0), new ThreeTuple<Double,Double,Double>(0.0,0.0,0.0),Math.toRadians(80));
		Canvas3D canvas = new Canvas3D(camera);
		
		for(int i=0;i<20;i++){
			double x1 = Random.uniform(-1, 1);
			double y1 = Random.uniform(0, 1);
			double z1 = Random.uniform(-1, 1);
			double width = Random.uniform(0.1, 1);
			double height = Random.uniform(0.1, 1);
			Rectangle3D rect = new Rectangle3D(new Point3D(x1, y1, z1), width, height);
			canvas.add(rect);
		}
		
		DrawableComponent dcomp = new DrawableComponent(canvas);
		frame.addKeyListener(canvas);
		
		frame.add(dcomp);
		frame.setVisible(true);
	}

}
