package tmrutil.graphics3d;

import java.awt.geom.Point2D;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.math.geometry.Point3D;
import tmrutil.util.ThreeTuple;

/**
 * Represents a camera in 3-dimensional space. The 3-dimensional space contains
 * 3 axes. The X-axis corresponds to horizontal position, the Y-axis corresponds
 * to depth, and the Z-axis corresponds to elevation.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Camera3D
{
	/** The viewer's position relative to the display surface. */
	private Point3D _viewer;
	/** The position of this camera. */
	private Point3D _position;
	/** Orientation of the camera around the X-, Y-, and Z-axes. */
	private ThreeTuple<Double, Double, Double> _orientation;

	/**
	 * A vector used to buffer points translated to the camera's coordinate
	 * system.
	 */
	private double[] _dBuff;

	public Camera3D(Point3D position,
			ThreeTuple<Double, Double, Double> orientation, double fieldOfView)
	{
		setPosition(position);
		_orientation = new ThreeTuple<Double, Double, Double>(
				orientation.getA(), orientation.getB(), orientation.getC());
		setViewer(new Point3D(0, 0, 1 / Math.tan(fieldOfView/2)));

		_dBuff = new double[3];
	}
	
	private double[][] buildRotateMatrix()
	{
		double thetaX = _orientation.getA();
		double thetaY = _orientation.getB();
		double thetaZ = _orientation.getC();
		
		double cosx = Math.cos(thetaX);
		double cosy = Math.cos(thetaY);
		double cosz = Math.cos(thetaZ);
		double sinx = Math.sin(thetaX);
		double siny = Math.sin(thetaY);
		double sinz = Math.sin(thetaZ);
		
		double[][] xrot = {{1, 0, 0},{0, cosx, -sinx},{0, sinx, cosx}};
		double[][] yrot = {{cosy, 0, siny}, {0, 1, 0}, {-siny, 0, cosy}};
		double[][] zrot = {{cosz, -sinz, 0},{sinz, cosz, 0},{0, 0, 1}};
		
		return MatrixOps.multiply(xrot, MatrixOps.multiply(yrot, zrot));
	}

	/**
	 * Sets the position of this camera.
	 * 
	 * @param position
	 *            a point
	 */
	public void setPosition(Point3D position)
	{
		if (position == null) {
			throw new NullPointerException(
					"The position of the camera cannot be null.");
		}
		_position = position;
	}
	
	/**
	 * Increments the position of the camera by the specified increment values.
	 * @param incx the value to increment along the X-axis
	 * @param incy the value to increment along the Y-axis
	 * @param incz the value to increment along the Z-axis
	 */
	public void incrementPosition(double incx, double incy, double incz)
	{
		Point3D oldPose = getPosition();
		Point3D position = new Point3D(oldPose.getX() + incx, oldPose.getY() + incy, oldPose.getZ() + incx);
		setPosition(position);
	}

	/**
	 * Returns the position of this camera.
	 * 
	 * @return a point
	 */
	public Point3D getPosition()
	{
		return _position;
	}

	/**
	 * Sets the orientation of this camera. The orientation with respect to the
	 * X, Y, and Z axes are set by the first, second, and third elements of the
	 * tuple, respectively.
	 * 
	 * @param orientation
	 *            a three tuple with orientations in radians
	 */
	public void setOrientation(ThreeTuple<Double, Double, Double> orientation)
	{
		_orientation.setA(normalizeRadian(orientation.getA()));
		_orientation.setB(normalizeRadian(orientation.getB()));
		_orientation.setC(normalizeRadian(orientation.getC()));
	}
	
	/**
	 * Used to normalize radians between 0 and 2pi. 
	 * @param radian a radian value
	 * @return an equivalent radian value between 0 and 2pi
	 */
	private double normalizeRadian(double radian)
	{
		if(radian > 2 * Math.PI){
			radian = radian % (2 * Math.PI);
			return radian;
		}else if(radian < 0){
			double div = Math.ceil(radian / (2 * Math.PI));
			return radian - ((div - 1) * 2 * Math.PI);
		}else{
			return radian;
		}
	}
	
	/**
	 * Increments the orientation of this camera by the specified values with respect to the X, Y, and Z axes.
	 * @param xinc an increment to the orientation with respect to the X-axis in radians
	 * @param yinc an increment to the orientation with respect to the Y-axis in radians
	 * @param zinc an increment to the orientation with respect to the Z-axis in radians
	 */
	public void incrementOrientation(double xinc, double yinc, double zinc)
	{
		double xr = _orientation.getA() + xinc;
		double yr = _orientation.getB() + yinc;
		double zr = _orientation.getC() + zinc;
		ThreeTuple<Double,Double,Double> orientation = new ThreeTuple<Double,Double,Double>(xr,yr,zr);
		setOrientation(orientation);
	}

	/**
	 * Gets the orientation of this camera. The orientation with respect to the
	 * X, Y, and Z axes are specified by the first, second, and third elements
	 * of the tuple, respectively.
	 * 
	 * @return a three tuple with orientations in radians
	 */
	public ThreeTuple<Double, Double, Double> getOrientation()
	{
		return new ThreeTuple<Double, Double, Double>(_orientation.getA(),
				_orientation.getB(), _orientation.getC());
	}

	/**
	 * Sets the position of the viewer with respect to the display surface.
	 * 
	 * @param viewer
	 *            a point
	 */
	public void setViewer(Point3D viewer)
	{
		if (viewer == null) {
			throw new NullPointerException(
					"The position of the viewer with respect to the display surface cannot be null.");
		}
		_viewer = viewer;
	}

	/**
	 * Returns the position of the viewer with respect to the display surface.
	 * 
	 * @return the position of the viewer with respect to the display surface
	 */
	public Point3D getViewer()
	{
		return _viewer;
	}

	/**
	 * Projects a single 3-dimensional point to a 2-dimensional point for
	 * rendering.
	 * 
	 * @param point
	 *            a 3-dimensional point
	 * @return a 2-dimensional point
	 */
	public Point2D project(Point3D point)
	{
		double[] cpoint = translateToCamera(point);
		double x = (cpoint[0] - _viewer.getX()) / (_viewer.getZ() / cpoint[2]);
		double y = (cpoint[1] - _viewer.getY()) / (_viewer.getZ() / cpoint[2]);
		return new Point2D.Double(x, y);
	}

	/**
	 * Translates a single 3-dimensional point in world space into a
	 * 3-dimensional point in the camera's space.
	 * 
	 * @param point
	 *            a 3-dimensional point in world space
	 * @return a 3-dimensional double array with translated point
	 */
	private double[] translateToCamera(Point3D point)
	{		
		double[] diff = {point.getX() - _position.getX(), point.getY() - _position.getY(), point.getZ() - _position.getZ()};

		MatrixOps.multiply(buildRotateMatrix(), diff, _dBuff);
		
		return _dBuff;
	}
}
