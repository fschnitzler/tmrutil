package tmrutil.stats;

/**
 * A filter is used to determine whether or not an element is accepted or not.
 * 
 * @author Timothy Mann
 * 
 * @param <X>
 *            the type of data that will be discriminated by this filter
 */
public interface Filter<X>
{
	/**
	 * Determines whether an element should be accepted or not. This method
	 * returns true if the filter recommends the element to be accepted and
	 * false otherwise.
	 * 
	 * @param x
	 *            an element
	 * @return true if the element should be accepted; otherwise false
	 */
	public boolean accept(X x);
}
