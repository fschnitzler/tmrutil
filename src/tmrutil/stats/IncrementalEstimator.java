package tmrutil.stats;

import java.util.Collection;

/**
 * An interface for incremental estimators.
 * 
 * @param <S> the sample type
 * @param <T> the estimate type
 * 
 * @author Timothy Mann
 * 
 */
public interface IncrementalEstimator<S,T>
{
	/**
	 * Returns an estimate for the parameter that this incremental estimator is
	 * designed to estimate.
	 * 
	 * @return an estimate for a parameter
	 * @throws InsufficientSamplesException
	 *             if this method is called before any samples have been
	 *             provided.
	 */
	public T estimate() throws InsufficientSamplesException;

	/**
	 * Adds a sample to this estimator.
	 * 
	 * @param sample
	 *            a sample
	 */
	public void add(S sample);

	/**
	 * Adds a collection of samples to this estimator.
	 * 
	 * @param samples
	 *            a collection of sample values
	 */
	public void add(Collection<S> samples);

	/**
	 * Adds an array of samples to this estimator.
	 * 
	 * @param samples
	 *            an array of sample values
	 */
	public void add(S[] samples);

	/**
	 * Returns the number of samples that have been added to this estimator.
	 * 
	 * @return the number of samples that have been added to this estimator
	 */
	public int numSamples();

	/**
	 * Returns true if this estimator has at least the minimum number of samples
	 * required to make an estimate of the parameter and false otherwise.
	 * 
	 * @return true if the estimator has at least the minimum number of samples
	 *         to make an estimate; otherwise false
	 */
	public boolean hasMinimumSamples();
	
	/**
	 * Returns the minimum number of samples required to estimate the parameter.
	 * @return the minimum number of samples required to estimate the parameter
	 */
	public int minimumNumSamples();
}
