package tmrutil.stats;

import java.util.Collection;

public abstract class AbstractIncrementalEstimator<S,T> implements
		IncrementalEstimator<S,T>
{


	@Override
	public void add(Collection<S> samples)
	{
		for(S sample : samples){
			add(sample);
		}
	}

	@Override
	public void add(S[] samples)
	{
		for(S sample : samples){
			add(sample);
		}
	}
	
	@Override
	public boolean hasMinimumSamples()
	{
		return numSamples() >= minimumNumSamples();
	}

}
