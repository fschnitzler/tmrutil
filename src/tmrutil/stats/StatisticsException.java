/*
 * StatisticsException.java
 */

package tmrutil.stats;

/**
 * Thrown to indicate a statistical exception.
 */
public class StatisticsException extends RuntimeException
{

	private static final long serialVersionUID = 2052966216360944120L;

	public StatisticsException(String message)
    {
	super(message);
    }
}