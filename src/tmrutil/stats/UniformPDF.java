/*
 * UniformPDF.java
 */

/* Package */
package tmrutil.stats;

/* Imports */

/**
 * Represents a univariate uniform probability density function.
 */
public class UniformPDF extends PDF
{
	private static final long serialVersionUID = 5850101772160416377L;
	
	private double _mean;
	private double _variance;
	
	/** Lower bound on the uniform density function. */
	private double _a;
	/** Upper bound on the uniform density function. */
	private double _b;

	/**
	 * Constructs a uniform probability distribution.
	 * @param a the lower bound of the nonzero region of this density function
	 * @param b the upper bound of the nonzero region of this density function
	 * @throws IllegalArgumentException if <code>b &lt; a</code>
	 */
	public UniformPDF(double a, double b)
	{
		if (b < a) {
			throw new IllegalArgumentException(
					"The lower bound of the uniform distribution must be less than the upper bound. The interval specified is [a="
							+ a + ", b=" + b + "]");
		}
		
		_mean = ((b + a) / 2);
		_variance = Math.pow((b - a), 2) / 12.0;
		_a = a;
		_b = b;
	}
	
	@Override
	public Double evaluate(Double x)
	{
		return 1.0 / (_b - _a);
	}

	@Override
	public Double integrate(Double a, Double b) throws IllegalArgumentException
	{
		if (b < a) {
			throw new IllegalArgumentException(
					"The lower bound of the interval is greater than the upper bound [a="
							+ a + ", b=" + b + "]");
		}
		a = Math.max(a, _a);
		b = Math.min(b, _b);
		return (b - a) * evaluate(b);
	}

	@Override
	public double mean()
	{
		return _mean;
	}

	@Override
	public double variance()
	{
		return _variance;
	}
}