package tmrutil.stats;

/**
 * A generative distribution can be sampled from.
 * @author Timothy A. Mann
 *
 * @param <X> the support type
 */
public interface GenerativeDistribution<X> {
	/**
	 * Draws a sample from this generative distribution.
	 * @return a sample from this distribution
	 */
	public X sample();
}
