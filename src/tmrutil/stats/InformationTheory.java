package tmrutil.stats;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class InformationTheory
{
	/**
	 * Computes the entropy of a discrete probability distribution.
	 * 
	 * @param pdist
	 *            a probability distribution
	 * @return the entropy of the distribution
	 */
	public static final <O> double entropy(Map<O, Double> pdist)
	{
		double h = 0;
		int size = pdist.size();
		if(size<= 1){
			return h;
		}
		double logBase = Math.log(size);
		Set<O> keys = pdist.keySet();
		for (O key : keys) {
			double p = pdist.get(key);
			if (p > 0) {
				h -= p * Math.log(p) / logBase;
			}
		}

		return h;
	}

	/**
	 * Computes the entropy of a discrete probability distribution.
	 * 
	 * @param pdist
	 *            a probability distribution
	 * @return the entropy of the distribution
	 */
	public static final double entropy(Collection<Double> pdist)
	{
		double h = 0;
		double logBase = Math.log(pdist.size());
		for (double p : pdist) {
			if (p > 0) {
				h -= p * Math.log(p) / logBase;
			}
		}
		return h;
	}

	/**
	 * Computes the entropy of a discrete probability distribution.
	 * 
	 * @param pdist
	 *            a probability distribution
	 * @return the entropy of the distribution
	 */
	public static final double entropy(double[] pdist)
	{
		double h = 0;
		double logBase = Math.log(pdist.length);
		for (double p : pdist) {
			if (p > 0) {
				h -= p * Math.log(p) / logBase;
			}
		}

		return h;
	}

	/**
	 * Computes the Kullback-Leibler divergence between two discrete probability
	 * distributions P and Q.
	 * 
	 * @param pdist
	 *            a probability distribution P on events 0, 1, ..., N
	 * @param qdist
	 *            a probability distribution Q on events 0, 1, ..., N
	 * @return the KL divergence between P and Q
	 */
	public static final double KLdivergence(double[] pdist, double[] qdist)
	{
		if (pdist.length != qdist.length) {
			throw new IllegalArgumentException(
					"Kullback-Leibler divergence is invalid for distributions that do not have the same outcomes.");
		}
		double kld = 0;
		for (int i = 0; i < pdist.length; i++) {
			if (pdist[i] != 0) {
				kld += pdist[i] * Math.log(pdist[i] / qdist[i]);
			}
		}
		return kld;
	}
}
