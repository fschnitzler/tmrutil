package tmrutil.stats.vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.util.Precision;

import tmrutil.stats.StdEstimator;
import tmrutil.util.Pair;

/**
 * ZScore normalizes a supervised training set so that both the input vectors
 * and targets have zero mean and standard deviation 1.
 * 
 * @author Timothy A. Mann
 *
 */
public class ZScore {

	/**
	 * Original input matrix.
	 */
	private RealMatrix _origX;
	/**
	 * Original output vector.
	 */
	private RealVector _origY;

	/**
	 * Normalized input matrix matrix.
	 */
	private RealMatrix _nX;
	/**
	 * Noramlized output vector.
	 */
	private RealVector _nY;

	private int _inputDims;
	private RealVector _inputMean;
	private RealVector _inputStd;

	private double _outputMean;
	private double _outputStd;

	public ZScore(Collection<Pair<RealVector, Double>> samples) {
		List<StdEstimator> inputStdEsts = new ArrayList<StdEstimator>();
		StdEstimator outputStdEst = new StdEstimator();

		int n = samples.size();
		_inputDims = -1;
		int r = 0;
		for (Pair<RealVector, Double> sample : samples) {
			RealVector x = sample.getA();
			double y = sample.getB();

			/*
			 * On the first sample, determine the input dimension.
			 */
			if (_inputDims < 1) {
				_inputDims = x.getDimension();
				_origX = MatrixUtils.createRealMatrix(n, _inputDims);
				_origY = new ArrayRealVector(n, 0.0);

				for (int c = 0; c < _inputDims; c++) {
					inputStdEsts.add(new StdEstimator());
				}
			}

			/*
			 * Add the samples the the standard deviation estimators and insert
			 * the input vector data and target responses.
			 */
			for (int c = 0; c < _inputDims; c++) {
				_origX.setEntry(r, c, x.getEntry(c));
				inputStdEsts.get(c).add(x.getEntry(c));
			}
			_origY.setEntry(r, y);
			outputStdEst.add(y);

			/*
			 * Increment the row index.
			 */
			r++;
		}
		
		_inputMean = new ArrayRealVector(_inputDims);
		_inputStd = new ArrayRealVector(_inputDims);
		for(int i=0;i<_inputDims;i++){
			StdEstimator istdEst = inputStdEsts.get(i);
			_inputMean.setEntry(i, istdEst.mean());
			
			double stdEst = istdEst.estimate();
			if(Precision.equals(stdEst, 0, tmrutil.math.Constants.EPSILON)){
				stdEst = 1;
			}
			_inputStd.setEntry(i, stdEst);
		}
		_outputMean = outputStdEst.mean();
		_outputStd = outputStdEst.estimate();
		if(Precision.equals(_outputStd, 0, tmrutil.math.Constants.EPSILON)){
			_outputStd = 1;
		}
		
		_nX = new Array2DRowRealMatrix(n, _inputDims);
		_nY = new ArrayRealVector(n);
		for(r=0; r<n;r++){
			RealVector x = _origX.getRowVector(r);
			RealVector nx = x.subtract(_inputMean).ebeDivide(_inputStd);
			
			double y = _origY.getEntry(r);
			double ny = (y-_outputMean) / _outputStd;
			
			_nX.setRow(r, nx.toArray());
			_nY.setEntry(r, ny);
		}

	}
	
	public int inputDimension(){
		return _inputDims;
	}
	
	public int numberOfInstances(){
		return _origY.getDimension();
	}

	public double normalizeOutput(double y) {
		return (y - _outputMean) / _outputStd;
	}

	public double unnormalizeOutput(double y) {
		return (y * _outputStd) + _outputMean;
	}

	public RealVector normalizeInput(RealVector x) {
		return (x.subtract(_inputMean)).ebeDivide(_inputStd);
	}

	public RealVector unnormalizeInput(RealVector x) {
		return (x.ebeMultiply(_inputStd)).add(_inputMean);
	}

	/**
	 * Returns a matrix containing the original (unnormalized) input vectors used to compute the z-score.
	 * @return a matrix of the original input vectors
	 */
	public RealMatrix origX() {
		return _origX;
	}

	/**
	 * Returns a vector containing the original (unnormalized) response variables used to compute the z-score.
	 * @return a vector of the original response variables
	 */
	public RealVector origY() {
		return _origY;
	}

	/**
	 * Returns a matrix containing the normalized input vectors used to compute the z-score.
	 * @return a matrix of normalized input vectors
	 */
	public RealMatrix nX() {
		return _nX;
	}

	/**
	 * Returns a vector of normalized targets.
	 * @return a vector of normalized output targets
	 */
	public RealVector nY() {
		return _nY;
	}
}
