package tmrutil.stats.vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;

/**
 * Implements the k-means clustering algorithm.
 * 
 * @author Timothy Mann
 */
public final class Kmeans
{
	/**
	 * Implements the k-mean flat clustering algorithm.
	 * 
	 * @param data
	 *          a set of vectors
	 * @param k
	 *          the number of clusters
	 * @return a list of the k clusters.
	 * @throws IllegalArgumentException
	 *           if <code>k &lt; 1</code>
	 */
	public static final List<Collection<double[]>> kmeans(Collection<double[]> data, int k)
	{
		if (k < 1)
		{
			throw new IllegalArgumentException("Cannot have less than 1 cluster: k = " + k);
		}

		java.util.Random rand = new java.util.Random();
		List<double[]> centroids = new ArrayList<double[]>();
		List<Collection<double[]>> clusters = new ArrayList<Collection<double[]>>();
		for (int i = 0; i < k; i++)
		{
			clusters.add(new LinkedList<double[]>());
		}

		// Assign each vector to a random cluster
		Iterator<double[]> iter = data.iterator();
		while (iter.hasNext())
		{
			int i = Math.abs(rand.nextInt()) % k;
			clusters.get(i).add(iter.next());
		}

		boolean reassigned = true;
		while (reassigned)
		{
			// Clean up from the last iteration
			reassigned = false;
			centroids.clear();
			
			List<Collection<double[]>> newClusters = new ArrayList<Collection<double[]>>(k);
			
			// Calculate the new centroids
			for (int i = 0; i < k; i++)
			{
				Collection<double[]> cluster = clusters.get(i);
				centroids.add(Statistics.vectorMean(cluster));
				newClusters.add(new LinkedList<double[]>());
			}

			// Assign the vectors to the nearest centroid
			for (int i = 0; i < k; i++)
			{
				for (double[] vector : clusters.get(i))
				{
					double minDist = VectorOps.distance(vector, centroids.get(0));
					int minI = 0;
					for (int j = 1; j < k; j++)
					{
						double dist = VectorOps.distance(vector, centroids.get(j));
						if (minDist > dist)
						{
							minDist = dist;
							minI = j;
						}
					}
					newClusters.get(minI).add(vector);
					if (minI != i)
					{
						reassigned = true;
					}
				}
			}
			clusters = newClusters;
		}

		return clusters;
	}

	public static void main(String[] args)
	{
		int n = 10000;
		List<double[]> points = new ArrayList<double[]>(n);
		java.util.Random rand = new java.util.Random();
		for(int i=0;i<n;i++)
		{
			if(rand.nextDouble() > 0.5){
				points.add(new double[]{tmrutil.stats.Random.normal(-1,1), tmrutil.stats.Random.normal(0,1)});
			}else{
				points.add(new double[]{tmrutil.stats.Random.normal(1,1), tmrutil.stats.Random.normal(0,1)});
			}
		}
		
		System.out.println("Begin Clustering ... ");
		List<Collection<double[]>> clusters = kmeans(points, 2);
		System.out.println("Finished Clustering!");
		
		double[][] data1 = new double[clusters.get(0).size()][];
		double[][] data2 = new double[clusters.get(1).size()][];
		int ind = 0;
		for(Iterator<double[]> iter = clusters.get(0).iterator(); iter.hasNext();){
			data1[ind++] = iter.next();
		}
		ind = 0;
		for(Iterator<double[]> iter = clusters.get(1).iterator(); iter.hasNext();)
		{
			data2[ind++] = iter.next();
		}
		tmrutil.graphics.plot2d.Plot2D.scatterPlot(data1,java.awt.Color.RED,data2,java.awt.Color.BLUE,"k-means Test", "X", "Y");
	}
}
