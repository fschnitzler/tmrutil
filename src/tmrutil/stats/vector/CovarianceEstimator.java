package tmrutil.stats.vector;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.stats.AbstractIncrementalEstimator;
import tmrutil.stats.InsufficientSamplesException;
import tmrutil.stats.MeanEstimator;

/**
 * Estimates the covariance matrix from vector samples.
 * 
 * @author Timothy Mann
 *
 */
public class CovarianceEstimator extends
		AbstractIncrementalEstimator<RealVector, RealMatrix> {
	private int _numSamples;
	private int _dimensions;
	private MeanEstimator[] _indMeans;
	private MeanEstimator[][] _coMeans;

	public CovarianceEstimator(int dimensions) {
		_numSamples = 0;
		_dimensions = dimensions;

		_indMeans = new MeanEstimator[dimensions];
		_coMeans = new MeanEstimator[dimensions][dimensions];

		for (int i = 0; i < dimensions; i++) {
			_indMeans[i] = new MeanEstimator();
			for (int j = 0; j <= i; j++) {
				_coMeans[i][j] = new MeanEstimator();
			}
		}
	}

	@Override
	public void add(RealVector sample) {
		_numSamples++;
		for (int i = 0; i < _dimensions; i++) {
			double si = sample.getEntry(i);
			_indMeans[i].add(si);
			for (int j = 0; j <= i; j++) {
				double sj = sample.getEntry(j);
				_coMeans[i][j].add(si * sj);
			}
		}
	}
	
	public void add(double[] sample){
		RealVector vsample = MatrixUtils.createRealVector(sample);
		add(vsample);
	}

	public void add(int[] sample) {
		double[] dsample = new double[sample.length];
		for (int i = 0; i < sample.length; i++) {
			dsample[i] = sample[i];
		}
		add(dsample);
	}

	public void add(byte[] sample) {
		double[] dsample = new double[sample.length];
		for (int i = 0; i < sample.length; i++) {
			dsample[i] = sample[i];
		}
		add(dsample);
	}

	public RealVector estimateMean() {
		double[] mean = new double[_dimensions];
		for (int i = 0; i < _dimensions; i++) {
			mean[i] = _indMeans[i].estimate();
		}
		return MatrixUtils.createRealVector(mean);
	}

	public RealVector estimateStd() {
		double[] std = new double[_dimensions];
		for (int i = 0; i < _dimensions; i++) {
			double meani = _indMeans[i].estimate();
			double vari = _coMeans[i][i].estimate() - (meani * meani);
			std[i] = Math.sqrt(vari);
		}
		return MatrixUtils.createRealVector(std);
	}

	@Override
	public RealMatrix estimate() throws InsufficientSamplesException {
		double[][] cov = new double[_dimensions][_dimensions];
		for (int i = 0; i < _dimensions; i++) {
			double meani = _indMeans[i].estimate();
			double meani2 = meani * meani;
			for (int j = 0; j <= i; j++) {
				if (i == j) {
					cov[i][j] = _coMeans[i][j].estimate()
							- meani2;
				} else {
					double val = _coMeans[i][j].estimate()
							- meani2;
					cov[i][j] = val;
					cov[j][i] = val;
				}
			}
		}
		return MatrixUtils.createRealMatrix(cov);
	}

	public RealMatrix correlation() throws InsufficientSamplesException {
		RealMatrix cov = estimate();
		double[][] cor = new double[_dimensions][_dimensions];
		for (int i = 0; i < _dimensions; i++) {
			double sigmai = Math.sqrt(cov.getEntry(i, i));
			for (int j = 0; j < _dimensions; j++) {
				double covij = cov.getEntry(i, j);
				double sigmaj = Math.sqrt(covij);
				cor[i][j] = covij / (sigmai * sigmaj);
			}
		}
		return MatrixUtils.createRealMatrix(cor);
	}

	@Override
	public int minimumNumSamples() {
		return 1;
	}

	@Override
	public int numSamples() {
		return _numSamples;
	}

}
