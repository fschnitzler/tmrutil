package tmrutil.cv;

import java.awt.Color;

/**
 * Contains information about a particular pixel (picture element) in an image.
 * @author Timothy A. Mann
 *
 */
public class Pixel
{
	private int _x;
	private int _y;
	private Color _color;
	
	public Pixel(int x, int y, Color color)
	{
		_x = x;
		_y = y;
		_color = new Color(color.getRGB());
	}
	
	public int getX()
	{
		return _x;
	}
	
	public int getY()
	{
		return _y;
	}
	
	public Color getColor()
	{
		return _color;
	}
}
