package tmrutil.cv;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ByteLookupTable;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.awt.image.LookupOp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 * This class contains static operations for image processing.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ImageOps
{	
	private static final float[] BLUR_KERNEL = {
		0.1f, 0.1f, 0.1f,
		0.1f, 0.2f, 0.1f,
		0.1f, 0.1f, 0.1f
	};
	
	/**
	 * Loads an image from a file.
	 * @param filename an image path and file name
	 * @return a buffered image with data loaded from the specified file
	 */
	public static final BufferedImage load(String filename)
	{
		Image image = new ImageIcon(filename).getImage();
		BufferedImage bimage = new BufferedImage(image.getWidth(null),image.getHeight(null),BufferedImage.TYPE_INT_ARGB);
		Graphics g = bimage.createGraphics();
		g.drawImage(image, 0, 0, null);
		return bimage;
	}
	
	/**
	 * Inverts the color of this image.
	 * @param image an image to invert
	 * @return a copy of the specified image with inverted colors
	 */
	public static final BufferedImage invertColor(BufferedImage image)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		BufferedImage newImage = new BufferedImage(width, height, image.getType());
		for(int x = 0; x < width; x++){
			for(int y = 0; y < height; y++){
				int rgb = image.getRGB(x,y);
				Color c = new Color(rgb);
				Color invertC = new Color(255 - c.getRed(), 255 - c.getGreen(), 255 - c.getBlue());
				newImage.setRGB(x, y, invertC.getRGB());
			}
		}
		return newImage;
	}
	
	/**
	 * Blur the image using a Guassian kernel.
	 * @param image the image to blur
	 * @return a blurred version of the specified image
	 */
	public static final BufferedImage gaussianBlur(BufferedImage image)
	{
		ConvolveOp cOp = new ConvolveOp(new Kernel(3, 3, BLUR_KERNEL), ConvolveOp.EDGE_NO_OP, null);
		return cOp.filter(image, null);
	}
	
	/**
	 * Blur the image using a Gaussian kernel.
	 * @param image the image to blur
	 * @param width the width of the kernel to convolve
	 * @param height the height of the kernel to convolve
	 * @param sigma the bandwidth of the Gaussian kernel
	 * @return a blurred version of the specified image
	 */
	public static final BufferedImage gaussianBlur(BufferedImage image, int width, int height, double sigma)
	{
		double sigma2 = sigma * sigma;
		Point2D center = new Point2D.Float(width/2.0f, height/2.0f);
		float[] kernel = new float[width * height];
		for(int r = 0; r < height; r++){
			for(int c = 0; c < width; c++){
				int index = (r * width) + c;
				kernel[index] = (float)Math.exp(-center.distanceSq(c, r) / sigma2);
			}
		}
		ConvolveOp cOp = new ConvolveOp(new Kernel(width, height, kernel), ConvolveOp.EDGE_NO_OP, null);
		return cOp.filter(image, null);
	}
	
	/**
	 * Computes the ratio of pixels accepted by a pixel filter to the total
	 * number of pixels in an image.
	 * 
	 * @param image an image
	 * @param filter a color filter
	 * @return the ratio of pixels accepted by the filter to the total number of pixels
	 */
	public static final double areaRatio(BufferedImage image, ColorFilter filter)
	{
		double total = image.getWidth() * image.getHeight();
		int acceptCount = 0;
		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				int rgb = image.getRGB(x, y);
				Color color = new Color(rgb);
				if (filter.accept(color)) {
					acceptCount++;
				}
			}
		}
		return acceptCount / total;
	}
	
	/**
	 * Finds the largest blob in an image.
	 * @param image an image
	 * @param filter a color filter used to determine blob regions
	 * @return the largest blob
	 */
	public static final Blob findLargestBlob(BufferedImage image, ColorFilter filter)
	{
		Blob largestBlob = null;
		double maxAreaRatio = 0;
		Collection<Blob> blobs = extractBlobs(image, filter);
		for(Blob blob : blobs){
			double areaRatio = blob.areaRatio();
			if(areaRatio > maxAreaRatio){
				maxAreaRatio = areaRatio;
				largestBlob = blob;
			}
		}
		return largestBlob;
	}
	
	/**
	 * Extracts all blobs from an image using a specified color filter.
	 * @param image an image
	 * @param filter a color filter
	 * @return a collection of blobs in the image
	 */
	public static final Collection<Blob> extractBlobs(BufferedImage image, ColorFilter filter)
	{
		List<Blob> blobs = new ArrayList<Blob>();
		
		int width = image.getWidth();
		int height = image.getHeight();
		boolean[][] acceptedPixels = new boolean[width][height];
		for(int x=0;x<width;x++){
			for(int y=0;y<height;y++){
				Color color = new Color(image.getRGB(x,y));
				if(filter.accept(color)){
					acceptedPixels[x][y] = true;
				}else{
					acceptedPixels[x][y] = false;
				}
			}
		}
		
		for(int x=0;x<image.getWidth();x++){
			for(int y=0;y<image.getHeight();y++){
				if(acceptedPixels[x][y]){
					boolean pixelInExistingBlob = false;
					for(Blob blob : blobs){
						if(blob.contains(x,y)){
							pixelInExistingBlob = true;
							break;
						}
					}
					if(!pixelInExistingBlob){
						Blob blob = Blob.constructBlob(acceptedPixels, new Point(x,y), image.getWidth(), image.getHeight());
						blobs.add(blob);
					}
				}
			}
		}
		
		return blobs;
	}
	
	public static void main(String[] args) throws Exception{
//		BufferedImage bimage = load("/home/tim/test_image2.png");
//		ColorFilter cfilter = new GrayscaleColorFilter(0.5);
//		Blob blob = findLargestBlob(bimage, cfilter);
//		System.out.println("Largest Blob Area Ratio: " + blob.areaRatio());
//		final Image blobImage = blob.maskedImage(bimage, Color.RED);
		
		final BufferedImage blobImage = invertColor(load("/home/tim/Pictures/conrad.png"));
		
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(640, 480);
		
		JComponent comp = new JComponent(){
			public void paintComponent(Graphics g){
				g.drawImage(blobImage, 0, 0, getWidth(), getHeight(), null);
			}
		};
		frame.add(comp);
		frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
