package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.util.Precision;

import tmrutil.math.Constants;
import tmrutil.stats.MeanEstimator;
import tmrutil.stats.Random;
import tmrutil.stats.StdEstimator;
import tmrutil.stats.VarianceEstimator;
import tmrutil.stats.vector.CovarianceEstimator;
import tmrutil.util.Pair;

/**
 * A base class for labeled data sets with real valued vector inputs.
 * 
 * @author Timothy A. Mann
 *
 * @param <L>
 *            the label type
 */
public class AbstractLabeledData<L> implements LabeledData<RealVector, L> {

	protected List<Pair<RealVector, L>> _data;
	protected Integer _dim;

	public AbstractLabeledData(Collection<Pair<RealVector, L>> data) {
		if (data.isEmpty()) {
			throw new IllegalArgumentException(
					getClass().getSimpleName()
							+ " instance cannot be constructed with no labeled samples.");
		}
		_data = new ArrayList<Pair<RealVector, L>>(data);
		_dim = _data.get(0).getA().getDimension();
	}

	/**
	 * The number of variables in the set of input vectors.
	 * 
	 * @return the dimensionality of the input vectors
	 */
	public int inputDimension() {
		return _dim;
	}

	/**
	 * Returns the input vectors as rows in a matrix.
	 * 
	 * @return a matrix where each row is an input vector
	 */
	public RealMatrix inputMatrix() {
		double[][] imat = new double[numberOfInstances()][];
		for (int i = 0; i < numberOfInstances(); i++) {
			imat[i] = getDatum(i).toArray();
		}
		return MatrixUtils.createRealMatrix(imat);
	}

	@Override
	public final Iterator<Pair<RealVector, L>> iterator() {
		return _data.iterator();
	}

	@Override
	public final int numberOfInstances() {
		return _data.size();
	}

	@Override
	public final Pair<RealVector, L> get(int i) {
		return _data.get(i);
	}

	@Override
	public final RealVector getDatum(int i) {
		return _data.get(i).getA();
	}

	@Override
	public final L getLabel(int i) {
		return _data.get(i).getB();
	}

	@Override
	public final Pair<RealVector, L> sample() {
		int ri = Random.nextInt(numberOfInstances());
		return get(ri);
	}

	@Override
	public final List<Pair<RealVector, L>> toList() {
		return toList(false);
	}
	
	public final List<Pair<RealVector,L>> toList(boolean zscore){
		return slice(0, _data.size(), zscore);
	}

	@Override
	public final List<Pair<RealVector, L>> slice(int beginIndex, int endIndex) {
		return slice(beginIndex, endIndex, false);
	}
	
	public final List<Pair<RealVector,L>> slice(int beginIndex, int endIndex, boolean zscore)
	{
		if(zscore){
			RealVector mean = inputMean();
			RealVector std = inputStd();
			List<Pair<RealVector,L>> data = new ArrayList<Pair<RealVector,L>>(endIndex - beginIndex);
			for(int i=beginIndex;i<endIndex;i++){
				Pair<RealVector,L> pair = get(i);
				Pair<RealVector,L> newPair = new Pair<RealVector,L>(zscore(pair.getA(), mean, std), pair.getB());
				data.add(newPair);
			}
			return data;
		}else{
			return new ArrayList<Pair<RealVector,L>>(_data.subList(beginIndex, endIndex));
		}
	}
	
	public static final RealVector zscore(RealVector x, RealVector mean, RealVector std){
		return x.subtract(mean).ebeDivide(std);
	}
	
	public static final RealVector unzscore(RealVector x, RealVector mean, RealVector std){
		return x.ebeMultiply(std).add(mean);
	}

	public static final RealVector extractSubset(RealVector x,
			List<Integer> subset) {
		int adim = subset.size();
		double[] ax = new double[adim];
		for (int i = 0; i < adim; i++) {
			ax[i] = x.getEntry(subset.get(i));
		}
		return MatrixUtils.createRealVector(ax);
	}

	public static final RealVector project(RealVector x, RealMatrix linTran) {
		return linTran.preMultiply(x);
	}

	/**
	 * Returns a list of indices corresponding to the variables of input vectors
	 * that have non-zero (nontrivial) variance.
	 * 
	 * @return a list of indices of variables with non-zero variance
	 */
	public List<Integer> variablesWithNontrivialVariance() {
		int dim = inputDimension();
		List<VarianceEstimator> vEsts = new ArrayList<VarianceEstimator>(dim);
		for (int i = 0; i < dim; i++) {
			vEsts.add(new VarianceEstimator());
		}

		for (Pair<RealVector, L> pair : _data) {
			RealVector v = pair.getA();
			for (int i = 0; i < dim; i++) {
				vEsts.get(i).add(v.getEntry(i));
			}
		}

		List<Integer> activeSet = new ArrayList<Integer>();
		for (int i = 0; i < dim; i++) {
			double std = vEsts.get(i).estimate();
			if (!Precision.equals(std, 0.0, Constants.EPSILON)) {
				activeSet.add(i);
			}
		}
		return activeSet;
	}

	/**
	 * Estimates the sample mean of the input vectors in this data set.
	 * 
	 * @return the sample mean vector
	 */
	public RealVector inputMean() {
		List<MeanEstimator> mest = new ArrayList<MeanEstimator>(
				inputDimension());
		for (int i = 0; i < inputDimension(); i++) {
			mest.add(new MeanEstimator());
		}

		for (Pair<RealVector, L> pair : _data) {
			RealVector x = pair.getA();
			for (int i = 0; i < inputDimension(); i++) {
				mest.get(i).add(x.getEntry(i));
			}
		}

		double[] dmean = new double[inputDimension()];
		for (int i = 0; i < inputDimension(); i++) {
			dmean[i] = mest.get(i).estimate();
		}
		return MatrixUtils.createRealVector(dmean);
	}

	/**
	 * Estimates the sample standard deviation of the input vectors in this data
	 * set.
	 * 
	 * @return the sample standard deviation
	 */
	public RealVector inputStd() {
		List<StdEstimator> mest = new ArrayList<StdEstimator>(inputDimension());
		for (int i = 0; i < inputDimension(); i++) {
			mest.add(new StdEstimator());
		}

		for (Pair<RealVector, L> pair : _data) {
			RealVector x = pair.getA();
			for (int i = 0; i < inputDimension(); i++) {
				mest.get(i).add(x.getEntry(i));
			}
		}

		double[] dstd = new double[inputDimension()];
		for (int i = 0; i < inputDimension(); i++) {
			dstd[i] = mest.get(i).estimate();
		}
		return MatrixUtils.createRealVector(dstd);
	}

	/**
	 * Estimates the covariance matrix of the input vectors in this data set.
	 * 
	 * @return a covariance estimator
	 */
	public CovarianceEstimator inputCovariance() {
		CovarianceEstimator covEst = new CovarianceEstimator(inputDimension());

		for (Pair<RealVector, L> pair : _data) {
			RealVector x = pair.getA();
			covEst.add(x);
		}

		return covEst;
	}

}
