package tmrutil.learning;


import java.util.Collection;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;

import tmrutil.stats.MeanEstimator;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * A simple implementation of ridge regression.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RidgeRegression implements
		BatchFunctionApproximator<RealVector, Double> {

	private static final long serialVersionUID = 1328180767316357356L;
	private int _inputSize;
	private RealVector _w;
	private double _outputMean;
	private double _lambda;

	public RidgeRegression(int inputSize, double lambda) {
		if(inputSize < 1){throw new IllegalArgumentException("Input size must be a positive integer.");}
		_inputSize = inputSize;
		if(lambda < 0){throw new IllegalArgumentException("Regularization parameter must be non-negative.");}
		_lambda = lambda;
	}
	
	/**
	 * Returns the number of features expected in a valid input vector.
	 * @return the number of features of an input vector
	 */
	public int getInputSize()
	{
		return _inputSize;
	}

	@Override
	public void reset() {
		_w = new ArrayRealVector(_inputSize);
	}

	@Override
	public Double evaluate(RealVector x) throws IllegalArgumentException {
		
		if(_w == null){
			return 0.0;
		}
		
		double response = _w.dotProduct(x) + _outputMean;
		return response;
	}

	@Override
	public void train(Collection<Pair<RealVector, Double>> trainingSet) {
		RealMatrix amat = new Array2DRowRealMatrix(trainingSet.size(), _inputSize);
		RealVector bvec = new ArrayRealVector(trainingSet.size());
		
		MeanEstimator mest = new MeanEstimator();
		for(Pair<RealVector,Double> pair : trainingSet){
			mest.add(pair.getB());
		}
		_outputMean = mest.estimate();
		
		int r = 0;
		for(Pair<RealVector,Double> pair : trainingSet){
			amat.setRow(r, pair.getA().toArray());
			bvec.setEntry(r, pair.getB() - _outputMean);
			r++;
		}
		
		RealMatrix aSqd = amat.transpose().multiply(amat);
		double lambdaSqd = _lambda * _lambda;
		for(int i=0;i<_inputSize;i++){
			aSqd.addToEntry(i, i, lambdaSqd);
		}
		
		SingularValueDecomposition svd = new SingularValueDecomposition(aSqd);
		DecompositionSolver solver = svd.getSolver();
		RealMatrix aSqdInv = solver.getInverse();
		
		RealMatrix ablock = aSqdInv.multiply(amat.transpose());
		_w = ablock.operate(bvec);
	}
	
	public double[] residuals(Collection<Pair<RealVector,Double>> samples)
	{
		double[] r = new double[samples.size()];
		int i=0;
		for(Pair<RealVector,Double> sample : samples){
			r[i] = sample.getB() - evaluate(sample.getA());
			i++;
		}
		return r;
	}

	@Override
	public RidgeRegression newInstance() {
		return new RidgeRegression(_inputSize, _lambda);
	}

}
