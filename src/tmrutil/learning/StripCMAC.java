package tmrutil.learning;

import java.util.ArrayList;
import java.util.List;

import tmrutil.stats.Statistics;
import tmrutil.util.Interval;

/**
 * A simpler version of the Cerebellar Model Arithmetic Computer that assumes
 * that each input component is independent of the other input components.
 * 
 * @author Timothy A. Mann
 * 
 */
public class StripCMAC implements IncrementalFunctionApproximator<double[],Double>
{
	private static final long serialVersionUID = 1415275718606947496L;

	private static final int BIAS_INDEX = 0;
	private static final int DEFAULT_NUM_BINS = 30;
	
	private int _inputSize;
	private List<Interval> _ibounds;
	private List<Integer> _ibins;
	private int _sumOfAllBins;
	
	private double _initBias;
	private double[] _weights;
	private int _numLayers;
	
	public StripCMAC(List<Interval> inputComponentBounds, int numLayers, double initBias)
	{
		this(inputComponentBounds, generateDefaultNumberOfBins(inputComponentBounds.size()), numLayers, initBias);
	}
	
	public StripCMAC(List<Interval> inputComponentBounds, List<Integer> inputComponentNumberOfBins, int numLayers, double initBias)
	{
		
		_ibounds = new ArrayList<Interval>(inputComponentBounds);
		_ibins = new ArrayList<Integer>(inputComponentNumberOfBins);
		_sumOfAllBins = (int)Statistics.sum(_ibins);
		_inputSize = _ibounds.size();
		
		_numLayers = numLayers;
		_initBias = initBias;
		
		_weights = new double[numberOfWeights()];
		
		reset();
	}
	
	private static final List<Integer> generateDefaultNumberOfBins(int numFeatures)
	{
		List<Integer> numBins = new ArrayList<Integer>(numFeatures);
		for(int i=0;i<numFeatures;i++){
			numBins.add(DEFAULT_NUM_BINS);
		}
		return numBins;
	}
	
	@Override
	public void reset()
	{
		for(int i=0;i<_weights.length;i++){
			_weights[i] = _initBias;
		}
	}
	
	@Override
	public Double evaluate(double[] x) throws IllegalArgumentException
	{
		double output = _weights[BIAS_INDEX];
		for(int i=0;i<x.length;i++){
			for(int j=0;j<_numLayers;j++){
				output += get(x, i, j);
			}
		}
		return (output / ((x.length * _numLayers) + 1));
	}
	@Override
	public Double train(double[] input, Double target, double learningRate)
	{
		double output = evaluate(input);
		double error = target - output;
		for(int i=0;i<input.length;i++){
			for(int j=0;j<_numLayers;j++){
				double oldW = get(input, i, j);
				set(input, i, j, oldW + learningRate * error);
			}
		}
		_weights[BIAS_INDEX] = _weights[BIAS_INDEX] + learningRate * error;
		return error;
	}
	
	private double offset(int index, int layer)
	{
		Interval range = _ibounds.get(index);
		int numBins = _ibins.get(index);
		double inc = (range.getDiff() / numBins) / _numLayers;
		return layer * inc;
	}
	
	private int numberOfWeights()
	{
		return _inputSize * _numLayers * _sumOfAllBins + 1;
	}
	
	private int getWeightIndex(double[] x, int index, int layer)
	{
		Interval range = _ibounds.get(index);
		double xv = x[index] - offset(index, layer) - range.getMin();
		if(xv < 0){
			xv = 0;
		}
		if(xv > range.getDiff()){
			xv = range.getDiff();
		}
		
		int numBins = _ibins.get(index);
		double inc = range.getDiff() / numBins;
		int bin = (int)(xv / inc);
		
		int sumOfIBins = 0;
		for(int i=0;i<index-1;i++){
			sumOfIBins += _ibins.get(i);
		}
		
		return (_sumOfAllBins * layer) + sumOfIBins + bin + 1;
	}
	
	private double get(double[] x, int index, int layer)
	{
		int wIndex = getWeightIndex(x, index, layer);
		return _weights[wIndex];
	}
	
	private void set(double[] x, int index, int layer, double value)
	{
		int wIndex = getWeightIndex(x, index, layer);
		_weights[wIndex] = value;
	}

	@Override
	public StripCMAC newInstance() {
		return new StripCMAC(_ibounds, _ibins, _numLayers, _initBias);
	}
}
