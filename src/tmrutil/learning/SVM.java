package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.stats.Random;
import tmrutil.util.Pair;

/**
 * A wrapper for the libsvm Support Vector Machine multiclass classifier.
 * @author Timothy A. Mann
 *
 */
public class SVM implements Classifier<RealVector, Integer> {

	public static enum Kernel {LINEAR, RBF, POLY, SIGMOID};
	
	private libsvm.svm_parameter _params;
	private libsvm.svm_model _model;
	private Kernel _kernelType;
	
	private int _numClasses;
	
	public SVM(int numberOfClasses, Kernel kernelType)
	{
		_numClasses = numberOfClasses;
		
		_params = new libsvm.svm_parameter();
		_params.svm_type = libsvm.svm_parameter.C_SVC;
		
		_kernelType = kernelType;
		switch(kernelType){
		case LINEAR:
			_params.kernel_type = libsvm.svm_parameter.LINEAR;
			break;
		case RBF:
			_params.kernel_type = libsvm.svm_parameter.RBF;
			break;
		case POLY:
			_params.kernel_type = libsvm.svm_parameter.POLY;
			break;
		case SIGMOID:
			_params.kernel_type = libsvm.svm_parameter.SIGMOID;
			break;
		default:
			_params.kernel_type = libsvm.svm_parameter.RBF;
		}
		_params.degree = 3;
		_params.gamma = 0.5;	// 1/num_features
		_params.coef0 = 0;
		_params.nu = 0.5;
		_params.cache_size = 100;
		_params.C = 1;
		_params.eps = 1e-3;
		_params.p = 0.1;
		_params.shrinking = 1;
		_params.probability = 0;
		_params.nr_weight = 0;
		_params.weight_label = new int[0];
		_params.weight = new double[0];
		
		libsvm.svm.svm_set_print_string_function(SVR.SVM_PRINT_NULL);
	}
	
	/**
	 * Sets the parameters controlling RBF kernels.
	 * 
	 * Radial basis function kernels are evaluated as:<br/>
	 * <code>Math.exp(-gamma*(x_square[i]+x_square[j]-2*dot(x[i],x[j])))</code>
	 * @param gamma determines the width of the radial basis
	 */
	public void setRBFParams(double gamma)
	{
		_params.gamma = gamma;
	}
	
	/**
	 * Sets the parameters controlling polynomial kernels.
	 * 
	 * Polynomial kernels are evaluated as:<br/>
	 * <code>powi(gamma*dot(x[i],x[j])+coef0,degree)</code>
	 * 
	 * @param degree the degree of the polynomial
	 * @param gamma multiplicative parameter
	 * @param coef0 bias parameter
	 */
	public void setPolyParams(int degree, double gamma, double coef0)
	{
		_params.degree = degree;
		_params.gamma = gamma;
		_params.coef0 = coef0;
	}
	
	/**
	 * Sets the parameters controlling sigmoid kernels (which are not true kernels).
	 * 
	 * Sigmoid kernels are evaluated as:<br/>
	 * <code>Math.tanh(gamma*dot(x[i],x[j])+coef0)</code>
	 * 
	 * @param gamma multiplicative parameter
	 * @param coef0 bias parameter
	 */
	public void setSigmoidParams(double gamma, double coef0)
	{
		_params.gamma = gamma;
		_params.coef0 = coef0;
	}

	@Override
	public Integer classify(RealVector instance) {
		double prediction = libsvm.svm.svm_predict(_model, fromVector(instance));
		return (int)prediction;
	}


	@Override
	public double[] scores(RealVector instance) {
		Integer c = classify(instance);
		double[] scores = new double[numClasses()];
		scores[c] = 1;
		return scores;
	}


	@Override
	public void train(Collection<Pair<RealVector, Integer>> trainingSet) {
		libsvm.svm_problem problem = new libsvm.svm_problem();
		int n = trainingSet.size();
		problem.l = n;
		problem.x = new libsvm.svm_node[n][];
		problem.y = new double[n];
		
		int index = 0;
		for(Pair<RealVector,Integer> sample : trainingSet){
			problem.x[index] = fromVector(sample.getA());
			problem.y[index] = sample.getB();
			index++;
		}
		
		_model = libsvm.svm.svm_train(problem, _params);
	}


	@Override
	public int numClasses() {
		return _numClasses;
	}
	
	private libsvm.svm_node[] fromVector(RealVector x)
	{
		libsvm.svm_node[] xnode = new libsvm.svm_node[x.getDimension()];
		for(int i=0;i<xnode.length;i++){
			xnode[i] = new libsvm.svm_node();
			xnode[i].index = i;
			xnode[i].value = x.getEntry(i);
		}
		return xnode;
	}
	
	public Kernel kernelType()
	{
		return _kernelType;
	}
	
	@Override
	public SVM newInstance() {
		
		return new SVM(numClasses(), kernelType());
	}
	
	public static List<Pair<RealVector,Integer>> generateThreeClassData(int n)
	{
		List<Pair<RealVector,Integer>> trainingSet = new ArrayList<Pair<RealVector,Integer>>();
		for(int i=0;i<n;i++){
			double x=0,y=0;
			Integer c=0;
			int r = Random.nextInt(3);
			switch(r){
			case 0:
				x = Random.uniform(0,0.35);
				y = Random.uniform(0,0.35);
				c = 0;
				break;
			case 1:
				x = Random.uniform(0.3, 0.65);
				y = Random.uniform(0.3, 0.65);
				c = 1;
				break;
			case 2:
				x = Random.uniform(0.6, 1);
				y = Random.uniform(0.6, 1);
				c = 2;
				break;
			}
			RealVector v = new ArrayRealVector(new double[]{x,y});
			Pair<RealVector,Integer> sample = new Pair<RealVector,Integer>(v, c);
			trainingSet.add(sample);
		}
		return trainingSet;
	}
	
	public static void evaluate(Classifier<RealVector,Integer> c, List<Pair<RealVector,Integer>> testSet)
	{
		int numClasses = c.numClasses();
		int[][] confMat = new int[numClasses][numClasses];
		int total = testSet.size();
		
		for(Pair<RealVector,Integer> sample : testSet){
			int pclass = c.classify(sample.getA());
			confMat[sample.getB()][pclass]++;
		}
		
		for(int col=0;col<numClasses;col++){
			for(int row=0;row<numClasses;row++){
				System.out.print((confMat[row][col] / (double) total) + " ");
			}
			System.out.println();
		}
	}
	
	public static void threeClassTest()
	{
		int n = 100;
		List<Pair<RealVector,Integer>> trainSet = generateThreeClassData(n);
		List<Pair<RealVector,Integer>> testSet = generateThreeClassData(n);
		
		SVM svm = new SVM(3, SVM.Kernel.LINEAR);
		svm.train(trainSet);
		
		evaluate(svm, testSet);
	}
	
	public static void main(String[] args){
		threeClassTest();
	}
}
