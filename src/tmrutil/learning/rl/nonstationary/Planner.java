package tmrutil.learning.rl.nonstationary;

/**
 * An interface for planning actions, given a Markov environment and reinforcement signal.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public interface Planner<S,A, E extends MarkovEnvironment<S,A>>
{
	/**
	 * Plans and returns an action to execute at the specified state.
	 * @param state a state in the environment
	 * @param tcounts a datastructure recording the number of times that each state-action pair has been tried
	 * @param time an integer representing the timestep
	 * @param environment a Markov environment
	 * @param rsignal a reinforcement signal
	 * @return an action
	 */
	public A plan(S state, TryCounts<S> tcounts, long time, E environment, ReinforcementSignal<S,A> rsignal);
}
