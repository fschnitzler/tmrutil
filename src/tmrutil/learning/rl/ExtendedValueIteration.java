package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;

import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Statistics;
import tmrutil.util.Interval;
import tmrutil.util.Sort;

/**
 * A finite-horizon implementation of the Extended Value Iteration (EVI)
 * algorithm needed for the UCRL2 algorithm. This implementation only maximizes
 * rewards (it does not have an option to minimize costs).
 * 
 * @author Timothy A. Mann
 * 
 */
public class ExtendedValueIteration {

	public static class QFunction {
		private List<double[]> _vs;
		private List<double[][]> _qs;
		private Optimization _opType;
		private int[] _rperm;

		public QFunction(int horizon, int numStates, int numActions,
				Optimization opType) {
			_vs = new ArrayList<double[]>(horizon);
			_qs = new ArrayList<double[][]>(horizon);
			for (int h = 0; h < horizon; h++) {
				_vs.add(new double[numStates]);
				_qs.add(new double[numStates][numActions]);
			}
			_opType = opType;
			_rperm = VectorOps.randperm(numActions);
		}

		public double value(int t, int state) {
			return _vs.get(t)[state];
		}

		public double value(int t, int state, int action) {
			return _qs.get(t)[state][action];
		}

		public double update(int t, int state, int action, double value) {
			double oldValue = _qs.get(t)[state][action];
			_qs.get(t)[state][action] = value;
			if (_opType.equals(Optimization.MAXIMIZE)) {
				_vs.get(t)[state] = Statistics.max(_qs.get(t)[state]);
			} else {
				_vs.get(t)[state] = Statistics.min(_qs.get(t)[state]);
			}
			return oldValue;
		}

		public int[] sortedStates(int t, boolean ascend) {
			return Sort.sortIndex(_vs.get(t), ascend);
		}

		public int policy(int t, int state) {
			double[] qs = _qs.get(t)[state];
			if (_opType.equals(Optimization.MAXIMIZE)) {
				int bestA = 0;
				double bestVal = 0;
				for (int a = 0; a < qs.length; a++) {
					double val = qs[ _rperm[a] ];
					if (a == 0 || val > bestVal) {
						bestA = _rperm[a];
						bestVal = val;
					}
				}
				return bestA;
			} else {
				int bestA = 0;
				double bestVal = 0;
				for (int a = 0; a < qs.length; a++) {
					double val = qs[ _rperm[a] ];
					if (a == 0 || val < bestVal) {
						bestA = _rperm[a];
						bestVal = val;
					}
				}
				return bestA;
			}
		}

		public int horizon() {
			return _vs.size();
		}
	}

	private int _numStates;
	private int _numActions;

	private double[][] _r;
	private double[][] _rd;

	private double[][][] _p;
	private double[][] _pd;

	private Interval _rewardInterval;

	/**
	 * Constructs an instance of Extended Value Iteration.
	 * 
	 * @param r
	 *            the average rewards (rows correspond to states and columns
	 *            correspond to actions)
	 * @param rd
	 *            the size of the confidence interval for rewards (rows
	 *            correspond to states and columns correspond to actions)
	 * @param p
	 *            the average transition probabilities (the first two indices
	 *            correspond to state-action pairs and the final index
	 *            corresponds to next states)
	 * @param pd
	 *            the size of the confidence interval for transition
	 *            probabilities at each state-action pair (rows correspond to
	 *            states and columns correspond to actions)
	 */
	public ExtendedValueIteration(double[][] r, double[][] rd, double[][][] p,
			double[][] pd, Interval rewardInterval) {
		_numStates = r.length;
		_numActions = r[0].length;

		_r = r;
		_rd = rd;
		_p = p;
		_pd = pd;

		_rewardInterval = rewardInterval;
	}

	public ExtendedValueIteration(
			DiscreteMarkovDecisionProcess<Integer, Integer> mdp, double[][] rd,
			double[][] pd) {
		_numStates = mdp.numberOfStates();
		_numActions = mdp.numberOfActions();

		_rewardInterval = new Interval(mdp.rmin(), mdp.rmax());

		_r = new double[_numStates][_numActions];
		_p = new double[_numStates][_numActions][_numStates];
		for (int s = 0; s < _numStates; s++) {
			for (int a = 0; a < _numActions; a++) {
				_r[s][a] = mdp.reinforcement(s, a);
				for (int ns = 0; ns < _numStates; ns++) {
					_p[s][a][ns] = mdp.transitionProb(s, a, ns);
				}
			}
		}
		_rd = rd;
		_pd = pd;
	}

	public double optimisticR(Integer state, Integer action) {
		return _rewardInterval.clip(_r[state][action] + _rd[state][action]);
	}

	public double pessimisticR(Integer state, Integer action) {
		return _rewardInterval.clip(_r[state][action] - _rd[state][action]);
	}

	public QFunction solveQOptimistic(int horizon) {
		QFunction qfunc = new QFunction(horizon, _numStates, _numActions,
				Optimization.MAXIMIZE);

		double[] pbuff = new double[_numStates];
		for (int t = horizon - 1; t >= 0; t--) {

			int[] sortedS = null;
			if (t < horizon - 1) {
				sortedS = qfunc.sortedStates(t + 1, false);
			}

			for (int s = 0; s < _numStates; s++) {
				for (int a = 0; a < _numActions; a++) {
					double newQ = optimisticR(s, a);

					if (sortedS != null) {
						// Puts the most probability mass on the state with the
						// highest value
						double[] sap = modifyP(pbuff, s, a, sortedS);
						for (int ns = 0; ns < sap.length; ns++) {
							newQ += sap[ns] * qfunc.value(t + 1, ns);
						}
					}
					qfunc.update(t, s, a, newQ);
				}
			}
		}

		return qfunc;
	}

	public double[] modifyP(double[] pbuff, Integer state, Integer action,
			int[] sortedS) {
		double pd = _pd[state][action];

		double psum = 0;
		pbuff[sortedS[0]] = Math.min(1, _p[state][action][sortedS[0]]
				+ (pd / 2));
		psum = pbuff[sortedS[0]];
		for (int i = 1; i < _numStates; i++) {
			pbuff[sortedS[i]] = _p[state][action][sortedS[i]];
			psum += pbuff[sortedS[i]];
		}

		int l = sortedS.length - 1;
		while (psum > 1) {
			int lind = sortedS[l];

			double oldP = pbuff[lind];
			double newP = Math.max(0, 1 - (psum - oldP));
			pbuff[lind] = newP;
			psum = psum - oldP + newP;

			l--;
		}

		if (psum < 1.0 - tmrutil.math.Constants.EPSILON) {
			System.err.println("Invalid distribution! (psum = " + psum + ")");
		}

		return pbuff;
	}

	public QFunction solveQPessimistic(int horizon) {
		QFunction qfunc = new QFunction(horizon, _numStates, _numActions,
				Optimization.MAXIMIZE);

		double[] pbuff = new double[_numStates];
		for (int t = horizon - 1; t >= 0; t--) {

			int[] sortedS = null;
			if (t < horizon - 1) {
				sortedS = qfunc.sortedStates(t + 1, true);
			}

			for (int s = 0; s < _numStates; s++) {
				for (int a = 0; a < _numActions; a++) {
					double newQ = pessimisticR(s, a);

					if (sortedS != null) {
						// Puts the most probability mass on the state with the
						// smallest value
						double[] sap = modifyP(pbuff, s, a, sortedS);
						for (int ns = 0; ns < sap.length; ns++) {
							newQ += sap[ns] * qfunc.value(t + 1, ns);
						}
					}
					qfunc.update(t, s, a, newQ);
				}
			}
		}

		return qfunc;
	}
}
