package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.stats.Random;
import tmrutil.util.ListUtil;

/**
 * Represents a collection of actions. This representation enables querying only
 * the subset of actions that are valid at a particular state.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public abstract class ActionSet<S, A> {

	/**
	 * Represents a small set of actions where all actions can be executed from
	 * any state.
	 * 
	 * @author Timothy A. Mann
	 * 
	 * @param <S>
	 *            the state type
	 * @param <A>
	 *            the action type
	 */
	public static class Unrestricted<S, A> extends ActionSet<S, A> {
		private List<A> _actions;

		public Unrestricted(Collection<A> actions) {
			_actions = new ArrayList<A>(actions);
		}

		@Override
		public List<Integer> validIndices(S state) {
			return ListUtil.range(numberOfActions());
		}

		@Override
		public A action(int index) {
			return _actions.get(index);
		}

		@Override
		public int numberOfActions() {
			return _actions.size();
		}
	}

	/**
	 * Returns a list of valid actions given the specified state.
	 * 
	 * @param state
	 *            a state
	 * @return a list of valid actions for the specified state
	 */
	public List<A> validList(S state) {
		Collection<Integer> indices = validIndices(state);
		List<A> actions = new ArrayList<A>(indices.size());
		for (Integer i : indices) {
			actions.add(action(i));
		}
		return actions;
	}

	/**
	 * Returns a list of valid indices of actions that can be initialized at a
	 * specific state.
	 * 
	 * @param state
	 *            a state
	 * @return a list of indices of valid actions
	 */
	public abstract List<Integer> validIndices(S state);

	/**
	 * Obtain an action by index.
	 * 
	 * @param index
	 *            the index of an action
	 * @return an action
	 */
	public abstract A action(int index);

	/**
	 * Obtain a list of actions by their indices. The actions in the returned
	 * list are in the same order that the <code>indices</code> iterator
	 * produces.
	 * 
	 * @param indices
	 *            a collection of indices of actions
	 * @return a list of actions
	 */
	public List<A> actions(Collection<Integer> indices) {
		List<A> actions = new ArrayList<A>(indices.size());
		for (Integer i : indices) {
			actions.add(action(i));
		}
		return actions;
	}

	/**
	 * The number of distinct actions in this set.
	 * 
	 * @return the number of actions in this set
	 */
	public abstract int numberOfActions();

	/**
	 * Returns a list of all actions.
	 * 
	 * @return a list of all actions
	 */
	public List<A> allActions() {
		List<A> allActions = new ArrayList<A>(numberOfActions());
		for (int i = 0; i < numberOfActions(); i++) {
			allActions.add(action(i));
		}
		return allActions;
	}

	/**
	 * Draws a valid action (with respect to the specified state) according to a
	 * uniform random distribution.
	 * 
	 * @param state
	 *            a state
	 * @return an action drawn according to a uniform random distribution over
	 *         valid action for <code>state</code>
	 */
	public A sampleUniform(S state) {
		List<A> actions = validList(state);
		int r = Random.nextInt(actions.size());
		return actions.get(r);
	}
}
