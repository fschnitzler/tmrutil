package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tmrutil.util.DebugException;

/**
 * Provides a multithreaded implementation of the value iteration algorithm.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ThreadedValueIteration
{
	/**
	 * A model of the Markov decision process.
	 */
	private DiscreteMarkovDecisionProcess<Integer,Integer> _mdp;

	/**
	 * The sensitivity threshold used to determine when to stop iterating.
	 */
	private double _theta;

	/**
	 * The number of threads to use.
	 */
	private int _numThreads;

	/**
	 * Constructs an instance of the value iteration algorithm with
	 * multithreading.
	 * 
	 * @param mdp
	 *            the Markov decision process to plan on
	 * @param theta
	 *            the convergence threshold
	 * @param numThreads
	 *            the number of threads to use
	 */
	public ThreadedValueIteration(DiscreteMarkovDecisionProcess<Integer,Integer> mdp,
			double theta, int numThreads)
	{
		_mdp = mdp;
		_theta = theta;
		setNumThreads(numThreads);
	}

	/**
	 * Sets the number of threads used to perform value iteration.
	 * 
	 * @param numThreads
	 *            an integer greater than 0 determining the number of threads to
	 *            use for value iteration
	 */
	public void setNumThreads(int numThreads)
	{
		if (numThreads < 1) {
			throw new IllegalArgumentException(
					"At least one thread is needed to perform value iteration.");
		}
		_numThreads = numThreads;
	}

	/**
	 * Returns the number of threads used to perform value iteration.
	 * 
	 * @return the number of threads used to perform value iteration
	 */
	public int getNumThreads()
	{
		return _numThreads;
	}

	public double[] valueIteration(int maxIterations, double[] Vbuff)
	{
		double[][] Qbuff = new double[_mdp.numberOfStates()][_mdp.numberOfActions()];
		actionValueIteration(maxIterations, Vbuff, Qbuff);
		return Vbuff;
	}

	public double[][] actionValueIteration(int maxIterations, double[] Vbuff,
			double[][] Qbuff)
	{
		double[] Vold = Arrays.copyOf(Vbuff, Vbuff.length);
		double[] Vnew = Vbuff;
		double[][] Qnew = Qbuff;

		int numStates = _mdp.numberOfStates();
		List<Helper> helpers = new ArrayList<Helper>(_numThreads);
		int numStatesPerHelper = numStates / _numThreads;
		for (int t = 0; t < _numThreads; t++) {
			Set<Integer> states = new HashSet<Integer>();
			for (int s = (t * numStatesPerHelper); s < ((t + 1) * numStatesPerHelper)
					|| ((t == _numThreads - 1) && s < numStates); s++) {
				states.add(s);
			}

			Helper h = new Helper(states, Vold, Vnew, Qnew);
			helpers.add(h);
		}

		for (int i = 0; i < maxIterations || (maxIterations < 0); i++) {
			double maxError = 0;

			List<Thread> threads = new ArrayList<Thread>(_numThreads);
			for (Helper h : helpers) {
				Thread t = new Thread(h);
				t.start();
				threads.add(t);
			}

			for (Thread t : threads) {
				try {
					t.join();
				} catch (InterruptedException ex) {
					throw new DebugException("An interrupt occurred while performing value iteration.", ex);
				}
			}
			
			for(Helper h : helpers){
				if(maxError < h.maxError()){
					maxError = h.maxError();
				}
			}
			
			System.arraycopy(Vnew, 0, Vold, 0, numStates);
			
			if(maxError < _theta){
				break;
			}
		}
		
		return Qnew;
	}

	/**
	 * A worker class for updating a subset of states.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	private class Helper implements Runnable
	{
		private Set<Integer> _states;
		private double[] _Vold;
		private double[] _Vnew;
		private double[][] _Qnew;

		private double _maxError;

		/**
		 * Constructs a helper instance that performs value iteration on a limited number of states.
		 * @param states the set of states that this helper is responsible for
		 * @param Vold the old value function estimate
		 * @param Vnew the new value function estimate
		 * @param Qnew the new action-value function estimate
		 */
		public Helper(Set<Integer> states, double[] Vold, double[] Vnew,
				double[][] Qnew)
		{
			_states = states;
			_Vold = Vold;

			_Vnew = Vnew;
			_Qnew = Qnew;

			_maxError = 0;
		}

		@Override
		public void run()
		{
			_maxError = 0;

			int numActions = _mdp.numberOfActions();

			for (Integer s : _states) {
				for (int a = 0; a < numActions; a++) {
					double qsa = actionValueUpdate(s, a);
					_Qnew[s][a] = qsa;
					if (qsa > _Vnew[s] || (a == 0)) {
						_Vnew[s] = qsa;
					}
				}
				double error = Math.abs(_Vold[s] - _Vnew[s]);
				if (_maxError < error) {
					_maxError = error;
				}
			}
		}

		public double maxError()
		{
			return _maxError;
		}

		public double actionValueUpdate(int state, int action)
		{
			Set<Integer> succs = _mdp.successors(state);
			double r = _mdp.reinforcement(state, action);
			double gamma = _mdp.getDiscountFactor();

			double q = 0;
			for (Integer ns : succs) {
				double tprob = _mdp.transitionProb(state, action, ns);
				q += gamma * tprob * _Vold[ns];
			}
			return r + q;
		}
	}
}
