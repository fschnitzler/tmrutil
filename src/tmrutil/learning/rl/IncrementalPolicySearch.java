package tmrutil.learning.rl;

import tmrutil.optim.Optimization;

/**
 * Randomly samples policies and evaluates each sampled policy for a finite time
 * horizon. This cycle repeats until a termination criterion is met. During each
 * iteration, the sampled policy with the current highest value is stored.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public abstract class IncrementalPolicySearch<S, A> implements
		LearningSystem<S, A>
{

	private Policy<S, A> _bestSoFar;
	private double _bestSoFarValue;

	private Policy<S, A> _current;
	private double _currentValue;

	private int _evaluationStepCount;
	private int _numEvaluationSteps;
	private int _numPoliciesSearched;

	private Optimization _opType;
	private boolean _keepSearching;

	public IncrementalPolicySearch(int numEvaluationSteps, Optimization opType)
	{
		_numEvaluationSteps = numEvaluationSteps;
		_opType = opType;
		reset();
	}

	public void reset()
	{
		_numPoliciesSearched = 0;
		_bestSoFar = null;
		_current = null;
		_evaluationStepCount = 0;

		if(isMinimizing()){
			_bestSoFarValue = Double.POSITIVE_INFINITY;
			_currentValue = Double.POSITIVE_INFINITY;
		}else{
			_bestSoFarValue = Double.NEGATIVE_INFINITY;
			_currentValue = Double.NEGATIVE_INFINITY;
		}
		_keepSearching = true;
	}

	public boolean isMaximizing()
	{
		return _opType.equals(Optimization.MAXIMIZE);
	}

	public boolean isMinimizing()
	{
		return _opType.equals(Optimization.MINIMIZE);
	}
	
	public int numPoliciesSearched()
	{
		return _numPoliciesSearched;
	}
	
	public double bestPolicysValue()
	{
		return _bestSoFarValue;
	}
	
	public double currentPolicysValue()
	{
		return _currentValue;
	}

	@Override
	public A policy(S state)
	{
		if(_current == null){
			_current = samplePolicy();
		}
		return _current.policy(state);
	}

	@Override
	public void train(S prevState, A action, S newState, double reinforcement)
	{
		if (_keepSearching) {
			_evaluationStepCount++;
			_currentValue += reinforcement;
			if (_evaluationStepCount >= _numEvaluationSteps) {
				_numPoliciesSearched++;

				if ((isMaximizing() && _currentValue > _bestSoFarValue)
						|| (isMinimizing() && _currentValue < _bestSoFarValue)) {
					_bestSoFarValue = _currentValue;
					_bestSoFar = _current;
				}

				_evaluationStepCount = 0;
				_currentValue = 0;

				if (terminateSearch()) {
					_current = _bestSoFar;
					_keepSearching = false;
				} else {
					_current = samplePolicy();
				}
			}
		}
	}

	/**
	 * Randomly generates a new policy.
	 * 
	 * @return a new policy
	 */
	public abstract Policy<S, A> samplePolicy();

	/**
	 * Determines whether or not to terminate policy search.
	 * 
	 * @return true if policy search should stop; otherwise false indicates that
	 *         policy search should continue
	 */
	public abstract boolean terminateSearch();
}
