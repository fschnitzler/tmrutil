package tmrutil.learning.rl;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import tmrutil.util.GenericComparator;
import tmrutil.util.UniqueValueHeap;

/**
 * Tweaked value iteration attempts to enhance regular value iteration so that
 * it runs faster on experiments. This implementation is a mixture of
 * prioritized sweeping with traditional value iteration. At every call to
 * {@link TweakedValueIteration.valueIteration} every state is updated at least
 * once. However, after being updated prioritized sweeping takes over to target
 * computational effort on states that need the most updates.
 * <p/>
 * 
 * A second enhancement is that only successor states with nonzero probability
 * are considered during computation of value updates for a state. For example,
 * if a state <code>s</code> can only transition to states <code>x</code> or
 * <code>y</code>, then only statistics about states <code>x</code> and
 * <code>y</code> will be utilized to compute the value update for state
 * <code>s</code>. This significantly reduces time required for a value update
 * when the number of successor states for each state is small.
 * 
 * @author Timothy A. Mann
 * 
 */
public class TweakedValueIteration extends ValueIteration
{
	private static final double MAX_PRIORITY = 1.0;

	private UniqueValueHeap<Double, Integer> _priorityQ;

	/**
	 * Constructs an instance of tweaked value iteration.
	 * 
	 * @param mdp
	 *            a Markov decision process
	 * @param convergenceSensitivity
	 *            the sensitivity of convergence used to decide when to
	 *            terminate value iteration
	 */
	public TweakedValueIteration(DiscreteMarkovDecisionProcess<Integer,Integer> mdp,
			double convergenceSensitivity)
	{
		super(mdp, convergenceSensitivity);
		_priorityQ = new UniqueValueHeap<Double, Integer>(
				_mdp.numberOfStates(), new GenericComparator<Double>(),
				new GenericComparator<Integer>());
	}

	@Override
	public double[] valueIteration(int maxIterations, double[] Vbuff,
			Collection<Integer> envelope)
	{
		// Construct the successor map
		Map<Integer, Collection<Integer>> successorMap = createSuccessorMap(envelope);

		// Clear and then fill the priority Q
		_priorityQ.clear();
		for (int state : envelope) {
			_priorityQ.insert(MAX_PRIORITY, state);
		}

		double gamma = _mdp.getDiscountFactor();
		int maxUpdates = maxIterations * _mdp.numberOfStates();
		for (int i = 0; i < maxUpdates && _priorityQ.size() > 0; i++) {
			int state = _priorityQ.popValue();
			double backup = Vbuff[state];
			Collection<Integer> succs = successorMap.get(state);
			double update = valueUpdate(state, Vbuff, succs);
			Vbuff[state] = update;

			double delta = Math.abs(update - backup);
			if(succs != null){
				for (int succ : succs) {
					insertState(succ, gamma * delta);
				}
			}
		}

		// Return a reference to the value iteration buffer
		return Vbuff;
	}

	private void insertState(int state, double priority)
	{
		if (priority >= getConvergenceSensitivity()) {
			_priorityQ.increase(priority, state);
		}
	}

	protected double valueUpdate(int s1, double[] Vbuff,
			Collection<Integer> successors)
	{
		int numActions = _mdp.numberOfActions();
		double gamma = _mdp.getDiscountFactor();
		double vmax = (_mdp.rmax() / (1 - gamma));
		double vUpdate = 0;

		// Check for self absorbing and maximally rewarding transitions
		for (int a = 0; a < numActions; a++) {
			double tprob = _mdp.transitionProb(s1, a, s1);
			double r = _mdp.reinforcement(s1, a);
			if(tprob == 1 && r == _mdp.rmax()){
				return vmax;
			}
		}
		
		// Perform long value iteration
		for (int a = 0; a < numActions; a++) {
			double r = _mdp.reinforcement(s1, a);
			double val = 0;
			
			// If there are successors states
			if (successors != null) {
				// Do normal update
				for (int s2 : successors) {
					double tprob = _mdp.transitionProb(s1, a, s2);
					double s2V = Vbuff[s2];
					val += tprob * (r + gamma * s2V);
				}
				if (val > vUpdate || a == 0) {
					vUpdate = val;
				}
			}else{
				// Otherwise, there are no successor states
				// so we only consider the reward
				if(r > vUpdate || a == 0){
					vUpdate = r;
				}
			}
		}
		return vUpdate;
	}

	protected Map<Integer, Collection<Integer>> createSuccessorMap(
			Collection<Integer> envelope)
	{
		Map<Integer, Collection<Integer>> successorMap = new TreeMap<Integer, Collection<Integer>>();

		for (int state : envelope) {
			for (int resultState : envelope) {
				for (int a = 0; a < _mdp.numberOfActions(); a++) {
					double tprob = _mdp.transitionProb(state, a, resultState);
					if (tprob > 0) {
						Collection<Integer> successors = successorMap.get(state);
						if (successors == null) {
							successors = new TreeSet<Integer>();
							successors.add(resultState);
							successorMap.put(state, successors);
						} else {
							successors.add(resultState);
						}
						break;
					}
				}

			}
		}

		return successorMap;
	}
}
