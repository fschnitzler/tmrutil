package tmrutil.learning.rl;


/**
 * Implemented by learning systems that evaluate a Q-function.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public interface ValueBasedLearningSystem<S,A> extends LearningSystem<S,A>
{
	/**
	 * Uses the current Q-function estimate to determine the value of a given state-action pair.
	 * @param state a state description
	 * @param action an action description
	 * @return the value of the given state-action pair
	 */
	public double evaluateQ(S state, A action);
	
	/**
	 * Returns the maximum action-value for a specified state.
	 * @param state a state
	 * @return the maximum action-value associated with the specified state
	 */
	public double maxQ(S state);
}
