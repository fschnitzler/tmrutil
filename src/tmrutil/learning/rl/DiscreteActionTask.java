package tmrutil.learning.rl;

/**
 * An abstract superclass for tasks that have a finite number of discrete
 * actions represented by integers 0 through K-1, where K is the total number of
 * actions.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S> the state type
 */
public abstract class DiscreteActionTask<S> extends Task<S, Integer>
{
	private int _numActions;

	public DiscreteActionTask(int numActions)
	{
		if (numActions < 1) {
			throw new IllegalArgumentException(
					"Cannot construct a task with less than 1 action.");
		}
		_numActions = numActions;
	}

	/**
	 * Returns the number of actions in this task.
	 * 
	 * @return the number of actions in this task
	 */
	public final int numberOfActions()
	{
		return _numActions;
	}
}
