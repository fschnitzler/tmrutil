package tmrutil.learning.rl;

import java.util.Arrays;

/**
 * Implements the policy evaluation dynamic programming algorithm for discrete
 * Markov decision processes.
 * 
 * @author Timothy A. Mann
 * 
 */
public class PolicyEvaluation
{
	/** A reference to the policy being evaluated. */
	private int[] _policy;
	/** A reference to a Markov decision process. */
	private DiscreteMarkovDecisionProcess<Integer,Integer> _mdp;
	/** The convergence sensitivity parameter. */
	private double _theta;

	/**
	 * Constructs a policy evaluation instance given a Markov decision process
	 * and policy. The constructed instance can either make a deep copy or
	 * shallow copy of the policy.
	 * 
	 * @param mdp
	 *            a discrete Markov decision process
	 * @param policy
	 *            a policy for the Markov decision process
	 * @param policyDeepCopy
	 *            true if a deep copy of the policy should be made; otherwise a
	 *            shallow copy of the policy is made
	 * @param convergenceSensitivity
	 *            a small scalar value (e.g. 0.001) that determines the
	 *            tolerable error at which to stop policy evaluation
	 */
	public PolicyEvaluation(DiscreteMarkovDecisionProcess<Integer,Integer> mdp, int[] policy,
			boolean policyDeepCopy, double convergenceSensitivity)
	{
		if (policy == null) {
			throw new NullPointerException(
					"Cannot perform policy evaluation with a null policy.");
		}
		if (mdp == null) {
			throw new NullPointerException(
					"Cannot perform policy evaluation with a null Markov decision process");
		}
		if (policy.length != mdp.numberOfStates()) {
			throw new IllegalArgumentException(
					"The specified policy is invalid for the given Markov decision process because of a mismatch in the number of states.");
		}
		for (int s = 0; s < policy.length; s++) {
			if (policy[s] < 0 || policy[s] >= mdp.numberOfActions()) {
				throw new IllegalArgumentException(
						"The specified policy is invalid for the given Markov decision process because one of the policies actions is invalid.");
			}
		}
		if (policyDeepCopy) {
			_policy = Arrays.copyOf(policy, policy.length);
		} else {
			_policy = policy;
		}
		_mdp = mdp;
		setConvergenceSensitivity(convergenceSensitivity);
	}

	/**
	 * Evaluates the current policy.
	 * @param Vbuff the buffer used for estimating the current policies value function
	 * @return a reference to <code>Vbuff</code>
	 */
	public double[] evaluatePolicy(double[] Vbuff)
	{
		if (Vbuff.length != _mdp.numberOfStates()) {
			throw new IllegalArgumentException(
					"Invalid value function buffer due to mismatch with Markov decision process number of states.");
		}

		double delta = 0;
		do {
			delta = 0;
			for (int s1 = 0; s1 < _mdp.numberOfStates(); s1++) {
				double oldV = Vbuff[s1];
				for (int s2 = 0; s2 < _mdp.numberOfStates(); s2++) {
					double tprob = _mdp.transitionProb(s1, _policy[s1], s2);
					double r = _mdp.reinforcement(s1, _policy[s1]);
					Vbuff[s1] += tprob
							* (r + _mdp.getDiscountFactor() * Vbuff[s2]);
				}
				delta = Math.max(delta, Math.abs(oldV - Vbuff[s1]));
			}
		} while (delta > _theta);

		return Vbuff;
	}

	/**
	 * Sets the convergence sensitivity of policy evaluation.
	 * 
	 * @param theta
	 *            a small scalar value (e.g. 0.001) that determines the
	 *            tolerable error at which to stop policy evaluation
	 */
	public void setConvergenceSensitivity(double theta)
	{
		if (theta <= 0) {
			throw new IllegalArgumentException(
					"Convergence sensitivity must be a small positive scalar value.");
		}
		_theta = theta;
	}

	/**
	 * Returns a small scalar value (e.g. 0.001) that determines the tolerable
	 * error at which to stop policy evaluation.
	 * 
	 * @return a small scalar value (e.g. 0.001) that determines the tolerable
	 *         error at which to stop policy evaluation
	 */
	public double getConvergenceSensitivity()
	{
		return _theta;
	}

}
