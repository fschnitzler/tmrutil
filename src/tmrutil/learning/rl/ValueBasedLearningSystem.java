package tmrutil.learning.rl;


/**
 * Implemented by learning systems that evaluate a Q-function.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public interface ValueBasedLearningSystem<S,A> extends LearningSystem<S,A>, QFunction<S,A>
{
}
