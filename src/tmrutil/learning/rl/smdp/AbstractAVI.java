package tmrutil.learning.rl.smdp;

import java.util.List;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.sim.Simulator;
import tmrutil.util.Factory;
import tmrutil.util.FileDataStore;

/**
 * An implementation of approximate value iteration that enables updating the
 * option set between iterations.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public abstract class AbstractAVI<S, A, QS> {
	
	/**
	 * The result of a single iteration of AVI.
	 * @author Timothy A. Mann
	 *
	 */
	public class AVIResult
	{
		/**
		 * The resulting action-value function of an iteration of AVI.
		 */
		private QFunction<QS,Integer> _qfunc;
		/**
		 * The time in milliseconds to perform an iteration of AVI.
		 */
		private long _timeInMillis;
		
		public AVIResult(QFunction<QS,Integer> qfunc, long timeInMillis)
		{
			_qfunc = qfunc;
			_timeInMillis = timeInMillis;
		}
		
		public QFunction<QS,Integer> qfunc()
		{
			return _qfunc;
		}
		
		public long iterationTimeInMillis()
		{
			return _timeInMillis;
		}
		
		public void record(FileDataStore dstore, String label)
		{
			dstore.bind(label + "_time_in_millis", iterationTimeInMillis());
		}
	}

	public AbstractAVI() {

	}
	
	public abstract QFunction<QS,Integer> initQ();

	public abstract AVIResult iterateQ(QFunction<QS,Integer> qfunc, ActionSet<S,? extends Option<S,A>> options, Simulator<S,?,A> sim, int maxTrajectory);

	public abstract ActionSet<S,? extends Option<S,A>> updateOptions(ActionSet<S,? extends Option<S,A>> prevOptions, List<? extends AVIResult> prevIterates);

}
