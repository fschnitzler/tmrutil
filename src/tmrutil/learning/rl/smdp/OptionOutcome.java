package tmrutil.learning.rl.smdp;

/**
 * Represents the outcome of executing a temporally extended action.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <O>
 *            the observation type
 */
public class OptionOutcome<S, O> {

	private O _tobs;
	private S _tstate;
	private double _dr;
	private double _r;
	private int _duration;

	/**
	 * Constructs an option outcome from a terminal state, cumulative reward,
	 * and lifetime that the option executed for.
	 * 
	 * @param terminalState
	 *            the state where the option terminated
	 * @param terminalObservation
	 *            the observation made by the agent when the option terminated
	 * @param discountedCumulativeReward
	 *            discounted cumulative reinforcement received during the
	 *            options execution
	 * @param cumulativeReward
	 *            undiscounted cumulative reinforcement recieved during the
	 *            options execution
	 * @param duration
	 *            the number of timesteps that the option executed for
	 */
	public OptionOutcome(S terminalState, O terminalObservation,
			double discountedCumulativeReward, double cumulativeReward,
			int duration) {
		_tstate = terminalState;
		_tobs = terminalObservation;
		_dr = discountedCumulativeReward;
		_r = cumulativeReward;
		_duration = duration;
	}

	/**
	 * The state that the option terminated in.
	 * 
	 * @return the state that the option terminated in
	 */
	public S terminalState() {
		return _tstate;
	}

	/**
	 * The observation made by the agent when the option terminated.
	 * 
	 * @return observation made by the agent in the state where the option
	 *         terminated
	 */
	public O terminalObservation() {
		return _tobs;
	}

	/**
	 * The discounted cumulative reinforcement (rewards or costs) received for
	 * following this option.
	 * 
	 * @return discounted cumulative reinforcement
	 */
	public double dCumR() {
		return _dr;
	}

	/**
	 * The undiscounted cumulative reinforcement (rewards or costs) received for
	 * following this option.
	 * 
	 * @return cumulative reinforcement
	 */
	public double cumR() {
		return _r;
	}

	/**
	 * The number of timesteps that this option executed for before terminating.
	 * 
	 * @return the number of timesteps that this option executed for before
	 *         terminating
	 */
	public int duration() {
		return _duration;
	}
}
