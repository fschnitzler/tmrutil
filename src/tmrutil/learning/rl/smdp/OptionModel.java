package tmrutil.learning.rl.smdp;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Implements an option model that can be used for dynamic programming.
 * 
 * @author Timothy A. Mann
 * 
 */
public class OptionModel<S> {
	
	private Set<S> _initStates;
	private Map<S,Double> _r;
	private Map<S,Map<S,Double>> _T;
	private Map<S,Set<S>> _succs;
	
	public OptionModel(Map<S,Double> r, Map<S,Map<S,Double>> tprobs)
	{
		_initStates = r.keySet();
		_r = r;
		_T = tprobs;
		
		_succs = new HashMap<S,Set<S>>();
		for(S state : _initStates){
			_succs.put(state, tprobs.get(state).keySet());
		}
	}
	
	public Set<S> initStateSet()
	{
		return _initStates;
	}
	
	public double reinforcement(S state)
	{
		Double r = _r.get(state);
		if(r == null){
			throw new IllegalArgumentException("Didn't find state " + state + " in the initial state set.");
		}else{
			return r.doubleValue();
		}
	}
	
	public double transitionProb(S state, S terminalState)
	{
		Map<S,Double> tprobs = _T.get(state);
		if(tprobs == null){
			throw new IllegalArgumentException("Didn't find state " + state + " in the initial state set.");
		}else{
			Double tprob = tprobs.get(terminalState);
			if(tprob == null){
				return 0;
			}else{
				return tprob.doubleValue();
			}
		}
	}
	
	public Set<S> successors(S state)
	{
		return _succs.get(state);
	}
}
