package tmrutil.learning.rl.smdp;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;
import tmrutil.learning.rl.sim.Simulator;

public abstract class OptionSimulator<S, O, A> extends Simulator<S, O, A> {

	private JFrame _frame;
	
	public OptionSimulator(){
		this(new JFrame());
		_frame.setSize(500, 500);
		_frame.setTitle("Trajectory Plot");
	}
	
	public OptionSimulator(JFrame frame){
		super();
		_frame = frame;
	}
	
	/**
	 * Samples the simulator for the outcome of executing a primitive action. Takes the option into account
	 * 
	 * @param state
	 * @param action
	 * @param oIdx
	 * @return
	 */
	public abstract OptionOutcome<S, O> samplePrimitive(S state, A action, int oIdx); 
	
	
	/**
	 * 
	 * @return a state sampled from the simulator
	 */
	public abstract S sampleState();
	
	/**
	 * Samples the simulator for the outcome of executing an ``option'' over MDP
	 * states at the specific observation.
	 * 
	 * @param state
	 *            a state
	 * @param option
	 *            the option to execute
	 * @param discountFactor
	 *            the discount factor used while accumulating reinforcements
	 * @param maxTrajectoryLength
	 *            the maximum number of timesteps to execute the option for
	 * @return the outcome of executing the specified option
	 */
	public final OptionOutcome<S, O> sample(S state, Option<S, A> option,
			double discountFactor, int maxTrajectoryLength, boolean showFrame, int optionIndex){
				S s = state;
				A lastAction = null;
				double cumR = 0;
				double dCumR = 0;
				S terminalState = s;
				int lifetime = 0;

				double gamma = 1;
				
				// Setup the frame
				if (_frame != null && this instanceof Drawable && showFrame) {
					DrawableComponent dcomp = new DrawableComponent((Drawable) this);
					_frame.add(dcomp);
					_frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					_frame.setVisible(true);
					if (this instanceof KeyListener) {
						_frame.addKeyListener((KeyListener) this);
					}
					if (this instanceof MouseListener) {
						_frame.addMouseListener((MouseListener) this);
					}
				} else {
					_frame = null;
				}

				for (int t = 1; t <= maxTrajectoryLength; t++) {
					
					// Pain the scene if the frame is not null and visible
					if (_frame != null && _frame.isVisible() && showFrame) {
						_frame.repaint();
						try {
							Thread.sleep(100);
						} catch (InterruptedException ex) { /* Do Nothing */
						}
					}
					
					lifetime = t;

					A a = option.policy(s);
					OptionOutcome<S, O> po = samplePrimitive(s, a, optionIndex);

					cumR = cumR + po.cumR();
					dCumR = dCumR + gamma * po.dCumR();
					terminalState = po.terminalState();

					s = terminalState;
					gamma *= discountFactor;
					
					// Paint the scene if the frame is not null and visible
					if (_frame != null && _frame.isVisible() && showFrame) {
						_frame.repaint();
					}

					lastAction = a;
					if (bernoulli(option.terminationProb(terminalState, t))) {
						break;
					}
				}
				O tobs = observe(lastAction, terminalState);
				OptionOutcome<S, O> outcome = new OptionOutcome<S, O>(terminalState,
						tobs, dCumR, cumR, lifetime);
				return outcome;
			}
}
