package tmrutil.learning.rl.smdp;

import tmrutil.learning.rl.MarkovDecisionProcess;

public interface SMDP<S,A> extends MarkovDecisionProcess<S,A>
{
	public Iterable<S> states();
	
	public Iterable<A> primitiveActions(S state);
	
	public Iterable<A> actions(S state);
}
