package tmrutil.learning.rl.smdp;

import tmrutil.learning.rl.QFunction;
import tmrutil.util.DebugException;

/**
 * Implements an interrupting option that penalizes interrupting the option .
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the primitive action type
 */
public class InterruptingOption<S, A> extends AbstractOption<S, A> {
	
	public static enum Penalty {CONST, LAMBDA_TIME_DECAY};

	private AbstractOption<S, A> _option;
	private QFunction<S, Integer> _qfunc;
	private double _lambda;
	private double _vconst;
	private Penalty _penType;

	/**
	 * Constructs an interrupting option with penalty for interruption that
	 * occurs too early during the lifetime of the options execution.
	 * 
	 * @param option
	 *            an option that might be interrupted
	 * @param qfunc
	 *            an action-value function
	 * @param lambda
	 *            a parameter controlling the penalty for interrupting an option
	 *            given the number of timesteps that the option has executed
	 * @param vconst
	 *            the value constant to multiply the time penalty by
	 */
	public InterruptingOption(AbstractOption<S, A> option,
			QFunction<S, Integer> qfunc, double lambda, double vconst, Penalty penType) {
		super(option.index());
		_option = option;
		_qfunc = qfunc;
		setLambda(lambda);
		_vconst = vconst;
		_penType = penType;
	}
	
	/**
	 * Copy constructor for interrupting options.
	 * @param other another interrupting option
	 */
	public InterruptingOption(InterruptingOption<S,A> other)
	{
		super(other._option.index());
		_option = other._option;
		_qfunc = other._qfunc;
		setLambda(other.lambda());
		_vconst = other._vconst;
	}

	public void setLambda(double lambda) {
		if (lambda < 0 || lambda > 1) {
			throw new IllegalArgumentException(
					"Penalty parameter lambda must be in [0, 1]. Tried to set lambda = "
							+ lambda);
		} else {
			_lambda = lambda;
		}
	}

	/**
	 * Returns the parameter controlling the penalty for interrupting an option
	 * dependent on the options current duration.
	 * 
	 * @return the parameter controlling the penalty for interrupting an option
	 */
	public double lambda() {
		return _lambda;
	}

	/**
	 * Sets the action-value function used to determine if an option should be
	 * interrupted.
	 * 
	 * @param qfunc
	 *            an action-value function
	 */
	public void setQFunction(QFunction<S, Integer> qfunc) {
		_qfunc = qfunc;
	}

	@Override
	public A policy(S state) {
		return _option.policy(state);
	}

	@Override
	public double terminationProb(S state, int duration) {
		double q = _qfunc.value(state, index());
		double v = _qfunc.greedyValue(state);
		double pen = penalty(duration);
		if ((_qfunc.isMaximizing() && q < v - pen) || (_qfunc.isMinimizing() && q > v + pen)) {
			return 1;
		} else {
			return _option.terminationProb(state, duration);
		}
	}

	/**
	 * Defines the term that penalizes option interruption. This method can be
	 * overridden by subclasses to create new time dependent penalty functions.
	 * 
	 * @param duration
	 *            the number of timesteps that the option has currently executed
	 *            for
	 * @return a penalty term
	 */
	public double penalty(double duration) {
		switch(_penType){
		case CONST:
			return _lambda * _vconst;
		case LAMBDA_TIME_DECAY:
			return Math.pow(_lambda, duration) * _vconst;
		default:
			throw new DebugException("Invalid penalty type for option interruption.");
		}
	}

	@Override
	public boolean inInitialSet(S state) {
		return _option.inInitialSet(state);
	}
	
}
