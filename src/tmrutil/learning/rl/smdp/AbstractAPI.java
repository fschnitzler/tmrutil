package tmrutil.learning.rl.smdp;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.Policy;

/**
 * An abstract implementation of Approximate Policy Iteration (API).
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public abstract class AbstractAPI<S, A> {

	/**
	 * The base option set.
	 */
	private ActionSet<S, Option<S, A>> _optionSet;

	public AbstractAPI(ActionSet<S, Option<S, A>> options) {
		_optionSet = options;
	}

	/**
	 * Returns the number of options that can be selected from.
	 * 
	 * @return the number of options
	 */
	public int numberOfOptions() {
		return _optionSet.numberOfActions();
	}

	/**
	 * Returns an option by its index.
	 * 
	 * @param index
	 *            the index of an option
	 * @return an option
	 */
	public Option<S, A> option(int index) {
		return _optionSet.action(index);
	}

	public ActionSet<S, Option<S, A>> options() {
		return _optionSet;
	}

	/**
	 * Performs a single iteration of API.
	 * 
	 * @param policy
	 *            an option policy
	 * @param states
	 *            an {@link Iterable} instance of states to evaluate and improve
	 *            the policy at
	 * @return an improved option policy
	 */
	public abstract Policy<S, Option<S,A>> iterate(Policy<S, Option<S,A>> policy,
			Iterable<S> states);
}
