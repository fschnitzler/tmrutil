package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.math.Constants;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;
import tmrutil.util.FourTuple;

/**
 * A finite, discrete Markovian decision process.
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the type of actions that can be performed by the learning system
 * 
 * @author Timothy Mann
 * 
 */
public class DiscreteMarkovDecisionProcessImpl<S, A> extends Task<S, A>
{
	private static final int DEFAULT_NUM_OBSERVATIONS = 10000;

	/** The total number of states. */
	private int _numStates;
	/** The total number of actions. */
	private int _numActions;

	/** A reference to the list of states. */
	private List<S> _states;
	/** A reference to the list of actions. */
	private List<A> _actions;

	/** The set of initial states. */
	private List<Integer> _initialStates;
	/** The set of final states. */
	private List<Integer> _finalStates;

	/** Index of the current state. */
	private int _cStateInd;

	/** A reference to the reinforcement signal. */
	private ReinforcementSignal<S, A> _rsignal;

	/**
	 * The complete transition probabilities for the underlying MDP.
	 */
	private double[][][] _tProbs;

	/**
	 * The discrete time step.
	 */
	private long _time;

	/** A list of observations. */
	private List<FourTuple<S, A, S, Double>> _observations;

	/**
	 * The total reinforcement of all actions taken.
	 */
	private double _totalReinforcement;

	/**
	 * Constructs a Markovian decision process at discrete time step zero and
	 * allocates the default amount of space for observations.
	 */
	public DiscreteMarkovDecisionProcessImpl(List<S> states, List<A> actions,
			Collection<Integer> initialStates, Collection<Integer> finalStates,
			double[][][] transitionProbabilities,
			ReinforcementSignal<S, A> rsignal)
	{
		_time = 0;
		_totalReinforcement = 0;
		_cStateInd = 0;
		_observations = new ArrayList<FourTuple<S, A, S, Double>>(
				DEFAULT_NUM_OBSERVATIONS);

		_numStates = states.size();
		_numActions = actions.size();
		_states = new ArrayList<S>(states);
		_actions = new ArrayList<A>(actions);
		_initialStates = new ArrayList<Integer>(initialStates);
		_finalStates = new ArrayList<Integer>(finalStates);

		if (!isValidTProb(_numStates, _numActions, transitionProbabilities)) {
			throw new IllegalArgumentException(
					"The transition probabilities data structure passed to this MDP constructor " +
					"is invalid either because one of the rows does not sum to 1 or because the " +
					"structure does not contain the correct format.");
		}
		_tProbs = transitionProbabilities;
		_rsignal = rsignal;
		
		reset(); // Use reset to initialize the MDP to an initial state
	}

	/**
	 * Determines whether or not a transition probability data structure is
	 * valid.
	 * 
	 * @param numStates
	 *            the number of states in the MDP
	 * @param numActions
	 *            the number of actions in the MDP
	 * @param tProbs
	 *            the transition probabilities data structure
	 * @return true if the transition probabilities structure is valid;
	 *         otherwise false
	 */
	private static final boolean isValidTProb(int numStates, int numActions,
			double[][][] tProbs)
	{
		if (tProbs.length != numStates) {
			return false;
		}

		for (int ps = 0; ps < numStates; ps++) {

			if (tProbs[ps].length != numActions) {
				return false;
			}
			for (int a = 0; a < numActions; a++) {

				if (tProbs[ps][a].length != numStates) {
					return false;
				}

				double sum = 0;
				for (int ns = 0; ns < numStates; ns++) {
					sum += tProbs[ps][a][ns];
				}
				if (sum < 1.0 - Constants.EPSILON
						|| sum > 1.0 + Constants.EPSILON) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Returns an observation vector describing the current state.
	 */
	@Override
	public S getState()
	{
		return _states.get(_cStateInd);
	}

	/**
	 * Returns the current discrete time step.
	 * 
	 * @return the current discrete time step
	 */
	public long getTime()
	{
		return _time;
	}

	/**
	 * Returns the total incurred reward of this process up to the current time
	 * step.
	 * 
	 * @return the total incurred reward of this process up to the current time
	 *         step
	 */
	public double getTotalReinforcement()
	{
		return _totalReinforcement;
	}

	@Override
	public double evaluate()
	{
		if (_observations.isEmpty()) {
			throw new DebugException(
					"Cannot evaluate the until the agent has performed at least one action.");
		} else {
			return _observations.get(_observations.size() - 1).getD();
		}
	}

	/**
	 * Steps the Markovian decision process one discrete time step into the
	 * future by executing the specified action.
	 */
	@Override
	public void execute(A action)
	{
		_time++;
		S prevState = getState();
		
		//
		// Perform the transition here
		//
		int actionIndex = findActionIndex(action);
		double[] pdist = _tProbs[_cStateInd][actionIndex];
		double rval = Random.uniform();
		int newStateInd = -1;
		double psum = 0;
		for(int i=0;i<_numStates;i++){
			psum += pdist[i];
			if(rval < psum){
				newStateInd = i;
				break;
			}
		}
		if(newStateInd < 0){
			throw new DebugException("State transition failure : (new state index = " + newStateInd + ")");
		}
		_cStateInd = newStateInd;

		S newState = getState();
		double reinforcement = _rsignal.evaluate(prevState, action);
		_totalReinforcement += reinforcement;
		_observations.add(new FourTuple<S, A, S, Double>(prevState, action,
				newState, reinforcement));
	}
	
	private int findActionIndex(A action)
	{
		int index = _actions.indexOf(action);
		if(index < 0){
			throw new IllegalArgumentException("The specified action is not valid for this MDP.");
		}else{
			return index;
		}
	}

	/**
	 * Returns true if the underlying MDP is in a final state.
	 */
	@Override
	public boolean isFinished()
	{
		return _finalStates.contains(_cStateInd);
	}

	/**
	 * Returns the list of observations.
	 */
	public List<FourTuple<S, A, S, Double>> getObservations()
	{
		return _observations;
	}

	/**
	 * Randomly selects an initial state and removes previously stored data.
	 */
	@Override
	public void reset()
	{
		_observations.clear();
		_time = 0;
		_totalReinforcement = 0;

		int rInd = Random.nextInt(_initialStates.size());
		_cStateInd = _initialStates.get(rInd);
	}
}
