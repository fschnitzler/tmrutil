package tmrutil.learning.rl;

import java.util.Map;

/**
 * A deterministic, stationary policy implemented by a {@link Map}. This implements a policy for a finite-state MDP.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public class MapPolicy<S, A> implements StationaryPolicy<S, A> {

	private Map<S,A> _policy;
	
	public MapPolicy(Map<S,A> policy) {
		_policy = policy;
	}

	@Override
	public A policy(S state) {
		return _policy.get(state);
	}

	@Override
	public boolean isDeterministic() {
		return true;
	}

	@Override
	public double actionProb(S state, A action) {
		A selected = _policy.get(state);
		return (selected.equals(action))? 1 : 0;
	}

}
