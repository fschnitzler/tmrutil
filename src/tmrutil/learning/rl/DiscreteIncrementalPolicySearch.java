package tmrutil.learning.rl;

import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.util.DataStore;

/**
 * An incremental policy search algorithm for tasks with discrete observation and action sets. 
 * @author Timothy A. Mann
 *
 */
public class DiscreteIncrementalPolicySearch extends
		IncrementalPolicySearch<Integer, Integer>
{
	private int _numObservations;
	private int _numActions;
	private int _maxPoliciesToSearch;
	private Double _rewardThreshold;

	public DiscreteIncrementalPolicySearch(int numObservations, int numActions, int numEvaluationSteps,
			Optimization opType, int maxPoliciesToSearch, Double rewardThreshold)
	{
		super(numEvaluationSteps, opType);
		_numObservations = numObservations;
		_numActions = numActions;
		
		_maxPoliciesToSearch = maxPoliciesToSearch;
		_rewardThreshold = rewardThreshold;
	}

	@Override
	public Policy<Integer, Integer> samplePolicy()
	{
		int[] policy = new int[_numObservations];
		for(int s = 0; s < _numObservations; s++){
			policy[s] = Random.nextInt(_numActions);
		}
		return new DiscretePolicy(policy);
	}

	@Override
	public boolean terminateSearch()
	{
		if(this.numPoliciesSearched() >= _maxPoliciesToSearch){
			return true;
		}
		if(!(_rewardThreshold == null) && !Double.isNaN(_rewardThreshold)){
			if(isMaximizing() && _rewardThreshold <= this.bestPolicysValue()){
				return true;
			}
			if(isMinimizing() && _rewardThreshold >= this.bestPolicysValue()){
				return true;
			}
		}
		return false;
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

}
