package tmrutil.learning.rl.pomdp;

/**
 * An interface for belief states.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <O> the observation type
 * @param <A> the action type
 */
public interface BeliefState<S,O,A> {
	
	/**
	 * Updates the underlying belief state.
	 * @param previousAction the previous action
	 * @param resultObservation the resulting observation
	 */
	public void update(A previousAction, O resultObservation);
	
	/**
	 * Returns the belief (probability) that the agent is in the specified state.
	 * @param state a state
	 * @return the probability of being in the specified state
	 */
	public double belief(S state);
	
	/**
	 * Samples a state from this belief state.
	 * @return a state
	 */
	public S sample();
}
