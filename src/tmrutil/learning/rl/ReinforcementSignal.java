package tmrutil.learning.rl;

import java.util.Map;

/**
 * A reinforcement signal is a potentially stochastic process that returns a
 * scalar value representing either immediate reward or cost given the previous
 * state, the action taken from the previous state, and the resulting new state.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state description type
 * @param <A>
 *            the action encoding type
 */
public interface ReinforcementSignal<S, A>
{
	/**
	 * Evaluates the reinforcement signal given a state and action. The returned
	 * value represents either immediate reward or cost of taking the specified
	 * action from the previous state. The reinforcement signal can potentially 
	 * be stochastic (meaning that the return value may be different for multiple evaluations with the same
	 * input values).
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @return a scalar value representing immediate reward or cost
	 */
	public double evaluate(S state, A action);
	
	
	/**
	 * A reinforcement signal defined by a map. Any state-action pairs not explicitly included in the map are given the default value.
	 * @author Timothy A. Mann
	 *
	 * @param <S> the state type
	 * @param <A> the action type
	 */
	public static class MapReinforcementSignal<S, A> implements ReinforcementSignal<S,A> {
		private Map<S,Map<A,Double>> _rmap;
		private double _defaultValue;
		
		public MapReinforcementSignal(Map<S,Map<A,Double>> rmap){
			this(rmap, 0.0);
		}
		
		public MapReinforcementSignal(Map<S,Map<A,Double>> rmap, double defaultValue){
			_rmap = rmap;
			_defaultValue = defaultValue;
		}

		@Override
		public double evaluate(S state, A action) {
			Map<A,Double> ars = _rmap.get(state);
			if(ars == null){
			return _defaultValue;
			}else{
				Double r = ars.get(action);
				if(r == null){
					return _defaultValue;
				}else{
					return r.doubleValue();
				}
			}
		}
		
		public double defaultValue(){
			return _defaultValue;
		}
	}
}
