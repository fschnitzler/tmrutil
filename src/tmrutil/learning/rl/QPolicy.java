package tmrutil.learning.rl;

import java.util.Collection;


/**
 * A policy constructed from an action-value function.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class QPolicy<S, A> implements Policy<S, A> {

	private QFunction<S, Integer> _qfunc;
	private ActionSet<S, A> _actions;

	public QPolicy(QFunction<S, Integer> qfunc, ActionSet<S, A> actions) {
		_qfunc = qfunc;
		_actions = actions;
	}

	@Override
	public A policy(S state) {
		Collection<Integer> indices = _actions.validIndices(state);
		Integer bestIndex = null;
		Double bestScore = null;
		for (Integer index : indices) {
			double score = _qfunc.value(state, index);

			if (bestScore == null
					|| (_qfunc.isMaximizing() && score > bestScore)
					|| (_qfunc.isMinimizing() && score < bestScore)) {
				bestScore = score;
				bestIndex = index;
			}
		}

		return _actions.action(bestIndex);
	}

}
