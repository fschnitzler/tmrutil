package tmrutil.learning.rl;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import tmrutil.optim.Optimization;
import tmrutil.stats.Statistics;
import tmrutil.util.DataStore;
import tmrutil.util.Interval;

/**
 * This class implements a hybrid model-based/model-free reinforcement learning
 * algorithm that is based on two PAC-MDP algorithms (R-MAX and Delayed
 * Q-learning).
 * 
 * @author Timothy A. Mann
 * 
 */
public class HybridPACMDPLearner extends
		AbstractTemporalDifferenceLearning<Integer, Integer>
{
	public static final int MAX_ITERATIONS = 30;

	private static final double DEFAULT_LEARNING_RATE = 0;
	private int _numStates;
	private int _numActions;
	private Interval _rewardInterval;
	private int _visitsUntilKnown;

	private double _sensitivity;

	/**
	 * The time of the most recent state-action value change.
	 */
	private long _mostRecentUpdate;

	/**
	 * The state-action value estimates.
	 */
	private double[][] _Q;
	/**
	 * Accumulators for updates on each state-action pair.
	 */
	private double[][] _AU;
	/**
	 * Counters for each state-action pair.
	 */
	private long[][] _l;
	/**
	 * Beginning timestep for an attempted update for each state-action pair.
	 */
	private long[][] _b;

	/**
	 * Flags used to determine whether a state-action pair is eligible for
	 * updates.
	 */
	private boolean[][] _learn;

	/**
	 * The set of states that this algorithm keeps a model for.
	 */
	private Set<Integer> _modeledStates;

	/**
	 * A map from modeled states to states with nonzero probability of being
	 * transitioned to.
	 */
	private Map<Integer, Set<Integer>> _modeledSuccessors;

	/**
	 * A map from modeled states to the number of times that state has been
	 * visited.
	 */
	private Map<Integer, Integer> _modeledStateVisitCount;
	/**
	 * A map from an integer representation of modeled state-action pairs to the
	 * number of times that state-action pair has been visited.
	 */
	private Map<Integer, Integer> _modeledStateActionVisitCount;
	/**
	 * A map from an integer representation of modeled state-action-state
	 * transitions to the number of times that particular transition has been
	 * observed.
	 */
	private Map<Integer, Integer> _modeledTransitionVisitCount;
	/**
	 * Maps from an integer representation of a modeled state-action pair to the
	 * cumulative reward received while visiting that state-action pair.
	 */
	private Map<Integer, Double> _modeledCumulativeReward;

	public HybridPACMDPLearner(int numStates, int numActions,
			double discountFactor, Interval rewardInterval,
			int numVisitsUntilKnown, Set<Integer> modeledStates,
			double sensitivity)
	{
		super(DEFAULT_LEARNING_RATE, discountFactor, Optimization.MAXIMIZE);
		if (numStates < 1) {
			throw new IllegalArgumentException("The number of states ("
					+ numStates + ") cannot be less than one.");
		}
		_numStates = numStates;
		if (numActions < 1) {
			throw new IllegalArgumentException("The number of actions ("
					+ numActions + ") cannot be less than one.");
		}
		_numActions = numActions;
		if (rewardInterval == null) {
			throw new NullPointerException(
					"The reward interval cannot be null.");
		}
		_rewardInterval = rewardInterval;
		if (numVisitsUntilKnown < 1) {
			throw new IllegalArgumentException(
					"The number of visits required for a state-action pair to become known ("
							+ numVisitsUntilKnown
							+ ") cannot be less than one.");
		}
		_visitsUntilKnown = numVisitsUntilKnown;

		_Q = new double[_numStates][_numActions];

		// Delayed Q-learning Data Structures
		_AU = new double[_numStates][_numActions];
		_l = new long[_numStates][_numActions];
		_b = new long[_numStates][_numActions];
		_learn = new boolean[_numStates][_numActions];

		// R-MAX Data Structures
		_modeledStates = new TreeSet<Integer>(modeledStates);
		_modeledSuccessors = new TreeMap<Integer, Set<Integer>>();
		_modeledStateVisitCount = new TreeMap<Integer, Integer>();
		_modeledStateActionVisitCount = new TreeMap<Integer, Integer>();
		_modeledTransitionVisitCount = new TreeMap<Integer, Integer>();
		_modeledCumulativeReward = new TreeMap<Integer, Double>();
	}

	@Override
	protected void resetImpl()
	{
		_mostRecentUpdate = 0;
		double vmax = rmax() / (1 - getDiscountFactor());
		for (int s = 0; s < _numStates; s++) {
			for (int a = 0; a < _numActions; a++) {
				_Q[s][a] = vmax;
				_AU[s][a] = 0;
				_l[s][a] = 0;
				_b[s][a] = 0;
				_learn[s][a] = true;
			}
		}
	}

	@Override
	protected void trainImpl(Integer prevState, Integer action,
			Integer newState, double reinforcement)
	{
		if (_modeledStates.contains(prevState)) {
			boolean doValueIteration = modelUpdate(prevState, action, newState,
					reinforcement);
			if (doValueIteration) {
				subsetValueIteration(MAX_ITERATIONS);
			}
		} else {
			if (_b[prevState][action] <= _mostRecentUpdate) {
				_learn[prevState][action] = true;
			}
			if (_learn[prevState][action]) {
				if (_l[prevState][action] == 0) {
					_b[prevState][action] = getTime();
				}
				_l[prevState][action] += 1;
				_AU[prevState][action] += reinforcement + getDiscountFactor()
						* Statistics.max(_Q[newState]);
				if (_l[prevState][action] == _visitsUntilKnown) {
					double update = _AU[prevState][action] / _visitsUntilKnown;
					double error = _Q[prevState][action] - update;
					if (error >= (2 * _sensitivity)) {
						_Q[prevState][action] = update + _sensitivity;
						subsetValueIteration(MAX_ITERATIONS);
						_mostRecentUpdate = getTime();
					} else if (_b[prevState][action] > _mostRecentUpdate) {
						_learn[prevState][action] = false;
					}
					_AU[prevState][action] = 0;
					_l[prevState][action] = 0;
				}
			}
		}
	}

	private boolean modelUpdate(Integer state, Integer action,
			Integer resultState, double reinforcement)
	{
		Integer sIndex = stateToIndex(state);
		Integer saIndex = stateActionToIndex(state, action);
		Integer sasIndex = transitionToIndex(state, action, resultState);
		incrementKey(_modeledStateVisitCount, sIndex, 1);
		incrementKey(_modeledStateActionVisitCount, saIndex, 1);
		incrementKey(_modeledTransitionVisitCount, sasIndex, 1);

		Double oldR = _modeledCumulativeReward.get(saIndex);
		if (oldR == null) {
			_modeledCumulativeReward.put(saIndex, reinforcement);
		} else {
			_modeledCumulativeReward.put(saIndex, oldR.doubleValue()
					+ reinforcement);
		}

		Set<Integer> successors = _modeledSuccessors.get(state);
		if (successors == null) {
			successors = new TreeSet<Integer>();
			successors.add(resultState);
			_modeledSuccessors.put(state, successors);
		} else {
			successors.add(resultState);
		}

		if (count(state, action) == _visitsUntilKnown) {
			return true;
		} else {
			return false;
		}
	}

	private void subsetValueIteration(int maxIterations)
	{
		double gamma = getDiscountFactor();
		for (int i = 0; i < maxIterations; i++) {
			double maxDelta = 0;
			for (Integer mstate : _modeledStates) {
				for (int a = 0; a < numberOfActions(); a++) {
					int saCount = count(mstate, a);
					if (saCount >= _visitsUntilKnown) {
						double oldQ = _Q[mstate][a];
						double r = reward(mstate, a);
						double Qupdate = r;
						Set<Integer> successors = _modeledSuccessors.get(mstate);
						if (successors != null) {
							for (Integer resultState : successors) {
								Qupdate += gamma
										* transitionProb(mstate, a, resultState)
										* Statistics.max(_Q[resultState]);
							}
						}
						_Q[mstate][a] = Qupdate;
						maxDelta = Math.max(Math.abs(Qupdate - oldQ), maxDelta);
					}
				}
			}
			if (maxDelta <= _sensitivity) {
				break;
			}
		}
	}

	private void incrementKey(Map<Integer, Integer> map, Integer key,
			Integer val)
	{
		Integer oldVal = map.get(key);
		if (oldVal == null) {
			map.put(key, val);
		} else {
			map.put(key, new Integer(oldVal.intValue() + val.intValue()));
		}
	}

	public double evaluateQ(Integer state, Integer action)
	{
		return _Q[state][action];
	}

	@Override
	public Integer policy(Integer state)
	{
		return Statistics.maxIndex(_Q[state]);
	}

	/**
	 * Returns the maximum possible immediate (one step) reward.
	 * 
	 * @return the maximum reward
	 */
	public double rmax()
	{
		return _rewardInterval.getMax();
	}

	/**
	 * Returns the number of states in the MDP of interest.
	 * 
	 * @return the number of states
	 */
	public int numberOfStates()
	{
		return _numStates;
	}

	/**
	 * Returns the number of actions available to the agent.
	 * 
	 * @return the number of actions
	 */
	public int numberOfActions()
	{
		return _numActions;
	}

	private double transitionProb(Integer state, Integer action,
			Integer resultState)
	{
		int saCount = count(state, action);
		if (saCount < _visitsUntilKnown) {
			if (state.equals(resultState)) {
				return 1;
			} else {
				return 0;
			}
		} else {
			int sasCount = count(state, action, resultState);
			return sasCount / (double) saCount;
		}
	}

	private double reward(Integer state, Integer action)
	{
		int saCount = count(state, action);
		if (saCount < _visitsUntilKnown) {
			return rmax();
		} else {
			Integer index = stateActionToIndex(state, action);
			return _modeledCumulativeReward.get(index) / saCount;
		}
	}

	private int count(Integer state)
	{
		if (!_modeledStates.contains(state)) {
			throw new IllegalArgumentException(
					"Cannot determine number of visits to unmodeled states.");
		}
		Integer count = _modeledStateVisitCount.get(state);
		if (count == null) {
			return 0;
		} else {
			return count.intValue();
		}
	}

	private int count(Integer state, Integer action)
	{
		if (!_modeledStates.contains(state)) {
			throw new IllegalArgumentException(
					"Cannot determine number of visits to unmodeled states.");
		}
		Integer index = stateActionToIndex(state, action);
		Integer count = _modeledStateActionVisitCount.get(index);
		if (count == null) {
			return 0;
		} else {
			return count.intValue();
		}
	}

	private int count(Integer state, Integer action, Integer resultState)
	{
		if (!_modeledStates.contains(state)) {
			throw new IllegalArgumentException(
					"Cannot determine number of visits to unmodeled states.");
		}
		Integer index = transitionToIndex(state, action, resultState);
		Integer count = _modeledTransitionVisitCount.get(index);
		if (count == null) {
			return 0;
		} else {
			return count.intValue();
		}
	}

	private Integer stateToIndex(Integer state)
	{
		return state;
	}

	private Integer stateActionToIndex(Integer state, Integer action)
	{
		return new Integer((state.intValue() * numberOfActions())
				+ action.intValue());
	}

	private Integer transitionToIndex(Integer state, Integer action,
			Integer resultState)
	{
		int index = (state.intValue() * numberOfStates() * numberOfActions())
				+ (action.intValue() * numberOfStates())
				+ resultState.intValue();
		return index;
	}

	public double maxQ(Integer state)
	{
		return Statistics.max(_Q[state]);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}
}
