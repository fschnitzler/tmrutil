package tmrutil.learning.rl;

/**
 * An interface for algorithms that face the exploration-exploitation dilemma.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public interface Explorer<S,A>
{
	/**
	 * Returns true if the specified state-action pair is considered to be an exploratory action. False is returned if the action is not exploratory.
	 * @param state a state
	 * @param action an action
	 * @return true if the state-action pair is exploratory; otherwise false
	 */
	public boolean isExploration(S state, A action);
}
