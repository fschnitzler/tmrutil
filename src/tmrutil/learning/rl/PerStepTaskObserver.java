package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;
import tmrutil.stats.Statistics;

/**
 * A task observer that records information at each timestep of execution.
 * @author Timothy A. Mann
 *
 */
public class PerStepTaskObserver implements
		TaskObserver<Integer, Integer, Task<Integer, Integer>>
{
	private List<Integer> _trajectory;
	private boolean _episodeBeginning;
	private List<Double> _reinforcements;
	private int _numExploreSteps;
	private Explorer<Integer,Integer> _explorer;
	
	/**
	 * Constructs an empty task observer.
	 */
	public PerStepTaskObserver()
	{
		this(null);
	}
	
	public PerStepTaskObserver(Explorer<Integer,Integer> explorer)
	{
		_reinforcements = new ArrayList<Double>();
		_trajectory = new ArrayList<Integer>();
		_numExploreSteps = 0;
		_explorer = explorer;
		_episodeBeginning = false;
	}

	@Override
	public void observeStep(Task<Integer, Integer> task, Integer prevState,
			Integer action, Integer newState, double reinforcement)
	{
		_reinforcements.add(reinforcement);
		if(_explorer != null && _explorer.isExploration(prevState, action)){
			_numExploreSteps++;
		}
		
		if(_episodeBeginning){
			_trajectory.add(prevState);
			_episodeBeginning = false;
		}
		_trajectory.add(newState);
	}

	@Override
	public void observeEpisodeBegin(Task<Integer, Integer> task)
	{
		_episodeBeginning = true;
	}

	@Override
	public void observeEpisodeEnd(Task<Integer, Integer> task)
	{
	
	}

	@Override
	public void reset()
	{
		_reinforcements.clear();
		_numExploreSteps = 0;
		_trajectory.clear();
		_episodeBeginning = false;
	}
	
	public List<Integer> stateTrajectory()
	{
		return _trajectory;
	}
	
	public int numberOfExplorationSteps()
	{
		if(_explorer != null){
			return _numExploreSteps;
		}else{
			throw new IllegalStateException("Exploration steps were not counted because a null explorer was given during construction.");
		}
	}
	
	public List<Double> getReinforcementsAtEachStep()
	{
		return _reinforcements;
	}
	
	public List<Double> getCumulativeReinforcementsAtEachStep()
	{
		List<Double> cumr = new ArrayList<Double>(_reinforcements.size());
		double sum = 0;
		for(int i=0;i<_reinforcements.size();i++){
			sum += _reinforcements.get(i);
			cumr.add(sum);
		}
		return cumr;
	}
	
	public double getCumulativeReward()
	{
		return Statistics.sum(_reinforcements);
	}

}
