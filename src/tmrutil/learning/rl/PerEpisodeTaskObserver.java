package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.List;

/**
 * A task observer that records data related to a series of episodes.
 * 
 * @author Timothy A. Mann
 * 
 */
public class PerEpisodeTaskObserver<S, A> implements
		TaskObserver<S, A, Task<S, A>>
{
	private double _rsum;
	private List<Double> _reinforcements;
	private List<Integer> _episodeLengths;
	private int _epLength;

	private Explorer<S, A> _explorer;
	private List<Integer> _exploreStepCounts;
	private int _numExploreSteps;

	/**
	 * Constructs an empty task observer.
	 */
	public PerEpisodeTaskObserver()
	{
		this(null);
	}

	public PerEpisodeTaskObserver(Explorer<S, A> explorer)
	{
		_explorer = explorer;
		_reinforcements = new ArrayList<Double>();
		_episodeLengths = new ArrayList<Integer>();
		_exploreStepCounts = new ArrayList<Integer>();
		reset();
	}

	@Override
	public void observeStep(Task<S, A> task, S prevState, A action, S newState,
			double reinforcement)
	{
		_rsum += reinforcement;
		_epLength += 1;
		if (_explorer != null) {
			if (_explorer.isExploration(prevState, action)) {
				_numExploreSteps += 1;
			}
		}
	}

	@Override
	public void observeEpisodeBegin(Task<S, A> task)
	{
		_rsum = 0;
		_epLength = 0;
		_numExploreSteps = 0;
	}

	@Override
	public void observeEpisodeEnd(Task<S, A> task)
	{
		_reinforcements.add(_rsum);
		_episodeLengths.add(_epLength);
		_exploreStepCounts.add(_numExploreSteps);
	}

	@Override
	public void reset()
	{
		_reinforcements.clear();
		_episodeLengths.clear();
		_exploreStepCounts.clear();
		_rsum = 0;
		_epLength = 0;
		_numExploreSteps = 0;
	}

	/**
	 * Returns the number of exploration steps that occurred at each episode.
	 * 
	 * @return the number of exploration steps that occurred at each episode
	 */
	public List<Integer> exploreStepCounts()
	{
		if (_explorer != null) {
			return _exploreStepCounts;
		} else {
			throw new IllegalStateException("Exploration information was not recorded because a null explorer was provided during construction.");
		}
	}

	/**
	 * Returns the sum of reinforcements observed at the end of each episode.
	 * More recent episodes correspond to higher indices in the returned list.
	 * 
	 * @return a list containing sums of reinforcements for each observed
	 *         episode
	 */
	public List<Double> getReinforcementSumAtEachEpisode()
	{
		return _reinforcements;
	}

	/**
	 * Returns the cumulative reinforcements at the end of each observed
	 * episode.
	 * 
	 * @return a list containing the cumulative reinforcements for each observed
	 *         episode
	 */
	public List<Double> getCumulativeReinforcementsAtEachEpisode()
	{
		List<Double> cumr = new ArrayList<Double>(_reinforcements.size());
		for (int i = 0; i < _reinforcements.size(); i++) {
			if (i > 0) {
				cumr.add(_reinforcements.get(i) + cumr.get(i - 1));
			} else {
				cumr.add(_reinforcements.get(i));
			}
		}
		return cumr;
	}

	/**
	 * Returns the number of timesteps in each episode. This data is only
	 * relevant for tasks that may finish before the maximum number of steps
	 * have passed.
	 * 
	 * @return a list containing the number of timesteps of each episode
	 */
	public List<Integer> getEpisodeLengths()
	{
		return _episodeLengths;
	}

}
