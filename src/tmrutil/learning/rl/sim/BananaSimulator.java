package tmrutil.learning.rl.sim;

import java.util.ArrayList;
import java.util.List;

import tmrutil.learning.rl.ActionSet;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.learning.rl.smdp.PrimitiveAction;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.DebugException;

/**
 * A simulator for "The Banana Problem", a supply chain management problem where
 * the agent is a banana supplier to a chain of supermarkets. The objective of
 * the problem is to maximize profit, while avoiding unmet demands.
 * 
 * @author Timothy A. Mann
 *
 */
public class BananaSimulator extends
		Simulator<FactoredState, FactoredState, Integer> {

	/**
	 * Index into the state where values reflect the stage of the problem.
	 */
	public static final int STAGE_INDEX = 0;
	/**
	 * Index into the state where values reflect the number of bananas remaining
	 * from previous shipments.
	 */
	public static final int REMAINING_INDEX = 1;
	/**
	 * Index into the state where values reflect the size of the current
	 * shipment.
	 */
	public static final int SHIPMENT_SIZE_INDEX = 2;
	/**
	 * Index into the state where values reflect the amount of damage to the
	 * shipment.
	 */
	public static final int AMOUNT_DAMAGED_INDEX = 3;

	public static final int STATE_SIZE = 4;

	public static final int MAX_INVENTORY = 100;
	public int SHIPMENT_UNIT_SIZE = 33;

	public enum Stage {
		QUANTITY, SUPPLIER, INTERNATIONAL_SHIPPING, DOMESTIC_SHIPPING, PRICE_TO_CUSTOMER;

		public int index() {
			return indexOf(this);
		}

		public static int indexOf(Stage value) {
			if (value == null) {
				throw new NullPointerException("Null has no index.");
			}
			Stage[] values = values();
			for (int i = 0; i < values.length; i++) {
				if (values[i].equals(value)) {
					return i;
				}
			}
			throw new DebugException("No index matching element: " + value);
		}
	}

	public enum QuantityAction {
		NONE, SMALL, MEDIUM, LARGE;

		public int index() {
			return indexOf(this);
		}

		public static int indexOf(QuantityAction value) {
			if (value == null) {
				throw new NullPointerException("Null has no index.");
			}
			QuantityAction[] values = values();
			for (int i = 0; i < values.length; i++) {
				if (values[i].equals(value)) {
					return i;
				}
			}
			throw new DebugException("No index matching element: " + value);
		}
	}

	public enum SupplierAction {
		HAWAII, JAMAICA, CHILIE, ICELAND;

		public double cost(QuantityAction qa) {
			double cost = 0;
			switch (this) {
			case HAWAII:
				cost = 5 + 5 * qa.index();
				break;
			case JAMAICA:
				cost = 2 + 5 * qa.index();
				break;
			case CHILIE:
				cost = 6 + 4 * qa.index();
				break;
			case ICELAND:
				cost = 10 + 4 * qa.index();
				break;
			}
			return cost;
		}

		public double failProb(QuantityAction qa) {
			double fprob = 0;
			switch (this) {
			case HAWAII:
				fprob = 0.3;
				break;
			case JAMAICA:
				fprob = 0.2;
				break;
			case CHILIE:
				fprob = 0.1;
				break;
			case ICELAND:
				fprob = 0;
				break;
			}
			return fprob;
		}
	}

	public enum InternationalShippingAction {
		PLANE, LARGE_SHIP, SMALL_SHIP, SUBMARINE;

		public double cost(QuantityAction qa) {
			switch (this) {
			case PLANE:
				return 10 + 5 * qa.index();
			case LARGE_SHIP:
				return 20;
			case SMALL_SHIP:
				return 3 + 5 * qa.index();
			case SUBMARINE:
				return 10 + 7 * qa.index();
			}
			return 0;
		}

		public boolean damage() {
			double r = Random.uniform();
			switch (this) {
			case PLANE:
				if (r < 0.1) {
					return true;
				} else {
					return false;
				}
			case LARGE_SHIP:
				if (r < 0.2) {
					return true;
				} else {
					return false;
				}
			case SMALL_SHIP:
				if (r < 0.3) {
					return true;
				} else {
					return false;
				}
			case SUBMARINE:
				if (r < 0.05) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}
	}

	public enum DomesticShippingAction {
		SMALL_TRUCK, LARGE_TRUCK, TRAIN;

		public double cost() {
			switch (this) {
			case SMALL_TRUCK:
				return 1;
			case LARGE_TRUCK:
				return 2;
			case TRAIN:
				return 3;
			}
			return 0;
		}

		public boolean damage() {
			double r = Random.uniform();
			switch (this) {
			case SMALL_TRUCK:
				if (r < 0.3) {
					return true;
				} else {
					return false;
				}
			case LARGE_TRUCK:
				if (r < 0.2) {
					return true;
				} else {
					return false;
				}
			case TRAIN:
				if (r < 0.1) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}
	}

	public enum PriceAction {
		LOW, MEDIUM, HIGH;

		int demand() {
			int rand = (int) Random.normal(20, 20);
			if (rand < -20) {
				rand = -20;
			}
			if (rand > 20) {
				rand = 20;
			}

			switch (this) {
			case LOW:
				return Math.max(0, 30 + rand);
			case MEDIUM:
				return Math.max(0, 20 + rand);
			case HIGH:
				return Math.max(0, 10 + rand);
			}
			return 0;
		}

		double reward(int metDemand, int unmetDemand) {
			int unmetDemandCost = 3 * unmetDemand;
			double demandReward = 0;
			switch (this) {
			case LOW:
				demandReward = 1.5 * metDemand;
				break;
			case MEDIUM:
				demandReward = 2.0 * metDemand;
				break;
			case HIGH:
				demandReward = 2.5 * metDemand;
			}

			return demandReward - unmetDemandCost;
		}
	}

	public enum Damage {
		NONE, QUARTER, HALF;

		public int index() {
			return indexOf(this);
		}

		public static int indexOf(Damage value) {
			if (value == null) {
				throw new NullPointerException("Null has no index.");
			}
			Damage[] values = values();
			for (int i = 0; i < values.length; i++) {
				if (values[i].equals(value)) {
					return i;
				}
			}
			throw new DebugException("No index matching element: " + value);
		}

		public int adjustShipment(int shipment) {
			switch (this) {
			case NONE:
				return shipment;
			case QUARTER:
				return shipment - shipment / 4;
			case HALF:
				return shipment - shipment / 2;
			}
			return shipment;
		}
	}

	@Override
	public FactoredState observe(Integer previousAction,
			FactoredState resultState) {
		return resultState;
	}

	@Override
	public OptionOutcome<FactoredState, FactoredState> samplePrimitive(
			FactoredState state, Integer action) {

		double reward = 0;
		FactoredState nextState = null;
		int stageValue = state.component(STAGE_INDEX);
		Stage stage = Stage.values()[stageValue];

		int[] nextStateValues = null;

		switch (stage) {
		case QUANTITY:
			nextStateValues = state.toArray();
			nextStateValues[SHIPMENT_SIZE_INDEX] = action;
			if (action > 0) {
				nextStateValues[STAGE_INDEX] = Stage.SUPPLIER.index();
			} else {
				nextStateValues[STAGE_INDEX] = Stage.PRICE_TO_CUSTOMER.index();
			}
			nextStateValues[AMOUNT_DAMAGED_INDEX] = Damage.NONE.index();
			nextState = new FactoredStateImpl(nextStateValues, maxValues());
			reward = 0;
			break;
		case SUPPLIER:
			nextStateValues = state.toArray();
			QuantityAction qa = QuantityAction.values()[state
					.component(SHIPMENT_SIZE_INDEX)];
			SupplierAction saction = SupplierAction.values()[action];

			double fprob = saction.failProb(qa);
			double r = Random.uniform();
			if (r < fprob) {
				nextStateValues[STAGE_INDEX] = Stage.PRICE_TO_CUSTOMER.index();
				reward = 0;
			} else {
				nextStateValues[STAGE_INDEX] = Stage.INTERNATIONAL_SHIPPING
						.index();
				reward = -saction.cost(qa);
			}
			nextStateValues[AMOUNT_DAMAGED_INDEX] = Damage.NONE.index();
			nextState = new FactoredStateImpl(nextStateValues, maxValues());
			break;
		case INTERNATIONAL_SHIPPING:
			nextStateValues = state.toArray();
			InternationalShippingAction isa = InternationalShippingAction
					.values()[action];
			qa = QuantityAction.values()[state.component(SHIPMENT_SIZE_INDEX)];
			reward = -isa.cost(qa);

			nextStateValues[STAGE_INDEX] = Stage.DOMESTIC_SHIPPING.index();
			if (isa.damage()) {
				nextStateValues[AMOUNT_DAMAGED_INDEX] = Damage.QUARTER.index();
			}

			nextState = new FactoredStateImpl(nextStateValues, maxValues());
			break;
		case DOMESTIC_SHIPPING:
			nextStateValues = state.toArray();
			DomesticShippingAction dsa = DomesticShippingAction.values()[action];
			reward = -dsa.cost();

			nextStateValues[STAGE_INDEX] = Stage.PRICE_TO_CUSTOMER.index();
			if (dsa.damage()) {
				nextStateValues[AMOUNT_DAMAGED_INDEX]++;
			}

			nextState = new FactoredStateImpl(nextStateValues, maxValues());
			break;
		case PRICE_TO_CUSTOMER:
			nextStateValues = state.toArray();
			PriceAction paction = PriceAction.values()[action];
			int demand = paction.demand();
			int remaining = nextStateValues[REMAINING_INDEX];

			int shipment = SHIPMENT_UNIT_SIZE
					* nextStateValues[SHIPMENT_SIZE_INDEX];
			Damage damage = Damage.values()[nextStateValues[AMOUNT_DAMAGED_INDEX]];
			shipment = damage.adjustShipment(shipment);

			int totalAvailable = remaining + shipment;
			int afterDemand = totalAvailable - demand;

			int demandMet = Math.min(demand, totalAvailable);
			int unmetDemand = Math.max(0, demand - totalAvailable);

			reward = paction.reward(demandMet, unmetDemand);

			nextStateValues[STAGE_INDEX] = Stage.QUANTITY.index();
			nextStateValues[REMAINING_INDEX] = (afterDemand > 0) ? Math.min(
					afterDemand, MAX_INVENTORY) : 0;
			nextStateValues[SHIPMENT_SIZE_INDEX] = QuantityAction.NONE.index();
			nextStateValues[AMOUNT_DAMAGED_INDEX] = Damage.NONE.index();

			nextState = new FactoredStateImpl(nextStateValues, maxValues());
			break;
		default:
			throw new DebugException("Stage " + stage
					+ " of the banana problem is not recognized!");
		}

		return new OptionOutcome<FactoredState, FactoredState>(nextState,
				nextState, reward, reward, 1);
	}

	public int[] maxValues() {
		int[] maxValues = new int[STATE_SIZE];
		maxValues[STAGE_INDEX] = Stage.values().length - 1;
		maxValues[REMAINING_INDEX] = MAX_INVENTORY;
		maxValues[SHIPMENT_SIZE_INDEX] = QuantityAction.values().length - 1;
		maxValues[AMOUNT_DAMAGED_INDEX] = Damage.values().length - 1;
		return maxValues;
	}

	public int[] upperLimits() {
		int[] upperLimits = new int[STATE_SIZE];
		upperLimits[STAGE_INDEX] = Stage.values().length;
		upperLimits[REMAINING_INDEX] = MAX_INVENTORY + 1;
		upperLimits[SHIPMENT_SIZE_INDEX] = QuantityAction.values().length;
		upperLimits[AMOUNT_DAMAGED_INDEX] = Damage.values().length;
		return upperLimits;
	}

	public ActionSet<FactoredState, Integer> actionSet() {
		return new BananaProblemActionSet();
	}

	public ActionSet<FactoredState, Option<FactoredState, Integer>> primitivesAsOptionsActionSet() {
		return new BananaProblemPrimitveActionOptionSet();
	}

	public static class BananaProblemActionSet extends
			ActionSet<FactoredState, Integer> {

		@Override
		public List<Integer> validIndices(FactoredState state) {
			int numActions = 0;
			int stageValue = state.component(STAGE_INDEX);
			Stage stage = Stage.values()[stageValue];
			switch (stage) {
			case QUANTITY:
				numActions = QuantityAction.values().length;
				break;
			case SUPPLIER:
				numActions = SupplierAction.values().length;
				break;
			case INTERNATIONAL_SHIPPING:
				numActions = InternationalShippingAction.values().length;
				break;
			case DOMESTIC_SHIPPING:
				numActions = DomesticShippingAction.values().length;
				break;
			case PRICE_TO_CUSTOMER:
				numActions = PriceAction.values().length;
				break;
			default:
				throw new DebugException("Stage " + stage
						+ " of the banana problem is not recognized!");
			}

			List<Integer> valid = new ArrayList<Integer>(numActions);
			for (int a = 0; a < numActions; a++) {
				valid.add(a);
			}

			return valid;
		}

		@Override
		public Integer action(int index) {
			return index;
		}

		@Override
		public int numberOfActions() {
			int[] actionSizes = { QuantityAction.values().length,
					SupplierAction.values().length,
					InternationalShippingAction.values().length,
					DomesticShippingAction.values().length,
					PriceAction.values().length };
			return Statistics.max(actionSizes);
		}

	}

	public static class BananaProblemPrimitveActionOptionSet extends
			ActionSet<FactoredState, Option<FactoredState,Integer>> {

		@Override
		public List<Integer> validIndices(FactoredState state) {
			int numActions = 0;
			int stageValue = state.component(STAGE_INDEX);
			Stage stage = Stage.values()[stageValue];
			switch (stage) {
			case QUANTITY:
				numActions = QuantityAction.values().length;
				break;
			case SUPPLIER:
				numActions = SupplierAction.values().length;
				break;
			case INTERNATIONAL_SHIPPING:
				numActions = InternationalShippingAction.values().length;
				break;
			case DOMESTIC_SHIPPING:
				numActions = DomesticShippingAction.values().length;
				break;
			case PRICE_TO_CUSTOMER:
				numActions = PriceAction.values().length;
				break;
			default:
				throw new DebugException("Stage " + stage
						+ " of the banana problem is not recognized!");
			}

			List<Integer> valid = new ArrayList<Integer>(numActions);
			for (int a = 0; a < numActions; a++) {
				valid.add(a);
			}

			return valid;
		}

		@Override
		public Option<FactoredState,Integer> action(int index) {
			return new PrimitiveAction<FactoredState,Integer>(index,index);
		}

		@Override
		public int numberOfActions() {
			int[] actionSizes = { QuantityAction.values().length,
					SupplierAction.values().length,
					InternationalShippingAction.values().length,
					DomesticShippingAction.values().length,
					PriceAction.values().length };
			return Statistics.max(actionSizes);
		}

	}

}
