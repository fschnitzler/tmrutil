package tmrutil.learning.rl.sim.imgmt;

import java.util.HashSet;
import java.util.Set;

import tmrutil.learning.rl.sim.Simulator;
import tmrutil.learning.rl.smdp.Option;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.stats.Random;
import tmrutil.util.Interval;

/**
 * A simple seasonal inventory management simulator with two kinds of widget
 * types (1) swimsuits, and (2) winter coats. The year starts in winter and
 * there are 24 decision points (corresponding to decision points twice per
 * month). Demand for winter coats is high in the winter and almost nonexistent
 * in the summer and the converse is true for swimsuits.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SeasonalInventorySimulator extends
		Simulator<int[], int[], Integer> {

	public static final int NO_ORDER_ACTION = 0;
	public static final int ORDER_10_SS_ACTION = 1;
	public static final int ORDER_10_WC_ACTION = 2;
	public static final int ORDER_10_OF_EACH_ACTION = 3;
	public static final int ORDER_50_SS_ACTION = 4;
	public static final int ORDER_50_WC_ACTION = 5;
	public static final int ORDER_50_OF_EACH_ACTION = 6;
	public static final int ORDER_100_SS_ACTION = 7;
	public static final int ORDER_100_WC_ACTION = 8;
	public static final int ORDER_100_OF_EACH_ACTION = 9;

	public static final int STATE_LENGTH = 3;
	public static final int NUM_ACTIONS = 10;

	public static final int LOOP_TIME = 24;
	public static final int SWIMSUIT_PEAK_TIME = LOOP_TIME / 2;
	public static final int WINTER_COAT_PEAK_TIME = 0;

	public static final double BASE_ORDER_COST = 200;
	public static final double SWIMSUIT_COST = 2;
	public static final double WINTER_COAT_COST = 4;

	public static final int UNMET_DEMAND_COST_BASE = 50;
	public static final int UNMET_DEMAND_COST_SCALE = 20;
	public static final int DEMAND_MIN = 0;
	public static final int DEMAND_MAX = 50;
	public static final double DEMAND_STD = (DEMAND_MAX - DEMAND_MIN) / 10;
	public static final Interval DEMAND_INTERVAL = new Interval(DEMAND_MIN,
			DEMAND_MAX);
	public static final int MAX_INVENTORY = 400;
	public static final double INVENTORY_COST = 1;

	public SeasonalInventorySimulator() {

	}

	@Override
	public int[] observe(Integer previousAction, int[] resultState) {
		return resultState;
	}

	@Override
	public OptionOutcome<int[], int[]> samplePrimitive(int[] state,
			Integer action) {
		State s = validateState(state);
		int time = s.time();

		//
		// Sample the demand for swimsuits and winter coats at the current time
		//
		int ssDemand = sampleDemand(time, SWIMSUIT_PEAK_TIME);
		int wcDemand = sampleDemand(time, WINTER_COAT_PEAK_TIME);

		//
		// Compute the number of swimsuits and coats left over after demand
		//
		int afterDemandSS = Math.max(0, s.numSwimsuits() - ssDemand);
		int afterDemandWC = Math.max(0, s.numWinterCoats() - wcDemand);

		//
		// Compute the unmet demand
		//
		int unmetDemandSS = Math.max(0, ssDemand - s.numSwimsuits());
		int unmetDemandWC = Math.max(0, wcDemand - s.numWinterCoats());
		double unmetDemandCost = unmetDemandCost(unmetDemandSS, unmetDemandWC);

		//
		// Determine the number ordered based on the specified action
		//
		int numSSOrdered = 0;
		int numWCOrdered = 0;
		switch (action) {
		case NO_ORDER_ACTION: // 0
			numSSOrdered = 0;
			numWCOrdered = 0;
			break;
		case ORDER_10_SS_ACTION:
			numSSOrdered = 10;
			numWCOrdered = 0;
			break;
		case ORDER_10_WC_ACTION:
			numSSOrdered = 0;
			numWCOrdered = 10;
			break;
		case ORDER_10_OF_EACH_ACTION:
			numSSOrdered = 10;
			numWCOrdered = 10;
			break;
		case ORDER_50_SS_ACTION:
			numSSOrdered = 50;
			numWCOrdered = 0;
			break;
		case ORDER_50_WC_ACTION:
			numSSOrdered = 0;
			numWCOrdered = 50;
			break;
		case ORDER_50_OF_EACH_ACTION: // 6
			numSSOrdered = 50;
			numWCOrdered = 50;
		case ORDER_100_SS_ACTION: // 7
			numSSOrdered = 100;
			numWCOrdered = 0;
			break;
		case ORDER_100_WC_ACTION:
			numSSOrdered = 0;
			numWCOrdered = 100;
			break;
		case ORDER_100_OF_EACH_ACTION:
			numSSOrdered = 100;
			numWCOrdered = 100;
		default:
		}

		//
		// Compute the order cost
		//
		double orderCost = orderCost(numSSOrdered, numWCOrdered);

		//
		// Compute the number of swimsuits and winter coats that need to be
		// stored
		//
		int newNumSS = afterDemandSS + numSSOrdered;
		int newNumWC = afterDemandWC + numWCOrdered;
		if (newNumSS + newNumWC > MAX_INVENTORY) {
			if (afterDemandSS + afterDemandWC + numWCOrdered > MAX_INVENTORY) {
				newNumSS = afterDemandSS;
				newNumWC = MAX_INVENTORY - afterDemandSS;
			} else {
				newNumSS = MAX_INVENTORY - newNumWC;
			}
		}
		//
		// Compute the storage cost
		//
		double storageCost = storageCost(newNumSS, newNumWC);

		//
		// Update the time
		//
		time++;
		if (time >= LOOP_TIME) {
			time = 0;
		}

		//
		// Compute the total cost
		//
		double cost = unmetDemandCost + orderCost + storageCost;

		//
		// Build the new state
		//
		int[] newState = { time, newNumSS, newNumWC };

		OptionOutcome<int[], int[]> outcome = new OptionOutcome<int[], int[]>(
				newState, observe(action, newState), cost, cost, 1);
		return outcome;
	}

	private double orderCost(int numSS, int numWC) {
		if (numSS > 0 || numWC > 0) {
			return BASE_ORDER_COST + SWIMSUIT_COST * numSS + WINTER_COAT_COST
					* numWC;
		} else {
			return 0;
		}
	}

	private double storageCost(int numSS, int numWC) {
		return INVENTORY_COST * (numSS + numWC);
	}

	private double unmetDemandCost(int numSS, int numWC) {
		if (numSS + numWC > 0) {
			return UNMET_DEMAND_COST_BASE + UNMET_DEMAND_COST_SCALE
					* (numSS + numWC);
		} else {
			return 0;
		}
	}
	
	public static double expectedDemand(int time, int initTime)
	{
		double x = ((time + initTime) / (double) LOOP_TIME) * 2 * Math.PI;
		double y = ((Math.cos(x) + 1) / 2) * DEMAND_INTERVAL.getDiff()
				+ DEMAND_INTERVAL.getMin();
		return y;
	}

	public static int sampleDemand(int time, int initTime) {
		Integer sample = 0;
		double y = expectedDemand(time, initTime);

		double std = DEMAND_STD;

		double z = DEMAND_INTERVAL.clip(y + Random.normal(0, std));

		sample = new Integer((int) z);
		return sample;
	}

	public State validateState(int[] state) {
		if (state.length != STATE_LENGTH) {
			throw new IllegalArgumentException(
					"Invalid state detected! State dimension must be "
							+ STATE_LENGTH + ".");
		}
		int time = (int) state[0];

		if (time < 0 || time >= LOOP_TIME) {
			throw new IllegalArgumentException(
					"Invalid state detected! Time must be in [0, " + LOOP_TIME
							+ "].");
		}

		int numSS = (int) state[1];
		int numWC = (int) state[2];

		if (numSS + numWC > MAX_INVENTORY) {
			throw new IllegalArgumentException(
					"Invalid state detected! Number of items exceeds maximum inventory levels.");
		}

		return new State(time, numSS, numWC);
	}

	private static class State {
		private int _time;
		private int _numSS;
		private int _numWC;

		public State(int time, int numSS, int numWC) {
			_time = time;
			_numSS = numSS;
			_numWC = numWC;
		}

		public int time() {
			return _time;
		}

		public int numSwimsuits() {
			return _numSS;
		}

		public int numWinterCoats() {
			return _numWC;
		}
	}

	/**
	 * Samples a state (approximately) uniformly from the state space.
	 * 
	 * @return a state
	 */
	public static final int[] sampleState() {
		int time = Random.nextInt(LOOP_TIME);
		int numSS = 0;
		int numWC = 0;

		boolean ssFirst = Random.nextBoolean();
		if (ssFirst) {
			numSS = Random.nextInt(MAX_INVENTORY);
			numWC = Random.nextInt(MAX_INVENTORY - numSS);
		} else {
			numWC = Random.nextInt(MAX_INVENTORY);
			numSS = Random.nextInt(MAX_INVENTORY - numWC);
		}
		return new int[] { time, numSS, numWC };
	}

	/**
	 * Samples a state such that inventory levels where zero widgets are held
	 * for one of the two products are given a boosted probability. The
	 * intuition here is that these edge states are where the cost function
	 * contains the most nonlinearity. Thus, sampling these "edge" states more
	 * frequently should be more informative for planning than sampling
	 * uniformly over the state space.
	 * 
	 * @return a state
	 */
	public static final int[] sampleStateBiasTowardEdges() {
		int time = Random.nextInt(LOOP_TIME);
		int numSS = 0;
		int numWC = 0;

		double r = Random.uniform();
		// if (r < 0.05){
		// numSS = 0;
		// numWC = 0;
		// }else
		if (r < 0.2) {
			boolean ssSmall = Random.nextBoolean();
			if (ssSmall) {
				numSS = Random.nextInt(MAX_INVENTORY / 10);
				numWC = Random.nextInt(MAX_INVENTORY - numSS);
			} else {
				numWC = Random.nextInt(MAX_INVENTORY / 10);
				numSS = Random.nextInt(MAX_INVENTORY - numWC);
			}
		} else if (r < 0.5) {
			numSS = Random.nextInt(MAX_INVENTORY / 4);
			numWC = Random.nextInt(MAX_INVENTORY / 4);
		} else {
			boolean ssFirst = Random.nextBoolean();
			if (ssFirst) {
				numSS = Random.nextInt(MAX_INVENTORY);
				numWC = Random.nextInt(MAX_INVENTORY - numSS);
			} else {
				numWC = Random.nextInt(MAX_INVENTORY);
				numSS = Random.nextInt(MAX_INVENTORY - numWC);
			}
		}
		return new int[] { time, numSS, numWC };
	}

	public static final Set<Integer> validActions() {
		Set<Integer> actions = new HashSet<Integer>();
		for (int a = 0; a < NUM_ACTIONS; a++) {
			actions.add(a);
		}
		return actions;
	}
	
	public static class BuyAtThresholdOption implements Option<int[],Integer>{
		
		public BuyAtThresholdOption()
		{
		}

		@Override
		public Integer policy(int[] state) {
			int time = state[0];
			int numSS = state[1];
			int numWC = state[2];
			
			double demandSS = expectedDemand(time, SWIMSUIT_PEAK_TIME);
			double demandWC = expectedDemand(time, WINTER_COAT_PEAK_TIME);
			
			int action = NO_ORDER_ACTION;
			if(numSS <= demandSS){
				action = ORDER_100_SS_ACTION;
			}
			if(numWC <= demandWC){
				if(action == ORDER_100_SS_ACTION){
					action = ORDER_100_OF_EACH_ACTION;
				}else{
					action = ORDER_100_WC_ACTION;
				}
			}
			return action;
		}


		@Override
		public double terminationProb(int[] state, int duration) {
			return 0.1;
		}

		@Override
		public boolean inInitialSet(int[] state) {
			return true;
		}
	}
	
	public static class WaitUntilThresholdsOption implements Option<int[], Integer>
	{
		private int _thresholdSS;
		private int _thresholdWC;
		
		public WaitUntilThresholdsOption(int thresholdSS, int thresholdWC)
		{
			_thresholdSS = thresholdSS;
			_thresholdWC = thresholdWC;
		}

		@Override
		public Integer policy(int[] state) {
			return NO_ORDER_ACTION;
		}



		@Override
		public double terminationProb(int[] state, int duration) {
			if(state[1] <= _thresholdSS || state[2] <= _thresholdWC){
				return 1;
			}else{
				return 0;
			}
		}

		@Override
		public boolean inInitialSet(int[] state) {
			return (state[1] > _thresholdSS && state[2] > _thresholdWC);
		}
	}
	
	public static class WaitUntilThresholdOption implements Option<int[], Integer>
	{
		private int _threshold;
		private int _index;
		
		public WaitUntilThresholdOption(int index, int threshold)
		{
			_index = index;
			_threshold = threshold;
		}

		@Override
		public Integer policy(int[] state) {
			return NO_ORDER_ACTION;
		}



		@Override
		public double terminationProb(int[] state, int duration) {
			if(state[_index] <= _threshold){
				return 1;
			}else{
				return 0;
			}
		}

		@Override
		public boolean inInitialSet(int[] state) {
			return (state[_index] > _threshold);
		}
	}

	/**
	 * This option skips to a specific time in the season cycle.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class SkipOption implements Option<int[], Integer> {
		private int _stopTime;

		public SkipOption(int stopTime) {
			_stopTime = stopTime;
		}

		@Override
		public Integer policy(int[] state) {
			return NO_ORDER_ACTION; // Order nothing
		}


		@Override
		public double terminationProb(int[] state, int duration) {
			if (state[0] == _stopTime) {
				return 1;
			} else {
				return 0;
			}
		}

		@Override
		public boolean inInitialSet(int[] state) {
			if (state[0] != _stopTime) {
				return true;
			} else {
				return false;
			}
		}

	}

}
