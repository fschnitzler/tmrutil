package tmrutil.learning.rl.sim.imgmt;

import java.util.Map;

/**
 * Describes a set of constraints placed on storage of {@link WidgetType}s.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface HoldingConstraints {
	/**
	 * Returns true if the specified inventory can be stored and returns
	 * false if the inventory cannot be stored.
	 * 
	 * @param inventory
	 *            a hypothetical inventory
	 * @return true if the inventory can be stored; otherwise false
	 */
	public boolean isAdmissible(Map<WidgetType, Integer> inventory);

	/**
	 * If the given inventory is admissible, then that inventory is returned
	 * unchanged. However, if the given inventory is inadmissible, then this
	 * method returns a nearby inventory that is admissible.
	 * 
	 * @param proposedInventory
	 *            a proposed inventory
	 * @return an admissible inventory that is "nearby"
	 *         <code>inventory</code>
	 */
	public Map<WidgetType, Integer> nearestAdmissibleInventory(
			Map<WidgetType, Integer> proposedInventory);

	/**
	 * The maximum allowable holdings of a specific type of widget
	 * independent of any other widget types.
	 * 
	 * @param wtype
	 *            a widget type
	 * @return the maximum amount of <code>wtype</code> under any possible
	 *         inventory
	 */
	public int maxHoldings(WidgetType wtype);
	
	/**
	 * Returns a random admissible inventory.
	 * @return an admissible inventory
	 */
	public Map<WidgetType,Integer> sampleAdmissibleInventory();
}