package tmrutil.learning.rl.sim.imgmt;

import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.math.Function;
import tmrutil.util.Interval;

/**
 * A cost function maps inventory to its associated immediate costs.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface CostFunction extends
		Function<Map<WidgetType, Integer>, Integer> {
	
	/**
	 * Returns the set of widget types considered by this cost function.
	 * @return a set of widget types
	 */
	public List<WidgetType> wtypes();

	/**
	 * Returns an interval containing the minimum and maximum possible
	 * values of this cost function.
	 * 
	 * @return an interval containing the range of this cost function
	 */
	public Interval range();
}