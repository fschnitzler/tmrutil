package tmrutil.learning.rl.sim;

import java.util.Arrays;

import tmrutil.learning.rl.sim.ZombieSimulator.NetworkState;
import tmrutil.learning.rl.sim.ZombieSimulator.CrashVector;
import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.stats.Random;

/**
 * Represents the problem of maintaining N computer systems in the presence of a
 * computer virus, called the 'zombie' computer virus. In this version of the
 * problem, the state is a boolean array specifying true for computers that are
 * infected and false for computers that are not infected. The observation is a
 * boolean array specifying true for computers that crashed during the last
 * timestep and false for computers that did not crash during the last timestep.
 * Computers infected with the 'zombie' virus are half as likely to crash as
 * those that are infected. The agent has the ability to run diagnostics on
 * computer systems, which have a high probability of detecting the virus on
 * computers that are infected and fixing the computer system. However, running
 * diagnostics is costly, so the agent should try to limit how often it runs
 * diagnostics. The total cost per timestep is a linear combination of the
 * number of computer systems that crashed and the cost of running diagnostics.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ZombieSimulator extends
		Simulator<NetworkState, CrashVector, boolean[]> {

	public static final double INHERENT_INFECTION_PROB = 0.01;
	public static final double INFECTION_PROB_BY_NEIGHBORS = 0.1;

	public static final double NORMAL_CRASH_PROB = 0.05;
	public static final double INFECTED_CRASH_PROB = 3 * NORMAL_CRASH_PROB;

	public static final double REPAIR_SUCCESS_PROB = 0.9;

	public static final double COST_PER_CRASH = 1.0;
	public static final double COST_PER_DIAGNOSTIC = 1.0;

	/**
	 * A network state instance determines the computers in the network that are
	 * infected with the zombie virus.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class NetworkState {
		private boolean[] _infected;

		public NetworkState(boolean[] infected) {
			_infected = Arrays.copyOf(infected, infected.length);
		}

		public boolean isInfected(int index) {
			return _infected[index];
		}

		private boolean[] infectionArray() {
			return _infected;
		}

		public int numberOfComputers() {
			return _infected.length;
		}
	}

	/**
	 * A crash vector instance determines the number of computers in the network
	 * that crashed during the last timestep.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static class CrashVector {
		private boolean[] _crashed;

		public CrashVector(boolean[] crashed) {
			_crashed = Arrays.copyOf(crashed, crashed.length);
		}

		public boolean isCrashed(int index) {
			return _crashed[index];
		}

		public int numberOfComputers() {
			return _crashed.length;
		}
	}

	public ZombieSimulator() {
	}

	@Override
	public OptionOutcome<NetworkState, CrashVector> samplePrimitive(
			NetworkState state, boolean[] action) {
		if (state.numberOfComputers() != action.length) {
			throw new IllegalArgumentException(
					"Invalid action. State size and action size do not match.");
		}

		boolean[] crashVector = crashVector(state.infectionArray());
		boolean[] afterD = afterDiagnostics(state.infectionArray(), action);

		boolean[] nextState = new boolean[state.numberOfComputers()];
		for (int i = 0; i < state.numberOfComputers(); i++) {
			nextState[i] = infected(afterD, i);
		}

		double crashCost = count(crashVector, true) * COST_PER_CRASH;
		double diagCost = count(action, true) * COST_PER_DIAGNOSTIC;
		double cost = crashCost + diagCost;

		return new OptionOutcome<NetworkState, CrashVector>(new NetworkState(
				nextState), new CrashVector(crashVector), cost, cost, 1);
	}

	public int count(boolean[] b, boolean v) {
		int sum = 0;
		for (int i = 0; i < b.length; i++) {
			if (b[i] == v) {
				sum++;
			}
		}
		return sum;
	}

	public boolean[] afterDiagnostics(boolean[] state, boolean[] action) {
		boolean[] fixed = new boolean[state.length];
		for (int i = 0; i < state.length; i++) {
			double r = Random.uniform();
			if (action[i] && r < REPAIR_SUCCESS_PROB) {
				fixed[i] = true;
			} else {
				fixed[i] = false;
			}
		}
		return fixed;
	}

	/**
	 * Generates a boolean vector indicating which computer systems crashed.
	 * 
	 * @param state
	 *            the state of the system
	 * @return boolean vector indicating which computer systems crashed during
	 *         the latest timestep
	 */
	public boolean[] crashVector(boolean[] state) {
		boolean[] crashed = new boolean[state.length];
		for (int i = 0; i < state.length; i++) {
			crashed[i] = false;
			double r = Random.uniform();
			if (state[i]) {
				if (r < INFECTED_CRASH_PROB) {
					crashed[i] = true;
				}
			} else {
				if (r < NORMAL_CRASH_PROB) {
					crashed[i] = true;
				}
			}
		}
		return crashed;
	}

	/**
	 * Helper function that stochastically determines whether or not a computer
	 * system, represented by the index <code>i</code> will be infected by the
	 * virus at the next timestep.
	 * 
	 * @param state
	 *            an array specifying whether or not computer systems are
	 *            infected.
	 * @param i
	 *            an index into the observation denoting the computer system
	 * @return true if the ith computer system will be infected during the next
	 *         timestep; otherwise false
	 */
	public boolean infected(boolean[] state, int i) {
		if (state[i]) {
			return true;
		} else {
			double r = Random.uniform();
			int n = numberOfInfectedNeighbors(state, i);
			if (n == 0) {
				if (r < INHERENT_INFECTION_PROB) {
					return true;
				}
			} else {
				if (r < n * INFECTION_PROB_BY_NEIGHBORS) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Determines the number of computer systems neighboring <code>i</code> that
	 * have been infected.
	 * 
	 * @param state
	 *            an array specifying whether or not computer systems are
	 *            infected.
	 * @param i
	 *            an index into the observation denoting a computer system
	 * @return the number of infected neighbors of <code>i</code>
	 */
	public int numberOfInfectedNeighbors(boolean[] state, int i) {
		int n = 0;
		try {
			if (state[i - 1]) {
				n = n + 1;
			}
		} catch (IndexOutOfBoundsException ex) {
		}
		try {
			if (state[i + 1]) {
				n = n + 1;
			}
		} catch (IndexOutOfBoundsException ex) {
		}
		return n;
	}

	@Override
	public CrashVector observe(boolean[] previousAction,
			NetworkState resultState) {
		return new CrashVector(crashVector(resultState.infectionArray()));
	}
}
