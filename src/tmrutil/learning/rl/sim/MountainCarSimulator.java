package tmrutil.learning.rl.sim;

import tmrutil.learning.rl.smdp.OptionOutcome;
import tmrutil.learning.rl.tasks.MountainCarTask;

/**
 * A simulator for the Mountain Car problem.
 * @author Timothy A. Mann
 *
 */
public class MountainCarSimulator extends Simulator<double[], double[], Integer> {

	private MountainCarTask _mcTask;
	
	public MountainCarSimulator()
	{
		_mcTask = new MountainCarTask(true);
	}
	
	@Override
	public OptionOutcome<double[],double[]> samplePrimitive(double[] state,
			Integer action) {
		_mcTask.setState(state[0], state[1]);
		_mcTask.execute(action);
		double[] newState = _mcTask.getState();
		double reward = _mcTask.evaluate();
		
		return new OptionOutcome<double[],double[]>(newState, newState, reward, reward, 1);
	}

	@Override
	public double[] observe(Integer previousAction, double[] resultState) {
		return resultState;
	}

}
