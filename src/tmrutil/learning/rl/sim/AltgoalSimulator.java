package tmrutil.learning.rl.sim;

import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.smdp.OptionOutcome;

/**
 * A wrapper class that allows specifying an alternate reinforcement signal.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <O> the observation type
 * @param <A> the primitive action type
 */
public class AltgoalSimulator<S, O, A> extends Simulator<S, O, A> {
	
	private Simulator<S,O,A> _sim;
	private ReinforcementSignal<S,A> _rsignal;
	
	public AltgoalSimulator(Simulator<S,O,A> sim, ReinforcementSignal<S,A> alternateRSignal)
	{
		_sim = sim;
		_rsignal = alternateRSignal;
	}

	@Override
	public O observe(A previousAction, S resultState) {
		return _sim.observe(previousAction, resultState);
	}

	@Override
	public OptionOutcome<S, O> samplePrimitive(S state, A action) {
		OptionOutcome<S,O> outcome = _sim.samplePrimitive(state, action);
		
		S tstate = outcome.terminalState();
		O obs = outcome.terminalObservation();
		int lt = outcome.duration();
		double r = _rsignal.evaluate(state, action);
		
		OptionOutcome<S,O> altOutcome = new OptionOutcome<S,O>(tstate, obs, r, r, lt);
		return altOutcome;
	}

}
