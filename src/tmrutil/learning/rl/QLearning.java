/*
 * QLearning.java
 */

/* Package */
package tmrutil.learning.rl;

/* Imports */
import java.util.List;

import tmrutil.learning.LearningRate;
import tmrutil.math.MatrixOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.util.DataStore;
import tmrutil.util.Interval;

/**
 * Implements the Q-Learning algorithm with an epsilon-greedy exploration strategy.
 */
public class QLearning extends AbstractDiscreteTemporalDifferenceLearning implements Explorer<Integer,Integer>
{
	/** The probability of taking a random action. */
	private double _epsilon;

	/**
	 * Constructs a Q-learner with a Q-table initialized to all zeros.
	 * 
	 * @param states
	 *            the number of states
	 * @param actions
	 *            the number of actions
	 * @param alpha
	 *            the learning rate
	 * @param gamma
	 *            the discount factor
	 */
	public QLearning(int states, int actions, double alpha, double gamma)
	{
		super(states, actions, new LearningRate.Constant(alpha), gamma,
				Optimization.MAXIMIZE);
		_epsilon = 0;
	}

	/**
	 * Constructs a Q-learner with state-actions pairs initialized to a
	 * specified value.
	 * 
	 * @param states
	 *            the number of states
	 * @param actions
	 *            the number of actions
	 * @param alpha
	 *            the learning rate
	 * @param gamma
	 *            the discount factor
	 * @param initValue
	 *            the value to initialize each element of the Q-table to
	 */
	public QLearning(int states, int actions, double alpha, double gamma,
			double initValue)
	{
		this(states, actions, alpha, gamma);
		fillQ(initValue);
	}

	/**
	 * Constructs a Q-learner with state-action pairs randomly initialized
	 * within the interval (vmin, vmax) where vmin and vmax are the minimum and
	 * maximum possible long-term values.
	 * 
	 * @param states
	 *            the number of states
	 * @param actions
	 *            the number of actions
	 * @param alpha
	 *            the learning rate
	 * @param gamma
	 *            the discount factor
	 * @param rewardInterval
	 *            the reward interval used to determine the minimum and maximum
	 *            long-term values (vmin, vmax)
	 */
	public QLearning(int states, int actions, double alpha, double gamma,
			Interval rewardInterval)
	{
		this(states, actions, alpha, gamma);
		double vmax = rewardInterval.getMax() / (1 - gamma);
		double vmin = rewardInterval.getMin() / (1 - gamma);
		for (int s = 0; s < states; s++) {
			for (int a = 0; a < actions; a++) {
				this.setQ(s, a, Random.uniform(vmin, vmax));
			}
		}
	}

	/**
	 * Constructs a Q-learner from a deep copy of a specified Q-table (
	 * <code>q</code>). The Q-table must have a row for each state and each row
	 * must have a column for each action.
	 * 
	 * @param q
	 *            a Q-table (# rows = # states, # cols = # actions)
	 * @param alpha
	 *            the learning rate
	 * @param gamma
	 *            the discount factor
	 */
	public QLearning(double[][] q, double alpha, double gamma)
	{
		super(MatrixOps.getNumRows(q), MatrixOps.getNumCols(q),
				new LearningRate.Constant(alpha), gamma, Optimization.MAXIMIZE);
		this.setQ(q);
		_epsilon = 0;
	}

	/**
	 * Sets the probability of making an random action.
	 * 
	 * @param epsilon
	 *            probability of taking a random action
	 */
	public void setEpsilon(double epsilon)
	{
		if (epsilon < 0 || epsilon > 1) {
			throw new IllegalArgumentException(
					"Epsilon must be in the interval [0, 1].");
		}

		_epsilon = epsilon;
	}

	/**
	 * Returns the probability of making a random action.
	 * 
	 * @return the probability of making a random action
	 */
	public double getEpsilon()
	{
		return _epsilon;
	}

	/**
	 * Update the Q matrix based on a single example.
	 * 
	 * @param prevState
	 *            the previous state
	 * @param newState
	 *            the new state
	 * @param action
	 *            the action taken to move from <code>prevState</code> to
	 *            <code>newState</code>
	 * @param reward
	 *            the reward received by moving from <code>prevState</code> to
	 *            <code>newState</code>
	 * @return the training error
	 */
	@Override
	public void trainImpl(Integer prevState, Integer action,
			Integer newState, double reward)
	{
		double qsa = evaluateQ(prevState, action);
		double qs1a = maxQ(newState);
		setQ(prevState, action, ((1.0 - getLearningRate()) * qsa)
				+ getLearningRate() * (reward + (getDiscountFactor() * qs1a)));
	}

	/**
	 * Update the Q matrix based on many examples.
	 * 
	 * @return a list of training errors
	 */
	public void train(int[] prevState, int[] action, int[] newState,
			double[] reward)
	{
		for (int i = 0; i < prevState.length; i++) {
			train(prevState[i], newState[i], action[i], reward[i]);
		}
	}

	/**
	 * Update the Q matrix based on many examples.
	 * 
	 * @return a list of training errors
	 */
	public void train(List<Integer> prevState, List<Integer> action,
			List<Integer> newState, List<Double> reward)
	{
		for (int i = 0; i < prevState.size(); i++) {
			train(prevState.get(i), newState.get(i), action.get(i),
					reward.get(i));
		}
	}

	/**
	 * Get the most probable action based on the current state.
	 */
	@Override
	public Integer policy(Integer state)
	{
		if (_epsilon > Random.uniform()) {
			return Random.nextInt(numberOfActions());
		} else {
			return super.policy(state);
		}
	}

	@Override
	public void resetImpl()
	{
		for (int s = 0; s < numberOfStates(); s++) {
			for (int a = 0; a < numberOfActions(); a++) {
				setQ(s, a, Random.uniform(0, 1));
			}
		}
	}

	@Override
	public boolean isExploration(Integer state, Integer action)
	{
		Integer greedyAction = super.policy(state);
		double greedyVal = evaluateQ(state, greedyAction);
		double val = evaluateQ(state, action);
		return val < greedyVal;
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<Integer> greedyValueFunction() {
		return new QFunction.GreedyVFunction<Integer>(this);
	}

	@Override
	public double value(Integer state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(Integer state) {
		return maxQ(state);
	}
}