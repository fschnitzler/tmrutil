package tmrutil.learning.rl;

import java.util.ArrayList;
import java.util.Collection;
import tmrutil.learning.TrainingException;
import tmrutil.stats.Statistics;

/**
 * Implements the value iteration algorithm, which is a dynamic programming (DP)
 * algorithm for approximating the optimal value function. This algorithm can
 * only be used when a complete model of the problem (i.e. state transition
 * probabilities and reinforcement signal) is available.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ValueIteration
{
	/** A reference to the discrete Markov decision process. */
	protected DiscreteMarkovDecisionProcess<Integer,Integer> _mdp;

	/** The sensitivity to convergence. */
	private double _theta;

	/**
	 * A full set of states.
	 */
	private Collection<Integer> _fullStates;

	/**
	 * Constructs an instance of the value iteration algorithm.
	 * 
	 * @param mdp
	 *            a Markov decision process model
	 * @param convergenceSensitivity
	 *            the convergence sensitivity to use
	 */
	public ValueIteration(DiscreteMarkovDecisionProcess<Integer,Integer> mdp,
			double convergenceSensitivity)
	{
		_mdp = mdp;
		setConvergenceSensitivity(convergenceSensitivity);

		_fullStates = new ArrayList<Integer>();
		for (int s = 0; s < _mdp.numberOfStates(); s++) {
			_fullStates.add(s);
		}
	}

	/**
	 * Run value iteration (on all states) until the value function estimate
	 * converges. There is no maximum number of iterations. If the convergence
	 * sensitivity is too small, then this function may run forever.
	 * 
	 * @param Vbuff
	 *            an array to be used for estimating the value function (the
	 *            initial array elements are used as initial estimates for the
	 *            value function entries)
	 * @return a reference to <code>Vbuff</code>
	 */
	public double[] valueIteration(double[] Vbuff)
	{

		return valueIteration(-1, Vbuff);
	}
	
	/**
	 * Run action-value iteration (on all states) until the action-value function estimate
	 * converges. There is no maximum number of iterations. If the convergence
	 * sensitivity is too small, then this function may run forever.
	 * 
	 * @param Vbuff
	 *            an array to be used for estimating the value function (the
	 *            initial array elements are used as initial estimates for the
	 *            value function entries)
	 * @param Qbuff
	 *            a table to be used for estimating the action-value function
	 *            (the initial array elements are used as initial estimates for
	 *            the action-value function entries)
	 * @return a reference to <code>Qbuff</code>
	 */
	public double[][] actionValueIteration(double[] Vbuff, double[][] Qbuff)
	{
		return actionValueIteration(-1, Vbuff, Qbuff);
	}

	/**
	 * Run value iteration (on all states) until the value function estimate
	 * either converges or the maximum number of iterations is reached.
	 * 
	 * @param maxIterations
	 *            the maximum number of iterations
	 * @param Vbuff
	 *            an array to be used for estimating the value function (the
	 *            initial array elements are used as initial estimates for the
	 *            value function entries)
	 * @return a reference to <code>Vbuff</code>
	 */
	public double[] valueIteration(int maxIterations, double[] Vbuff)
	{
		return valueIteration(maxIterations, Vbuff, _fullStates);
	}
	
	/**
	 * Run action-value iteration (on all states) until the action-value function estimate
	 * either converges or the maximum number of iterations is reached.
	 * 
	 * @param maxIterations
	 *            the maximum number of iterations
	 * @param Vbuff
	 *            an array to be used for estimating the value function (the
	 *            initial array elements are used as initial estimates for the
	 *            value function entries)
	 * @param Qbuff
	 *            a table to be used for estimating the action-value function
	 *            (the initial array elements are used as initial estimates for
	 *            the action-value function entries)
	 * @return a reference to <code>Qbuff</code>
	 */
	public double[][] actionValueIteration(int maxIterations, double[] Vbuff, double[][] Qbuff)
	{
		return actionValueIteration(maxIterations, Vbuff, Qbuff, _fullStates);
	}

	/**
	 * Run value iteration until either the value function estimate converges or
	 * the maximum number of iterations is reached.
	 * 
	 * @param maxIterations
	 *            the maximum number of iterations
	 * @param Vbuff
	 *            an array to be used for estimating the value function (the
	 *            initial array elements are used as initial estimates for the
	 *            value function entries)
	 * @param evelope
	 *            the collection of states to consider while running value
	 *            iteration
	 * @return a reference to <code>Vbuff</code>
	 */
	public double[] valueIteration(int maxIterations, double[] Vbuff,
			Collection<Integer> envelope)
	{
		// Loop up to the maximum number of iterations or no limit if
		// maxIterations is negative
		for (int i = 0; i < maxIterations || (maxIterations < 0); i++) {
			double delta = 0;

			for (Integer state : envelope) {
				// Backup the old value function estimate
				double oldV = Vbuff[state];
				// Create a variable to hold the new value function estimate
				double newV = valueUpdate(state, Vbuff);

				if (Double.isNaN(newV)) {
					throw new TrainingException(TrainingException.NAN_DETECTED);
				}
				if (Double.isInfinite(newV)) {
					throw new TrainingException(
							TrainingException.INFINITY_DETECTED);
				}

				// Set the new value function estimate for this state
				Vbuff[state] = newV;

				// Update the largest change to the value function estimate
				delta = Math.max(delta, Math.abs(oldV - newV));
			}

			if (delta < _theta) {
				break;
			}
		}
		return Vbuff;
	}

	/**
	 * Run action-value iteration until either the action-value function
	 * estimate converges or the maximum number of iterations is reached.
	 * 
	 * @param maxIterations
	 *            the maximum number of iterations
	 * @param Vbuff
	 *            an array to be used for estimating the value function (the
	 *            initial array elements are used as initial estimates for the
	 *            value function entries)
	 * @param Qbuff
	 *            a table to be used for estimating the action-value function
	 *            (the initial array elements are used as initial estimates for
	 *            the action-value function entries)
	 * @param evelope
	 *            the collection of states to consider while running value
	 *            iteration
	 * @return a reference to <code>Qbuff</code>
	 */
	public double[][] actionValueIteration(int maxIterations, double[] Vbuff,
			double[][] Qbuff, Collection<Integer> envelope)
	{
		// Loop up to the maximum number of iterations or no limit if
		// maxIterations is negative
		for (int i = 0; i < maxIterations || (maxIterations < 0); i++) {
			double delta = 0;

			for (Integer state : envelope) {
				for (int action = 0; action < numberOfActions(); action++) {
					// Backup the old value function estimate
					double oldQ = Qbuff[state][action];
					// Create a variable to hold the new value function estimate
					double newQ = actionValueUpdate(state, action, Vbuff);

					if (Double.isNaN(newQ)) {
						throw new TrainingException(
								TrainingException.NAN_DETECTED);
					}
					if (Double.isInfinite(newQ)) {
						throw new TrainingException(
								TrainingException.INFINITY_DETECTED);
					}

					// Set the new action-value function estimate for this state-action pair
					Qbuff[state][action] = newQ;


					// Update the largest change to the action-value function estimate
					delta = Math.max(delta, Math.abs(oldQ - newQ));
				}
				// Set the new value function estimate for this state
				Vbuff[state] = Statistics.max(Qbuff[state]);
			}

			if (delta < _theta) {
				break;
			}
		}
		return Qbuff;
	}

	/**
	 * Performs a value update for a specific state.
	 * 
	 * @param s
	 *            a state
	 * @param Vbuff
	 *            the value function array buffer
	 * @return the updated value for state <code>s</code>
	 */
	protected double valueUpdate(int s, double[] Vbuff)
	{
		double bestVal = 0;
		boolean bestValSet = false;
		int nActions = _mdp.numberOfActions();

		for (int a = 0; a < nActions; a++) {
			double val = actionValueUpdate(s, a, Vbuff);
			if (val > bestVal || !bestValSet) {
				bestVal = val;
				bestValSet = true;
			}
		}
		return bestVal;
	}

	/**
	 * Performs an action-value update for a state-action pair.
	 * 
	 * @param s
	 *            a state
	 * @param a
	 *            an action
	 * @param Vbuff
	 *            the value function array buffer
	 * @return the updated action-value for state <code>s</code>
	 */
	protected double actionValueUpdate(int s, int a, double[] Vbuff)
	{
		double gamma = _mdp.getDiscountFactor();
		Collection<Integer> succStates = _mdp.successors(s);
		double qval = 0;
		for (Integer s2 : succStates) {
			double tprob = _mdp.transitionProb(s, a, s2);
			//double r = (1-gamma) * _mdp.reinforcement(s, a);
			double r = _mdp.reinforcement(s, a);
			double vs = Vbuff[s2];
			qval += tprob * (r + gamma * vs);
		}
		return qval;
	}

	/**
	 * Returns the number of states.
	 * 
	 * @return the number of states
	 */
	public int numberOfStates()
	{
		return _mdp.numberOfStates();
	}

	/**
	 * Returns the number of actions.
	 * 
	 * @return the number of actions
	 */
	public int numberOfActions()
	{
		return _mdp.numberOfActions();
	}

	/**
	 * Returns the discount factor associated with the modeled Markov decision
	 * process.
	 * 
	 * @return the discount factor
	 */
	public double getDiscountFactor()
	{
		return _mdp.getDiscountFactor();
	}

	public double getConvergenceSensitivity()
	{
		return _theta;
	}

	public void setConvergenceSensitivity(double theta)
	{
		if (theta < 0) {
			throw new IllegalArgumentException(
					"Convergence sensitivity must be nonnegative.");
		}
		_theta = theta;
	}
}
