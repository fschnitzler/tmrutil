package tmrutil.learning.rl;

import java.util.HashMap;
import java.util.Map;

import tmrutil.optim.Optimization;

/**
 * An implementation of action-value functions for finite-state, finite-action MDPs.
 * @author Timothy A. Mann
 *
 * @param <S> the state type
 * @param <A> the action type
 */
public class DiscreteQFunction<S, A> implements QFunction<S, A> {

	private Map<S, Map<A, Double>> _qvalues;
	private Map<S, Double> _values;
	private Optimization _opType;
	private double _defaultValue;

	public DiscreteQFunction(Optimization opType) {
		this(opType, 0);
	}

	public DiscreteQFunction(Optimization opType, double defaultValue) {
		_opType = opType;
		_defaultValue = defaultValue;

		_qvalues = new HashMap<S, Map<A, Double>>();
		_values = new HashMap<S, Double>();
	}

	@Override
	public double greedyValue(S state) {
		Double val = _values.get(state);
		if (val == null) {
			return _defaultValue;
		} else {
			return val.doubleValue();
		}
	}

	@Override
	public double value(S state, A action) {
		Map<A, Double> qvals = _qvalues.get(state);
		if (qvals == null) {
			return _defaultValue;
		} else {
			Double qval = qvals.get(action);
			if (qval == null) {
				return _defaultValue;
			} else {
				return qval.doubleValue();
			}
		}
	}

	public void update(S state, A action, double qval) {
		Map<A, Double> qvals = _qvalues.get(state);
		if (qvals == null) {
			qvals = new HashMap<A, Double>();
			qvals.put(action, qval);
			_qvalues.put(state, qvals);
		} else {
			qvals.put(action, qval);
		}

		Double bestQ = null;
		for (A a : qvals.keySet()) {
			double q = qvals.get(a);
			if (bestQ == null || _opType.firstIsBetter(q, bestQ)) {
				bestQ = q;
			}
		}
		_values.put(state, bestQ);
	}

	@Override
	public boolean isMaximizing() {
		return _opType.equals(Optimization.MAXIMIZE);
	}

	@Override
	public boolean isMinimizing() {
		return !isMaximizing();
	}

	@Override
	public VFunction<S> greedyValueFunction() {
		return new tmrutil.learning.rl.QFunction.GreedyVFunction<S>(this);
	}

}
