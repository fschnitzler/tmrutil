package tmrutil.learning.rl.continuous;

import tmrutil.learning.rl.Policy;

public interface StochasticPolicy extends Policy<double[], double[]>
{
	/**
	 * Computes the derivative of this policy on action <code>action</code>
	 * conditioned on <code>conditionedOnState</code>.
	 * 
	 * @param action an action vector
	 * @param conditionedOnState the state to condition on
	 * @return the policy derivative
	 */
	public double[] derivative(double[] action, double[] conditionedOnState);

	/**
	 * Returns a deep copy of the parameters that determine this policy's
	 * behavior.
	 * 
	 * @return the parameters that determine this policy's behavior
	 */
	public double[] parameters();

	/**
	 * Sets the parameters that determine this policy's behavior. The values in
	 * the array are copied so that further changes to the <code>params</code>
	 * array do not affect the parameters of the policy.
	 * 
	 * @param params
	 *            an array of parameter values
	 */
	public void parameters(double[] params);

	/**
	 * Returns the number of parameters used by this policy.
	 * 
	 * @return the number of parameters used by this policy
	 */
	public int numberOfParameters();
}
