package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.NNet;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.util.ArrayOps;
import tmrutil.util.DataStore;
import tmrutil.util.Pair;

/**
 * Dyna is a simple model-based reinforcement learning algorithm developed by
 * Sutton (1991). The primary advantage of Dyna is that it learns the transition
 * function and reinforcement signal so that random examples can be generated to
 * learn the optimal Q-function more efficiently.
 * 
 * 1. R. Sutton,
 * "Dyna, an integrated architecture for learning, planning, and reacting",
 * 1991.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DynaLearning extends AbstractTemporalDifferenceLearning<double[], double[]>
{
	/** The number of components in a valid state description vector. */
	private int _stateSize;
	/** The number of components in a valid action vector. */
	private int _actionSize;

	/**
	 * A neural network that approximates the environmental dynamics
	 * (transitions).
	 */
	private NNet _tfunc;
	/** A neural network that approximates the reinforcement signal. */
	private NNet _rfunc;
	/** A neural network that approximates the Q-function. */
	private NNet _qfunc;

	/** The probability of taking a random action. */
	private double _epsilonGreedy;

	/** A list of valid collections. */
	private List<double[]> _validActions;

	/** The number of hypothetical training iterations after each training step. */
	private int _k;

	/**
	 * Constructs an instance of the Dyna reinforcement learning algorithm.
	 * 
	 * @param stateSize
	 *            the number of components in a valid state description vector
	 * @param actionSize
	 *            the number of components in a valid action vector
	 * @param tfunc
	 *            a neural network that will be used to model the sensory
	 *            consequences of actions (a forward model)
	 * @param rfunc
	 *            a neural network that will be used to learn the reinforcement
	 *            signal
	 * @param qfunc
	 *            a neural network that will be used to estimate the optimal
	 *            Q-function
	 * @param validActions
	 *            a collection of valid actions
	 * @param k
	 *            the number of hypothetical training iterations after each
	 *            training step
	 * @param learningRate
	 *            the learning rate to use for training the neural networks
	 * @param discountFactor
	 *            the discount factor to use for estimating the optimal
	 *            Q-function
	 * @param epsilonGreedy
	 *            the probability of selecting an action at random instead of
	 *            following the estimated policy
	 * @param opType
	 *            the type of optimizing to use (either minimize or maximize
	 *            reinforcements)
	 */
	public DynaLearning(int stateSize, int actionSize, NNet tfunc, NNet rfunc,
			NNet qfunc, Collection<double[]> validActions, int k,
			double learningRate, double discountFactor, double epsilonGreedy,
			Optimization opType)
	{
		super(learningRate, discountFactor, opType);
		
		_stateSize = stateSize;
		_actionSize = actionSize;

		_tfunc = tfunc;
		_rfunc = rfunc;
		_qfunc = qfunc;

		// Copy the collection of valid actions into a list
		_validActions = new ArrayList<double[]>(validActions);

		_k = k;

		setEpsilonGreedy(epsilonGreedy);
	}

	/**
	 * Implements the greedy policy by always selecting the best action
	 * according to the estimated Q-function
	 * 
	 * @param state
	 *            a state description vector
	 * @return the best action according to the estimated Q-function
	 */
	public double[] greedyPolicy(double[] state)
	{
		if (isMaximizing()) {
			return maxActVal(state).getA();
		} else {
			return minActVal(state).getA();
		}
	}

	@Override
	public double[] policy(double[] state)
	{
		if (Random.uniform() < getEpsilonGreedy()) {
			int index = Random.nextInt(_validActions.size());
			return _validActions.get(index);
		} else {
			return greedyPolicy(state);
		}
	}

	@Override
	protected void trainImpl(double[] prevState, double[] action, double[] newState,
			double reinforcement)
	{
		double[] input = ArrayOps.concat(prevState, action);

		_tfunc.train(input, newState, getLearningRate());
		_rfunc.train(input, new double[] { reinforcement }, getLearningRate());

		Double error = trainQ(input, newState, reinforcement);

		for (int i = 0; i < _k; i++) {
			double[] hPrevState = randomState();
			double[] hAction = policy(hPrevState);
			double[] hInput = ArrayOps.concat(hPrevState, hAction);
			double[] hNewState = _tfunc.evaluate(hInput);
			Double hReinforcement = _rfunc.evaluate(hInput)[0];
			trainQ(hInput, hNewState, hReinforcement);
		}

	}

	/**
	 * Trains the neural network estimate for the Q-function.
	 * 
	 * @param input
	 *            a state-action vector
	 * @param newState
	 *            the resulting state
	 * @param reinforcement
	 *            the resulting reinforcement value
	 * @return the error before training
	 */
	private Double trainQ(double[] input, double[] newState,
			Double reinforcement)
	{
		Pair<double[], Double> out;
		if (isMaximizing()) {
			out = maxActVal(newState);
		} else {
			out = minActVal(newState);
		}
		double[] target = { reinforcement + getDiscountFactor() * out.getB() };
		return _qfunc.train(input, target, getLearningRate())[0];
	}

	protected Pair<double[], Double> minActVal(double[] state)
	{
		double[] minAct = new double[getActionSize()];
		double minVal = 0;

		double[] input = new double[getStateSize() + getActionSize()];
		System.arraycopy(state, 0, input, 0, state.length);

		for (int i = 0; i < _validActions.size(); i++) {
			double[] action = _validActions.get(i);
			System.arraycopy(action, 0, input, state.length, action.length);
			double val = evaluateQ(input);
			if (val < minVal || i == 0) {
				minVal = val;
				System.arraycopy(action, 0, minAct, 0, action.length);
			}
		}
		return new Pair<double[], Double>(minAct, minVal);
	}

	protected Pair<double[], Double> maxActVal(double[] state)
	{
		double[] maxAct = new double[getActionSize()];
		double maxVal = 0;

		double[] input = new double[getStateSize() + getActionSize()];
		System.arraycopy(state, 0, input, 0, state.length);

		for (int i = 0; i < _validActions.size(); i++) {
			double[] action = _validActions.get(i);
			System.arraycopy(action, 0, input, state.length, action.length);
			double val = evaluateQ(input);
			if (val > maxVal || i == 0) {
				maxVal = val;
				System.arraycopy(action, 0, maxAct, 0, action.length);
			}
		}
		return new Pair<double[], Double>(maxAct, maxVal);
	}

	/**
	 * Returns the value of the estimated Q-function for a state-action pair
	 * 
	 * @param state
	 *            a state description vector
	 * @param action
	 *            an action description vector
	 * @return the value of the estimated Q-function for the state-action pair
	 */
	public double evaluateQ(double[] state, double[] action)
	{
		double[] input = ArrayOps.concat(state, action);
		return evaluateQ(input);
	}

	/**
	 * Returns the value of the estimated Q-function for a state-action vector.
	 * 
	 * @param input
	 *            a state-action vector
	 * @return the value of the estimated Q-function for the state-action pair
	 */
	private double evaluateQ(double[] input)
	{
		return _qfunc.evaluate(input)[0];
	}

	/**
	 * Returns the probability of selecting an action at random instead of
	 * following the learned policy.
	 * 
	 * @return the probability of selecting a random action
	 */
	public double getEpsilonGreedy()
	{
		return _epsilonGreedy;
	}

	/**
	 * Sets the probability of selecting an action at random instead of
	 * following the learned policy.
	 * 
	 * @param epsilonGreedy
	 *            a scalar value in interval [0, 1]
	 */
	public void setEpsilonGreedy(double epsilonGreedy)
	{
		if (epsilonGreedy < 0 || epsilonGreedy > 1) {
			throw new IllegalArgumentException(
					"The epsilon greedy parameter is a probability and therefore must be in interval [0, 1]");
		}
		_epsilonGreedy = epsilonGreedy;
	}

	/**
	 * Returns the number of components in a valid state description vector.
	 * 
	 * @return the number of components in a valid state vector
	 */
	public int getStateSize()
	{
		return _stateSize;
	}

	/**
	 * Returns the number of components in a valid action vector.
	 * 
	 * @return the number of components in a valid action vector
	 */
	public int getActionSize()
	{
		return _actionSize;
	}

	/**
	 * Selects a valid, random state.
	 * 
	 * @return a state description vector
	 */
	public double[] randomState()
	{
		double[] state = new double[getStateSize()];

		for (int i = 0; i < getStateSize(); i++) {
			state[i] = Random.uniform(-1, 1);
		}

		return state;
	}
	
	@Override
	public void resetImpl()
	{
		_tfunc.reset();
		_rfunc.reset();
		_qfunc.reset();
	}

	/**
	 * This operation is not supported.
	 */
	public double maxQ(double[] state)
	{
		throw new UnsupportedOperationException("This operation is not supported by this class.");
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<double[]> greedyValueFunction() {
		return new QFunction.GreedyVFunction<double[]>(this);
	}

	@Override
	public double value(double[] state, double[] action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(double[] state) {
		return maxQ(state);
	}
}
