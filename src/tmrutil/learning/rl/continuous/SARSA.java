package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.learning.LearningRate;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.StationaryPolicy;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.Function;
import tmrutil.optim.Optimization;
import tmrutil.util.DataStore;

/**
 * An implementation of SARSA with linear value function approximation. This
 * implementation allows the use of arbitrary state and action representations.
 * 
 * @author Timothy A. Mann
 *
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class SARSA<S, A> implements LearningSystem<S, A>, QFunction<S, A> {

	private Function<S, RealVector> _toStateVector;
	private int _stateDim;
	private ActionSelector<S, A> _actionSelector;
	private LearningRate _lr;
	private double _discountFactor;

	private List<A> _actions;
	private long _numUpdates;
	private Map<A, RealVector> _qfuncs;

	public SARSA(Function<S, RealVector> toStateVector, int stateDim,
			ActionSelector<S, A> actionSelector, LearningRate lr,
			double discountFactor) {
		if (toStateVector == null) {
			throw new NullPointerException(
					"Cannot construct SARSA instance with a null mapping from a state to vector.");
		}
		_toStateVector = toStateVector;
		if (stateDim < 1) {
			throw new IllegalArgumentException(
					"The number of state dimensions must be positive. Found : "
							+ stateDim);
		}
		_stateDim = stateDim;
		if (actionSelector == null) {
			throw new NullPointerException(
					"Cannot construct SARSA instance with a null action selection strategy.");
		}
		_actionSelector = actionSelector;
		_actions = new ArrayList<A>(actionSelector.getActions());

		if (lr == null) {
			throw new NullPointerException(
					"Cannot construct SARSA instance with a null learning rate.");
		}
		_lr = lr;
		if (discountFactor < 0 || discountFactor > 1) {
			throw new IllegalArgumentException(
					"Discount factor must be in [0, 1]. Found : "
							+ discountFactor);
		}
		_discountFactor = discountFactor;

		reset();
	}

	public void reset() {
		_numUpdates = 0;

		_qfuncs = new HashMap<A, RealVector>();
		for (int i = 0; i < _actions.size(); i++) {
			A action = _actions.get(i);
			RealVector w = new ArrayRealVector(_stateDim);
			_qfuncs.put(action, w);
		}
	}

	public double discountFactor() {
		return _discountFactor;
	}

	@Override
	public A policy(S state) {
		return _actionSelector.policy(state, this);
	}

	@Override
	public void train(S prevState, A action, S newState, double reinforcement) {
		_numUpdates++;
		double newQ = reinforcement + discountFactor()
				* value(newState, policy(newState));
		double oldQ = value(prevState, action);
		double tdErr = newQ - oldQ;

		double alpha = _lr.value(_numUpdates);
		RealVector vPrevState = _toStateVector.evaluate(prevState);
		RealVector aweights = _qfuncs.get(action);
		aweights.combineToSelf(1, alpha * tdErr, vPrevState);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber, int episodeTimestep) {
		// TODO Auto-generated method stub

	}

	@Override
	public VFunction<S> greedyValueFunction() {
		return new QFunction.GreedyVFunction<S>(this);
	}

	@Override
	public double value(S state, A action) {
		RealVector vstate = _toStateVector.evaluate(state);
		return vstate.dotProduct(_qfuncs.get(action));
	}

	@Override
	public double greedyValue(S state) {
		Double bestVal = null;
		for (int i = 0; i < _actions.size(); i++) {
			A action = _actions.get(i);
			double val = value(state, action);
			if (bestVal == null || (isMaximizing() && bestVal < val)
					|| (isMinimizing() && bestVal > val)) {
				bestVal = val;
			}
		}
		return bestVal;
	}

	@Override
	public boolean isMaximizing() {
		return _actionSelector.isMaximizing();
	}

	@Override
	public boolean isMinimizing() {
		return _actionSelector.isMinimizing();
	}

	public StationaryPolicy<S, A> extractPolicy() {
		return new GreedyPolicy<S, A>(_toStateVector, _actions, _qfuncs,
				isMaximizing() ? Optimization.MAXIMIZE : Optimization.MINIMIZE);
	}

	/**
	 * A greedy policy based on a linear action-value function.
	 * @author Timothy A. Mann
	 *
	 * @param <S> the state type
	 * @param <A> the action type
	 */
	public static class GreedyPolicy<S, A> implements StationaryPolicy<S, A> {

		private List<A> _actions;
		private Map<A, RealVector> _qfuncs;
		private Function<S, RealVector> _toStateVector;
		private Optimization _opType;

		public GreedyPolicy(Function<S, RealVector> toStateVector,
				List<A> actions, Map<A, RealVector> qfuncs, Optimization opType) {
			_toStateVector = toStateVector;
			_actions = actions;
			_opType = opType;

			_qfuncs = new HashMap<A, RealVector>();
			for (A action : actions) {
				RealVector qfunc = qfuncs.get(action).copy();
				_qfuncs.put(action, qfunc);
			}
		}

		@Override
		public A policy(S state) {
			RealVector vstate = _toStateVector.evaluate(state);
			A bestA = null;
			Double bestV = null;
			for (A action : _actions) {
				double val = vstate.dotProduct(_qfuncs.get(action));
				if (bestV == null || _opType.firstIsBetter(val, bestV)) {
					bestV = val;
					bestA = action;
				}
			}
			return bestA;
		}

		@Override
		public boolean isDeterministic() {
			return true;
		}

		@Override
		public double actionProb(S state, A action) {
			return (action.equals(policy(state))) ? 1.0 : 0.0;
		}

	}

}
