package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.learning.IncrementalFunctionApproximator;
import tmrutil.learning.LearningRate;
import tmrutil.learning.NNet;
import tmrutil.learning.rl.AbstractTemporalDifferenceLearning;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.QFunction;
import tmrutil.learning.rl.VFunction;
import tmrutil.optim.Optimization;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.DataStore;

/**
 * A continuous state implementation of the Q-learning reinforcement learning
 * algorithm with a finite action set.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DiscreteActionQLearning extends
		AbstractTemporalDifferenceLearning<double[], Integer>
{
	private int _stateSize;
	private List<IncrementalFunctionApproximator<double[],double[]>> _qfuncs;

	private ActionSelector<double[], Integer> _actionSelector;

	public DiscreteActionQLearning(int stateSize, Collection<? extends IncrementalFunctionApproximator<double[],double[]>> qfuncs,
			LearningRate learningRate, double discountFactor, double epsilon)
	{
		super(learningRate, discountFactor, Optimization.MAXIMIZE);
		_stateSize = stateSize;
		_qfuncs = new ArrayList<IncrementalFunctionApproximator<double[],double[]>>(qfuncs);

		Collection<Integer> validActions = new ArrayList<Integer>();
		for (int i = 0; i < qfuncs.size(); i++) {
			validActions.add(i);
		}
		_actionSelector = new ActionSelector.EpsilonGreedy<double[], Integer>(
				validActions, epsilon, Optimization.MAXIMIZE);
		// _actionSelector = new ActionSelector.Softmax<double[],
		// Integer>(validActions, alpha);
	}

	@Override
	protected void resetImpl()
	{
		for (IncrementalFunctionApproximator<double[], double[]> qfunc : _qfuncs) {
			qfunc.reset();
		}
	}

	@Override
	protected void trainImpl(double[] prevState, Integer action,
			double[] newState, double reinforcement)
	{
		//double[] target = { reinforcement + getDiscountFactor()
		//		* evaluateQ(newState, policy(newState)) };
		double[] target = null;
		if(newState == null){
			target = new double[]{reinforcement};
		}else{
			target = new double[]{ reinforcement + getDiscountFactor() * maxQ(newState) };
		}
		IncrementalFunctionApproximator<double[],double[]> qfunc = _qfuncs.get(action);
		qfunc.train(prevState, target, getLearningRate());
	}

	
	public double evaluateQ(double[] state, Integer action)
	{
		return _qfuncs.get(action).evaluate(state)[0];
	}

	@Override
	public Integer policy(double[] state)
	{
		return _actionSelector.policy(state, this);
	}

	
	public double maxQ(double[] state)
	{
		double maxQ = 0;
		for(int a=0;a<_qfuncs.size();a++){
			double qval = evaluateQ(state, a);
			if(qval > maxQ || a == 0){
				maxQ = qval;
			}
		}
		return maxQ;
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VFunction<double[]> greedyValueFunction() {
		return new QFunction.GreedyVFunction<double[]>(this);
	}

	@Override
	public double value(double[] state, Integer action) {
		return evaluateQ(state, action);
	}

	@Override
	public double greedyValue(double[] state) {
		return maxQ(state);
	}

}
