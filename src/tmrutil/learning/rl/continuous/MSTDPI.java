package tmrutil.learning.rl.continuous;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.learning.BatchFunctionApproximator;
import tmrutil.learning.BatchLinearApproximator;
import tmrutil.learning.CrossValidation;
import tmrutil.learning.CrossValidation.CrossValidationResult;
import tmrutil.learning.RidgeRegression;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.Policy;
import tmrutil.learning.rl.RLInstance;
import tmrutil.learning.rl.VFunction;
import tmrutil.math.Metric;
import tmrutil.optim.ObjectiveFunction;
import tmrutil.optim.ObjectiveFunction.Loss;
import tmrutil.optim.ObjectiveFunction.Penalty;
import tmrutil.optim.Optimization;
import tmrutil.optim.SGD;
import tmrutil.stats.Statistics;
import tmrutil.util.DataStore;
import tmrutil.util.Pair;

/**
 * Model Selection Temporal Difference + Policy Improvements (MSTDPI) algorithm.
 * This algorithm goes through a sequence of iterations. It first uses the
 * temporal difference learning rule to evaluate the current policy using a
 * model selection procedure. Then the estimated value is used to perform a
 * greedy policy improvement step. These two steps are repeated.
 * 
 * @author Timothy A. Mann
 *
 */
public class MSTDPI implements LearningSystem<double[], Integer> {

	public static class MSTDPIParams {
		public int numPolicyEvaluationSamples = 10000;
		public int minActionSamples = 100;

		public double trainRatio = 0.75;
		public int numTests = 1;

		public int numIterations = 20;
		public int numBatchIterations = 50;
		public double[] regularizers = { 0.0, 0.1, 0.3, 1, 3, 10, 30 };

	}

	public class FAPolicy implements Policy<double[], Integer> {

		private Random _rand = new Random();
		private List<BatchFunctionApproximator<RealVector, Double>> _qfuncs;
		private List<Boolean> _canUse;

		public FAPolicy(
				List<BatchFunctionApproximator<RealVector, Double>> qfuncs,
				List<Boolean> canUse) {
			_qfuncs = new ArrayList<BatchFunctionApproximator<RealVector, Double>>(
					qfuncs);
			_canUse = canUse;
		}

		@Override
		public Integer policy(double[] state) {
			if (state == null) {
				return _rand.nextInt(_qfuncs.size());
			} else {
				RealVector vx = new ArrayRealVector(state);
				double[] qvals = new double[_qfuncs.size()];
				for (int a = 0; a < _qfuncs.size(); a++) {
					if (_canUse.get(a)) {
						qvals[a] = _qfuncs.get(a).evaluate(vx);
					} else {
						if (_opType.equals(Optimization.MAXIMIZE)) {
							qvals[a] = Double.NEGATIVE_INFINITY;
						} else {
							qvals[a] = Double.POSITIVE_INFINITY;
						}
					}
				}

				if (_opType.equals(Optimization.MAXIMIZE)) {
					return Statistics.maxIndex(qvals);
				} else {
					return Statistics.minIndex(qvals);
				}
			}
		}

	}

	public class FAVFunc implements VFunction<double[]> {

		private List<BatchFunctionApproximator<RealVector, Double>> _qfuncs;
		private List<Boolean> _canUse;

		public FAVFunc(
				List<BatchFunctionApproximator<RealVector, Double>> qfuncs,
				List<Boolean> canUse) {
			_qfuncs = new ArrayList<BatchFunctionApproximator<RealVector, Double>>(
					qfuncs);
			_canUse = canUse;
		}

		@Override
		public double value(double[] state) {
			RealVector vx = new ArrayRealVector(state);
			double[] qvals = new double[_qfuncs.size()];
			for (int a = 0; a < _qfuncs.size(); a++) {
				if (_canUse.get(a)) {
					qvals[a] = _qfuncs.get(a).evaluate(vx);
				} else {
					if (_opType.equals(Optimization.MAXIMIZE)) {
						qvals[a] = Double.NEGATIVE_INFINITY;
					} else {
						qvals[a] = Double.POSITIVE_INFINITY;
					}
				}
			}

			if (_opType.equals(Optimization.MAXIMIZE)) {
				return Statistics.max(qvals);
			} else {
				return Statistics.min(qvals);
			}
		}

	}

	public class RandPolicy implements Policy<double[], Integer> {
		private Random _rand = new Random();

		@Override
		public Integer policy(double[] state) {
			return _rand.nextInt(_numActions);
		}
	}

	/**
	 * Squared difference between two scalar values.
	 * 
	 * @author Timothy A. Mann
	 *
	 */
	public static class SquaredError implements Metric<Double> {

		@Override
		public double distance(Double x, Double y) {
			double diff = x - y;
			return diff * diff;
		}

	}

	public static class DummyVFunction implements VFunction<double[]> {

		@Override
		public double value(double[] state) {
			return 0;
		}

	}

	private int _stateDims;
	private int _numActions;
	private MSTDPIParams _params;
	private double _epsilonGreedy;
	private double _discountFactor;
	private Optimization _opType;

	private Policy<double[], Integer> _policy;
	private List<List<RLInstance<double[], Integer>>> _samples;
	private int _numSamples;

	public MSTDPI(int stateDimensions, int numActions, MSTDPIParams params,
			double discountFactor, double epsilonGreedy, Optimization opType) {
		if (stateDimensions < 1) {
			throw new IllegalArgumentException(
					"Dimension of the state-space must be positive.");
		}
		_stateDims = stateDimensions;
		if (numActions < 1) {
			throw new IllegalArgumentException(
					"There must be at least 1 action.");
		}
		_numActions = numActions;

		if (params == null) {
			throw new NullPointerException(
					"Cannot use null MSTDPIParams instance to initialize MSTDPI.");
		}
		_params = params;

		setDiscountFactor(discountFactor);
		setEpsilonGreedy(epsilonGreedy);

		if (opType == null) {
			throw new NullPointerException("Optimization type cannot be null.");
		}
		_opType = opType;

		reset();
	}

	public void reset() {
		// Initialize to a random policy
		_policy = new RandPolicy();
		_samples = new ArrayList<List<RLInstance<double[], Integer>>>(
				_numActions);
		for (int a = 0; a < _numActions; a++) {
			_samples.add(new ArrayList<RLInstance<double[], Integer>>());
		}
	}

	public int numberOfStateDimensions() {
		return _stateDims;
	}

	public int numberOfActions() {
		return _numActions;
	}

	public void setEpsilonGreedy(double epsilon) {
		if (epsilon < 0 || epsilon > 1) {
			throw new IllegalArgumentException(
					"Epsilon greedy parameter represents a probability. Expected value in [0, 1]. Received "
							+ epsilon + ".");
		}
		_epsilonGreedy = epsilon;
	}

	public double epsilonGreedy() {
		return _epsilonGreedy;
	}

	private void setDiscountFactor(double gamma) {
		if (gamma < 0 || gamma > 1) {
			throw new IllegalArgumentException(
					"Discount factor parameter must be in [0, 1]. Received "
							+ gamma + ".");
		}
		_discountFactor = gamma;
	}

	public double discountFactor() {
		return _discountFactor;
	}

	@Override
	public Integer policy(double[] state) {
		double r = tmrutil.stats.Random.uniform();
		if (r < _epsilonGreedy) {
			return tmrutil.stats.Random.nextInt(_numActions);
		} else {
			Integer action = _policy.policy(state);
			return action;
		}
	}

	@Override
	public void train(double[] prevState, Integer action, double[] newState,
			double reinforcement) {

		/*
		 * Record the new sample.
		 */
		_numSamples++;
		RLInstance<double[], Integer> sample = new RLInstance<double[], Integer>(
				prevState, action, newState, reinforcement);
		_samples.get(action).add(sample);

		/*
		 * If we have enough samples for policy evaluation then we do policy
		 * evaluation and update the policy.
		 */
		if (_numSamples >= _params.numPolicyEvaluationSamples) {

			System.out.println("Training");

			/*
			 * Estimate the action-value function.
			 */
			List<BatchFunctionApproximator<RealVector, Double>> qfuncs = new ArrayList<BatchFunctionApproximator<RealVector, Double>>();
			for (int a = 0; a < _numActions; a++) {
				qfuncs.add(null);
			}
			List<Boolean> canUse = new ArrayList<Boolean>();

			VFunction<double[]> vfunc = new DummyVFunction();

			for (int i = 0; i < _params.numIterations; i++) {
				System.out.println("\tIteration: " + (i + 1));
				for (int a = 0; a < _numActions; a++) {
					System.out.println("\t\tAction: " + a);
					List<RLInstance<double[], Integer>> asamples = _samples
							.get(a);
					if (asamples.size() >= _params.minActionSamples) {
						canUse.add(true);

						BatchFunctionApproximator<RealVector, Double> fa = estimateQ(
								a, vfunc);
						qfuncs.set(a, fa);
					} else {
						canUse.add(false);
					}
				}
				vfunc = new FAVFunc(qfuncs, canUse);
			}

			/*
			 * Update the policy.
			 */
			_policy = new FAPolicy(qfuncs, canUse);

			/*
			 * Clear all of the samples.
			 */
			_numSamples = 0;
			for (int a = 0; a < _numActions; a++) {
				_samples.get(a).clear();
			}
		}
	}

	private BatchFunctionApproximator<RealVector, Double> estimateQ(
			Integer action, VFunction<double[]> vfunc) {
		List<RLInstance<double[], Integer>> samples = _samples.get(action);
		List<Pair<RealVector, Double>> vsamples = new ArrayList<Pair<RealVector, Double>>(
				samples.size());
		for (RLInstance<double[], Integer> sample : samples) {
			RealVector vstate = new ArrayRealVector(sample.state());
			double target = 0;
			if (sample.nextState() == null) {
				target = sample.reinforcement();
			} else {
				target = sample.reinforcement() + discountFactor()
						* vfunc.value(sample.nextState());
			}
			Pair<RealVector, Double> vsample = new Pair<RealVector, Double>(
					vstate, target);
			vsamples.add(vsample);
		}

		List<BatchFunctionApproximator<RealVector, Double>> models = new ArrayList<BatchFunctionApproximator<RealVector, Double>>();
		for (double lambda : _params.regularizers) {
			models.add(new RidgeRegression(_stateDims, lambda));
		}

		CrossValidation<RealVector, Double> cv = new CrossValidation<RealVector, Double>(
				new SquaredError(), models, vsamples);

		CrossValidationResult<RealVector, Double> cvResult = cv.cv(
				_params.trainRatio, _params.numTests);

		int bestModelIndex = cvResult.bestModelIndex();
		double bestScore = cvResult.score(bestModelIndex);

		System.out.println("MSE: " + bestScore);

		return cvResult.bestModel();
	}

	@Override
	public void record(DataStore dstore, int episodeNumber, int episodeTimestep) {
	}

}
