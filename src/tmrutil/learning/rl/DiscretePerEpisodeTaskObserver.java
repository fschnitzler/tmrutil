package tmrutil.learning.rl;

import java.util.HashMap;
import java.util.Map;
import tmrutil.util.Pair;

public class DiscretePerEpisodeTaskObserver extends
		PerEpisodeTaskObserver<Integer, Integer>
{
	private Map<Pair<Integer,Integer>,Integer> _saCounts;
	
	public DiscretePerEpisodeTaskObserver()
	{
		this(null);
	}
	
	public DiscretePerEpisodeTaskObserver(Explorer<Integer,Integer> explorer)
	{
		super(explorer);
		_saCounts = new HashMap<Pair<Integer,Integer>,Integer>();
	}
	
	@Override
	public void observeStep(Task<Integer, Integer> task, Integer prevState,
			Integer action, Integer newState, double reinforcement)
	{
		super.observeStep(task, prevState, action, newState, reinforcement);
		
		Pair<Integer,Integer> saPair = new Pair<Integer,Integer>(prevState, action);
		Integer count = _saCounts.get(saPair);
		if(count == null){
			_saCounts.put(saPair, 1);
		}else{
			_saCounts.put(saPair, count.intValue() + 1);
		}
	}

	@Override
	public void reset()
	{
		super.reset();
		if(_saCounts != null){
			_saCounts.clear();
		}
	}
	
	public int countVisits(Integer state, Integer action)
	{
		Pair<Integer,Integer> saPair = new Pair<Integer,Integer>(state, action);
		Integer count = _saCounts.get(saPair);
		if(count == null){
			return 0;
		}else{
			return count.intValue();
		}
	}
	
	public int[][] visitTable(int numStates, int numActions)
	{
		int[][] saCounts = new int[numStates][numActions];
		for(int s = 0 ; s < numStates; s++){
			for (int a =0; a< numActions; a++){
				saCounts[s][a] = countVisits(s, a);
			}
		}
		return saCounts;
	}
	
	public int[] vectorizedVisitTable(int numStates, int numActions)
	{
		int[] saCounts = new int[numStates * numActions];
		for(int s=0;s<numStates;s++){
			for(int a=0;a<numActions;a++){
				int index = (s * numActions) + a;
				saCounts[index] = countVisits(s, a);
			}
		}
		return saCounts;
	}
	
	public int numStateActionPairsVisited(int numStates, int numActions)
	{
		int count = 0;
		for(int s=0;s<numStates;s++){
			for(int a=0;a<numActions;a++){
				if(countVisits(s, a) >= 1){
					count++;
				}
			}
		}
		return count;
	}
	
	public int numStateActionPairsNotVisited(int numStates, int numActions)
	{
		int count = 0;
		for(int s=0;s<numStates;s++){
			for(int a=0;a<numActions;a++){
				if(countVisits(s, a) < 1){
					count++;
				}
			}
		}
		return count;
	}
}
