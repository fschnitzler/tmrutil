package tmrutil.learning.rl.tasks;

import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

import tmrutil.learning.rl.DiscreteMarkovDecisionProcess;
import tmrutil.stats.Filter;
import tmrutil.stats.GenerativeDistribution;
import tmrutil.stats.Random;

/**
 * Defines the RedHerring Markov decision process model.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RedHerringMDP implements
		DiscreteMarkovDecisionProcess<Integer, Integer> {

	public static final int NUM_STATES = 121;
	public static final int NUM_ACTIONS = 4;

	public static final int ENV_WIDTH = 11;
	public static final int ENV_HEIGHT = 11;

	private double[][] _r;
	private double[][][] _p;
	private DiscreteMarkovDecisionProcess.Model _mdp;

	public RedHerringMDP(double discountFactor) {
		_r = defineR();
		_p = defineP();
		_mdp = new DiscreteMarkovDecisionProcess.Model(NUM_STATES, NUM_ACTIONS,
				_p, _r, discountFactor);
	}

	@Override
	public double transitionProb(Integer state, Integer action,
			Integer resultState) {
		return _mdp.transitionProb(state, action, resultState);
	}

	@Override
	public double reinforcement(Integer state, Integer action) {
		return _mdp.reinforcement(state, action);
	}

	@Override
	public double getDiscountFactor() {
		return _mdp.getDiscountFactor();
	}

	@Override
	public Set<Integer> states() {
		return _mdp.states();
	}

	@Override
	public Set<Integer> actions(Integer state) {
		return _mdp.actions(state);
	}

	@Override
	public Set<Integer> successors(Integer state) {
		return _mdp.successors(state);
	}

	@Override
	public Set<Integer> predecessors(Integer state) {
		return _mdp.predecessors(state);
	}

	@Override
	public int numberOfStates() {
		return _mdp.numberOfStates();
	}

	@Override
	public int numberOfActions() {
		return _mdp.numberOfActions();
	}

	@Override
	public double rmax() {
		return _mdp.rmax();
	}

	@Override
	public double rmin() {
		return _mdp.rmin();
	}

	@Override
	public boolean isKnown(Integer state, Integer action) {
		return _mdp.isKnown(state, action);
	}

	private int xyToIndex(int x, int y) {
		return y * ENV_WIDTH + x;
	}

	private int xyToIndex(Point p) {
		return xyToIndex(p.x, p.y);
	}

	public Point indexToXY(Integer state) {
		return new Point(state % ENV_WIDTH, state / ENV_WIDTH);
	}

	private boolean inBounds(int x, int y) {
		return (x >= 0 && x < ENV_WIDTH) && (y >= 0 && y < ENV_HEIGHT);
	}

	private Set<Point> generateAbsorbing() {
		Set<Point> absorbing = new HashSet<Point>();
		absorbing.add(new Point(10, 9));
		absorbing.add(new Point(3, 7));
		absorbing.add(new Point(7, 3));
		return absorbing;
	}

	private Set<Point> generateWalls() {
		Set<Point> walls = new HashSet<Point>();

		for (int i = 0; i <= 5; i++) {
			if (i != 2) {
				walls.add(new Point(5, i));
			}
			if (i != 1) {
				walls.add(new Point(i, 5));
			}
		}

		for (int i = 5; i <= 10; i++) {
			if (i != 8) {
				walls.add(new Point(i, 6));
			}
			if (i != 9) {
				walls.add(new Point(5, i));
			}
		}
		return walls;
	}

	public boolean wall(int x, int y) {
		return !inBounds(x, y) || generateWalls().contains(new Point(x, y));
	}

	private boolean wall(Point p) {
		return wall(p.x, p.y);
	}

	private boolean hitsWall(Integer state, Integer action) {
		Point xy = indexToXY(state);
		return hitsWall(xy.x, xy.y, action);
	}

	private boolean hitsWall(int x, int y, Integer action) {
		switch (action) {
		case NORTH_ACTION:
			y = y - 1;
			break;
		case SOUTH_ACTION:
			y = y + 1;
			break;
		case EAST_ACTION:
			x = x + 1;
			break;
		case WEST_ACTION:
			x = x - 1;
			break;
		default:
		}

		return wall(x, y);
	}

	private static final int NORTH_ACTION = 0;
	private static final int SOUTH_ACTION = 1;
	private static final int EAST_ACTION = 2;
	private static final int WEST_ACTION = 3;

	private double[][] defineR() {
		double[][] r = new double[NUM_STATES][NUM_ACTIONS];

		for(int s=0;s<NUM_STATES;s++){
			for(int a=0;a<NUM_ACTIONS;a++){
				r[s][a] = 0.05;
			}
		}
		
		for (int a = 0; a < NUM_ACTIONS; a++) {
			// Define main goal state
			r[xyToIndex(10, 9)][a] = 0.9;

			// Define red herring states
			r[xyToIndex(3, 7)][a] = 0.;
			r[xyToIndex(7, 3)][a] = 0.;
		}

		return r;
	}
	
	private static final double USUAL_PROB = 0.8;
	private static final double ERR_PROB = (1 - USUAL_PROB)/2;

	private double[][][] defineP() {
		double[][][] p = new double[NUM_STATES][NUM_ACTIONS][NUM_STATES];

		for (int s = 0; s < NUM_STATES; s++) {
			for (int a = 0; a < NUM_ACTIONS; a++) {
				Point xy = indexToXY(s);
				if (wall(xy) || generateAbsorbing().contains(xy) || hitsWall(s, a)) {
					p[s][a][s] = 1;
				} else {
					Point usual = null;
					Point err1 = null;
					Point err2 = null;

					switch (a) {
					case NORTH_ACTION:
						usual = new Point(xy.x, xy.y - 1);
						err1 = new Point(xy.x + 1, xy.y - 1);
						err2 = new Point(xy.x - 1, xy.y - 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					case SOUTH_ACTION:
						usual = new Point(xy.x, xy.y + 1);
						err1 = new Point(xy.x + 1, xy.y + 1);
						err2 = new Point(xy.x - 1, xy.y + 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					case EAST_ACTION:
						usual = new Point(xy.x + 1, xy.y);
						err1 = new Point(xy.x + 1, xy.y + 1);
						err2 = new Point(xy.x + 1, xy.y - 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					case WEST_ACTION:
						usual = new Point(xy.x - 1, xy.y);
						err1 = new Point(xy.x - 1, xy.y + 1);
						err2 = new Point(xy.x - 1, xy.y - 1);

						p[s][a][xyToIndex(usual)] = USUAL_PROB;

						if (wall(err1) && wall(err2)) {
							p[s][a][xyToIndex(usual)] = 1;
						} else if (wall(err1)) {
							p[s][a][xyToIndex(err2)] = 2 * ERR_PROB;
						} else if (wall(err2)) {
							p[s][a][xyToIndex(err1)] = 2 * ERR_PROB;
						} else {
							p[s][a][xyToIndex(err1)] = ERR_PROB;
							p[s][a][xyToIndex(err2)] = ERR_PROB;
						}
						break;
					}
				}
			}
		}

		return p;
	}
	
	public static class InitStateDistribution implements GenerativeDistribution<Integer>
	{

		@Override
		public Integer sample() {
			int x = Random.nextInt(5);
			int y = Random.nextInt(5);
			
			Integer state = y * ENV_WIDTH + x;
			return state;
		}
		
	}
	
	public static class TerminalStateFilter implements Filter<Integer>
	{

		@Override
		public boolean accept(Integer x) {
			return false;
		}
		
	}
}
