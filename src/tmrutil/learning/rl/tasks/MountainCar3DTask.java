package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.util.DebugException;

/**
 * Implements the 3-dimensional mountain car problem.
 * @author Timothy A. Mann
 *
 */
public class MountainCar3DTask extends Task<double[], double[]> implements Drawable
{
	/** The minimum allowed position of the car. */
	public static final double POS_MIN = -1.2;
	/** The maximum allowed position and goal state of the car. */
	public static final double POS_MAX = 0.6;
	/** The goal position. */
	public static final double POS_GOAL = 0.5;
	/** The minimum velocity of the car. */
	public static final double VEL_MIN = -0.07;
	/** The maximum velocity of the car. */
	public static final double VEL_MAX = 0.07;
		
	/**
	 * The reinforcement signal used to calculate the immediate reward or cost
	 * associated with taking a particular action in a given state.
	 */
	private ReinforcementSignal<double[], double[]> _rsignal;
	/**
	 * The X-coordinate of the car.
	 */
	private double _x;
	/**
	 * The Y-coordinate of the car.
	 */
	private double _y;
	/**
	 * The velocity of the car along the X-axis.
	 */
	private double _xv;
	/**
	 * The velocity of the car along the Y-axis.
	 */
	private double _yv;

	/** Buffer for the previously visited state. */
	private double[] _prevState;
	/** Buffer for the previously executed action. */
	private double[] _prevAction;
	/** Buffer for the new visited state. */
	private double[] _newState;
	
	/**
	 * Constructs an instance of the 3D mountain car task.
	 * @param rsignal the reinforcement signal used to compute immediate reward or cost
	 */
	public MountainCar3DTask(ReinforcementSignal<double[],double[]> rsignal)
	{
		_rsignal = rsignal;
		_prevState = new double[4];
		_newState = new double[4];
		_prevAction = new double[2];
	}

	@Override
	public double evaluate()
	{
		return _rsignal.evaluate(_prevState, _prevAction);
	}

	@Override
	public void execute(double[] action)
	{
		// Copy the previous state
		System.arraycopy(getState(),0,_prevState,0,_prevState.length);
		// Copy the previous action
		System.arraycopy(action,0,_prevAction,0,_prevAction.length);
		
		//
		// Update the system state
		//
		
		// Update the X velocity
		_xv = _xv + (action[0] * 0.001) + (-0.0025 * Math.cos(3 * _x));
		if (_xv < VEL_MIN) {
			_xv = VEL_MIN;
		}
		if (_xv > VEL_MAX) {
			_xv = VEL_MAX;
		}
		// Update the Y velocity
		_yv = _yv + (action[1] * 0.001) + (-0.0025 * Math.cos(3 * _y));
		if(_yv < VEL_MIN){
			_yv = VEL_MIN;
		}
		if(_yv > VEL_MAX){
			_yv = VEL_MAX;
		}
		// Update the X position
		_x = _x + _xv;
		if (_x < POS_MIN) {
			_x = POS_MIN;
		}
		if (_x > POS_MAX) {
			_x = POS_MAX;
		}
		// Update the Y position
		_y = _y + _yv;
		if (_y < POS_MIN) {
			_y = POS_MIN;
		}
		if (_y > POS_MAX) {
			_y = POS_MAX;
		}
		
		// Set X velocity to zero if the car hits the minimum X value
		if (_x == POS_MIN && _xv < 0) {
			_xv = 0;
		}
		// Set Y velocity to zero if the car hits the minimum Y value
		if(_y == POS_MIN && _yv < 0){
			_yv = 0;
		}
		
		// Copy the new state
		System.arraycopy(getState(), 0, _newState, 0, _newState.length);
	}

	@Override
	public double[] getState()
	{
		return new double[]{_x, _xv, _y, _yv};
	}

	@Override
	public boolean isFinished()
	{
		return (_x >= POS_GOAL && _y >= POS_GOAL);
	}

	@Override
	public void reset()
	{
		_x = -Math.PI/6;
		_xv = 0;
		_y = -Math.PI/6;
		_yv = 0;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		throw new DebugException("MountainCar3DTask rendering is not implemented");
	}

}
