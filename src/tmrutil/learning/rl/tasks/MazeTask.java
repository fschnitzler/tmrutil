package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.Task;
import tmrutil.util.DebugException;

/**
 * Represents a 2-dimensional grid-like maze.
 * 
 * @author Timothy A. Mann
 * 
 */
public class MazeTask extends Task<double[], double[]> implements Drawable
{
	public static enum MazeTile {
		EMPTY, WALL, INIT, GOAL, MARKER
	};

	private MazeTile[][] _maze;

	private List<Point> _walls;
	private List<Point> _markers;

	private Point _goal;
	private Point _init;

	private Point _agent;

	/** The width of the maze. */
	private int _mWidth;
	/** The height of the maze. */
	private int _mHeight;

	/**
	 * Constructs a maze with a specified width, height, and tile types. The
	 * maze layout is validated to ensure that there is one initial state and
	 * one goal state.
	 * 
	 * @param mazeWidth
	 *            the width of the maze (number of tiles wide)
	 * @param mazeHeight
	 *            the height of the maze (number of tiles high)
	 * @param maze
	 *            a 2-dimensional array of tiles where the first index
	 *            corresponds to the maze width and the second corresponds to
	 *            the maze height
	 */
	public MazeTask(int mazeWidth, int mazeHeight, MazeTile[][] maze)
	{
		validate(mazeWidth, mazeHeight, maze);
		_mWidth = mazeWidth;
		_mHeight = mazeHeight;

		_goal = new Point();
		_init = new Point();
		_agent = new Point();

		_walls = new ArrayList<Point>();
		_markers = new ArrayList<Point>();

		_maze = new MazeTile[mazeWidth][mazeHeight];

		for (int r = 0; r < mazeWidth; r++) {
			System.arraycopy(maze, 0, _maze, 0, mazeHeight);

			for (int c = 0; c < mazeHeight; c++) {
				if (maze[r][c].equals(MazeTile.GOAL)) {
					_goal.setLocation(r, c);
				}
				if (maze[r][c].equals(MazeTile.INIT)) {
					_init.setLocation(r, c);
				}
				if (maze[r][c].equals(MazeTile.WALL)) {
					_walls.add(new Point(r, c));
				}
				if (maze[r][c].equals(MazeTile.MARKER)) {
					_markers.add(new Point(r, c));
				}
			}
		}

		reset();
	}

	private void validate(int mazeWidth, int mazeHeight, MazeTile[][] maze)
	{
		// Check if the maze width is accurate
		if (mazeWidth != maze.length) {
			throw new IllegalArgumentException(
					"The specified maze width does not match the maze layouts width.");
		}
		// Check if the maze height is accurate
		for (int r = 0; r < mazeWidth; r++) {
			if (mazeHeight != maze[r].length) {
				throw new IllegalArgumentException(
						"The specified maze height does not match the maze layouts height.");
			}
		}

		// Check if the maze contains a single goal and a single init tile
		int numGoals = 0;
		int numInits = 0;
		for (int r = 0; r < mazeWidth; r++) {
			for (int c = 0; c < mazeHeight; c++) {
				if (maze[r][c].equals(MazeTile.GOAL)) {
					numGoals++;
				}
				if (maze[r][c].equals(MazeTile.INIT)) {
					numInits++;
				}
			}
		}
		if (numGoals != 1) {
			throw new IllegalArgumentException(
					"The maze layout must contain one goal state.");
		}
		if (numInits != 1) {
			throw new IllegalArgumentException(
					"The maze layout must contain one initial state.");
		}
	}

	/**
	 * Returns the tile type at a specified maze coordinate (x, y).
	 * 
	 * @param x
	 *            the horizontal coordinate in the maze
	 * @param y
	 *            the vertical coordinate in the maze
	 * @return a maze tile type
	 */
	public MazeTile get(int x, int y)
	{
		return _maze[x][y];
	}

	/**
	 * Returns true if the tile at coordinate (x, y) is the goal.
	 * 
	 * @param x
	 *            an horizontal coordinate in the maze
	 * @param y
	 *            a vertical coordinate in the maze
	 * @return true if the tile at coordinate (x, y) is the goal; otherwise
	 *         false
	 */
	public boolean isGoal(int x, int y)
	{
		return get(x, y).equals(MazeTile.GOAL);
	}

	/**
	 * Returns true if the tile at coordinate (x, y) is a wall.
	 * 
	 * @param x
	 *            an horizontal coordinate in the maze
	 * @param y
	 *            a vertical coordinate in the maze
	 * @return true if the tile at coordinate (x, y) is a wall; otherwise false
	 */
	public boolean isWall(int x, int y)
	{
		return get(x, y).equals(MazeTile.WALL);
	}

	/**
	 * Returns the width of this maze.
	 * 
	 * @return a positive integer
	 */
	public int getWidth()
	{
		return _mWidth;
	}

	/**
	 * Returns the height of this maze.
	 * 
	 * @return a positive integer
	 */
	public int getHeight()
	{
		return _mHeight;
	}

	/**
	 * Returns the current location of the agent in this maze.
	 * 
	 * @return the current location of the agent in this maze
	 */
	public Point getAgent()
	{
		return new Point(_agent.x, _agent.y);
	}

	/**
	 * Sets the location of the agent in the maze. Returns true if operation was
	 * successful. The operation will fail if the specified location is a wall.
	 * 
	 * @param x the agent's X-coordinate
	 * @param y the agent's Y-coordinate
	 * @return true if the agent's location is set to (x,y); otherwise false
	 */
	public boolean setAgent(int x, int y)
	{
		if (x < 0 || x >= _mWidth || y < 0 || y >= _mHeight) {
			throw new IndexOutOfBoundsException("Cannot set agent location ("
					+ x + ", " + y + ") because it exceeds maze bounds.");
		}
		if (isWall(x, y)) {
			return false;
		} else {
			_agent.setLocation(x, y);
			return true;
		}
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Color agentColor = Color.BLUE;
		Color goalColor = Color.RED;
		Color initColor = Color.GREEN;
		Color wallColor = Color.DARK_GRAY;
		Color markerColor = Color.GRAY;
		Color emptyColor = Color.LIGHT_GRAY;

		Graphics2D g2 = (Graphics2D) g.create();
		double rwidth = Math.min(width / (double)_mWidth, height / (double)_mHeight);

		for (int x = 0; x < _mWidth; x++) {
			for (int y = 0; y < _mHeight; y++) {
				MazeTile tile = get(x, y);
				Color tcolor = null;
				switch (tile) {
				case GOAL:
					tcolor = goalColor;
					break;
				case EMPTY:
					tcolor = emptyColor;
					break;
				case INIT:
					tcolor = initColor;
					break;
				case MARKER:
					tcolor = markerColor;
					break;
				case WALL:
					tcolor = wallColor;
					break;
				default:
					throw new DebugException(
							"No color specified for tile type : " + tile);
				}

				g2.setColor(tcolor);
				Rectangle2D rect = new Rectangle2D.Double(x * rwidth, y
						* rwidth, rwidth, rwidth);
				g2.fill(rect);
				g2.setColor(Color.BLACK);
				g2.draw(rect);
			}
		}

		Rectangle2D arect = new Rectangle2D.Double(_agent.x * rwidth, _agent.y
				* rwidth, rwidth, rwidth);
		g2.setColor(agentColor);
		g2.fill(arect);
		g2.setColor(Color.BLACK);
		g2.draw(arect);

		g2.dispose();
	}

	@Override
	public double evaluate()
	{
		if (_agent.equals(_goal)) {
			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public void execute(double[] action)
	{
		int deltaX = (int) Math.round(action[0]);
		int deltaY = (int) Math.round(action[1]);

		int newX = _agent.x + deltaX;
		int newY = _agent.y + deltaY;
		if (newX >= 0 && newX < _mWidth && newY >= 0 && newY < _mHeight
				&& !isWall(newX, newY)) {
			_agent.setLocation(newX, newY);
		}
	}

	@Override
	public double[] getState()
	{
		double[] state = new double[4];
		state[0] = 2 * (_agent.x / (double)_mWidth) - 1;
		state[1] = 2 * (_agent.y / (double)_mHeight) - 1;

		//int numSteps = 2;
		//double yes = 1;
		//double no = -1;
		//state[2] = isMarkerLeft(numSteps) ? yes : no;
		//state[3] = isMarkerRight(numSteps) ? yes : no;

		//state[4] = isMarkerUp(numSteps) ? yes : no;
		//state[5] = isMarkerDown(numSteps) ? yes : no;

		if(_maze[_agent.x][_agent.y].equals(MazeTile.MARKER)){
			if(_goal.x > _agent.x){
				state[2] = 1;
			}else if(_goal.x == _agent.x){
				state[2] = 0;
			}else{
				state[2] = -1;
			}
			if(_goal.y > _agent.y){
				state[3] = 1;
			}else if(_goal.y == _agent.y){
				state[3] = 0;
			}else{
				state[3] = -1;
			}
		}else{
			state[2] = 0.0;
			state[3] = 0.0;
		}
		
		return state;
	}

	@Override
	public boolean isFinished()
	{
		return _agent.equals(_goal);
		//return false;
	}

	@Override
	public void reset()
	{
		_agent.setLocation(_init);
	}

	/**
	 * Convenience function used for generating valid actions for navigation in
	 * a maze.
	 * 
	 * @return a collection of actions
	 */
	public static final Collection<double[]> generateActions()
	{
		List<double[]> actions = new ArrayList<double[]>();
		actions.add(new double[] { 0, 0 });
		actions.add(new double[] { -1, 0 });
		actions.add(new double[] { 1, 0 });
		actions.add(new double[] { 0, -1 });
		actions.add(new double[] { 0, 1 });
		
		actions.add(new double[] {-1, -1});
		actions.add(new double[] {-1, 1});
		actions.add(new double[]{1, -1});
		actions.add(new double[]{1, 1});

		return actions;
	}

	private boolean isMarkerLeft(int numSteps)
	{
		for (int x = 1; x < numSteps + 1; x++) {
			try {
				if (_maze[_agent.x - x][_agent.y].equals(MazeTile.MARKER)) {
					return true;
				}
			} catch (IndexOutOfBoundsException ex) {
				return false;
			}
		}
		return false;
	}

	private boolean isMarkerRight(int numSteps)
	{
		for (int x = 1; x < numSteps + 1; x++) {
			try {
				if (_maze[_agent.x + x][_agent.y].equals(MazeTile.MARKER)) {
					return true;
				}
			} catch (IndexOutOfBoundsException ex) {
				return false;
			}
		}
		return false;
	}

	private boolean isMarkerUp(int numSteps)
	{
		for (int y = 1; y < numSteps + 1; y++) {
			try {
				if (_maze[_agent.x][_agent.y - y].equals(MazeTile.MARKER)) {
					return true;
				}
			} catch (IndexOutOfBoundsException ex) {
				return false;
			}
		}
		return false;
	}

	private boolean isMarkerDown(int numSteps)
	{
		for (int y = 1; y < numSteps; y++) {
			try {
				if (_maze[_agent.x][_agent.y + y].equals(MazeTile.MARKER)) {
					return true;
				}
			} catch (IndexOutOfBoundsException ex) {
				return false;
			}
		}
		return false;
	}
}
