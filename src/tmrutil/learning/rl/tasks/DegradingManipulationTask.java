package tmrutil.learning.rl.tasks;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.AbstractDiscreteTemporalDifferenceLearning;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.stats.Random;
import tmrutil.util.ArrayOps;
import tmrutil.util.Interval;

/**
 * A nonstationary control task where the agent's ability to control a manipulator degrades over time.
 * @author Timothy A. Mann
 *
 */
public class DegradingManipulationTask extends DiscreteTask implements Drawable
{
	private static final int NORTH1 = 0;
	private static final int SOUTH1 = 1;
	private static final int EAST1 = 2;
	private static final int WEST1 = 3;
	private static final int NORTH2 = 4;
	private static final int SOUTH2 = 5;
	private static final int EAST2 = 6; 
	private static final int WEST2 = 7;
	private static final int NUM_ACTIONS = 8;
	
	private static final Interval REWARD_INTERVAL = new Interval(0, 1);
	
	private int _time;
	private Point _agent;
	private Point _goal;
	private Dimension _workspace;

	public DegradingManipulationTask(int width, int height)
	{
		super((width * height * width * height), NUM_ACTIONS, REWARD_INTERVAL);
		
		_agent = new Point();
		_goal = new Point();
		
		reset();
	}

	@Override
	public Integer getState()
	{
		int[] state = {_agent.x, _agent.y, _goal.x, _goal.y};
		int[] maxVals = {_workspace.width, _workspace.height, _workspace.width, _workspace.height};
		return ArrayOps.encode(state, maxVals);
	}

	@Override
	public void execute(Integer action)
	{
		_time++;
		
		if(_agent.equals(_goal)){
			int ax = Random.nextInt(_workspace.width);
			int ay = Random.nextInt(_workspace.height);
			_agent.setLocation(ax, ay);
			
			int gx = Random.nextInt(_workspace.width);
			int gy = Random.nextInt(_workspace.height);
			_goal.setLocation(gx, gy);
		}
		
		
	}

	@Override
	public double evaluate()
	{
		if(_agent.equals(_goal)){
			return REWARD_INTERVAL.getMax();
		}else{
			return REWARD_INTERVAL.getMin();
		}
	}

	@Override
	public void reset()
	{
		_time = 0;

		int ax = Random.nextInt(_workspace.width);
		int ay = Random.nextInt(_workspace.height);
		_agent.setLocation(ax, ay);
		
		int gx = Random.nextInt(_workspace.width);
		int gy = Random.nextInt(_workspace.height);
		_goal.setLocation(gx, gy);
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		// TODO Auto-generated method stub
		
	}

}
