package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.RandomLearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;

/**
 * The sorting task presents the learning system with a shuffled sequence of
 * numbers. The objective of the learning system is to swap pairs of values to
 * sort the sequence of numbers in ascending order. This is a very difficult
 * reinforcement learning problem because there are a large number of states and
 * actions.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SortTask extends Task<int[], Integer> implements Drawable
{
	private int[] _unsortedVals;

	public SortTask(int numElms)
	{
		_unsortedVals = VectorOps.randperm(numElms);
	}

	@Override
	public int[] getState()
	{
		return Arrays.copyOf(_unsortedVals, _unsortedVals.length);
	}

	@Override
	public void execute(Integer action)
	{
		int index = action.intValue();
		if (index >= 0 && index < _unsortedVals.length - 1) {
			int backupA = _unsortedVals[index];
			int backupB = _unsortedVals[index + 1];
			_unsortedVals[index] = backupB;
			_unsortedVals[index + 1] = backupA;
		}
	}

	@Override
	public double evaluate()
	{
		return -mistakeCount();
	}

	@Override
	public void reset()
	{
		int numElms = _unsortedVals.length;
		_unsortedVals = VectorOps.randperm(numElms);
	}

	@Override
	public boolean isFinished()
	{
		return isSorted();
	}

	public boolean isSorted()
	{
		boolean sorted = true;
		int prev = _unsortedVals[0];
		for (int i = 1; i < _unsortedVals.length; i++) {
			int val = _unsortedVals[i];
			if (prev > val) {
				sorted = false;
				break;
			}
			prev = val;
		}
		return sorted;
	}

	public int mistakeCount()
	{
		int mc = 0;
		int prev = _unsortedVals[0];
		for (int i = 1; i < _unsortedVals.length; i++) {
			int val = _unsortedVals[i];
			if (prev > val) {
				mc++;
			}
			prev = val;
		}
		return mc;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		double cwidth = width / (double) _unsortedVals.length;
		for (int i = 0; i < _unsortedVals.length; i++) {
			Rectangle2D rect = new Rectangle2D.Double(i * cwidth, 0, cwidth,
					height);

			int val = _unsortedVals[i];
			if (i + 1 < _unsortedVals.length) {
				if (val > _unsortedVals[i + 1]) {
					g.setColor(Color.RED);
					g.fill(rect);
				}
			}

			g.setColor(Color.BLACK);
			String valStr = String.valueOf(val);
			FontMetrics fm = g.getFontMetrics();
			Rectangle2D bounds = fm.getStringBounds(valStr, g);
			g.drawString(valStr,
					(int) (rect.getCenterX() - bounds.getWidth() / 2),
					(int) (rect.getCenterY() + bounds.getHeight() / 2));

			g.draw(rect);
		}
	}

	public static void main(String[] args)
	{
		int numElms = 10;
		List<Integer> actions = new ArrayList<Integer>(numElms - 1);
		for (int i = 0; i < numElms - 1; i++) {
			actions.add(i);
		}
		RandomLearningSystem<int[], Integer> ragent = new RandomLearningSystem<int[], Integer>(
				actions);
		SortTask task = new SortTask(numElms);

		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(400, 100);
		Task.runTask(task, ragent, 10, 100, false, null, frame);
	}

}
