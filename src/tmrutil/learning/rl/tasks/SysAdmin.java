package tmrutil.learning.rl.tasks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tmrutil.learning.rl.Task;
import tmrutil.stats.Random;

/**
 * Implements the SysAdmin task described by 
 * Guestrin et al. 2003, "Efficient Solution Algorithms for Factored MDPs".
 * 
 * 
 * @author Timothy A. Mann
 *
 */
public class SysAdmin extends Task<int[], int[]> {

	private int _numComputers;
	private int _maxReboots;
	
	private int[][] _connectivity;
	private double _fixProbability;
	private double _failProbability;
	
	private int[] _state;
	private List<Set<Integer>> _parents;
	
	public SysAdmin(int numComputers, int maxReboots, int[][] connectivity, double fixProbability, double failProbability)
	{
		_numComputers = numComputers;
		_maxReboots = maxReboots;
		
		_state = new int[_numComputers];
		_connectivity = connectivity;
		_fixProbability = fixProbability;
		_failProbability = failProbability;
		
		_parents = new ArrayList<Set<Integer>>(_numComputers);
		for(int i=0;i<_numComputers;i++){
			Set<Integer> pi = new HashSet<Integer>();
			_parents.add(pi);
			
			for(int j=0;j<_numComputers;j++){
				if(connectivity[i][j] > 0){
					pi.add(j);
				}
			}
		}
	}
	
	public int maxReboots()
	{
		return _maxReboots;
	}
	
	@Override
	public int[] getState() {
		return _state;
	}

	@Override
	public void execute(int[] action) {
		for(int m=0;m<_numComputers;m++){
			double r = Random.uniform();
			if(r < failProbability(m)){
				_state[m] = 0;
			}
		}
		
		int numReboots = 0;
		for(int i=0;i<action.length;i++){
			if(action[i] > 0 && numReboots < _maxReboots){
				numReboots++;
				double r = Random.uniform();
				if(r < _fixProbability){
					_state[i] = 1;
				}
			}
		}
	}
	
	private double failProbability(int m)
	{
		if(_state[m]==0){
			return 1;
		}else{
			int numFailedParents = 0;
			for(Integer pi : _parents.get(m)){
				if(_state[pi] == 0){
					numFailedParents += 1;
				}
			}
			return 1 - Math.pow((1-_failProbability), 1 + numFailedParents);
		}
	}

	@Override
	public double evaluate() {
		double reward = 0;
		for(int i=0;i<_state.length;i++){
			reward = reward + _state[i];
		}
		return reward / _numComputers;
	}

	@Override
	public void reset() {
		for(int i=0;i<_state.length;i++){
			_state[i] = 1;
		}
	}

	@Override
	public boolean isFinished() {
		return false;
	}

}
