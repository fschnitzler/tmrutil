package tmrutil.learning.rl.tasks;

import java.util.HashMap;
import java.util.Map;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.TaskObserver;

/**
 * A task observer for reporting on the number of visited states, the number of
 * times each state was visited, etc.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S> the state type
 * @param <A> the action type
 * @param <T> the task type
 */
public class VisitCountTaskObserver<S, A, T extends Task<S, A>> implements
		TaskObserver<S, A, T>
{
	private int _totalSteps;
	private int _numStates;
	private int _numActions;

	public Map<S, Integer> _visitCounts;

	/**
	 * Constructs a visitation counting task observer.
	 * @param numStates the number of task states
	 * @param numActions the number of actions
	 */
	public VisitCountTaskObserver(int numStates, int numActions)
	{
		if (numStates < 1) {
			throw new IllegalArgumentException(
					"The number of states must be at least 1.");
		}
		if (numActions < 1) {
			throw new IllegalArgumentException(
					"The number of actions must be at least 1.");
		}
		_numStates = numStates;
		_numActions = numActions;

		_visitCounts = new HashMap<S, Integer>();

		reset();
	}

	@Override
	public void observeEpisodeBegin(T task)
	{
		S state = task.getState();
		incrementCount(state);
	}

	@Override
	public void observeEpisodeEnd(T task)
	{
	}

	@Override
	public void observeStep(T task, S prevState, A action, S newState,
			double reinforcement)
	{
		_totalSteps++;
		incrementCount(newState);
	}

	@Override
	public void reset()
	{
		_totalSteps = 0;
		_visitCounts.clear();
	}
	
	/**
	 * Returns the number of times that a particular state has been visited.
	 * @param state a state
	 * @return the number of times that <code>state</code> has been visited
	 */
	public int stateVisitCount(S state)
	{
		Integer count = _visitCounts.get(state);
		if(count == null){
			return 0;
		}else{
			return count.intValue();
		}
	}

	/**
	 * Returns the proportion of states that have been visited at least once.
	 * @return a number between 0 and 1 representing the proportion of visited states
	 */
	public double proportionOfVisitedStates()
	{
		return _visitCounts.size() / (double) numberOfStates();
	}

	/**
	 * The total number of states for the task.
	 * @return the number of states
	 */
	public int numberOfStates()
	{
		return _numStates;
	}

	/**
	 * The total number of actions for the task.
	 * @return the number of actions
	 */
	public int numberOfActions()
	{
		return _numActions;
	}

	private void incrementCount(S state)
	{
		Integer count = _visitCounts.get(state);
		if (count == null) {
			_visitCounts.put(state, new Integer(1));
		} else {
			_visitCounts.put(state, new Integer(count.intValue() + 1));
		}
	}

}
