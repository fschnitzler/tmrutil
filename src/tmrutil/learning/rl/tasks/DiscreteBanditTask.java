package tmrutil.learning.rl.tasks;

import java.util.ArrayList;
import java.util.List;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.ram.RAMNextStateFunction;
import tmrutil.learning.rl.ram.RAMTask;
import tmrutil.learning.rl.ram.RAMTypeFunction;
import tmrutil.util.Interval;

/**
 * A discrete multiarmed bandit task.
 * @author Timothy A. Mann
 *
 */
public class DiscreteBanditTask extends RAMTask
{
	
	private List<BanditTask.RewardDistribution> _arms;
	private double _lastReward = 0;

	public DiscreteBanditTask(List<BanditTask.RewardDistribution> arms, Interval reinforcementInterval)
	{
		super(1, arms.size(), reinforcementInterval);
		_arms = new ArrayList<BanditTask.RewardDistribution>(arms);
	}

	@Override
	public Integer getState()
	{
		return 0;
	}

	@Override
	public void execute(Integer action)
	{
		_lastReward = _arms.get(action).sample();
	}

	@Override
	public double evaluate()
	{
		if(_lastReward > rmax()){
			return rmax();
		}else if(_lastReward < rmin()){
			return rmin();
		}else{
			return _lastReward;
		}
	}
	
	public BanditTask.RewardDistribution rewardDistribution(int armIndex)
	{
		return _arms.get(armIndex);
	}

	@Override
	public void reset()
	{
		_lastReward = 0;
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public RAMTypeFunction<Integer> typeFunction()
	{
		return new RAMTypeFunction<Integer>()
		{

			@Override
			public int classify(Integer state)
			{
				return 0;
			}

			@Override
			public int numberOfClasses()
			{
				return 1;
			}

			@Override
			public String classToString(Integer ramClass)
			{
				return ramClass.toString();
			}
			
		};
	}

	@Override
	public RAMNextStateFunction<Integer, Integer> nextStateFunction()
	{
		return new RAMNextStateFunction<Integer,Integer>(){

			@Override
			public Integer nextState(Integer state, Integer outcome)
			{
				return 0;
			}

			@Override
			public Integer outcomeMap(Integer state, Integer nextState)
			{
				return 0;
			}

			@Override
			public String outcomeToString(Integer outcome)
			{
				return outcome.toString();
			}

			@Override
			public Integer nullOutcome()
			{
				return 0;
			}

			@Override
			public int numberOfOutcomes()
			{
				return 1;
			}
			
		};
	}

	@Override
	public ReinforcementSignal<Integer, Integer> reinforcementSignal()
	{
		return new ReinforcementSignal<Integer,Integer>(){

			@Override
			public double evaluate(Integer state, Integer action)
			{
				return _arms.get(action).sample();
			}

		};
	}

}
