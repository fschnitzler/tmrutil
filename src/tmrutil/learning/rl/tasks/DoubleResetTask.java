package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Set;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DelayedQLearning;
import tmrutil.learning.rl.DiscreteMarkovDecisionProcess;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.RMax;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.ram.RAMNextStateFunction;
import tmrutil.learning.rl.ram.RAMRMax;
import tmrutil.learning.rl.ram.RAMTask;
import tmrutil.learning.rl.ram.RAMTypeFunction;
import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;

/**
 * The double reset task is similar to the reset task, except that there are two
 * goal states.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DoubleResetTask extends RAMTask implements Drawable
{
	public static final int DEFAULT_NUM_ACTIONS = 5;
	public static final double DEFAULT_SUCCESS_PROB = 0.8;
	public static final Interval DEFAULT_REINFORCEMENT_INTERVAL = new Interval(
			0, 1);

	private int _currentState;
	private int _lastState;
	private int _lastAction;

	private int _path1Action;
	private int _path2Action;

	private double _successProb;

	private boolean _path1IsOptimal;

	public DoubleResetTask(int numStates)
	{
		this(numStates, DEFAULT_NUM_ACTIONS);
	}

	public DoubleResetTask(int numStates, int numActions)
	{
		this(numStates, numActions, DEFAULT_REINFORCEMENT_INTERVAL,
				DEFAULT_SUCCESS_PROB, true);
	}

	public DoubleResetTask(int numStates, int numActions,
			Interval reinforcementInterval, double successProb,
			boolean path1IsOptimal)
	{
		super(numStates, numActions, reinforcementInterval);

		_successProb = successProb;

		// int[] rperm = VectorOps.randperm(numActions);
		_path1Action = numActions - 1;
		_path2Action = numActions - 2;

		_path1IsOptimal = path1IsOptimal;

		reset();
	}

	public boolean isOnPath1(int state)
	{
		return (state <= numberOfStates() / 2);
	}

	public boolean isOnPath2(int state)
	{
		return (state == 0) || (state > numberOfStates() / 2);
	}

	public int endOfPath1()
	{
		return numberOfStates() / 2;
	}

	public int endOfPath2()
	{
		return numberOfStates() - 1;
	}

	public double reinforcement(Integer state, Integer action)
	{
		if (state == endOfPath1() && action == _path1Action) {
			if (_path1IsOptimal) {
				return rmax();
			} else {
				return rmin() + (rmax() - rmin()) / 2;
			}
		}
		if (state == endOfPath2() && action == _path2Action) {
			if (_path1IsOptimal) {
				return rmin() + (rmax() - rmin()) / 2;
			} else {
				return rmax();
			}
		}
		return rmin();
	}

	@Override
	public Integer getState()
	{
		return _currentState;
	}

	@Override
	public void execute(Integer action)
	{
		_lastAction = action;
		_lastState = _currentState;

		boolean success = Random.uniform() < _successProb;
		if (_currentState == 0) {
			if (action == _path1Action) {
				if (success) {
					_currentState = _currentState + 1;
				}
			}
			if (action == _path2Action) {
				if (success) {
					_currentState = endOfPath1() + 1;
				}
			}
		} else if (_currentState == endOfPath1()
				|| _currentState == endOfPath2()) {
			_currentState = 0;
		} else {
			if (action == _path1Action && isOnPath1(_currentState)) {
				if (success) {
					_currentState = _currentState + 1;
				}
			} else if (action == _path2Action && isOnPath2(_currentState)) {
				if (success) {
					_currentState = _currentState + 1;
				}
			} else {
				_currentState = 0;
			}
		}
	}

	@Override
	public double evaluate()
	{
		return reinforcement(_lastState, _lastAction);
	}

	@Override
	public void reset()
	{
		_currentState = 0;
		_lastState = 0;
		_lastAction = 0;
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();

		double swidth = width / (double) (endOfPath1() + 1);
		double sheight = height / (double) (endOfPath2() - endOfPath1() + 1);

		for (int i = 0; i < numberOfStates(); i++) {
			Rectangle2D srect = null;
			if (i == 0) {
				srect = new Rectangle2D.Double(0, 0, swidth, sheight);
			} else {
				if (isOnPath1(i)) {
					srect = new Rectangle2D.Double(i * swidth, 0, swidth,
							sheight);
				} else {
					srect = new Rectangle2D.Double(0, (i - endOfPath1())
							* sheight, swidth, sheight);
				}
			}

			if (i == _currentState) {
				g2.setColor(Color.RED);
			} else if (i == 0) {
				g2.setColor(Color.BLUE);
			} else if (i == endOfPath1()) {
				g2.setColor(new Color(0f, 0.5f, 0f));
			} else if (i == endOfPath2()) {
				g2.setColor(Color.GREEN);
			} else {
				g2.setColor(Color.LIGHT_GRAY);
			}
			g2.fill(srect);

			g2.setColor(Color.BLACK);
			g2.draw(srect);
		}

		g2.dispose();
	}

	@Override
	public RAMTypeFunction<Integer> typeFunction()
	{
		return new DoubleResetTypeFunction();
	}

	private class DoubleResetTypeFunction implements RAMTypeFunction<Integer>
	{
		private static final int START_STATE = 0;
		private static final int GOAL_STATE = 1;
		private static final int PATH1_STATE = 2;
		private static final int PATH2_STATE = 3;

		@Override
		public int classify(Integer state)
		{
			if (state == 0) {
				return START_STATE;
			} else if (state == endOfPath1() || state == endOfPath2()) {
				return GOAL_STATE;
			} else if (state < endOfPath1()) {
				return PATH1_STATE;
			} else {
				return PATH2_STATE;
			}
		}

		@Override
		public int numberOfClasses()
		{
			return 4;
		}

		@Override
		public String classToString(Integer ramClass)
		{
			switch (ramClass) {
			case START_STATE:
				return "Start State";
			case GOAL_STATE:
				return "Goal State";
			case PATH1_STATE:
				return "Path 1 State";
			case PATH2_STATE:
				return "Path 2 State";
			}
			throw new DebugException("Class label not found.");
		}

	}

	@Override
	public RAMNextStateFunction<Integer, Integer> nextStateFunction()
	{
		return new DoubleResetNextStateFunction();
	}

	private class DoubleResetNextStateFunction implements
			RAMNextStateFunction<Integer, Integer>
	{
		public static final int NULL_OUTCOME = 0;
		public static final int FORWARD_PATH1_OUTCOME = 1;
		public static final int FORWARD_PATH2_OUTCOME = 2;
		public static final int RESET_OUTCOME = 3;

		@Override
		public Integer nextState(Integer state, Integer outcome)
		{
			if (outcome.equals(NULL_OUTCOME)) {
				return state;
			} else if (outcome.equals(FORWARD_PATH1_OUTCOME)) {
				return state + 1;
			} else if (outcome.equals(FORWARD_PATH2_OUTCOME)) {
				if (state == 0) {
					return endOfPath1() + 1;
				} else {
					return state + 1;
				}
			} else {
				return 0;
			}
		}

		@Override
		public Integer outcomeMap(Integer state, Integer nextState)
		{
			int outcome = 0;
			if (state.equals(nextState)) {
				outcome = NULL_OUTCOME;
			} else if (state >= 0 && state <= endOfPath1()
					&& nextState.equals(state + 1)) {
				outcome = FORWARD_PATH1_OUTCOME;
			} else if ((state == 0 && nextState.equals(endOfPath1() + 1))
					|| (state > endOfPath1() && nextState.equals(state + 1))) {
				outcome = FORWARD_PATH2_OUTCOME;
			} else {
				outcome = RESET_OUTCOME;
			}

			if (nextState(state, outcome) != nextState) {
				throw new DebugException(
						"Outcome does not produce correct next state.");
			}
			return outcome;
		}

		@Override
		public Integer nullOutcome()
		{
			return NULL_OUTCOME;
		}

		@Override
		public int numberOfOutcomes()
		{
			return 4;
		}

		@Override
		public String outcomeToString(Integer outcome)
		{
			switch (outcome) {
			case NULL_OUTCOME:
				return "Null";
			case FORWARD_PATH1_OUTCOME:
				return "Forward(Path1)";
			case FORWARD_PATH2_OUTCOME:
				return "Forward(Path2)";
			case RESET_OUTCOME:
				return "Reset";
			}
			throw new DebugException("Outcome label not found.");
		}

	}

	@Override
	public ReinforcementSignal<Integer, Integer> reinforcementSignal()
	{
		return new ReinforcementSignal<Integer, Integer>() {

			@Override
			public double evaluate(Integer state, Integer action)
			{
				return reinforcement(state, action);
			}

		};
	}

	public static void main(String[] args)
	{
		double discountFactor = 0.95;
		int numVisitsBeforeKnown = 5;

		int numEpisodes = 100;
		int episodeLength = 500;

		DoubleResetTask task = new DoubleResetTask(25, 5);
		Interval rewardInterval = new Interval(task.rmin(), task.rmax());

		// double sensitivity = 0.01;
		// DelayedQLearning agent = new DelayedQLearning(task.numberOfStates(),
		// task.numberOfActions(), rewardInterval.getMax(), sensitivity,
		// discountFactor, numVisitsBeforeKnown);
		// RMax agent = new RMax(task.numberOfStates(), task.numberOfActions(),
		// discountFactor, rewardInterval, numVisitsBeforeKnown);

		RAMRMax agent = new RAMRMax(task.numberOfStates(),
				task.numberOfActions(), task.typeFunction(),
				task.nextStateFunction(), task.reinforcementSignal(),
				rewardInterval, discountFactor, numVisitsBeforeKnown);

		Task.runTask(task, agent, numEpisodes, episodeLength, true, null, null,
				true);

		agent.plotKnown();
		agent.inspectModel();

		MatrixOps.displayGraphically(agent.getQ(), "Q-Values");
		// MatrixOps.displayGraphically(agent.knownMatrix(), "Known Matrix");

		javax.swing.JFrame frame = new javax.swing.JFrame("Reset Task");
		frame.setSize(400, 400);
		Task.runTask(task, agent, numEpisodes, episodeLength, true, null,
				frame, false);
	}

}
