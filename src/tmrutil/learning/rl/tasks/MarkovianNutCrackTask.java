package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import java.awt.Point;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.util.ArrayOps;
import tmrutil.util.Interval;

public class MarkovianNutCrackTask extends DiscreteTask implements Drawable
{
	private NutCrackDriver _driver;
	private int _size;
	
	public MarkovianNutCrackTask(int size)
	{
		super(numStates(size), NutCrackDriver.NUMBER_OF_ACTIONS, NutCrackDriver.REWARD_INTERVAL);
		_driver = new NutCrackDriver(size);
		_size = size;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_driver.draw(g, width, height);
	}
	
	private static final int numStates(int size)
	{
		return 8 * (size * size * size);
	}

	@Override
	public Integer getState()
	{
		int isNutCracked = (_driver.isNutCracked())? 1 : 0;
		Point agent = _driver.whereIsAgent();
		Point rock = _driver.whereIsRock();
		Point nut = _driver.whereIsNut();
		
		int[] vstate = {isNutCracked, agent.x, rock.x, rock.y, nut.x, nut.y};
		int[] maxValues = {1, _size, _size, 1, _size, 1};
		int state = ArrayOps.encode(vstate, maxValues);
		return state;
	}

	@Override
	public void execute(Integer action)
	{
		_driver.execute(action);
	}

	@Override
	public double evaluate()
	{
		return _driver.evaluate();
	}

	@Override
	public void reset()
	{
		_driver.reset();
	}

	@Override
	public boolean isFinished()
	{
		return _driver.isFinished();
	}

}
