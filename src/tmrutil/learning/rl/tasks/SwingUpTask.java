package tmrutil.learning.rl.tasks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.LearningRate;
import tmrutil.learning.NNet;
import tmrutil.learning.RBFNetwork;
import tmrutil.learning.SingleLayerPerceptron;
import tmrutil.learning.rl.ActionSelector;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.RandomLearningSystem;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.continuous.AdvantageLearning;
import tmrutil.math.Function;
import tmrutil.optim.Optimization;
import tmrutil.physics.RungeKutta;
import tmrutil.util.Interval;

/**
 * Implements the single pendulum swing up task.
 * @author Timothy A. Mann
 *
 */
public class SwingUpTask extends Task<double[], double[]> implements Drawable
{
	
	public static final double GRAVITY_CONSTANT = -9.81;
	public static final double DEFAULT_POLE_LENGTH = 1.0;
	public static final double DEFAULT_TIME_STEP = 0.05;
	public static final double DEFAULT_DECISION_INTERVAL = 0.2;
	public static final double DEFAULT_GOAL_ANGLE = Math.PI;
	
	/** Represents the time in seconds. */
	private double _time;
	/** Represents the time step. */
	private double _timeStep;
	/** Represents the amount of time between decisions. */
	private double _decisionInterval;
	
	/** The length of the pole. */
	private double _poleLength;
	/** The angle of the pole. */
	private double _poleAngle;
	/** The pole velocity. */
	private double _poleVel;
	
	private Interval _poleAngleLimits;
	private Interval _poleVelLimits;
	
	/** The goal angle. */
	private double _goalAngle;
	
	private double[] _prevStateBuff;
	private double[] _actionBuff;
	
	public SwingUpTask()
	{
		this(DEFAULT_TIME_STEP, DEFAULT_DECISION_INTERVAL, DEFAULT_GOAL_ANGLE, DEFAULT_POLE_LENGTH);
	}
	
	public SwingUpTask(double timeStep, double decisionInterval, double goalAngle, double poleLength)
	{
		_timeStep = timeStep;
		_decisionInterval = decisionInterval;
		_goalAngle = goalAngle;
		_poleLength = poleLength;
		
		_poleAngleLimits = new Interval(-2.25 * Math.PI, 1.5 * Math.PI);
		_poleVelLimits = new Interval(-10, 10);
		
		_prevStateBuff = new double[2];
		_actionBuff = new double[1];
		reset();
	}
	
	@Override
	public double evaluate()
	{
		return (Math.pow(_poleAngle - _goalAngle,2) + Math.pow(_actionBuff[0], 2)) * _timeStep;
	}

	@Override
	public void execute(double[] action)
	{
		System.arraycopy(action, 0, _actionBuff, 0, _actionBuff.length);
		System.arraycopy(getState(), 0, _prevStateBuff, 0, _prevStateBuff.length);
		for(double t=0;t<_decisionInterval;t+=_timeStep){
			step(action);
		}
	}
	
	private void step(double[] action)
	{
		_time += _timeStep;
		
		VelODE velODE = new VelODE(action[0]);
		_poleVel = RungeKutta.solve(_poleVel, _time, _timeStep, velODE);
		//_poleVel += _timeStep * (action[0] + GRAVITY_CONSTANT * Math.cos(_poleAngle));
		_poleAngle += _timeStep * _poleVel;
		
		_poleVel = _poleVelLimits.clip(_poleVel);
	}
	
	private class VelODE implements Function<double[],Double>
	{
		private double _action;
		
		public VelODE(double action)
		{
			_action = action;
		}
		
		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException
		{
			return _action + GRAVITY_CONSTANT * Math.cos(_poleAngle);
		}
	}

	@Override
	public double[] getState()
	{
		return new double[]{_poleAngle, _poleVel};
	}

	@Override
	public boolean isFinished()
	{
		return !_poleAngleLimits.contains(_poleAngle);
	}

	@Override
	public void reset()
	{
		_time = 0;
		_poleAngle = -Math.PI;
		_poleVel = 0;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		AffineTransform xform = new AffineTransform();
		double scale = Math.min(width/(2*_poleLength), height/(2*_poleLength));
		xform.scale(scale, -scale);
		xform.translate(_poleLength, -_poleLength);
		g2.transform(xform);
		g2.setStroke(new BasicStroke((float)(1/scale)));
		
		double x = _poleLength * Math.sin(_poleAngle);
		double y = _poleLength * Math.cos(_poleAngle);
		g2.setColor(Color.BLACK);
		Line2D pole = new Line2D.Double(0, 0, x, y);
		g2.draw(pole);
	}

	
	public static void main(String[] args){
		double learningRate = 0.1;
		double discountFactor = 0.9;
		double epsilonGreedy = 0.05;
		
		SwingUpTask task = new SwingUpTask();
		List<double[]> actions = new ArrayList<double[]>();
		//actions.add(new double[]{0});
		for(double v=-5;v<=5;v+=0.5){
			actions.add(new double[]{v});
		}
		//LearningSystem<double[],double[]> agent = new RandomLearningSystem<double[],double[]>(actions);
		NNet afunc = new RBFNetwork(3, 1, 50, 0.2);
		ActionSelector<double[],double[]> actionSelector = new ActionSelector.EpsilonGreedy<double[], double[]>(actions, epsilonGreedy, Optimization.MINIMIZE);
		LearningSystem<double[], double[]> agent = new AdvantageLearning(2, 1, afunc, actionSelector, 1, new LearningRate.Constant(learningRate), discountFactor);
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(400,400);
		Task.runTask(task, agent, 1000, 1000, true, null);
		Task.runTask(task, agent, 100, 1000, true, null, frame);
	}
}
