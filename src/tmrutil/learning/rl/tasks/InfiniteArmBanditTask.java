package tmrutil.learning.rl.tasks;

import tmrutil.learning.rl.Task;
import tmrutil.util.Interval;

/**
 * A multiarmed bandit task with an infinite number of arms. The arm encodings
 * may or may not induce a topological space that can be used to improve
 * learning speed.
 * 
 * @author Timothy A. Mann
 * 
 * @param <A>
 *            the arm type
 */
public abstract class InfiniteArmBanditTask<A> extends Task<Integer, A>
{
	private double _lastReward;
	private A _lastAction;

	@Override
	public Integer getState()
	{
		return 0;
	}

	@Override
	public void execute(A action)
	{
		_lastAction = action;
		_lastReward = sampleReward(action);
	}

	@Override
	public double evaluate()
	{
		return _lastReward;
	}
	
	public A lastAction()
	{
		return _lastAction;
	}

	@Override
	public void reset()
	{
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	/**
	 * Samples a reward from the distribution associated with the specified
	 * action.
	 * 
	 * @param action
	 *            an action
	 * @return a reward sampled from the distribution associated with
	 *         <code>action</code>
	 */
	public abstract double sampleReward(A action);

	/**
	 * Returns the mean reward of the distribution associated with the specified
	 * action.
	 * 
	 * @param action
	 *            an action
	 * @return the mean of the reward distribution for an action
	 */
	public abstract double meanReward(A action);
	
	/**
	 * Returns the interval that the rewards are bounded between.
	 * @return the reward interval
	 */
	public abstract Interval rewardInterval();

}
