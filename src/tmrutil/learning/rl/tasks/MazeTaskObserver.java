package tmrutil.learning.rl.tasks;

import java.awt.Point;

/**
 * An observer for maze tasks.
 * @author Timothy A. Mann
 *
 */
public class MazeTaskObserver extends
		SumOfReinforcementsTaskObserver<double[], double[], MazeTask>
{

	private double[][] _visits;
	private int _mazeWidth;
	private int _mazeHeight;
	
	public MazeTaskObserver(int mazeWidth, int mazeHeight)
	{
		_visits = new double[mazeWidth][mazeHeight];
		_mazeWidth = mazeWidth;
		_mazeHeight = mazeHeight;
	}

	@Override
	public void observeStep(MazeTask task, double[] prevState, double[] action,
			double[] newState, double reinforcement)
	{
		super.observeStep(task, prevState, action, newState, reinforcement);
		
		Point agent = task.getAgent();
		_visits[agent.x][agent.y] += 1.0; 
	}
	
	@Override
	public void reset()
	{
		super.reset();
		
		for(int r=0;r<_visits.length;r++){
			for(int c=0;c<_visits[r].length;c++){
				_visits[r][c] = 0.0;
			}
		}
	}

	/**
	 * Returns a copy of the visitation table (the number of times each state has been visited).
	 * @return the visitation table
	 */
	public double[][] getVisitationTable()
	{
		double[][] vtable = new double[_mazeWidth][_mazeHeight];
		for(int r=0;r<_mazeWidth;r++){
			for(int c=0;c<_mazeHeight;c++){
				vtable[r][c] = _visits[r][c];
			}
		}
		return vtable;
	}
}
