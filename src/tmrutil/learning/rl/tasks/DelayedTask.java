package tmrutil.learning.rl.tasks;

import java.util.ArrayList;
import java.util.List;
import tmrutil.learning.rl.Task;

/**
 * Wrapper that transforms an underlying task into a delayed task. Delay is
 * specified by a number of discrete time steps and can be added to sensory
 * information, reinforcement signal, or action execution. Delay must be
 * nonnegative.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 * @param <R>
 *            the reinforcement type
 */
public final class DelayedTask<S, A, T extends Task<S, A>> extends Task<S, A>
{
	/** A reference to the underlying task. */
	private T _task;

	/** The number of time steps to delay sensory information by. */
	private int _sensoryDelay;
	/** The number of time steps to delay action execution by. */
	private int _actionDelay;
	/** The number of time steps to delay the reinforcement signal by. */
	private int _rsDelay;

	private List<S> _sensoryQueue;
	private List<A> _actionQueue;
	private List<Double> _rsQueue;

	/**
	 * Constructs a sensory delayed task wrapped around another task.
	 * 
	 * @param task
	 *            a task
	 * @param sensoryDelay
	 *            the number of time steps to delay sensory information by
	 * @param actionDelay
	 *            the number of time steps to delay action execution by
	 * @param reinforcementDelay
	 *            the number of time steps to delay the reinforcement signal by
	 * @throws NullPointerException
	 *             if <code>task == null</code>
	 * @throws IllegalArgumentException
	 *             if <code>sensoryDelay &lt; 0</code>,
	 *             <code>actionDelay &lt; 0</code>, or
	 *             <code>reinforcementDelay &lt; 0</code>
	 */
	public DelayedTask(T task, int sensoryDelay, int actionDelay,
			int reinforcementDelay) throws NullPointerException,
			IllegalArgumentException
	{
		if (task == null) {
			throw new NullPointerException("The specified task cannot be null.");
		}
		if (sensoryDelay < 0) {
			throw new IllegalArgumentException(
					"The sensory delay must be nonnegative.");
		}
		if (actionDelay < 0) {
			throw new IllegalArgumentException(
					"The action delay must be nonnegative.");
		}
		if (reinforcementDelay < 0) {
			throw new IllegalArgumentException(
					"The reinforcement delay must be nonnegative.");
		}
		_task = task;
		_sensoryDelay = sensoryDelay;
		_actionDelay = actionDelay;
		_rsDelay = reinforcementDelay;

		_sensoryQueue = new ArrayList<S>(_sensoryDelay + 1);
		_actionQueue = new ArrayList<A>(_actionDelay + 1);
		_rsQueue = new ArrayList<Double>(_rsDelay + 1);
	}

	@Override
	public double evaluate()
	{
		if (_rsQueue.isEmpty()) {
			return _task.evaluate();
		} else {
			return _rsQueue.get(0);
		}
	}

	@Override
	public void execute(A action)
	{
		_actionQueue.add(action);
		if (_actionQueue.size() > _actionDelay) {
			_task.execute(_actionQueue.remove(0));
		}
		_sensoryQueue.add(_task.getState());
		if (_sensoryQueue.size() > _sensoryDelay) {
			_sensoryQueue.remove(0);
		}
		_rsQueue.add(_task.evaluate());
		if (_rsQueue.size() > _rsDelay) {
			_rsQueue.remove(0);
		}
	}

	@Override
	public S getState()
	{
		if (_sensoryQueue.isEmpty()) {
			return _task.getState();
		} else {
			return _sensoryQueue.get(0);
		}
	}

	@Override
	public boolean isFinished()
	{
		return _task.isFinished();
	}

	@Override
	public void reset()
	{
		_task.reset();
		_sensoryQueue.clear();
		_actionQueue.clear();
		_rsQueue.clear();
	}

	/**
	 * Returns the number of time steps that sensory information (sensory state)
	 * is delayed by.
	 * 
	 * @return a nonnegative integer
	 */
	public int getSensoryDelay()
	{
		return _sensoryDelay;
	}

	/**
	 * Returns the number of time steps that action execution is delayed by.
	 * 
	 * @return a nonnegative integer
	 */
	public int getActionDelay()
	{
		return _actionDelay;
	}

	/**
	 * Returns the number of time steps that the reinforcement signal is delayed
	 * by.
	 * 
	 * @return a nonnegative integer
	 */
	public int getReinforcementDelay()
	{
		return _rsDelay;
	}

	/**
	 * Returns the underlying undelayed task.
	 * 
	 * @return a task
	 */
	public T getUndelayedTask()
	{
		return _task;
	}

}
