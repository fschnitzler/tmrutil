package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;
import tmrutil.util.ArrayOps;
import tmrutil.util.Interval;

/**
 * An unconstrained reaching task. The agent (represented as a point) moves as close as possible to the target (represented as a point). 
 * @author Timothy A. Mann
 *
 */
public class UnconstrainedReachTask extends Task<double[], double[]> implements Drawable
{
	private static final int POINT_SIZE = 6;
	
	private static class DefaultReinforcementSignal implements ReinforcementSignal<double[],double[]>
	{
		@Override
		public double evaluate(double[] prevState, double[] action)
		{
			double sum = 0;
			int half = prevState.length/2;
			for(int i=0;i<half;i++){
				double d = prevState[i] - prevState[half + i];
				sum = d * d;
			}
			return sum;
		}
	}
	
	private double[] _agentLocation;
	private double[] _targetLocation;
	private Interval[] _agentBounds;
	private Interval[] _targetBounds;
	private int _dimension;
	private double _actionScale;
	
	private double[] _prevStateBuff;
	private double[] _actionBuff;
	private ReinforcementSignal<double[],double[]> _rsignal;
	
	/**
	 * Constructs an unconstrained reaching task with the default reinforcement signal, which is the squared Euclidean distance between the agent and target.
	 * @param agentBounds the interval in which the agent is initially located
	 * @param targetBounds the interval in which the target is located
	 */
	public UnconstrainedReachTask(Interval[] agentBounds, Interval[] targetBounds)
	{
		this(agentBounds, targetBounds, new DefaultReinforcementSignal());
	}
	
	/**
	 * Constructs an unconstrained reaching task with a specified reinforcement signal.
	 * @param agentBounds the interval in which the agent is initially located
	 * @param targetBounds the interval in which the target is located
	 * @param rsignal the reinforcement signal
	 */
	public UnconstrainedReachTask(Interval[] agentBounds, Interval[] targetBounds, ReinforcementSignal<double[],double[]> rsignal)
	{
		_dimension = agentBounds.length;
		if(agentBounds.length != targetBounds.length){
			throw new IllegalArgumentException("The dimension of agent bounds must match the dimension of the target bounds.");
		}
		_agentBounds = new Interval[agentBounds.length];
		_targetBounds = new Interval[targetBounds.length];
		for(int i=0;i<agentBounds.length;i++){
			Interval ainterval = agentBounds[i];
			Interval tinterval = targetBounds[i];
			if(ainterval == null || tinterval == null){
				throw new NullPointerException("Bounds cannot contain null intervals.");
			}
			_agentBounds[i] = new Interval(ainterval);
			_targetBounds[i] = new Interval(tinterval);
		}
		_actionScale = 1;
		_prevStateBuff = new double[_dimension];
		_actionBuff = new double[_dimension];
		_rsignal = rsignal;
	}
	
	/**
	 * Returns the scalar value that actions are scaled by before being applied.
	 * @return the scalar value applied to actions
	 */
	public double getActionScale()
	{
		return _actionScale;
	}
	
	/**
	 * Sets the scalar value that actions are scaled by before being applied.
	 * @param actionScale the new action scale value
	 */
	public void setActionScale(double actionScale)
	{
		_actionScale = actionScale;
	}
	
	@Override
	public double evaluate()
	{
		return _rsignal.evaluate(_prevStateBuff, _actionBuff);
	}

	@Override
	public void execute(double[] action)
	{
		double[] prevState = getState();
		System.arraycopy(prevState, 0, _prevStateBuff, 0, _dimension);
		System.arraycopy(action, 0, _actionBuff, 0, _dimension);
		double[] scaledAction = VectorOps.multiply(_actionScale, action);
		_agentLocation = VectorOps.add(_agentLocation, scaledAction, _agentLocation);
	}

	@Override
	public double[] getState()
	{
		return ArrayOps.concat(_agentLocation, _targetLocation);
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void reset()
	{
		_agentLocation = Interval.random(_agentBounds);
		_targetLocation = Interval.random(_agentBounds);
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		switch(_dimension){
		case 1:
			draw1D(g2, width, height);
			break;
		case 2:
			draw2D(g2, width, height);
			break;
		default:
			// Not supported (so do nothing)
		}
		g2.dispose();
	}
	
	private void draw1D(Graphics2D g, int width, int height)
	{
		g.setColor(Color.BLACK);
		g.drawLine(0, height/2, width, height/2);
		
		double minBounds = Math.min(_agentBounds[0].getMin(), _targetBounds[0].getMin());
		double minPoints = Math.min(_agentLocation[0], _targetLocation[0]);
		double minX = Math.min(minBounds, minPoints);
		
		double maxBounds = Math.max(_agentBounds[0].getMax(), _targetBounds[0].getMax());
		double maxPoints = Math.max(_agentLocation[0], _targetLocation[0]);
		double maxX = Math.max(maxBounds, maxPoints);
		
		double diffX = maxX - minX;
		
		double tx = (width * ((_targetLocation[0]-minX)/diffX)) - POINT_SIZE/2.0;
		double ty = height/2 - POINT_SIZE/2.0;
		double twidth = POINT_SIZE;
		double theight = POINT_SIZE;
		Ellipse2D target = new Ellipse2D.Double(tx, ty, twidth, theight);
		g.setColor(Color.RED);
		g.fill(target);
		g.setColor(Color.BLACK);
		g.draw(target);
		
		double ax = (width * ((_agentLocation[0]-minX)/diffX)) - POINT_SIZE/2.0;
		double ay = height/2 - POINT_SIZE/2.0;
		double awidth = POINT_SIZE;
		double aheight = POINT_SIZE;
		Ellipse2D agent = new Ellipse2D.Double(ax, ay, awidth, aheight);
		g.setColor(Color.WHITE);
		g.fill(agent);
		g.setColor(Color.BLACK);
		g.draw(agent);
	}
	
	private void draw2D(Graphics2D g, int width, int height)
	{
		
	}
}
