package tmrutil.learning.rl.tasks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.kinematics.Arm;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.continuous.VectorStateConstructor;
import tmrutil.math.VectorOps;
import tmrutil.util.ArrayOps;
import tmrutil.util.Interval;

/**
 * This task implements a coordinated bimanual reaching task. Coordinated
 * reaching tasks are more difficult than reaching with a single limb because
 * there are more degrees of freedom.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BimanualReachTask extends Task<BimanualState, double[]> implements
		Drawable
{
	
	/** A reference to the reinforcement signal. */
	private ReinforcementSignal<BimanualState, double[]> _rsignal;

	/** A reference to the left arm. */
	private Arm _leftArm;
	/** A reference to the right arm. */
	private Arm _rightArm;

	/** The number of components in a valid action vector. */
	private int _actionSize;

	/** A buffer to store the previous visited state. */
	private BimanualState _prevStateBuff;
	/** A buffer to store the previously applied action. */
	private double[] _actionBuff;

	private static class DefaultRSignal implements
			ReinforcementSignal<BimanualState, double[]>
	{

		private Arm _leftArm;
		private Arm _rightArm;
		private double _sigma2;

		public DefaultRSignal(Arm leftArm, Arm rightArm, double sigma)
		{
			_leftArm = leftArm;
			_rightArm = rightArm;
			_sigma2 = sigma * sigma;
		}

		@Override
		public double evaluate(BimanualState prevState,
				double[] action)
		{	
			Point2D lHand = _leftArm.endPoint();
			Point2D rHand = _rightArm.endPoint();
			double d2 = lHand.distanceSq(rHand);
			double r = (d2 < _sigma2) ? 1 : 0;
			return r;
			//return Math.exp(-d2/_sigma2);
		}

	}

	/**
	 * Constructs a bimanual reach task.
	 * 
	 * @param leftArm
	 *            the left arm
	 * @param rightArm
	 *            the right arm
	 */
	public BimanualReachTask(Arm leftArm, Arm rightArm)
	{
		this(leftArm, rightArm, new DefaultRSignal(leftArm,
				rightArm, 1));
	}

	/**
	 * Constructs a bimanual reach task.
	 * 
	 * @param leftArm
	 *            the left arm
	 * @param rightArm
	 *            the right arm
	 * @param rsignal
	 *            the reinforcement signal
	 */
	public BimanualReachTask(Arm leftArm, Arm rightArm,
			ReinforcementSignal<BimanualState, double[]> rsignal)
	{
		_rsignal = rsignal;

		_leftArm = leftArm;
		_rightArm = rightArm;
		
		_actionSize = _leftArm.size() + _rightArm.size();

		_prevStateBuff = null;
		_actionBuff = new double[_actionSize];
	}

	/**
	 * Returns a copy of the left arm in the same configuration as the
	 * underlying left arm.
	 * 
	 * @return a copy of the left arm
	 */
	public Arm leftArm()
	{
		return new Arm(_leftArm);
	}

	/**
	 * Returns a copy of the right arm in the same configuration as the
	 * underlying right arm.
	 * 
	 * @return a copy of the right arm
	 */
	public Arm rightArm()
	{
		return new Arm(_rightArm);
	}

	@Override
	public double evaluate()
	{
		return _rsignal.evaluate(_prevStateBuff, _actionBuff);
	}

	@Override
	public void execute(double[] action)
	{
		if (action.length != getActionSize()) {
			throw new IllegalArgumentException(
					"The specified action's length does not contain the correct number of components");
		}
		_prevStateBuff = new BimanualState(getState());
		double[] leftArmAction = new double[_leftArm.size()];
		double[] rightArmAction = new double[_rightArm.size()];
		System.arraycopy(action, 0, leftArmAction, 0, leftArmAction.length);
		System.arraycopy(action, leftArmAction.length, rightArmAction, 0,
				rightArmAction.length);

		_leftArm.setJointAngles(VectorOps.add(leftArmAction,
				_leftArm.getJointAngles()));
		_rightArm.setJointAngles(VectorOps.add(rightArmAction,
				_rightArm.getJointAngles()));

		System.arraycopy(action, 0, _actionBuff, 0, _actionBuff.length);
	}

	@Override
	public BimanualState getState()
	{
		return new BimanualState(_leftArm, _rightArm);
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	@Override
	public void reset()
	{
		_leftArm.random();
		_rightArm.random();
	}

	/**
	 * Returns the number of dimensions of a valid action vector.
	 * 
	 * @return the number of dimensions of a valid action vector
	 */
	public int getActionSize()
	{
		return _actionSize;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D) g.create();
		AffineTransform xform = new AffineTransform();
		xform.translate(width / 2, height / 2);
		double scale = Math.min(width, height) / 5.0;
		xform.scale(scale, -scale);
		g2.transform(xform);

		g2.setStroke(new BasicStroke((float) (1 / scale)));
		drawPose(g2, _leftArm, _leftArm.getJointAngles(), Color.BLACK);
		drawPose(g2, _rightArm, _rightArm.getJointAngles(), Color.BLACK);
		
		Ellipse2D rewardCirc = new Ellipse2D.Double(0, 0, 10, 10);
		if (this.evaluate() > 0.5) {
			g.setColor(Color.RED);
			g.fill(rewardCirc);
		}
		
		g.setColor(Color.BLACK);
		g.draw(rewardCirc);

		g2.dispose();
	}

	/**
	 * Draws an arm with a specified pose and color.
	 * 
	 * @param g
	 *            a graphics 2d object
	 * @param arm
	 *            an arm
	 * @param pose
	 *            a pose
	 * @param color
	 *            a color
	 */
	private void drawPose(Graphics2D g, Arm arm, double[] pose, Color color)
	{
		Point2D base = arm.getBase();
		double x = base.getX();
		double y = base.getY();
		double theta = 0;
		g.setColor(color);

		for (int i = 0; i < arm.size(); i++) {
			double length = arm.getLinkLength(i);
			theta += pose[i];
			double newX = x + length * Math.cos(theta);
			double newY = y + length * Math.sin(theta);
			Line2D line = new Line2D.Double(x, y, newX, newY);
			g.draw(line);
			x = newX;
			y = newY;
		}
	}

	private static final Arm buildLeftArm()
	{
		Arm leftArm = new Arm();
		leftArm.setBase(new Point2D.Double(-0.25, 0));
		leftArm.addLink(0.5, Math.PI / 4, 5 * Math.PI / 6);
		leftArm.addLink(0.5, -Math.PI / 2, 0);
		return leftArm;
	}

	private static final Arm buildRightArm()
	{
		Arm rightArm = new Arm();
		rightArm.setBase(new Point2D.Double(0.25, 0));
		rightArm.addLink(0.5, Math.PI / 6, 3 * Math.PI / 4);
		rightArm.addLink(0.5, 0, Math.PI / 2);
		return rightArm;
	}

	public static final BimanualReachTask buildTask()
	{
		Arm leftArm = buildLeftArm();
		Arm rightArm = buildRightArm();

		BimanualReachTask task = new BimanualReachTask(
				leftArm, rightArm);
		return task;
	}

	public static final BimanualReachTask buildTask(double sigma)
	{
		Arm leftArm = buildLeftArm();
		Arm rightArm = buildRightArm();

		DefaultRSignal rsignal = new DefaultRSignal(leftArm, rightArm, sigma);
		BimanualReachTask task = new BimanualReachTask(leftArm, rightArm, rsignal);
		return task;
	}

	public static final Collection<double[]> generateValidActions(
			BimanualReachTask task, double actionMagnitude)
	{
		Arm leftArm = task._leftArm;
		Arm rightArm = task._rightArm;
		return Arm.generateActionsWithCombinations(actionMagnitude,
				leftArm.size() + rightArm.size());
	}
}
