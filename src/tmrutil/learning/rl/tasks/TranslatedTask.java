package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.Translator;
import tmrutil.learning.rl.Task;

/**
 * This class allows tasks to be easily translated to have different state and action representations.
 * @author Timothy A. Mann
 *
 * @param <S> the new state type
 * @param <SF> the native state type of the task
 * @param <A> the new action type
 * @param <AF> the native action type of the task
 */
public class TranslatedTask<S, SF, A, AF> extends Task<S, A> implements
		Drawable
{
	private Task<SF,AF> _task;
	private Translator<SF,S> _stateTranslator;
	private Translator<A,AF> _actionTranslator;
	
	/**
	 * Constructs a task where the state and action type are translated to new types.
	 * @param task the underlying task
	 * @param stateTranslator the state translator
	 * @param actionTranslator the action translator
	 */
	public TranslatedTask(Task<SF,AF> task, Translator<SF, S> stateTranslator, Translator<A, AF> actionTranslator)
	{
		_task = task;
		_stateTranslator = stateTranslator;
		_actionTranslator = actionTranslator;
	}
	
	@Override
	public double evaluate()
	{
		return _task.evaluate();
	}

	@Override
	public void execute(A action)
	{
		AF foreignAction = _actionTranslator.translate(action);
		_task.execute(foreignAction);
	}

	@Override
	public S getState()
	{
		SF state = _task.getState();
		return _stateTranslator.translate(state);
	}

	@Override
	public boolean isFinished()
	{
		return _task.isFinished();
	}

	@Override
	public void reset()
	{
		_task.reset();
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		if(_task instanceof Drawable){
			Drawable drawable = (Drawable)_task;
			drawable.draw(g, width, height);
		}
	}

}
