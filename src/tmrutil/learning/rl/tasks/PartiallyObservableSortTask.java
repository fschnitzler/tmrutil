package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.util.Interval;

/**
 * A partially observable sorting task.
 * @author Timothy A. Mann
 *
 */
public class PartiallyObservableSortTask extends DiscreteTask implements Drawable
{
	private SortTask _sortDriver;
	private int _numElms;
	private int _cindex;
	
	public PartiallyObservableSortTask(int numElms)
	{
		super(2 * numElms, numElms, new Interval(-(numElms-1), 0));
		_numElms = numElms;
		_sortDriver = new SortTask(numElms);
		_cindex = 0;
	}

	@Override
	public Integer getState()
	{
		int[] state = _sortDriver.getState();
		int v1 = state[_cindex];
		int v2 = state[_cindex+1];
		if(v1 < v2){
			return _cindex;
		}else{
			return _numElms + _cindex;
		}
	}

	@Override
	public void execute(Integer action)
	{
		_sortDriver.execute(action);
		
		_cindex++;
		if(_cindex > _numElms-2){
			_cindex = 0;
		}
	}

	@Override
	public double evaluate()
	{
		return _sortDriver.evaluate();
	}

	@Override
	public void reset()
	{
		_sortDriver.reset();
		_cindex = 0;
	}

	@Override
	public boolean isFinished()
	{
		return _sortDriver.isFinished();
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_sortDriver.draw(g, width, height);
	}

}
