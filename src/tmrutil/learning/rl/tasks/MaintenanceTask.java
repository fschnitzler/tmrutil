package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.RandomLearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.stats.Random;
import tmrutil.util.DataStore;

/**
 * This task represents a machine maintenance task that may occur in a
 * manufacturing plant. The agent manages a set of machines called producers
 * that create products. Producers convert resources into a product and the
 * agent is rewarded based on the factory's total output at each timestep.
 * Producers can also suffer damage while converting resources into products. At
 * each timestep the agent can order more resources, fix a producer, replace a
 * producer, toggle a producer on or off, or do nothing. However, the agent has
 * a small budget that it must maintain.
 * 
 * @author Timothy A. Mann
 * 
 */
public class MaintenanceTask extends Task<int[], int[]> implements Drawable {

	private static final int REPAIR_COST = 5;

	private static final int ACTION_NULL = 0;
	private static final int ACTION_TOGGLE = 1;
	private static final int ACTION_REPAIR = 2;
	private static final int NUM_ACTIONS = 3;

	private static final double GOOD_PRODUCTION_PROB = 0.99;
	private static final double FAIR_PRODUCTION_PROB = 0.85;
	private static final double POOR_PRODUCTION_PROB = 0.6;

	private static final double GOOD_TO_FAIR_PROB = 0.05;
	private static final double FAIR_TO_POOR_PROB = 0.1;
	private static final double POOR_TO_BROKEN_PROB = 0.2;

	public enum ProducerStatus {
		GOOD_ON, GOOD_OFF, FAIR_ON, FAIR_OFF, POOR_ON, POOR_OFF, BROKEN_ON, BROKEN_OFF
	};

	public static class Producer {
		private ProducerStatus _status;

		public Producer(ProducerStatus status) {
			_status = status;
		}

		public ProducerStatus status() {
			return _status;
		}

		public void status(ProducerStatus status) {
			_status = status;
		}

		public boolean isStatusGood() {
			return _status.equals(ProducerStatus.GOOD_ON)
					|| _status.equals(ProducerStatus.GOOD_OFF);
		}

		public boolean isStatusFair() {
			return _status.equals(ProducerStatus.FAIR_ON)
					|| _status.equals(ProducerStatus.FAIR_OFF);
		}

		public boolean isStatusPoor() {
			return _status.equals(ProducerStatus.POOR_ON)
					|| _status.equals(ProducerStatus.POOR_OFF);
		}

		public boolean isOn() {
			return _status.equals(ProducerStatus.GOOD_ON)
					|| _status.equals(ProducerStatus.FAIR_ON)
					|| _status.equals(ProducerStatus.POOR_ON)
					|| _status.equals(ProducerStatus.BROKEN_ON);
		}

		public void toggleOnOff() {
			switch (_status) {
			case GOOD_ON:
				_status = ProducerStatus.GOOD_OFF;
				break;
			case GOOD_OFF:
				_status = ProducerStatus.GOOD_ON;
				break;
			case FAIR_ON:
				_status = ProducerStatus.FAIR_OFF;
				break;
			case FAIR_OFF:
				_status = ProducerStatus.FAIR_ON;
				break;
			case POOR_ON:
				_status = ProducerStatus.POOR_OFF;
				break;
			case POOR_OFF:
				_status = ProducerStatus.POOR_ON;
				break;
			case BROKEN_ON:
				_status = ProducerStatus.BROKEN_OFF;
				break;
			case BROKEN_OFF:
				_status = ProducerStatus.BROKEN_ON;
				break;
			default:
			}
		}

		public boolean isBroken() {
			return _status.equals(ProducerStatus.BROKEN_ON)
					|| _status.equals(ProducerStatus.BROKEN_OFF);
		}

		public Color color() {
			switch (_status) {
			case GOOD_ON:
				return new Color(0, 255, 0);
			case GOOD_OFF:
				return new Color(0, 50, 0);
			case FAIR_ON:
				return new Color(55, 200, 0);
			case FAIR_OFF:
				return new Color(10, 40, 0);
			case POOR_ON:
				return new Color(100, 155, 0);
			case POOR_OFF:
				return new Color(20, 30, 0);
			case BROKEN_ON:
				return new Color(100, 100, 100);
			case BROKEN_OFF:
				return new Color(15, 15, 15);
			}
			return Color.WHITE;
		}

		public int intState() {
			switch (_status) {
			case GOOD_ON:
				return 0;
			case GOOD_OFF:
				return 1;
			case FAIR_ON:
				return 2;
			case FAIR_OFF:
				return 3;
			case POOR_ON:
				return 4;
			case POOR_OFF:
				return 5;
			case BROKEN_ON:
				return 6;
			case BROKEN_OFF:
				return 7;
			default:
				return -1;
			}
		}

	}

	private List<Producer> _producers;
	private int _numProducers;
	private int _initResources;

	private int _numResources;
	private int _budget;

	private int _output;

	public MaintenanceTask(int numProducers, int initResources,
			int budgetAllowance) {
		_numProducers = numProducers;
		_initResources = initResources;
		_budget = budgetAllowance;
		
		_producers = new ArrayList<Producer>(numProducers);
		for(int i=0;i<numProducers;i++){
			_producers.add(new Producer(ProducerStatus.GOOD_ON));
		}

		reset();
	}

	@Override
	public int[] getState() {
		int[] state = new int[1 + _numProducers];
		state[0] = _numResources;
		for (int i = 0; i < _numProducers; i++) {
			state[i + 1] = _producers.get(i).intState();
		}
		return state;
	}

	@Override
	public void execute(int[] action) {
		// b is the budget for this timestep
		int b = _budget;

		//
		// Apply the possibly actions to each producer
		//
		for (int i = 0; i < action.length; i++) {
			int a = action[i];
			Producer p = _producers.get(i);
			if (a == ACTION_TOGGLE) { // Turn a producer on/off
				p.toggleOnOff();
			} else if (a == ACTION_REPAIR) { // Repair a producer if there is
												// enough money in the budget
				if (b > REPAIR_COST) {
					p.status(ProducerStatus.GOOD_ON);
					b -= REPAIR_COST;
				}
			} else {
				// Do nothing
			}
		}

		// Assign all remaining budget to purchasing resources
		if (b > 0) {
			_numResources += b;
		}

		//
		// Compute the total output of the producers
		//
		_output = 0;
		for (Producer p : _producers) {
			if (p.isOn()) {
				if (_numResources > 0) {
					_numResources--;

					if (p.isStatusGood()) {
						if (Random.uniform() < GOOD_PRODUCTION_PROB) {
							_output++;
						}
					}
					if (p.isStatusFair()) {
						if (Random.uniform() < FAIR_PRODUCTION_PROB) {
							_output++;
						}
					}
					if (p.isStatusPoor()) {
						if (Random.uniform() < POOR_PRODUCTION_PROB) {
							_output++;
						}
					}
				}

				if (p.isStatusGood()) {
					if (Random.uniform() < GOOD_TO_FAIR_PROB) {
						p.status(ProducerStatus.FAIR_ON);
					}
				} else if (p.isStatusFair()) {
					if (Random.uniform() < FAIR_TO_POOR_PROB) {
						p.status(ProducerStatus.POOR_ON);
					}
				} else if (p.isStatusPoor()) {
					if (Random.uniform() < POOR_TO_BROKEN_PROB) {
						p.status(ProducerStatus.BROKEN_ON);
					}
				}
			}
		}
	}

	@Override
	public double evaluate() {
		return totalOutput();
	}

	@Override
	public void reset() {
		_output = 0;
		_numResources = _initResources;
		for (Producer p : _producers) {
			p.status(ProducerStatus.GOOD_ON);
		}
	}

	@Override
	public boolean isFinished() {
		return false;
	}

	/**
	 * The total number of products output by this factory during the last
	 * timestep.
	 * 
	 * @return the total number of products output by this factory during the
	 *         last timestep
	 */
	public int totalOutput() {
		return _output;
	}

	@Override
	public void draw(Graphics2D g, int width, int height) {
		// We need to display:
		// 1. The Budget
		// 2. The Resource Amount
		// 3. Producers' Conditions (age, repair, working/broken, on/off)
		int numCells = 2 + _numProducers;
		int cwidth = (int) (width / (double) numCells);

		// Draw the resource amount
		g.setColor(Color.BLACK);
		Rectangle2D rrect = new Rectangle2D.Double(cwidth, 0, cwidth, height);
		g.draw(rrect);
		g.setColor(Color.BLUE);
		g.drawString(String.valueOf(_numResources), cwidth, height / 2);

		// Draw the producers
		for (int i = 0; i < _producers.size(); i++) {
			int x = cwidth * i;
			Producer p = _producers.get(i);
			Rectangle2D prect = new Rectangle2D.Double(x, 0, cwidth, height);

			g.setColor(p.color());
			g.fill(prect);

			g.setColor(Color.BLACK);
			g.drawString(p.status().toString(), x, height / 2);
			g.draw(prect);
		}

		// Draw the reward bar.
		int x = (2 + _numProducers) * cwidth;
		int y = height
				- (int) (height * (totalOutput() / (double) _numProducers));
		Rectangle2D rewardRect = new Rectangle2D.Double(x, y, width - x, height
				- y);
		g.setColor(Color.BLUE);
		g.fill(rewardRect);
	}

	public static void main(String[] args) {
		final int numProducers = 10;
		int initResources = 0;
		int budget = numProducers;
		
		LearningSystem<int[], int[]> agent = new LearningSystem<int[], int[]>() {

			@Override
			public int[] policy(int[] state) {
				int[] action = new int[numProducers];
				for (int i = 0; i < action.length; i++) {
					action[i] = Random.nextInt(NUM_ACTIONS);
				}
				return action;
			}

			@Override
			public void train(int[] prevState, int[] action, int[] newState,
					double reinforcement) {
			}

			@Override
			public void record(DataStore dstore, int episodeNumber,
					int episodeTimestep) {
			}

		};
		
		MaintenanceTask task = new MaintenanceTask(numProducers, initResources, budget);
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(800,100);
		Task.runTask(task, agent, 1000, 1000, false, null, frame);
	}

}
