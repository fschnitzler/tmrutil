package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.util.Interval;

public class FactoredCursorSortTask extends DiscreteActionTask<FactoredState> implements Drawable
{
	private CursorSortTask _task;
	private int[] _maxVals;
	
	public FactoredCursorSortTask(int numElms)
	{
		super(CursorSortTask.NUM_ACTIONS);
		_task = new CursorSortTask(numElms);
		_maxVals = new int[numElms + 1];
		for(int i=0;i<numElms;i++){
			_maxVals[i] = numElms;
		}
		_maxVals[numElms] = numElms-1;
	}

	@Override
	public FactoredState getState()
	{
		int[] istate = _task.getState();
		return new FactoredStateImpl(istate, _maxVals);
	}

	@Override
	public void execute(Integer action)
	{
		_task.execute(action);
	}

	@Override
	public double evaluate()
	{
		return _task.evaluate();
	}

	@Override
	public void reset()
	{
		_task.reset();	
	}

	@Override
	public boolean isFinished()
	{
		return _task.isFinished();
	}
	
	public int numberOfStates()
	{
		FactoredState fstate = getState();
		int numStates = 1;
		for(int i=0;i<fstate.size();i++){
			numStates = numStates * fstate.choices(i);
		}
		return numStates;
	}
	
	public Interval rewardInterval()
	{
		return new Interval(0, 1);
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_task.draw(g, width, height);	
	}

}
