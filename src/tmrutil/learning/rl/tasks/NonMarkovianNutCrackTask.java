package tmrutil.learning.rl.tasks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.DiscreteIncrementalPolicySearch;
import tmrutil.learning.rl.DiscreteTask;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.QLearning;
import tmrutil.learning.rl.RMax;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.util.DebugException;
import tmrutil.util.Interval;

public class NonMarkovianNutCrackTask extends DiscreteTask implements Drawable
{
		
	private static final int NUMBER_OF_STATES = 16;

	private static final int LEFT_OF_NUT_AND_LEFT_OF_ROCK = 0;
	private static final int LEFT_OF_NUT_AND_ABOVE_ROCK = 1;
	private static final int LEFT_OF_NUT_AND_RIGHT_OF_ROCK = 2;
	private static final int LEFT_OF_NUT_AND_HOLDING_ROCK = 3;

	private static final int ABOVE_NUT_AND_LEFT_OF_ROCK = 4;
	private static final int ABOVE_NUT_AND_ABOVE_ROCK = 5;
	private static final int ABOVE_NUT_AND_RIGHT_OF_ROCK = 6;
	private static final int ABOVE_NUT_AND_HOLDING_ROCK = 7;

	private static final int RIGHT_OF_NUT_AND_LEFT_OF_ROCK = 8;
	private static final int RIGHT_OF_NUT_AND_ABOVE_ROCK = 9;
	private static final int RIGHT_OF_NUT_AND_RIGHT_OF_ROCK = 10;
	private static final int RIGHT_OF_NUT_AND_HOLDING_ROCK = 11;

	private static final int HOLDING_NUT_AND_LEFT_OF_ROCK = 12;
	private static final int HOLDING_NUT_AND_ABOVE_ROCK = 13;
	private static final int HOLDING_NUT_AND_RIGHT_OF_ROCK = 14;
	private static final int NUT_CRACKED = 15;

	private static final int X = 0;
	private static final int Y = 1;
	private static final int NUT_INDEX = 0;
	private static final int ROCK_INDEX = 1;
	private static final int AGENT_INDEX = 2;

	private NutCrackDriver _driver;

	public NonMarkovianNutCrackTask(int size)
	{
		super(NUMBER_OF_STATES, NutCrackDriver.NUMBER_OF_ACTIONS, NutCrackDriver.REWARD_INTERVAL);
		_driver = new NutCrackDriver(size);
	}

	@Override
	public void reset()
	{
		_driver.reset();
	}

	@Override
	public void execute(Integer action)
	{
		_driver.execute(action);
	}

	@Override
	public Integer getState()
	{
		if (_driver.isNutCracked()) {
			return NUT_CRACKED;
		}
		Point nut = _driver.whereIsNut();
		int nutX = nut.x;
		int nutY = nut.y;
		Point rock = _driver.whereIsRock();
		int rockX = rock.x;
		int rockY = rock.y;
		Point agent = _driver.whereIsAgent();
		int agentX = agent.x;

		if (agentX < nutX) {
			if (agentX < rockX) {
				return LEFT_OF_NUT_AND_LEFT_OF_ROCK;
			} else if (agentX > rockX) {
				return LEFT_OF_NUT_AND_RIGHT_OF_ROCK;
			} else {
				if (rockY > 0) {
					return LEFT_OF_NUT_AND_HOLDING_ROCK;
				} else {
					return LEFT_OF_NUT_AND_ABOVE_ROCK;
				}
			}
		} else if (agentX > nutX) {
			if (agentX < rockX) {
				return RIGHT_OF_NUT_AND_LEFT_OF_ROCK;
			} else if (agentX > rockX) {
				return RIGHT_OF_NUT_AND_RIGHT_OF_ROCK;
			} else {
				if (rockY > 0) {
					return RIGHT_OF_NUT_AND_HOLDING_ROCK;
				} else {
					return RIGHT_OF_NUT_AND_ABOVE_ROCK;
				}
			}
		} else {
			if (nutY > 0) {
				if (agentX < rockX) {
					return HOLDING_NUT_AND_LEFT_OF_ROCK;
				} else if (agentX > rockX) {
					return HOLDING_NUT_AND_RIGHT_OF_ROCK;
				} else {
					if (rockY > 0) {
						throw new DebugException(
								"An agent cannot hold a rock and nut at the same time!");
					} else {
						return HOLDING_NUT_AND_ABOVE_ROCK;
					}
				}
			} else {
				if (agentX < rockX) {
					return ABOVE_NUT_AND_LEFT_OF_ROCK;
				} else if (agentX > rockX) {
					return ABOVE_NUT_AND_RIGHT_OF_ROCK;
				} else {
					if (rockY > 0) {
						return ABOVE_NUT_AND_HOLDING_ROCK;
					} else {
						return ABOVE_NUT_AND_ABOVE_ROCK;
					}
				}
			}
		}
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_driver.draw(g, width, height);
	}

	@Override
	public double evaluate()
	{
		return _driver.evaluate();
	}

	@Override
	public boolean isFinished()
	{
		return _driver.isFinished();
	}

	/**
	 * Returns a collection of valid actions.
	 * @return a collection of valid actions
	 */
	public static Collection<Integer> generateActions()
	{		
		return NutCrackDriver.generateActions();
	}
	
	public static void main(String[] args){
		int size = 10;
		double learningRate = 0.05;
		double discountFactor = 0.95;
		
		int maxBlindExplore = 10;
		
		int numEvaluationSteps = 200;
		int maxPoliciesToSearch = 50;
		Double rewardThreshold = null;
		
		NonMarkovianNutCrackTask task = new NonMarkovianNutCrackTask(size);
		
		//LearningSystem<Integer,Integer> agent = new DiscreteIncrementalPolicySearch(task.numberOfStates(), task.numberOfActions(), numEvaluationSteps, Optimization.MAXIMIZE, maxPoliciesToSearch, rewardThreshold);
		//LearningSystem<Integer,Integer> agent = new QLearning(task.numberOfStates(), task.numberOfActions(), learningRate, discountFactor);
		LearningSystem<Integer,Integer> agent = new RMax(task.numberOfStates(), task.numberOfActions(), discountFactor, new Interval(0, 1), size);
		//LearningSystem<Integer,Integer> agent = new BlindFrog(task.numberOfStates(), task.numberOfActions(), discountFactor, new Interval(0, 1), size, maxBlindExplore);
		
		
		Task.runTask(task, agent, 1000, 1000, true, null, null, true);
		
		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(600, 200);
		Task.runTask(task, agent, -1, 1000, true, null, frame, false);
	}
}
