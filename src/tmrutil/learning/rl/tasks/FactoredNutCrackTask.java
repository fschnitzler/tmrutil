package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import tmrutil.graphics.Drawable;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.factored.DBNStructure;
import tmrutil.learning.rl.factored.DBNStructureImpl;
import tmrutil.learning.rl.factored.DBNTask;
import tmrutil.learning.rl.factored.FactoredState;
import tmrutil.learning.rl.factored.FactoredStateImpl;
import tmrutil.util.ArrayOps;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

public class FactoredNutCrackTask extends DBNTask implements Drawable
{
	private DBNStructureImpl _structImpl;
	private NutCrackDriver _driver;
	private int _size;

	public FactoredNutCrackTask(int size)
	{
		super(NutCrackDriver.NUMBER_OF_ACTIONS, NutCrackDriver.REWARD_INTERVAL);
		_driver = new NutCrackDriver(size);
		_size = size;
		
		int[] maxValues = {2, _size, _size, 2, _size, 2};
		Map<FactoredState,Map<Integer,Double>> rewardMap = new HashMap<FactoredState,Map<Integer,Double>>();
	
		int numStates = numStates(size);
		for(int s = 0; s < numStates; s++){
			int[] istate = ArrayOps.decode(s, maxValues);
			// If the nut is cracked, then add a reward
			if(istate[0] == 1){
				FactoredState fstate = new FactoredStateImpl(istate, maxValues);
				Map<Integer,Double> amap = new HashMap<Integer,Double>();
				for(int a=0;a<NutCrackDriver.NUMBER_OF_ACTIONS;a++){
					amap.put(a, NutCrackDriver.REWARD_INTERVAL.getMax());
				}
				rewardMap.put(fstate, amap);
			}
		}
		
		_structImpl = new DBNStructureImpl(maxValues, NutCrackDriver.NUMBER_OF_ACTIONS, new ReinforcementSignal.MapReinforcementSignal<FactoredState, Integer>(rewardMap));
	}
	
	private static final int numStates(int size)
	{
		return 8 * (size * size * size);
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_driver.draw(g, width, height);
	}

	@Override
	public DBNStructure getStructure()
	{
		return _structImpl;
	}

	@Override
	public FactoredState getState()
	{
		int isNutCracked = (_driver.isNutCracked())? 1 : 0;
		Point agent = _driver.whereIsAgent();
		Point rock = _driver.whereIsRock();
		Point nut = _driver.whereIsNut();
		
		int[] vstate = {isNutCracked, agent.x, rock.x, rock.y, nut.x, nut.y};
		int[] maxValues = {2, _size, _size, 2, _size, 2};
		FactoredState fstate = new FactoredStateImpl(vstate, maxValues);
		return fstate;
	}

	@Override
	public void execute(Integer action)
	{
		_driver.execute(action);
	}

	@Override
	public double evaluate()
	{
		return _driver.evaluate();
	}

	@Override
	public void reset()
	{
		_driver.reset();
	}

	@Override
	public boolean isFinished()
	{
		return _driver.isFinished();
	}

}
