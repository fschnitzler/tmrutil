package tmrutil.learning.rl.tasks;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import tmrutil.graphics.Drawable;
import tmrutil.learning.IncrementalFunctionApproximator;
import tmrutil.learning.LearningRate;
import tmrutil.learning.MultiFunctionApproximator;
import tmrutil.learning.StripCMAC;
import tmrutil.learning.rl.DiscreteActionTask;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.Task;
import tmrutil.learning.rl.continuous.DiscreteActionQLearning;
import tmrutil.util.Interval;

public class VelocityOnlyMountainCarTask extends DiscreteActionTask<double[]> implements Drawable
{
	private MountainCarTask _mcar = new MountainCarTask();

	public VelocityOnlyMountainCarTask()
	{
		super(MountainCarTask.NUM_ACTIONS);
	}
	
	@Override
	public double[] getState()
	{
		double[] state = {_mcar.getVelocity()};
		return state;
	}

	@Override
	public void execute(Integer action)
	{
		_mcar.execute(action);
	}

	@Override
	public double evaluate()
	{
		return _mcar.evaluate();
	}

	@Override
	public void reset()
	{
		_mcar.reset();
	}

	@Override
	public boolean isFinished()
	{
		return _mcar.isFinished();
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		_mcar.draw(g, width, height);
	}

	public static void main(String[] args)
	{
		double learningRate = 0.1;
		double epsilonGreedy = 0.05;
		
		int numActions = 3;
		double discountFactor = 0.95;

		VelocityOnlyMountainCarTask task = new VelocityOnlyMountainCarTask();
		int numFeatures = 1;

		List<IncrementalFunctionApproximator<double[],double[]>> qfuncs = new ArrayList<IncrementalFunctionApproximator<double[],double[]>>();
		for (int a = 0; a < numActions; a++) {
			//RealFunction f = new Compose(new
			// ConstantScale(1/(1-discountFactor),0), new HyperbolicTangent());
			//NNet nnet = new SingleLayerPerceptron(numFeatures, 1);
			// NNet nnet = new GeneralLinearNetwork(numFeatures, 1);
			//int numHidden = 3;
			//NNet nnet = new TwoLayerPerceptron(f, numFeatures, 1, numHidden);
			
			 
			List<IncrementalFunctionApproximator<double[],Double>> fs = new ArrayList<IncrementalFunctionApproximator<double[],Double>>();
			List<Interval> bounds = new ArrayList<Interval>();
			List<Integer> numBins = new ArrayList<Integer>();
			for(int i=0;i<numFeatures;i++){
				bounds.add(new Interval(-1, 1));
				numBins.add(10);
			}
			fs.add(new StripCMAC(bounds, numBins, 8, 1.0/(1-discountFactor)));
			IncrementalFunctionApproximator<double[],double[]> nnet = new MultiFunctionApproximator(fs);
			qfuncs.add(nnet);
		}
		LearningSystem<double[], Integer> agent = new DiscreteActionQLearning(
				numFeatures, qfuncs, new LearningRate.Constant(learningRate),
				discountFactor, epsilonGreedy);

		int maxEpisodeLength = 500;
		Task.runTask(task, agent, 10000, maxEpisodeLength, true, null, null, true);

		javax.swing.JFrame frame = new javax.swing.JFrame();
		frame.setSize(600, 200);
		Task.runTask(task, agent, -1, maxEpisodeLength, true, null, frame, false);
	}
}
