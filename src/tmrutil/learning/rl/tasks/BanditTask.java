package tmrutil.learning.rl.tasks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import tmrutil.learning.rl.Task;
import tmrutil.stats.Random;

/**
 * Bandit tasks are simple 1-state reinforcement learning problems where each
 * time step the agent must decide which lever (i.e. arm) of the machine to play
 * with the objective of maximizing long-term gain.
 * 
 * @author Timothy A. Mann
 * 
 * @param <A>
 *            the action type
 */
public class BanditTask<A> extends Task<Integer, A>
{
	public static class Static extends BanditTask<Integer>
	{
		public Static(List<Double> mean, List<Double> standardDeviations)
		{
			super(buildArms(mean, standardDeviations));
		}

		private static Map<Integer, RewardDistribution> buildArms(
				List<Double> mean, List<Double> std)
		{
			Map<Integer, RewardDistribution> arms = new HashMap<Integer, RewardDistribution>();
			for (int i = 0; i < mean.size(); i++) {
				RewardDistribution dist = new StaticRewardDistribution(
						mean.get(i), std.get(i));
				arms.put(i, dist);
			}
			return arms;
		}
	}

	/**
	 * Represents a possibly time dependent reward distribution.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	public static interface RewardDistribution
	{
		/**
		 * Samples a reward from this distribution.
		 * 
		 * @return a reward sample
		 */
		public double sample();

		/**
		 * Returns the current expected value of this reward distribution.
		 * 
		 * @return the current expected value of this reward distribution
		 */
		public double expectedValue();

		/**
		 * Updates this reward distribution based on the current timestep.
		 * 
		 * @param timestep
		 *            a discrete count of the number of times an arm has been
		 *            pulled
		 * @param tryCount
		 *            the number of times the reward distribution has been
		 *            sampled
		 */
		public void update(int timestep, int tryCount);
	}
	
	public static class StaticBernoulliRewardDistribution implements RewardDistribution
	{
		private double _p;
		
		public StaticBernoulliRewardDistribution(double p)
		{
			_p = p;
		}

		@Override
		public double sample()
		{
			double r = Random.uniform();
			return (r <= _p)? 1.0 : 0.0;
		}

		@Override
		public double expectedValue()
		{
			return _p;
		}

		@Override
		public void update(int timestep, int tryCount)
		{	
		}
		
		@Override
		public String toString()
		{
			return "p=" + _p;
		}
	}

	public static class StaticRewardDistribution implements RewardDistribution
	{
		private double _mu;
		private double _sigma;

		public StaticRewardDistribution(double mu, double sigma)
		{
			_mu = mu;
			_sigma = sigma;
		}

		@Override
		public double sample()
		{
			return Random.normal(_mu, _sigma);
		}

		@Override
		public double expectedValue()
		{
			return _mu;
		}

		@Override
		public void update(int timestep, int tryCount)
		{
			// Do nothing
		}

		@Override
		public String toString()
		{
			return "N(" + _mu + "," + _sigma + ")";
		}
	}

	private Map<A, RewardDistribution> _arms;
	private double _lastReward;
	private int _timestep;
	private Map<A, Integer> _armPullCount;

	public BanditTask(Map<A, RewardDistribution> arms)
	{
		_arms = new HashMap<A, RewardDistribution>(arms);
		_armPullCount = new HashMap<A, Integer>();
		reset();
	}

	/**
	 * Returns the number of arms of this multi-armed bandit task.
	 * 
	 * @return the number of actions/arms
	 */
	public int numberOfActions()
	{
		return _arms.size();
	}

	@Override
	public Integer getState()
	{
		return 0;
	}

	public int getTimestep()
	{
		return _timestep;
	}

	@Override
	public void execute(A action)
	{
		_timestep++;
		int count = pullCount(action);
		_armPullCount.put(action, count + 1);

		double reward = _arms.get(action).sample();
		_lastReward = reward;

		Set<A> actions = _arms.keySet();
		for (A a : actions) {

			_arms.get(a).update(_timestep, pullCount(a));
		}
	}

	/**
	 * Returns the number of times that the specified arm/lever has been played.
	 * 
	 * @param arm
	 *            an arm
	 * @return the number of times the specified arm has been played
	 */
	public int pullCount(A arm)
	{
		Integer count = _armPullCount.get(arm);
		if (count == null) {
			return 0;
		} else {
			return count.intValue();
		}
	}

	/**
	 * Returns a map from each arm to the current expected value of its
	 * associated reward distribution.
	 * 
	 * @return a map from each arm to expected values of their reward
	 *         distributions
	 */
	public Map<A, Double> expectedValues()
	{
		Map<A, Double> means = new HashMap<A, Double>();
		Set<A> arms = _arms.keySet();
		for (A arm : arms) {
			double mu = _arms.get(arm).expectedValue();
			means.put(arm, mu);
		}
		return means;
	}

	@Override
	public double evaluate()
	{
		return _lastReward;
	}

	@Override
	public void reset()
	{
		_lastReward = 0;
		_timestep = 0;
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}
	
	@Override
	public String toString()
	{
		StringBuffer buff = new StringBuffer( getClass().getSimpleName() + ": [");
		Set<A> arms = _arms.keySet();
		int i = 0;
		for(A arm : arms){
			if(i == arms.size()-1){
			buff.append(arm.toString() + ":" + _arms.get(arm));
			}else{
				buff.append(arm.toString() + ":" + _arms.get(arm) + ", ");
			}
			i++;
		}
		buff.append("]");
		
		return buff.toString();
	}
}
