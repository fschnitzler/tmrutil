package tmrutil.learning.rl.tasks;

import tmrutil.learning.rl.DiscreteActionTask;

/**
 * A class to see if we are in the Task goal
 * 
 * @author daniel
 *
 * @param <S>
 * @param <A>
 */
public abstract class GoalTask<S> extends DiscreteActionTask<S>{
	
	
	public GoalTask(int numActions){
		super(numActions);
	}
	/**
	 * Determines if a task is in the Task goal
	 * 
	 * @return
	 */
	public abstract boolean isInTaskGoal();

}
