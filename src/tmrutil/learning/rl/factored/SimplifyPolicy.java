package tmrutil.learning.rl.factored;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import tmrutil.learning.BinaryDecisionTree;
import tmrutil.learning.rl.BinaryDecisionTreeWrapperPolicy;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.LearningSystemWrapperPolicy;
import tmrutil.learning.rl.Policy;
import tmrutil.stats.Filter;
import tmrutil.stats.Statistics;
import tmrutil.util.IntArrayEnumeration;
import tmrutil.util.Pair;

/**
 * Takes as input an algorithm over a task with a factored state representation
 * and produces a new policy that is dependent on partial or relative state
 * information.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SimplifyPolicy {
	public static class PolicyDataSet {
		private Policy<FactoredState, Integer> _oldPolicy;
		private Collection<FactoredState> _consideredStates;

		public PolicyDataSet(Policy<FactoredState, Integer> oldPolicy,
				Collection<FactoredState> consideredStates) {
			_oldPolicy = oldPolicy;
			_consideredStates = consideredStates;
		}

		public PolicyDataSet(LearningSystem<FactoredState, Integer> oldPolicy,
				Collection<FactoredState> consideredStates) {
			_oldPolicy = new LearningSystemWrapperPolicy<FactoredState, Integer>(
					oldPolicy);
			_consideredStates = consideredStates;
		}

		public Integer policy(FactoredState state) {
			return _oldPolicy.policy(state);
		}

		public Collection<FactoredState> consideredStates() {
			return _consideredStates;
		}
	}

	/*
	public BinaryDecisionTreeWrapperPolicy<FactoredState> transform(
			LearningSystem<FactoredState, Integer> oldPolicy,
			int[] stateVariableChoices, int numActions) {
		return transform(
				new LearningSystemWrapperPolicy<FactoredState, Integer>(
						oldPolicy), stateVariableChoices, numActions);
	}

	public BinaryDecisionTreeWrapperPolicy<FactoredState> transform(
			LearningSystem<FactoredState, Integer> oldPolicy,
			int[] stateVariableChoices, int numActions,
			Collection<FactoredState> consideredStates) {
		return transform(
				new LearningSystemWrapperPolicy<FactoredState, Integer>(
						oldPolicy), stateVariableChoices, numActions,
				consideredStates);
	}

	public BinaryDecisionTreeWrapperPolicy<FactoredState> transform(
			Policy<FactoredState, Integer> oldPolicy,
			int[] stateVariableChoices, int numActions) {
		List<FactoredState> consideredStates = generateAllStates(stateVariableChoices);
		return transform(oldPolicy, stateVariableChoices, numActions,
				consideredStates);
	}
	

	public BinaryDecisionTreeWrapperPolicy<FactoredState> transform(
			Policy<FactoredState, Integer> policy, Map<Integer,Set<Integer>> stateVariableChoices,
			int numActions, Collection<FactoredState> consideredStates) {
		List<PolicyDataSet> dsets = new ArrayList<PolicyDataSet>();
		PolicyDataSet dset = new PolicyDataSet(policy, consideredStates);
		dsets.add(dset);
		return transform(dsets, stateVariableChoices, numActions);
	}
	*/
	
	

	public BinaryDecisionTreeWrapperPolicy<FactoredState> transform(
			Collection<PolicyDataSet> dataSets, Map<Integer,Set<Integer>> stateVariableChoices,
			int numActions) {

		List<Pair<FactoredState, Integer>> trainingSet = new ArrayList<Pair<FactoredState, Integer>>();

		for (PolicyDataSet pdset : dataSets) {
			for (FactoredState fstate : pdset.consideredStates()) {
				Integer action = pdset.policy(fstate);
				trainingSet
						.add(new Pair<FactoredState, Integer>(fstate, action));
			}
		}

		List<Filter<FactoredState>> splitRules = new ArrayList<Filter<FactoredState>>();
		
		for (final Integer fi : stateVariableChoices.keySet()) {
			//final int fi = i;
			// Create relational split rules
			for (final Integer fj : stateVariableChoices.keySet()) {
				//final int fj = j;
				splitRules.add(new Filter<FactoredState>() {
					@Override
					public boolean accept(FactoredState x) {
						return x.component(fi) > x.component(fj);
					}

					@Override
					public String toString() {
						return "x" + fi + "_gt_x" + fj;
					}
				});
				splitRules.add(new Filter<FactoredState>() {

					@Override
					public boolean accept(FactoredState x) {
						return x.component(fi) == x.component(fj);
					}

					@Override
					public String toString() {
						return "x" + fi + "_eq_x" + fj;
					}

				});
				splitRules.add(new Filter<FactoredState>() {

					@Override
					public boolean accept(FactoredState x) {
						return x.component(fi) < x.component(fj);
					}

					@Override
					public String toString() {
						return "x" + fi + "_lt_x" + fj;
					}
				});
			}
			// Create absolute split rules
			for (int v : stateVariableChoices.get(fi)) {
				final int fv = v;
				splitRules.add(new Filter<FactoredState>() {

					@Override
					public boolean accept(FactoredState x) {
						return x.component(fi) > fv;
					}

					@Override
					public String toString() {
						return "x" + fi + "_gt_" + fv;
					}

				});
				splitRules.add(new Filter<FactoredState>() {

					@Override
					public boolean accept(FactoredState x) {
						return x.component(fi) == fv;
					}

					@Override
					public String toString() {
						return "x" + fi + "_eq_" + fv;
					}
				});
				splitRules.add(new Filter<FactoredState>() {

					@Override
					public boolean accept(FactoredState x) {
						return x.component(fi) < fv;
					}

					@Override
					public String toString() {
						return "x" + fi + "_lt_" + fv;
					}

				});
			}
		}

		// Add self indexing rules
		splitRules.add(new Filter<FactoredState>() {

			@Override
			public boolean accept(FactoredState x) {
				int index = x.component(x.size() - 1);
				if (index < x.size() - 2) {
					return x.component(index) > x.component(index + 1);
				} else {
					return false;
				}
			}

			@Override
			public String toString() {
				return "xxend_gt_xxend_plus_1";
			}

		});
		splitRules.add(new Filter<FactoredState>() {

			@Override
			public boolean accept(FactoredState x) {
				int index = x.component(x.size() - 1);
				if (index < x.size() - 2) {
					return x.component(index) == x.component(index + 1);
				} else {
					return false;
				}
			}

			@Override
			public String toString() {
				return "xxend_eq_xxend_plus_1";
			}

		});
		splitRules.add(new Filter<FactoredState>() {

			@Override
			public boolean accept(FactoredState x) {
				int index = x.component(x.size() - 1);
				if (index < x.size() - 2) {
					return x.component(index) < x.component(index + 1);
				} else {
					return false;
				}
			}

			@Override
			public String toString() {
				return "xxend_lt_xxend_plus_1";
			}

		});

		BinaryDecisionTree<FactoredState> btree = new BinaryDecisionTree<FactoredState>(
				numActions, splitRules);
		btree.train(trainingSet);

		return new BinaryDecisionTreeWrapperPolicy<FactoredState>(btree);
	}

	public List<FactoredState> generateAllStates(int[] stateVariableChoices) {
		int numStates = Statistics.product(stateVariableChoices);
		List<FactoredState> allStates = new ArrayList<FactoredState>(numStates);

		List<Set<Integer>> values = new ArrayList<Set<Integer>>(
				stateVariableChoices.length);
		for (int i = 0; i < stateVariableChoices.length; i++) {
			Set<Integer> vs = new HashSet<Integer>();
			for (int j = 0; j < stateVariableChoices[i]; j++) {
				vs.add(j);
			}
			values.add(vs);
		}

		IntArrayEnumeration iaEnum = new IntArrayEnumeration(values);
		while (iaEnum.hasMoreElements()) {
			int[] svalues = iaEnum.nextElement();
			FactoredState fstate = new FactoredStateImpl(svalues,
					stateVariableChoices);
			allStates.add(fstate);
		}

		return allStates;
	}

}
