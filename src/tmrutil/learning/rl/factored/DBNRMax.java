package tmrutil.learning.rl.factored;

import java.util.HashSet;
import java.util.Set;

import tmrutil.learning.rl.MonteCarloPlanner;
import tmrutil.learning.rl.UCT;
import tmrutil.optim.Optimization;
import tmrutil.stats.Statistics;
import tmrutil.util.DataStore;
import tmrutil.util.Interval;

/**
 * An implementation of R-MAX that exploits the structure of a dynamic Bayesian
 * network for faster learning.
 * 
 * @author Timothy A. Mann
 * 
 */
public class DBNRMax extends FactoredStateLearningSystem
{
	private static final int MAX_ITERATIONS = 50;
	private static final double CONVERGENCE_THRESHOLD = 0.01;
	
	private static final int MAX_DEPTH = 30;
	private static final int NUM_SAMPLES = 100;
	
	private DBNEstimator _dbn;
	private Set<Integer> _actions;
	private double _rmax;
	
	private FactoredValueIteration _viter;
	MonteCarloPlanner<FactoredState,Integer> _planner;
	
	public DBNRMax(DBNStructure structure, Interval rewardInterval, double discountFactor, int numVisitsUntilKnown)
	{
		super(structure);
		_rmax = rewardInterval.getMax();
		_dbn = new DBNEstimator(structure, rewardInterval, discountFactor, numVisitsUntilKnown);
		
		_actions = new HashSet<Integer>();
		for(int a=0;a<structure.numberOfActions();a++){
			_actions.add(a);
		}
		
		_viter = new FactoredValueIteration(_dbn);
		_planner = null;
	}

	@Override
	public Integer policy(FactoredState state)
	{ 
		//_planner = new UCT<FactoredState,Integer>(_dbn, _actions, MAX_DEPTH, NUM_SAMPLES, _dbn.getDiscountFactor(), Optimization.MAXIMIZE, _rmax);
		//return _planner.policy(state);
		
		int s = state.uniqueID();
		return Statistics.maxIndex(_viter.getQ()[s]);
	}

	@Override
	public void train(FactoredState prevState, Integer action,
			FactoredState newState, double reinforcement)
	{
		boolean update = _dbn.addSample(prevState, action, newState, reinforcement);
		if(update){
			_viter.valueIteration(MAX_ITERATIONS, CONVERGENCE_THRESHOLD);
		}
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
	}
}
