package tmrutil.learning.rl.factored;

import tmrutil.learning.rl.LearningSystem;
import tmrutil.util.DataStore;

/**
 * Provides a wrapper around a discrete state-action learning system so that it can be used to learn in factored state tasks.
 * @author Timothy A. Mann
 *
 */
public class DiscreteLearningSystemFactoredWrapper implements
		LearningSystem<FactoredState, Integer>
{
	private LearningSystem<Integer,Integer> _agent;
	
	public DiscreteLearningSystemFactoredWrapper(LearningSystem<Integer,Integer> agent)
	{
		_agent = agent;
	}
	
	@Override
	public Integer policy(FactoredState state)
	{
		int istate = state.uniqueID();
		return _agent.policy(istate);
	}

	@Override
	public void train(FactoredState prevState, Integer action,
			FactoredState newState, double reinforcement)
	{
		int prevIState = prevState.uniqueID();
		int newIState = newState.uniqueID();
		_agent.train(prevIState, action, newIState, reinforcement);
	}

	@Override
	public void record(DataStore dstore, int episodeNumber,
			int episodeTimestep) {
		
	}

}
