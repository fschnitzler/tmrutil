package tmrutil.learning.rl.factored;

import java.util.Set;
import tmrutil.stats.Statistics;

/**
 * Implements value iteration for factored-state MDPs.
 * @author Timothy A. Mann
 *
 */
public class FactoredValueIteration
{
	private DBN _model;
	private double[] _V;
	private double[][] _Q;
	
	public FactoredValueIteration(DBN model)
	{
		_model = model;
		
		_V = new double[_model.numberOfStates()];
		_Q = new double[_model.numberOfStates()][_model.numberOfActions()];
	}
	
	public void valueIteration(int maxIterations, double threshold)
	{
		int numStates = _model.numberOfStates();
		int numActions = _model.numberOfActions();
		double gamma = _model.getDiscountFactor();
		
		boolean converged = false;
		for (int i = 0; i < maxIterations && !converged; i++) {

			double delta = 0;
			
			for(int s=0;s<numStates;s++){
				FactoredState state = _model.decode(s);
				Set<FactoredState> succs = _model.successors(state);
				
				for(int a=0;a<numActions;a++){
					double oldQ = _Q[s][a];
					double newQ = _model.reinforcement(state, a);
					for(FactoredState nextState : succs){
						double tprob = _model.transitionProb(state, a, nextState);
						int ns = nextState.uniqueID();
						newQ += tprob * gamma * _V[ns];
					}
					
					double diff = Math.abs(newQ - oldQ);
					delta = Math.max(delta, diff);
					_Q[s][a] = newQ;
				}
				
				_V[s] = Statistics.max(_Q[s]);
			}
			
			if (delta <= threshold) {
				converged = true;
			}
		}
	}
	
	public double[] getV()
	{
		return _V;
	}
	
	public double[][] getQ()
	{
		return _Q;
	}
}
