package tmrutil.learning.rl.factored;

/**
 * Represents the conditional probability table for a single action.
 * 
 * @author Timothy A. Mann
 *
 */
public abstract class CPT {

	/**
	 * Returns the probability of the <code>componentIndex</code> of
	 * <code>resultState</code> given the previous <code>state</code>.
	 * 
	 * @param state
	 *            the previous state
	 * @param resultState
	 *            the proposed next state
	 * @param componentIndex
	 *            the index of the component that we want to determine the
	 *            probability of
	 * @return the probability of observing the specified component from
	 *         <code>resultState</code> given the previous state
	 */
	public final double transProb(FactoredState state, FactoredState resultState,
			int componentIndex){
		return transProb(state, resultState.component(componentIndex), componentIndex);
	}
	
	/**
	 * Returns the probability of the <code>componentIndex</code> of the next state being <code>value</code> given the previous <code>state</code>.
	 * @param state a state
	 * @param value a value
	 * @param componentIndex a component of the next state 
	 * @return the probability of <code>value</code> at <code>componentIndex</code> in the next state given <code>state</code>
	 */
	public abstract double transProb(FactoredState state, int value, int componentIndex);

	/**
	 * Samples the next state according to this CPT model conditional on
	 * <code>state</code> being the previous state.
	 * 
	 * @param state
	 *            a state
	 * @return a new state
	 */
	public final FactoredState sample(FactoredState state){
		int[] vals = new int[state.size()];
		int[] maxVals = new int[state.size()];
		for(int i=0;i<vals.length;i++){
			vals[i] = sample(state, i);
			maxVals[i] = state.choices(i)-1;
		}
		return new FactoredStateImpl(vals, maxVals);
	}

	/**
	 * Samples a specific component for the next state according to this CPT
	 * model conditional on <code>state</code> being the previous state.
	 * 
	 * @param state
	 *            a state
	 * @param componentIndex
	 *            the index of the component of the next state to sample
	 * @return a value for the <code>componentIndex</code> of the next state
	 */
	public abstract int sample(FactoredState state, int componentIndex);
}
