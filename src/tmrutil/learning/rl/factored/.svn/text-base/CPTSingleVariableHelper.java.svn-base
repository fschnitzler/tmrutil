package tmrutil.learning.rl.factored;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;

/**
 * A helper class for estimating conditional probability tables.
 * @author Timothy A. Mann
 *
 */
public class CPTSingleVariableHelper
{
	private int _componentIndex;
	
	private Set<Integer> _parents;
	private Map<Integer,Map<Integer,Integer>> _valueCounts;
	private Map<Integer,Integer> _count;
	private int _numVisitsUntilKnown;
	
	public CPTSingleVariableHelper(int componentIndex, Set<Integer> parents, int numVisitsUntilKnown)
	{
		_componentIndex = componentIndex;
		_parents = new HashSet<Integer>(parents);
		_valueCounts = new HashMap<Integer,Map<Integer,Integer>>();
		_count = new HashMap<Integer,Integer>();
		_numVisitsUntilKnown = numVisitsUntilKnown;
	}
	
	public boolean addSample(FactoredState state, FactoredState resultState, double reinforcement)
	{
		boolean known = isKnown(state);
		
		int pID = state.uniqueID(_parents);
		int rVal = resultState.component(_componentIndex);
	
		Integer c = _count.get(pID);
		if(c == null){
			_count.put(pID, 1);
		}else{
			_count.put(pID, c.intValue() + 1);
		}
		
		Map<Integer,Integer> countMap = _valueCounts.get(pID);
		if(countMap == null){
			countMap = new HashMap<Integer,Integer>();
			countMap.put(rVal, 1);
			_valueCounts.put(pID, countMap);
		}else{
			Integer rValCount = countMap.get(rVal);
			if(rValCount == null){
				countMap.put(rVal, 1);
			}else{
				countMap.put(rVal, rValCount.intValue() + 1);
			}
		}
		
		return !known && isKnown(state);
	}
	
	public boolean isKnown(FactoredState state)
	{
		int pID = state.uniqueID(_parents);
		int m = count(pID);
		return (m >= _numVisitsUntilKnown);
	}
	
	public int generate(FactoredState state)
	{
		if(isKnown(state)){
			int pID = state.uniqueID(_parents);
			int m = count(pID);
			Map<Integer,Integer> countMap = _valueCounts.get(pID);
			double rand = Random.uniform();
			double sum = 0;
			Set<Integer> outcomes = countMap.keySet();
			for(Integer outcome : outcomes){
				sum = sum + (countMap.get(outcome) / (double) m);
				if(sum >= rand){
					return outcome;
				}
			}
			
			throw new DebugException("No outcome was returned (which should not happen).");
		}else{
			return state.component(_componentIndex);
		}
	}
	
	public Set<Integer> nonzeroProbabilityComponents(FactoredState state)
	{
		Set<Integer> vals = new HashSet<Integer>();
		if(isKnown(state)){
			int pID = state.uniqueID(_parents);
			Map<Integer,Integer> countMap = _valueCounts.get(pID);
			vals.addAll(countMap.keySet());
		}else{
			vals.add(state.component(_componentIndex));
		}
		return vals;
	}
	
	public double transProb(FactoredState state, FactoredState resultState)
	{
		int rVal = resultState.component(_componentIndex);
		int pID = state.uniqueID(_parents);
		
		if(count(pID) < _numVisitsUntilKnown){
			if(state.component(_componentIndex) == rVal){
				return 1;
			}else{
				return 0;
			}
		}else{
			int n = rValCount(pID, rVal);
			int m = count(pID);
			
			double p = n / (double) m;
			
			return p;
		}
	}
	
	private int rValCount(int pID, int rVal)
	{
		Map<Integer,Integer> countMap = _valueCounts.get(pID);
		if(countMap == null){
			return 0;
		}else{
			Integer c = countMap.get(rVal);
			if(c == null){
				return 0;
			}else{
				return c.intValue();
			}
		}
	}
	
	private int count(int pID){
		Integer c = _count.get(pID);
		if(c == null){
			return 0;
		}else{
			return c.intValue();
		}
	}
	
	public void reset()
	{
		_count.clear();
		_valueCounts.clear();
	}
}
