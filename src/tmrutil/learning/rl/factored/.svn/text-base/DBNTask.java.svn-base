package tmrutil.learning.rl.factored;

import tmrutil.learning.rl.Task;
import tmrutil.util.Interval;

/**
 * Represents a task that is factored and represented by a dynamic Bayesian network.
 * @author Timothy A. Mann
 *
 */
public abstract class DBNTask extends Task<FactoredState, Integer>
{
	private Interval _rewardInterval;
	
	public DBNTask(Interval rewardInterval)
	{
		_rewardInterval = rewardInterval;
	}

	/**
	 * Returns the structure of this dynamic Bayesian network.
	 * @return the structure of this dynamic Bayesian network
	 */
	public abstract DBNStructure getStructure();
	
	public double rmax()
	{
		return _rewardInterval.getMax();
	}
	
	public double rmin()
	{
		return _rewardInterval.getMin();
	}
}
