package tmrutil.learning.rl.factored;

/**
 * A factored action is an ordered list of <code>n</code> components. Each of the
 * <code>n</code> components can take one of a finite number of values.
 * 
 * @author Timothy A. Mann
 *
 */
public interface FactoredAction extends Factored {

}
