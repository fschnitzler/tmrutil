package tmrutil.learning.rl.factored;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import tmrutil.util.IntArrayEnumeration;

/**
 * A helper class for estimating conditional probability tables (CPTs) for a
 * dynamic Bayesian network.
 * 
 * @author Timothy A. Mann
 * 
 */
public class CPTHelper extends CPT
{
	private List<CPTSingleVariableHelper> _helpers;
	private int _numVisitsUntilKnown;

	public CPTHelper(Integer action, DBNStructure structure,
			int numVisitsUntilKnown)
	{
		_numVisitsUntilKnown = numVisitsUntilKnown;
		int n = structure.numberOfStateVariables();
		_helpers = new ArrayList<CPTSingleVariableHelper>(n);
		for (int i = 0; i < n; i++) {
			CPTSingleVariableHelper h = new CPTSingleVariableHelper(i,
					structure.parents(action, i), numVisitsUntilKnown);
			_helpers.add(h);
		}
	}

	public boolean addSample(FactoredState state, FactoredState resultState,
			double reinforcement)
	{
		boolean update = false;
		for (CPTSingleVariableHelper h : _helpers) {
			if(h.addSample(state, resultState, reinforcement)){
				update = true;
			}
		}
		return update;
	}
	
	public double transProb(FactoredState state, int value, int componentIndex){
		CPTSingleVariableHelper h = _helpers.get(componentIndex);
		return h.transProb(state, value);
	}

	public boolean isKnown(FactoredState state)
	{
		boolean known = true;
		for (int i = 0; i < state.size(); i++) {
			boolean iknown = isKnown(state, i);
			if (!iknown) {
				known = false;
				break;
			}
		}
		return known;
	}

	public boolean isKnown(FactoredState state, int componentIndex)
	{
		CPTSingleVariableHelper h = _helpers.get(componentIndex);
		return h.isKnown(state);
	}

	public int numberOfVisitsUntilKnown()
	{
		return _numVisitsUntilKnown;
	}

	public void reset()
	{
		for (CPTSingleVariableHelper h : _helpers) {
			h.reset();
		}
	}
	
	public int sample(FactoredState state, int componentIndex){
		CPTSingleVariableHelper h = _helpers.get(componentIndex);
		return h.generate(state);
	}
	
	public Set<FactoredState> generateSuccessors(FactoredState state)
	{
		Set<FactoredState> succs = new HashSet<FactoredState>();
		succs.add(state);
		
		int[] maxVals = new int[state.size()];
		List<Set<Integer>> valSets = new ArrayList<Set<Integer>>(state.size());
		for(int i=0;i<state.size();i++){
			maxVals[i] = state.choices(i)-1;
			CPTSingleVariableHelper h = _helpers.get(i);
			valSets.add(h.nonzeroProbabilityComponents(state));
		}
		
		IntArrayEnumeration ienum = new IntArrayEnumeration(valSets);
		for(int[] vals : ienum){
			succs.add(new FactoredStateImpl(vals, maxVals));
		}
		
		return succs;
	}
}
