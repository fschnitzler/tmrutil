package tmrutil.learning.rl.factored;

/**
 * A discrete set determines how to discretize an underlying interval or collection of objects into a finite set.
 * @author Timothy A. Mann
 *
 * @param <D> the type of the underlying interval or collection of objects
 * @param <E> the element type 
 */
public interface DiscreteSet<D,E>
{
	/**
	 * Maps each value in the underlying input set to an integer index.
	 * @param value a value in the input set
	 * @return an integer from <code>0</code> to <code>size()-1</code>
	 */
	public int index(D value);
	
	/**
	 * Maps each value in the underlying input set to an element in this discrete set.
	 * @param value a value in the input set
	 * @return an element in this discrete set
	 */
	public E element(D value);
	
	/**
	 * Returns the number of elements in this set.
	 * @return the number of elements in this set
	 */
	public int size();
}
