package tmrutil.learning.rl.tl;

public interface Library<K>
{
	public K relevantKnowledge(TaskDescription desc);
}
