package tmrutil.learning.rl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import tmrutil.learning.rl.smdp.OptionModel;
import tmrutil.optim.Optimization;

/**
 * Implements useful Dynamic Programming algorithms for MDPs with discrete state
 * and discrete action spaces.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S>
 *            the state type
 * @param <A>
 *            the action type
 */
public class DynamicProgramming<S, A> {

	private DiscreteMarkovDecisionProcess<S, A> _mdp;
	private Optimization _opType;

	public DynamicProgramming(DiscreteMarkovDecisionProcess<S, A> mdp,
			Optimization opType) {
		_mdp = mdp;
		_opType = opType;
	}

	/**
	 * Computes the max-norm-error over all states in the MDP between the two
	 * specified value functions.
	 * 
	 * @param vleft
	 *            a value function
	 * @param vright
	 *            a value function
	 * @return the max-norm-error between the two specified value functions
	 */
	public double maxError(VFunction<S> vleft, VFunction<S> vright) {
		return maxError(vleft, vright, _mdp.states());
	}

	/**
	 * Computes the max-norm-error over a subset of states between two specified
	 * value functions.
	 * 
	 * @param vleft
	 *            a value function
	 * @param vright
	 *            a value function
	 * @param states
	 *            a subset of the MDP's state space
	 * @return the max-norm-error between the two specified value functions (wrt
	 *         the subset of states)
	 */
	public double maxError(VFunction<S> vleft, VFunction<S> vright,
			Set<S> states) {
		double err = 0;
		for (S state : states) {
			double serr = Math.abs(vleft.value(state) - vright.value(state));
			err = Math.max(err, serr);
		}
		return err;
	}

	/**
	 * Computes the L1-norm-error between two specified value function over all
	 * states in the underlying MDP.
	 * 
	 * @param vleft
	 *            a value function
	 * @param vright
	 *            a value function
	 * @return the L1-norm-error between the two specified value functions
	 */
	public double l1Error(VFunction<S> vleft, VFunction<S> vright) {
		return l1Error(vleft, vright, _mdp.states());
	}

	public double l1Error(VFunction<S> vleft, VFunction<S> vright, Set<S> states) {
		double err = 0;
		for (S state : states) {
			err += Math.abs(vleft.value(state) - vright.value(state));
		}
		return err;
	}

	/**
	 * Computes a backup of the action-value function at a specific state-action
	 * pair.
	 * 
	 * @param state
	 *            a state
	 * @param action
	 *            an action
	 * @param vfunc
	 *            the previous estimate of the value function
	 * @return the result of the action-value backup
	 */
	public double valueUpdate(S state, A action, VFunction<S> vfunc) {
		double gamma = _mdp.getDiscountFactor();
		double r = _mdp.reinforcement(state, action);
		double vsum = 0;
		Set<S> nextStates = _mdp.successors(state);
		for (S nstate : nextStates) {
			double tprob = _mdp.transitionProb(state, action, nstate);
			vsum += tprob * vfunc.value(nstate);
		}
		double newV = r + gamma * vsum;
		return newV;
	}

	/**
	 * Computes a backup of the option-value function at a specified
	 * state-option pair.
	 * 
	 * @param state
	 *            a state
	 * @param omodel
	 *            an option model
	 * @param vfunc
	 *            the previous estimate of the value function
	 * @return the result of the option-value backup
	 */
	public double valueUpdate(S state, OptionModel<S> omodel, VFunction<S> vfunc) {
		double r = omodel.reinforcement(state);
		double vsum = 0;
		Set<S> tstates = omodel.successors(state);
		for (S tstate : tstates) {
			double tprob = omodel.transitionProb(state, tstate);
			vsum += tprob * vfunc.value(tstate);
		}
		double newV = r + vsum;
		return newV;
	}

	public double minValueUpdate(S state, VFunction<S> vfunc) {
		Double minV = null;
		Set<A> actions = _mdp.actions(state);
		for (A action : actions) {
			double v = valueUpdate(state, action, vfunc);
			if (minV == null || minV > v) {
				minV = v;
			}
		}
		return minV;
	}

	public double minValueUpdate(S state, VFunction<S> vfunc,
			Set<OptionModel<S>> options) {
		Double minV = null;

		for (OptionModel<S> omodel : options) {
			if (omodel.initStateSet().contains(state)) {
				double v = valueUpdate(state, omodel, vfunc);
				if (minV == null || minV > v) {
					minV = v;
				}
			}
		}
		return minV;
	}

	public double maxValueUpdate(S state, VFunction<S> vfunc) {
		Double maxV = null;
		Set<A> actions = _mdp.actions(state);
		for (A action : actions) {
			double v = valueUpdate(state, action, vfunc);
			if (maxV == null || maxV < v) {
				maxV = v;
			}
		}

		return maxV;
	}

	public double maxValueUpdate(S state, VFunction<S> vfunc,
			Set<OptionModel<S>> options) {
		Double maxV = null;

		for (OptionModel<S> omodel : options) {
			if (omodel.initStateSet().contains(state)) {
				double v = valueUpdate(state, omodel, vfunc);
				if (maxV == null || maxV < v) {
					maxV = v;
				}
			}
		}

		return maxV;
	}

	public VFunction<S> iteratePolicyEvaluation(StationaryPolicy<S, A> policy,
			VFunction<S> vfunc) {
		DiscreteVFunction<S> newV = new DiscreteVFunction<S>();
		Set<S> states = _mdp.states();
		for (S state : states) {
			if (policy.isDeterministic()) {
				A action = policy.policy(state);
				double val = valueUpdate(state, action, vfunc);
				newV.update(state, val);
			} else {
				Set<A> actions = _mdp.actions(state);
				double val = 0;
				for (A action : actions) {
					double aprob = policy.actionProb(state, action);
					if (aprob > 0.0) {
						double qv = valueUpdate(state, action, vfunc);
						val += aprob * qv;
					}
				}
				newV.update(state, val);
			}
		}
		return newV;
	}
	
	public StationaryPolicy<S,A> policyImprovement(VFunction<S> vfunc){
		Map<S,A> policy = new HashMap<S,A>();
		
		for(S state : _mdp.states()){
			A bestAction = null;
			double bestValue = 0;
			for(A action : _mdp.actions(state)){
				double value = valueUpdate(state, action, vfunc);
				if(bestAction == null || _opType.firstIsBetter(value, bestValue)){
					bestAction = action;
					bestValue = value;
				}
			}
			policy.put(state, bestAction);
		}
		
		return new MapPolicy<S,A>(policy);
	}

	public QFunction<S, A> iterateActionValueIteration(VFunction<S> vfunc) {
		return iterateActionValueIteration(vfunc, _mdp.states());
	}

	public QFunction<S, A> iterateActionValueIteration(VFunction<S> vfunc,
			Set<S> states) {
		DiscreteQFunction<S, A> qfunc = new DiscreteQFunction<S, A>(_opType);

		for (S state : states) {
			Set<A> actions = _mdp.actions(state);
			for (A action : actions) {
				double qval = valueUpdate(state, action, vfunc);
				qfunc.update(state, action, qval);
			}
		}

		return qfunc;
	}
	
	public VFunction<S> iterateValueIterationOverOptions(VFunction<S> vfunc, Set<OptionModel<S>> options)
	{
		return iterateValueIterationOverOptions(vfunc, options, _mdp.states());
	}

	public VFunction<S> iterateValueIterationOverOptions(VFunction<S> vfunc,
			Set<OptionModel<S>> options, Set<S> states) {
		DiscreteVFunction<S> newV = new DiscreteVFunction<S>();

		for (S state : states) {
			double v = 0;
			if (isMaximizing()) {
				v = maxValueUpdate(state, vfunc, options);
			} else {
				v = minValueUpdate(state, vfunc, options);
			}
			newV.update(state, v);
		}

		return newV;
	}
	
	public VFunction<S> iterateValueIterationOverPrimitivesAndOptions(VFunction<S> vfunc, Set<OptionModel<S>> options)
	{
		return iterateValueIterationOverPrimitivesAndOptions(vfunc, options, _mdp.states());
	}

	public VFunction<S> iterateValueIterationOverPrimitivesAndOptions(
			VFunction<S> vfunc, Set<OptionModel<S>> options, Set<S> states) {
		DiscreteVFunction<S> newV = new DiscreteVFunction<S>();
		for (S state : states) {
			double v = 0;
			double vOptions = 0;
			double vPrimitives = 0;
			if (isMaximizing()) {
				vPrimitives = maxValueUpdate(state, vfunc);
				vOptions = maxValueUpdate(state, vfunc, options);
				v = Math.max(vPrimitives, vOptions);
			} else {
				vPrimitives = minValueUpdate(state, vfunc);
				vOptions = minValueUpdate(state, vfunc, options);
				v = Math.min(vPrimitives, vOptions);
			}
			newV.update(state, v);
		}

		return newV;
	}

	public boolean isMaximizing() {
		return _opType.equals(Optimization.MAXIMIZE);
	}

	public boolean isMinimizing() {
		return !isMaximizing();
	}
}
