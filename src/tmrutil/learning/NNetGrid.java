package tmrutil.learning;

import java.util.Arrays;
import tmrutil.graphics.plot2d.Plot2D;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.Identity;
import tmrutil.stats.Statistics;

/**
 * A piecewise grid of neural networks used to approximate a function.
 * @author Timothy Mann
 *
 */
public class NNetGrid extends NNet
{

	private NNet[] _nnets;
	private int _totalNumNets;
	private int[] _dimDensities;
	private double[] _dimMins;
	private double[] _dimMaxs;
	
	/**
	 * Constructs a neural network grid of single-layer perceptrons.
	 * @param inputSize the input vector size
	 * @param outputSize the output vector size
	 * @param dimMins an array of minimum values for each input dimension
	 * @param dimMaxs an array of maximum values for each input dimension
	 * @param dimDensities an array determining the number of partitions for each dimension
	 */
	public NNetGrid(int inputSize, int outputSize, double[] dimMins, double[] dimMaxs, int[] dimDensities)
	{
		super(inputSize, outputSize);
		_dimMins = Arrays.copyOf(dimMins, dimMins.length);
		_dimMaxs = Arrays.copyOf(dimMaxs, dimMaxs.length);
		_dimDensities = Arrays.copyOf(dimDensities, dimDensities.length);
		
		_totalNumNets = Statistics.product(dimDensities);
		_nnets = new NNet[_totalNumNets];
		for(int i=0;i<_totalNumNets;i++){
			_nnets[i] = new SingleLayerPerceptron(new Identity(), inputSize, outputSize);
		}
	}
	
	@Override
	public int numberOfWeights()
	{
		int sumOfWeights = 0;
		for(int i=0;i<_nnets.length;i++){
			sumOfWeights += _nnets[i].numberOfWeights();
		}
		return sumOfWeights;
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		int srcPos = 0;
		for(int i=0;i<_nnets.length;i++){
			int numWeights = _nnets[i].numberOfWeights();
			double[] w = new double[numWeights];
			System.arraycopy(weights, srcPos, w, 0, numWeights);
			_nnets[i].setWeights(w);
			srcPos += numWeights;
		}
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		return _nnets[findNNet(input)].train(input, target, learningRate);
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		return _nnets[findNNet(x)].evaluate(x, result);
	}

	@Override
	public double[] evaluate(double[] x) throws IllegalArgumentException
	{
		return evaluate(x, new double[getOutputSize()]);
	}

	/**
	 * Finds the index of the neural network responsible for the portion of the grid that <code>x</code> belongs to.
	 * @param x a valid input vector
	 * @return the index of the neural network responsible for the portion of the grid that <code>x</code> belongs to
	 */
	private int findNNet(double[] x)
	{
		int[] indices = new int[getInputSize()];
		for(int d=0;d<getInputSize();d++){
			indices[d] = binarySearch(x[d], _dimMins[d], _dimMaxs[d], _dimDensities[d]);
		}
		
		int numNets = _totalNumNets;
		int index = 0;
		for(int d = 0; d < getInputSize(); d++){
			numNets = numNets / _dimDensities[d];
			index += numNets * indices[d];
		}
		
		return index;
	}
	
	/**
	 * Performs a binary search over the interval [<code>min</code>, <code>max</code>] where the interval
	 * is divided into <code>density</code> discrete, equal size portions.
	 * 
	 * @param x the value to look for
	 * @param min the minimum value
	 * @param max the maximum value
	 * @param density the number of discrete, equal size portions in the interval
	 * @return the lower index of the interval containing <code>x</code>
	 */
	private int binarySearch(double x, double min, double max, int density)
	{
		if (x < min) {
			return 0;
		} else if (x > max) {
			return density - 1;
		} else {
			double diff = max - min;
			double inc = diff / density;
			boolean notFound = true;
			int left = 0;
			int right = density;
			int index = 0;

			do {
				int middle = (right + left) / 2;
				double val = (inc * middle) + min;
				if (val > x) {
					if (right - left < 2) {
						index = left;
						notFound = false;
					}
					right = middle;
				} else {
					if (right - left < 2) {
						index = left;
						notFound = false;
					}
					left = middle;
				}
			} while (notFound);

			return index;
		}
	}
	
	public static void main(String[] args){
		double learningRate = 0.3;
		int numSamples = 1000;
		int numTraining = 200;
		int numEpochs = 50;
		
		double[] x = VectorOps.range(-1, 1, numSamples);
		double[] y = new double[x.length];
		for(int i=0;i<x.length;i++){
			y[i] = Math.sin(4 * x[i]);
		}
		
		int[] rperm = VectorOps.randperm(numSamples);
		double[] trainX = new double[numTraining];
		double[] trainY = new double[numTraining];
		for(int i=0;i<numTraining;i++){
			trainX[i] = x[rperm[i]];
			trainY[i] = y[rperm[i]];
		}
		
		NNet net = new NNetGrid(1, 1, new double[]{-1}, new double[]{1}, new int[]{6});
		for(int epoch = 0; epoch < numEpochs; epoch++){
			rperm = VectorOps.randperm(numTraining);
			for(int i=0;i<numTraining;i++){
				net.train(new double[]{trainX[rperm[i]]}, new double[]{trainY[rperm[i]]}, learningRate);
			}
		}
		
		double[] yhat = new double[numSamples];
		for(int i=0;i<numSamples;i++){
			yhat[i] = net.evaluate(new double[]{x[i]})[0];
		}
		
		Plot2D.linePlot(x, y, java.awt.Color.BLUE, x, yhat, java.awt.Color.RED, "Sin(x) vs. Grid NNet", "X", "Y");
	}

	@Override
	public NNetGrid newInstance() {
		return new NNetGrid(getInputSize(), getOutputSize(), _dimMins, _dimMaxs, _dimDensities);
	}
}
