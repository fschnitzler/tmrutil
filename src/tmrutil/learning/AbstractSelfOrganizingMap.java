package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import tmrutil.math.Function;
import tmrutil.math.VectorOps;
import tmrutil.optim.Optimization;
import tmrutil.stats.Statistics;
import tmrutil.util.IntArrayComparator;

/**
 * An abstract self-organizing map that is made up of a collection of nodes
 * arranged in a grid with dimensions specified at construction.
 * 
 * @author Timothy A. Mann
 * 
 * @param <N>
 *            the node type
 */
public abstract class AbstractSelfOrganizingMap<N> implements
		Function<double[], N>
{
	private List<N> _nodes;
	private List<int[]> _nodeCoords;
	private Map<int[], N> _nodeMap;
	private int[] _gridProperties;
	private Optimization _opType;

	/**
	 * An iterator class for iterating over the nodes of a self-organizing map.
	 * The purpose of this iterator is to ensure that all nodes are retrieved
	 * using the <code>getNode()</code> method.
	 * 
	 * @author Timothy A. Mann
	 * 
	 */
	private class NodeIterator implements Iterator<N>
	{
		Iterator<int[]> _iterator;

		public NodeIterator(Iterator<int[]> iterator)
		{
			_iterator = iterator;
		}

		@Override
		public boolean hasNext()
		{
			return _iterator.hasNext();
		}

		@Override
		public N next()
		{
			return getNode(_iterator.next());
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException(
					"Removing nodes is not supported by "
							+ getClass().getName());
		}
	}

	/**
	 * Constructs an abstract self-organizing map.
	 * 
	 * @param nodes
	 *            a list of nodes
	 * @param gridProperties
	 *            an array of integers where the length of the array is the
	 *            number of dimensions in the grid and each component specifies
	 *            the number of elements along each dimension of the grid
	 */
	public AbstractSelfOrganizingMap(List<N> nodes, int[] gridProperties, Optimization opType)
	{
		int product = Statistics.product(gridProperties);
		if (nodes.size() != product) {
			throw new IllegalArgumentException("The number of nodes ("
					+ nodes.size() + ") does not match the number required ("
					+ product
					+ ") to construct a grid with the specified dimensions : "
					+ VectorOps.toString(gridProperties));
		}
		_nodes = new ArrayList<N>(nodes);
		_nodeCoords = new ArrayList<int[]>(_nodes.size());
		_nodeMap = new TreeMap<int[], N>(new IntArrayComparator());
		_gridProperties = gridProperties;
		_opType = opType;

		// Construct the node coordinate mapping
		for (int i = 0; i < _nodes.size(); i++) {
			int[] coord = indexToCoord(i, gridProperties);
			_nodeCoords.add(coord);
			_nodeMap.put(coord, _nodes.get(i));
		}
	}

	/**
	 * Evaluates all nodes against the input vector <code>x</code>. The winning
	 * node of the competition is returned.
	 * 
	 * @param x
	 *            an input vector
	 * @return the winning node
	 * @throws IllegalArgumentException
	 *             if the dimension of the input vector is incorrect
	 */
	@Override
	public N evaluate(double[] x) throws IllegalArgumentException
	{
		return _nodeMap.get(compete(x));
	}

	/**
	 * Evaluates all nodes against the input vector <code>x</code>. The
	 * coordinate in the grid of the winning node is returned.
	 * 
	 * @param x
	 *            an input vector
	 * @return the coordinate in the grid of the winning node
	 * @throws IllegalArgumentException
	 *             if the dimension of the input vector is incorrect
	 */
	public int[] compete(double[] x) throws IllegalArgumentException
	{
		double[] activity = new double[_nodes.size()];
		for (int i = 0; i < _nodes.size(); i++) {
			activity[i] = evaluate(x, _nodes.get(i), i, _nodeCoords.get(i));
		}
		if(_opType == Optimization.MAXIMIZE){
			int maxInd = Statistics.maxIndex(activity);
			return _nodeCoords.get(maxInd);
		}else{
			int minInd = Statistics.minIndex(activity);
			return _nodeCoords.get(minInd);
		}
	}

	/**
	 * Evaluates the activity of a particular node given the input vector
	 * <code>x</code>.
	 * 
	 * @param x
	 *            an input vector
	 * @param node
	 *            a node
	 * @param nodeIndex
	 *            the index of the node in the list of nodes
	 * @param nodeCoord
	 *            the coordinate of the node in the grid
	 * @return the activity of the node given the input vector
	 * @throws IllegalArgumentException
	 *             if the input vector's dimensions are invalid
	 */
	protected abstract double evaluate(double[] x, N node, int nodeIndex,
			int[] nodeCoord) throws IllegalArgumentException;

	/**
	 * Returns a value between 0 and 1 denoting how close two nodes are from
	 * each other. A value of 1 indicates that the two nodes are the same, while
	 * a value of zero indicates that the two nodes are far apart in the grid.
	 * 
	 * @param nodeACoord
	 *            the index of a node
	 * @param nodeBCoord
	 *            the index of a node
	 * @return a value between 0 and 1
	 */
	protected abstract double neighborhoodFunction(int[] nodeACoord,
			int[] nodeBCoord);

	/**
	 * Implements the update rule for a single node in the self-organizing map.
	 * 
	 * @param updateNode
	 *            the node data to be updated
	 * @param updateCoord
	 *            the coordinate in the grid of the node to be updated
	 * @param winnerNode
	 *            the node data of the winner
	 * @param winnerCoord
	 *            the coordinate in the grid of the winning node
	 * @param neighborhoodValue
	 *            the value reported by the neighborhood function for the two
	 *            nodes
	 */
	protected abstract void updateRule(N updateNode, int[] updateCoord,
			N winnerNode, int[] winnerCoord, double neighborhoodValue);

	/**
	 * Trains this self-organizing map on a single sample.
	 * 
	 * @param x
	 *            an input vector
	 */
	public void train(double[] x)
	{
		int[] winnerCoord = compete(x);
		N winnerNode = getNode(winnerCoord);
		for (int i = 0; i < _nodes.size(); i++) {
			int[] updateCoord = _nodeCoords.get(i);
			N updateNode = getNode(updateCoord);
			double neighborhoodValue = neighborhoodFunction(updateCoord,
					winnerCoord);
			updateRule(updateNode, updateCoord, winnerNode, winnerCoord,
					neighborhoodValue);
		}
	}

	/**
	 * Returns the node data of the node stored at the specified coordinate.
	 * 
	 * @param coord
	 *            a coordinate in the grid
	 * @return the node data
	 */
	public N getNode(int[] coord)
	{
		return _nodeMap.get(coord);
	}

	/**
	 * Sets the node data at the specified coordinate.
	 * 
	 * @param coord
	 *            a coordinate in the grid
	 * @param node
	 *            the node data
	 */
	public void setNode(int[] coord, N node)
	{
		int index = coordToIndex(coord, _gridProperties);
		if (index < 0 || index > _nodes.size()) {
			throw new IndexOutOfBoundsException(
					"The specified coordinate is not in this SOM's grid.");
		}
		_nodeMap.put(coord, node);
		_nodes.set(index, node);
		_nodeCoords.set(index, coord);
	}

	/**
	 * Returns an iterator over the nodes of this map.
	 * 
	 * @return an iterator over the nodes of this map
	 */
	public Iterator<N> nodeIterator()
	{
		return new NodeIterator(coordIterator());
	}

	/**
	 * Returns an iterator over the coordinates of this map.
	 * 
	 * @return an iterator over the coordinates of this map
	 */
	public Iterator<int[]> coordIterator()
	{
		return _nodeMap.keySet().iterator();
	}

	/**
	 * Returns a deep copy of the grid properties array. Grid properties is an
	 * array of integers where the length of the array is the number of
	 * dimensions in the grid and each component specifies the number of
	 * elements along each dimension of the grid.
	 * 
	 * @return integer array containing the grid properties
	 */
	public int[] getGridProperties()
	{
		return Arrays.copyOf(_gridProperties, _gridProperties.length);
	}

	/**
	 * Returns the number of dimensions used by the self-organizing map grid.
	 * 
	 * @return the number of dimensions used by the grid
	 */
	public int numDimensions()
	{
		return _gridProperties.length;
	}

	/**
	 * Returns the total number of nodes in this map.
	 * 
	 * @return the total number of nodes in this map
	 */
	public int size()
	{
		return _nodes.size();
	}

	/**
	 * Given the grid properties vector and an index in a list of nodes, the
	 * coordinate in the grid for the corresponding node is generated.
	 * 
	 * @param index
	 *            an index in a list of nodes
	 * @param gridProperties
	 *            an array of integers where the length of the array is the
	 *            number of dimensions in the grid and each component specifies
	 *            the number of elements along each dimension of the grid
	 * @return the coordinate in the grid of the node at the corresponding
	 *         <code>index</code>
	 */
	public static final int[] indexToCoord(int index, int[] gridProperties)
	{
		int w = Statistics.product(gridProperties);
		int[] coords = new int[gridProperties.length];
		for (int i = 0; i < gridProperties.length; i++) {
			w = w / gridProperties[i];
			coords[i] = index / w;
			index = index % w;
		}
		return coords;
	}

	/**
	 * Given the grid properties vector and a coordinate in the grid, the index
	 * in the list of nodes is generated.
	 * 
	 * @param coord
	 *            a coordinate in the grid
	 * @param gridProperties
	 *            an array of integers where the length of the array is the
	 *            number of dimensions in the grid and each component specifies
	 *            the number of elements along each dimension of the grid
	 * @return the index in the list of nodes corresponding to the node at
	 *         coordinate <code>coord</code>
	 */
	public static final int coordToIndex(int[] coord, int[] gridProperties)
	{
		int w = Statistics.product(gridProperties);
		int index = 0;
		for (int i = 0; i < gridProperties.length; i++) {
			w = w / gridProperties[i];
			index += w * coord[i];
		}
		return index;
	}
}
