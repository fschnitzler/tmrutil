package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import libsvm.svm;
import libsvm.svm_print_interface;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.graphics.plot2d.LinePlot;
import tmrutil.learning.SVM.Kernel;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.util.DebugException;
import tmrutil.util.Pair;

/**
 * An implementation of Support Vector Regression based on the nu-SVR
 * implementation from LIBSVM.
 * 
 * @author Timothy A. Mann
 * 
 */
public class SVR implements BatchFunctionApproximator<RealVector, Double> {
	private static final long serialVersionUID = 4959796856044969064L;

	public static svm_print_interface SVM_PRINT_NULL = new svm_print_interface() {
		public void print(String s) {
		}
	};

	private libsvm.svm_parameter _params;
	private libsvm.svm_model _model;
	private Kernel _kernelType;

	private boolean _trained;
	private double _defaultValue;

	public SVR(Kernel kernelType, double nu, double defaultValue)
	{
		_kernelType = kernelType;
		_trained = false;
		_defaultValue = defaultValue;
		
		_params = new libsvm.svm_parameter();
		_params.svm_type = libsvm.svm_parameter.NU_SVR;
		
		switch(kernelType){
		case LINEAR:
			_params.kernel_type = libsvm.svm_parameter.LINEAR;
			break;
		case RBF:
			_params.kernel_type = libsvm.svm_parameter.RBF;
			break;
		case POLY:
			_params.kernel_type = libsvm.svm_parameter.POLY;
			break;
		case SIGMOID:
			_params.kernel_type = libsvm.svm_parameter.SIGMOID;
			break;
		default:
			_params.kernel_type = libsvm.svm_parameter.RBF;
		}
		_params.degree = 3;
		_params.gamma = 0.5;	// 1/num_features
		_params.coef0 = 0;
		_params.nu = nu;
		_params.cache_size = 100;
		_params.C = 1;
		_params.eps = 1e-3;
		_params.p = 0.1;
		_params.shrinking = 1;
		_params.probability = 0;
		_params.nr_weight = 0;
		_params.weight_label = new int[0];
		_params.weight = new double[0];
		
		libsvm.svm.svm_set_print_string_function(SVM_PRINT_NULL);
	}

	public Kernel kernelType() {
		return _kernelType;
	}

	/**
	 * Sets the parameters controlling RBF kernels.
	 * 
	 * Radial basis function kernels are evaluated as:<br/>
	 * <code>Math.exp(-gamma*(x_square[i]+x_square[j]-2*dot(x[i],x[j])))</code>
	 * 
	 * @param gamma
	 *            determines the width of the radial basis
	 */
	public void setRBFParams(double gamma) {
		_params.gamma = gamma;
	}

	/**
	 * Sets the parameters controlling polynomial kernels.
	 * 
	 * Polynomial kernels are evaluated as:<br/>
	 * <code>powi(gamma*dot(x[i],x[j])+coef0,degree)</code>
	 * 
	 * @param degree
	 *            the degree of the polynomial
	 * @param gamma
	 *            multiplicative parameter
	 * @param coef0
	 *            bias parameter
	 */
	public void setPolyParams(int degree, double gamma, double coef0) {
		_params.degree = degree;
		_params.gamma = gamma;
		_params.coef0 = coef0;
	}

	/**
	 * Sets the parameters controlling sigmoid kernels (which are not true
	 * kernels).
	 * 
	 * Sigmoid kernels are evaluated as:<br/>
	 * <code>Math.tanh(gamma*dot(x[i],x[j])+coef0)</code>
	 * 
	 * @param gamma
	 *            multiplicative parameter
	 * @param coef0
	 *            bias parameter
	 */
	public void setSigmoidParams(double gamma, double coef0) {
		_params.gamma = gamma;
		_params.coef0 = coef0;
	}

	@Override
	public void reset() {
		_trained = false;
	}

	@Override
	public Double evaluate(RealVector x) throws IllegalArgumentException {
		if (_trained) {
			double prediction = libsvm.svm.svm_predict(_model, fromVector(x));
			return prediction;
		}
		return _defaultValue;
	}

	@Override
	public BatchFunctionApproximator<RealVector, Double> newInstance() {
		return new SVR(_kernelType, _params.nu, _defaultValue);
	}

	@Override
	public void train(Collection<Pair<RealVector, Double>> trainingSet) {
		libsvm.svm_problem problem = new libsvm.svm_problem();
		int n = trainingSet.size();
		problem.l = n;
		problem.x = new libsvm.svm_node[n][];
		problem.y = new double[n];

		int index = 0;
		for (Pair<RealVector, Double> sample : trainingSet) {
			problem.x[index] = fromVector(sample.getA());
			problem.y[index] = sample.getB();
			index++;
		}

		String errorMsg = svm.svm_check_parameter(problem, _params);
		if (errorMsg != null) {
			throw new DebugException("Error while training "
					+ getClass().getSimpleName() + " : " + errorMsg);
		}

		_model = libsvm.svm.svm_train(problem, _params);
		_trained = true;
	}

	private libsvm.svm_node[] fromVector(RealVector x) {
		libsvm.svm_node[] xnode = new libsvm.svm_node[x.getDimension()];
		for (int i = 0; i < xnode.length; i++) {
			xnode[i] = new libsvm.svm_node();
			xnode[i].index = i;
			xnode[i].value = x.getEntry(i);
		}
		return xnode;
	}

	public static RealVector vectorFrom(Double x) {
		double[] dx = { x };
		return new ArrayRealVector(dx);
	}

	public static double func(double x, double sigma) {
		return 0.5 * (Math.sin(8 * x) + Random.normal(0, sigma));
	}

	public static void main(String[] args) {
		int numTrainSamples = 20;
		int numTestSamples = 100;
		double sigma = 0.2;
		List<Pair<RealVector, Double>> trainSet = new ArrayList<Pair<RealVector, Double>>(
				numTrainSamples);

		for (int i = 0; i < numTrainSamples; i++) {
			double x = Random.uniform();
			double y = func(x, sigma);
			trainSet.add(new Pair<RealVector, Double>(vectorFrom(x), y));
		}

		SVR alg = new SVR(Kernel.RBF, 0.5, 0);
		alg.setRBFParams(10);
		alg.train(trainSet);

		double[] xs = VectorOps.range(0, 1, numTestSamples);
		double[] yhats = new double[numTestSamples];
		double[] ys = new double[numTestSamples];
		for (int i = 0; i < numTestSamples; i++) {
			double x = xs[i];
			yhats[i] = alg.evaluate(vectorFrom(x));
			ys[i] = func(x, 0);
		}

		LinePlot lp = tmrutil.graphics.plot2d.Plot2D.linePlot(xs, ys,
				java.awt.Color.BLUE, "f(x)", "x", "y");
		lp.addData(xs, yhats, java.awt.Color.RED, "");
	}

}
