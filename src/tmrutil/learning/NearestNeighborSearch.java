package tmrutil.learning;

import java.util.Collection;
import java.util.Set;
import tmrutil.util.Pair;

/**
 * An interface for nearest neighbor algorithms over a finite set of data.
 * 
 * @author Timothy A. Mann
 * 
 * @param <T>
 *            the type of data elements stored
 */
public interface NearestNeighborSearch<K,V>
{
	/**
	 * Returns the single nearest neighbor to the specified element.
	 * 
	 * @param element
	 *            an element
	 * @return the nearest neighbor
	 */
	public K searchKey(K element);
	
	public Pair<K,V> searchPair(K element);
	
	public V searchValue(K element);

	/**
	 * Returns the K-nearest neighbors to the specified element.
	 * 
	 * @param element
	 *            an element
	 * @param k
	 *            the number of elements to return
	 * @return a set containing the K-nearest neighbors
	 */
	public Set<K> searchKey(K element, int k);
	
	public Set<Pair<K,V>> searchPair(K element, int k);
	
	public Set<V> searchValue(K element, int k);

	/**
	 * Returns a set containing all of the neighbors that fall within a
	 * specified distance to the element.
	 * 
	 * @param element
	 *            an element
	 * @param dist
	 *            represents the distance cutoff
	 * @return a set of elements that are within <code>dist</code> of the
	 *         argument <code>element</code>
	 */
	public Set<K> distSearchKey(K element, double dist);
	
	public Set<Pair<K,V>> distSearchPair(K element, double dist);
	
	public Set<V> distSearchValue(K element, double dist);

	/**
	 * Returns the distance between two elements according to the metric used
	 * for nearest neighbor search.
	 * 
	 * @param leftElm
	 *            an element
	 * @param rightElm
	 *            an element
	 * @return the distance between the left and right elements
	 */
	public double dist(K leftElm, K rightElm);

	/**
	 * Adds a collection of samples to this set or replaces all samples and
	 * updates the underlying data structures.
	 * 
	 * @param samples
	 *            a collection of samples
	 */
	public void update(Collection<Pair<K,V>> samples);

	/**
	 * Resets this nearest neighbor search algorithm by emptying the element set
	 * and discarding underlying data structures.
	 */
	public void reset();

	/**
	 * Returns the number of elements in this set.
	 * 
	 * @return the number of elements in this set
	 */
	public int size();
}
