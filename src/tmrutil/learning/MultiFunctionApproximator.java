package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MultiFunctionApproximator implements
		IncrementalFunctionApproximator<double[], double[]>
{
	private static final long serialVersionUID = -936924726953223360L;
	
	private List<IncrementalFunctionApproximator<double[],Double>> _fs;
	
	public MultiFunctionApproximator(Collection<IncrementalFunctionApproximator<double[],Double>> functionApproximators)
	{
		_fs = new ArrayList<IncrementalFunctionApproximator<double[],Double>>(functionApproximators);
	}

	@Override
	public void reset()
	{
		for(IncrementalFunctionApproximator<double[],Double> f : _fs){
			f.reset();
		}
	}

	@Override
	public double[] evaluate(double[] x) throws IllegalArgumentException
	{
		double[] output = new double[_fs.size()];
		
		for(int i=0; i<output.length;i++){
			IncrementalFunctionApproximator<double[],Double> f = _fs.get(i);
			output[i] = f.evaluate(x);
		}
		return output;
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		double[] error = new double[target.length];
		for(int i=0;i<_fs.size();i++){
			IncrementalFunctionApproximator<double[],Double> f = _fs.get(i);
			Double r = f.train(input, target[i], learningRate);
			if(r != null){
				error[i] = r.doubleValue();
			}
		}
		return error;
	}

	@Override
	public MultiFunctionApproximator newInstance() {
		List<IncrementalFunctionApproximator<double[],Double>> fs = new ArrayList<IncrementalFunctionApproximator<double[],Double>>(_fs.size());
		for(int i=0;i<_fs.size();i++){
			fs.add(_fs.get(i).newInstance());
		}
		return new MultiFunctionApproximator(fs);
	}

}
