package tmrutil.learning;

/**
 * A tiling is a technique used for course coding. It allows a continuous vector
 * to be transformed into a high dimensional binary vector.
 * 
 * @author Timothy Mann
 * 
 * @param <D>
 *            the type of data stored at each tile
 */
public interface Tiling<D>
{

	/**
	 * Given a vector <code>x</code> finds the tile in this hyperspace
	 * containing vector <code>x</code>.
	 * 
	 * @param x
	 *            a vector
	 * @return the coordinate of a tile in hyperspace
	 * @throws IllegalArgumentException
	 *             if the dimension of <code>x</code> is not supported by this
	 *             tiling
	 */
	public int[] findTile(double[] x) throws IllegalArgumentException;

	/**
	 * Given a vector <code>x</code> finds the tile in this hyperspace and
	 * returns the data stored at that tile.
	 * 
	 * @param x
	 *            a vector
	 * @return data stored at the tile that <code>x</code> is mapped to
	 * @throws IllegalArgumentException
	 *             if the dimension of <code>x</code> is not supported by this
	 *             tiling
	 */
	public D findTileData(double[] x) throws IllegalArgumentException;

	/**
	 * Given the coordinate of a tile in hyperspace the data stored at that tile
	 * is returned.
	 * 
	 * @param coord
	 *            a coordinate of a tile in hyperspace
	 * @return data stored the the tile that <code>coord</code> is mapped to
	 * @throws IllegalArgumentException
	 *             if the coordinate has the wrong dimensions or does not have
	 *             an associated tile
	 */
	public D findTileData(int[] coord) throws IllegalArgumentException;
}
