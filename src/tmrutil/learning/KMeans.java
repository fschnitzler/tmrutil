package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import tmrutil.math.Metric;
import tmrutil.stats.Random;

/**
 * Implementation of the K-Means clustering algorithm.
 * @author Timothy A. Mann
 *
 * @param <D> the data type
 */
public abstract class KMeans<D> implements Clustering<D>
{
	private Metric<D> _metric;
	private int _K;
	private int _maxIterations;
	
	List<D> _means;
	List<List<D>> _clusters;
	
	public KMeans(int K, Metric<D> metric, int maxIterations)
	{
		setK(K);
		if(metric == null){
			throw new NullPointerException("Cannot construct kmeans instance with null metric.");
		}
		_metric = metric;
		if(maxIterations < 1){
			throw new IllegalArgumentException("The maximum number of iterations cannot be less than 1.");
		}
		_maxIterations = maxIterations;
	}
	
	public int getK()
	{
		return _K;
	}
	
	public void setK(int K)
	{
		if(K < 1){
			throw new IllegalArgumentException("The number of clusters cannot be less than 1.");
		}
		_K = K;
		_means = new ArrayList<D>(K);
		_clusters = new ArrayList<List<D>>(K);
		for(int k=0;k<K;k++){
			_means.add(null);
			_clusters.add(new ArrayList<D>());
		}
	}

	@Override
	public void cluster(Collection<D> samples)
	{
		if(samples.size() < getK()){
			throw new IllegalArgumentException("Cannot cluster " + samples.size() + " elements into " + getK() + " clusters.");
		}else{
			initClusters(samples);
			for(int i=0;i<_maxIterations;i++){
				update(_clusters);
				assign(_means, samples);
			}
		}
	}
	
	/**
	 * Assigns each sample to an initial cluster.
	 * @param samples a collection of samples
	 */
	protected void initClusters(Collection<D> samples)
	{
		for(D sample : samples){
			int rk = Random.nextInt(getK());
			_clusters.get(rk).add(sample);
		}
	}

	/**
	 * Given a list of centroids, assigns each sample to its closest centroid.
	 * @param centroids a list of centroids
	 * @param samples a collection of samples
	 */
	protected void assign(List<D> centroids, Collection<D> samples)
	{
		// Clear the clusters
		for(int k=0;k<getK();k++){
			_clusters.get(k).clear();
		}
		
		// Assign each sample to the cluster with nearest centroid
		for(D sample : samples){
			int minC = 0;
			double minD = 0;
			for(int c=0;c<centroids.size();c++){
				double d = _metric.distance(centroids.get(c), sample);
				if(d < minD || c == 0){
					minD = d;
					minC = c;
				}
			}
			_clusters.get(minC).add(sample);
		}
	}
	
	/**
	 * Updates the centroids given a list of clusters.
	 * @param clusters a list of clusters
	 */
	protected void update(List<List<D>> clusters)
	{
		// Find the centroids of each cluster
		for(int k=0;k<getK();k++){
			D centroid = centroid(clusters.get(k));
			_means.set(k, centroid);
		}
	}
	
	/**
	 * Determines the centroid of a collection of samples.
	 * @param samples a collection of samples
	 * @return the centroid of the collection of samples
	 */
	public abstract D centroid(Collection<D> samples);

	@Override
	public Collection<D> getCluster(int k)
	{
		return _clusters.get(k);
	}

	@Override
	public D getClusterRepresentative(int k)
	{
		return _means.get(k);
	}

	@Override
	public int size()
	{
		return getK();
	}

	@Override
	public boolean inCluster(int k, D sample)
	{
		return _clusters.get(k).contains(sample);
	}

}
