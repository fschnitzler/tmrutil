package tmrutil.learning.ga;

import java.util.Arrays;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tmrutil.stats.Random;
import tmrutil.util.Interval;

/**
 * Genetic operators defined for arrays of intervals.
 * 
 * @author Timothy A. Mann
 * 
 */
public class IntervalArrayGeneticOperators implements
		GeneticOperators<Interval[]>
{
	public static final String INTERVAL_ARRAY_ELM = GAXML.NS
			+ ":INTERVAL_ARRAY";
	public static final String INTERVAL_ELM = GAXML.NS + ":INTERVAL";
	public static final String MIN_ATTR = GAXML.NS + ":MIN";
	public static final String MAX_ATTR = GAXML.NS + ":MAX";

	private double DEFAULT_INTERVAL_MIN = -1;
	private double DEFAULT_INTERVAL_MAX = 1;

	/** The dimension of valid interval arrays. */
	private int _dim;
	/** The mutation rate. */
	private double _mutationRate;
	/** The crossover rate. */
	private double _crossoverRate;
	/** The bounds on generated intervals. */
	private Interval _bounds;

	/**
	 * Constructs genetic operators for interval arrays.
	 * 
	 * @param dimension
	 *            the dimension of the arrays
	 * @param bounds
	 *            all generated intervals are subsets of this interval
	 * @param crossoverRate
	 *            the crossover rate (probability of crossover)
	 * @param mutationRate
	 *            the mutation rate (probability of mutation)
	 */
	public IntervalArrayGeneticOperators(int dimension, Interval bounds,
			double crossoverRate, double mutationRate)
	{
		if (dimension < 1) {
			throw new IllegalArgumentException(
					"Dimension must be a positive integer.");
		}
		if (crossoverRate < 0 || crossoverRate > 1) {
			throw new IllegalArgumentException(
					"The crossover rate must be a value between 0 and 1 (inclusive).");
		}
		if (mutationRate < 0 || mutationRate > 1) {
			throw new IllegalArgumentException(
					"The mutation rate must be a value between 0 and 1 (inclusive).");
		}
		_dim = dimension;
		_crossoverRate = crossoverRate;
		_mutationRate = mutationRate;
		_bounds = new Interval(bounds);
	}

	/**
	 * Constructs an object that handles genetic operations for arrays of
	 * intervals. The crossover and mutation rates are set to default values.
	 * 
	 * @param dimension
	 *            the dimension of the arrays
	 * @param bounds
	 *            all generated intervals are subsets of this interval
	 */
	public IntervalArrayGeneticOperators(int dimension, Interval bounds)
	{
		this(dimension, bounds, DEFAULT_CROSSOVER_RATE, DEFAULT_MUTATION_RATE);
	}

	@Override
	public Interval[] crossover(Interval[] p1, Interval[] p2)
	{
		Interval[] child = new Interval[_dim];
		if (Random.uniform() < _crossoverRate) {
			int r = Random.nextInt(_dim);
			for (int i = 0; i < r; i++) {
				child[i] = new Interval(p1[i]);
			}
			for (int i = r; i < _dim; i++) {
				child[i] = new Interval(p2[i]);
			}
		} else {
			for (int i = 0; i < _dim; i++) {
				child[i] = new Interval(p1[i]);
			}
		}
		return child;
	}

	@Override
	public Interval[] generate()
	{
		Interval[] genome = new Interval[_dim];
		for (int i = 0; i < _dim; i++) {
			genome[i] = randomInterval();
		}
		return genome;
	}

	@Override
	public Interval[] mutate(Interval[] p)
	{
		Interval[] child = Arrays.copyOf(p, p.length);
		for (int i = 0; i < _dim; i++) {
			if (Random.uniform() < _mutationRate) {
				child[i] = randomInterval();
			}
		}
		return child;
	}

	@Override
	public Element toXML(Interval[] t, Document document)
	{
		Element element = document.createElement(INTERVAL_ARRAY_ELM);
		for (int i = 0; i < t.length; i++) {
			Element intervalElm = document.createElement(INTERVAL_ELM);
			intervalElm.setAttribute(MIN_ATTR, String.valueOf(t[i].getMin()));
			intervalElm.setAttribute(MAX_ATTR, String.valueOf(t[i].getMax()));
			element.appendChild(intervalElm);
		}
		return element;
	}

	@Override
	public Interval[] fromXML(Element element) throws IllegalArgumentException
	{
		Interval[] genome = new Interval[_dim];
		NodeList children = element.getChildNodes();
		int ind = 0;
		try {
			for (int i = 0; i < children.getLength(); i++) {
				Node node = children.item(i);
				if (node instanceof Element) {
					Element intervalElm = (Element) node;
					double minVal = Double.parseDouble(intervalElm.getAttribute(MIN_ATTR));
					double maxVal = Double.parseDouble(intervalElm.getAttribute(MAX_ATTR));
					genome[ind++] = new Interval(minVal, maxVal);
				}
			}
		} catch (IndexOutOfBoundsException ex) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		if (ind != _dim) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		return genome;
	}

	/**
	 * Generates a random open interval.
	 * 
	 * @return an interval
	 */
	private Interval randomInterval()
	{
		double a = Random.uniform(DEFAULT_INTERVAL_MIN, DEFAULT_INTERVAL_MAX);
		double b = Random.uniform(DEFAULT_INTERVAL_MIN, DEFAULT_INTERVAL_MAX);
		double min = Math.min(a, b);
		double max = Math.max(a, b);
		return new Interval(min, max);
	}

}
