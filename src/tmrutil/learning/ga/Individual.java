package tmrutil.learning.ga;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tmrutil.xml.FromXML;
import tmrutil.xml.ToXML;

/**
 * Represents an individual of a population in a genetic algorithm.
 * 
 * @author Timothy A. Mann
 * 
 * @param <T>
 *            the genome type
 */
public class Individual<T> implements Comparable<Individual<T>>,
		ToXML<Individual<T>>, FromXML<Individual<T>>
{
	private double _fitness;
	private T _genome;
	private String _metaData;
	private GeneticOperators<T> _operators;

	/**
	 * Constructs an individual with genome and genetic operators without any
	 * fitness score yet. The fitness score is set to <code>Double.NaN</code>.
	 * 
	 * @param genome
	 *            the genome of this individual
	 * @param operators
	 *            a reference to gentic operators compatible with the genome
	 *            type
	 */
	public Individual(T genome, GeneticOperators<T> operators)
	{
		this(genome, Double.NaN, operators);
	}

	/**
	 * Constructs an individual with genome, fitness, and genetic operators.
	 * 
	 * @param genome
	 *            the genome for this individual
	 * @param fitness
	 *            the fitness score for this individual
	 * @param operators
	 *            a reference to genetic operators compatible with the genome
	 *            type
	 */
	public Individual(T genome, double fitness, GeneticOperators<T> operators)
	{
		_fitness = fitness;
		_genome = genome;
		_operators = operators;
		_metaData = "";
	}

	/**
	 * Returns the fitness of this individual.
	 * 
	 * @return the fitness of this individual
	 */
	public double getFitness()
	{
		return _fitness;
	}

	/**
	 * Sets the fitness associated with this individual.
	 * 
	 * @param fitness
	 *            a fitness value
	 */
	public void setFitness(double fitness)
	{
		_fitness = fitness;
	}

	/**
	 * Returns the genome of this individual.
	 * 
	 * @return the genome of this individual
	 */
	public T getGenome()
	{
		return _genome;
	}

	/**
	 * Sets the meta data string for this individual.
	 * 
	 * @param metaData
	 *            a string containing meta data about this individual
	 */
	public void setMetaData(String metaData)
	{
		_metaData = metaData;
	}

	/**
	 * Returns the meta data string for this individual.
	 * 
	 * @return the meta data string for this individual
	 */
	public String getMetaData()
	{
		return _metaData;
	}

	/**
	 * Compares this individual with another.
	 */
	@Override
	public int compareTo(Individual<T> t)
	{
		double fit = getFitness();
		double tfit = t.getFitness();
		if (fit > tfit) {
			return 1;
		} else if (fit < tfit) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * Implements the crossing over genetic operator variant.
	 * 
	 * @param parent
	 *            a parent
	 * @return a new individual
	 */
	public Individual<T> crossover(Individual<T> parent)
	{
		T p2chrom = parent.getGenome();
		T childChrom = _operators.crossover(_genome, p2chrom);
		return new Individual<T>(childChrom, _operators);
	}

	/**
	 * Mutates this individual to obtain a new individual.
	 * 
	 * @return a new individual
	 */
	public Individual<T> mutate()
	{
		T childChrom = _operators.mutate(_genome);
		return new Individual<T>(childChrom, _operators);
	}

	/**
	 * Returns the string representation of this individual.
	 */
	@Override
	public String toString()
	{
		return "(Fitness : " + _fitness + " Chromosome : " + _genome + ")";
	}

	@Override
	public Element toXML(Individual<T> individual, Document document)
	{
		Element element = document.createElement(GAXML.INDIVIDUAL_ELM);
		element.setAttribute(GAXML.FITNESS_ATTR, String.valueOf(getFitness()));
		element.appendChild(_operators.toXML(individual.getGenome(), document));
		return element;
	}

	@Override
	public Individual<T> fromXML(Element element)
			throws IllegalArgumentException
	{

		double fitness = 0;
		try {
			fitness = Double.parseDouble(element.getAttribute(GAXML.FITNESS_ATTR));
		} catch (NumberFormatException ex) {
			fitness = Double.NaN;
		}
		NodeList childNodes = element.getChildNodes();
		int numChildren = childNodes.getLength();
		Element chromosome = null;
		boolean found = false;
		for (int i = 0; i < numChildren; i++) {
			Node node = childNodes.item(i);
			if (node instanceof Element) {
				chromosome = (Element) node;
				found = true;
			}
		}
		if (found) {
			Individual<T> individual = new Individual<T>(
					_operators.fromXML(chromosome), fitness, _operators);
			return individual;
		} else {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
	}

}
