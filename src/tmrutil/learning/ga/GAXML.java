package tmrutil.learning.ga;

/**
 * A list of XML constants for the genetic algorithms package.
 * @author Timothy Mann
 *
 */
public interface GAXML
{
	/**
	 * The genetic algorithms XML namespace.
	 */
	public static final String NS = "GA";
	
	/**
	 * The genetic algorithms XML population element name.
	 */
	public static final String POPULATION_ELM = NS + ":POPULATION";
	/**
	 * The genetic algorithms XML individual element name.
	 */
	public static final String INDIVIDUAL_ELM = NS + ":INDIVIDUAL";
	
	/**
	 * The genetic algorithms XML fitness attribute for an individual.
	 */
	public static final String FITNESS_ATTR = NS + ":FITNESS";
	/**
	 * The genetic algorithms XML generation number attribute for a population.
	 */
	public static final String GENERATION_ATTR = NS + ":GENERATION";
}
