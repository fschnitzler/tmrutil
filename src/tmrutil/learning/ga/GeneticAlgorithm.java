/*
 * GeneticAlgorithm.java
 */

/* Package */
package tmrutil.learning.ga;

import java.awt.Container;
import java.awt.Window;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tmrutil.graphics.ProgressIterator;
import tmrutil.graphics.plot2d.LinePlot;
import tmrutil.graphics.plot2d.Plot2D;
import tmrutil.math.VectorOps;
import tmrutil.stats.Statistics;
import tmrutil.xml.XMLUtil;

/**
 * Implements an evolutionary search algorithm. An instance of this
 * {@link GeneticAlgorithm} class can be created by implementing the
 * {@link FitnessFunction} interface, the {@link GeneticOperators} interface,
 * and specifying a population size.
 * <p>
 * 
 * Each individual of the population is represented by an instance of
 * {@link Individual}, which is a wrapper containing the individual's chromosome
 * data and fitness score. Individuals can be retrieved from the population by
 * calling {@link GeneticAlgorithm#iterator}. This returns an iterator over the
 * entire population such that the individuals are retrieved in order of
 * decreasing fitness. The first individual has the highest fitness.
 * <p>
 * 
 * Because individual chromosome data is often implemented by an array of
 * doubles {@link DoubleArrayGeneticOperators} provides implementations of
 * genetic operators for chromosomes represented as arrays of doubles.
 * <p>
 * 
 * For an example see {@link GeneticAlgorithm#main}.
 */
public class GeneticAlgorithm<T> implements Iterable<Individual<T>>
{
	public static final double ELITE_PROPORTION = 0.25;
	public static final double DEFAULT_MUTATION_RATE = 0.02;
	public static final double DEFAULT_CROSSOVER_RATE = 0.5;

	/**
	 * The generation number.
	 */
	private int _generation;
	/**
	 * A list of the mean of the fitness scores for each generation.
	 */
	private List<Double> _meanFitnessHistory;
	/**
	 * A list of the standard deviations of the fitness scores for each
	 * generation.
	 */
	private List<Double> _stdFitnessHistory;

	/** The size of each generation. */
	private int _populationSize;

	/** A reference to the fitness function. */
	private FitnessFunction<T> _f;

	/**
	 * A list of individuals.
	 */
	List<Individual<T>> _population;

	/**
	 * Genetic operators for creating new individuals.
	 */
	GeneticOperators<T> _operators;

	/**
	 * Initializes the population.
	 */
	private final void init()
	{
		_generation = 1;
		_population = new ArrayList<Individual<T>>();
		for (int i = 0; i < _populationSize; i++) {
			Individual<T> individual = new Individual<T>(_operators.generate(),
					_operators);
			_population.add(individual);
		}
		updateFitness(_f);
	}

	/**
	 * Update the fitness score for each chromosome.
	 */
	private final void updateFitness(FitnessFunction<T> f)
	{
		List<Double> fitnesses = new ArrayList<Double>(_population.size());
		Iterator<Individual<T>> iter = _population.iterator();
		while (iter.hasNext()) {
			Individual<T> individual = iter.next();
			double fitness = f.evaluate(individual);
			fitnesses.add(fitness);
			individual.setFitness(fitness);
		}
		_meanFitnessHistory.add(Statistics.mean(fitnesses, true));
		_stdFitnessHistory.add(Statistics.std(fitnesses, true));
		Collections.sort(_population);
		Collections.reverse(_population);
	}

	/**
	 * Implements the selection strategy for choosing parents from the current
	 * population. This selection strategy is used when generating a new
	 * population (by selecting parents).
	 */
	public Individual<T> chooseParent()
	{
		// Choose the tournament size
		int k = (int) (_population.size() * ELITE_PROPORTION);
		double p = 0.8; // probability of choosing the best fitness individual
		int i = 0; // An index
		Iterator<Individual<T>> iter = _population.iterator();
		while (iter.hasNext() && i < k) {
			p = p * Math.pow(1 - p, i); // probability of choosing the next best
			// individual
			Individual<T> individual = iter.next();
			if (tmrutil.stats.Random.uniform() < p) {
				return individual;
			}
			i++;
		}

		return iter.next();
	}

	/**
	 * Evolves the population for <code>iterations</code> iterations using the
	 * fitness function <code>f</code>.
	 * 
	 * @param iterations
	 *            a positive number of iterations
	 */
	public final void evolve(int iterations)
	{
		evolve(iterations, null, false);
	}

	/**
	 * Evolves the population for <code>iterations</code> iterations using the
	 * fitness function <code>f</code>.
	 * 
	 * @param iterations
	 *            a positive number of iterations
	 * @param log
	 *            a print stream to log evolution information to
	 * @param displayProgress
	 *            true to display a progress bar; otherwise false will not
	 *            display anything
	 */
	public final void evolve(int iterations, PrintStream log,
			boolean displayProgress)
	{
		ProgressIterator piter = null;
		if (displayProgress) {
			piter = ProgressIterator.iterator(0, iterations, 1);
		}

		for (int iter = 0; iter < iterations; iter++) {
			if (log != null) {
				log.print("Evolving Generation : " + _generation);
			}

			List<Individual<T>> population = new ArrayList<Individual<T>>();
			int top10Percent = _population.size() / 10;
			// Add the top 10% from the last population to the new population
			for (int i = 0; i < top10Percent; i++) {
				population.add(_population.get(i));
			}
			// Generate random individuals for 10% of population
			for (int i = top10Percent; i < 2 * top10Percent; i++) {
				population.add(new Individual<T>(_operators.generate(),
						_operators));
			}
			// Generate the last 80% of the population through mutation and crossover
			for (int i = (2 * top10Percent); i < _population.size(); i++) {
				Individual<T> p1 = chooseParent();
				Individual<T> child = p1.crossover(chooseParent());
				child = child.mutate();
				population.add(child);
			}
			_population = population;
			updateFitness(_f);

			if (log != null) {
				log.println(", Max. Fitness = (" + maxFitness() + ")");
			}

			_generation++;
			if (piter != null) {
				piter.next();
			}
		}

		if (piter != null) {
			Container parentComp = piter.getTopLevelAncestor();
			if (parentComp instanceof Window) {
				Window parentWin = (Window) parentComp;
				parentWin.dispose();
			}
		}
	}

	/**
	 * Returns an iterator over the individuals sorted so that the first
	 * individual has the highest fitness score.
	 * 
	 * @return an iterator over the individuals
	 */
	@Override
	public final Iterator<Individual<T>> iterator()
	{
		Collections.sort(_population);
		Collections.reverse(_population);
		return _population.iterator();
	}
	
	/**
	 * Returns the mean fitness for each generation.
	 * @return a list of mean fitness for each generation
	 */
	public final List<Double> meanFitnessHistory()
	{
		return _meanFitnessHistory;
	}

	/**
	 * Returns the mean fitness of the current population of individuals.
	 * 
	 * @return average fitness of current population
	 */
	public final double meanFitness()
	{
		return _meanFitnessHistory.get(_meanFitnessHistory.size() - 1);
	}

	/**
	 * Returns the maximum fitness of the current population.
	 * 
	 * @return maximum fitness of the current population
	 */
	public final double maxFitness()
	{
		return _population.get(0).getFitness();
	}

	/**
	 * Plots the average fitness trend with standard deviation.
	 */
	public LinePlot plotFitness()
	{
		List<double[]> stdAbove = new ArrayList<double[]>(
				_meanFitnessHistory.size());
		List<double[]> stdBelow = new ArrayList<double[]>(
				_meanFitnessHistory.size());

		for (int i = 0; i < _meanFitnessHistory.size(); i++) {
			double mean = _meanFitnessHistory.get(i);
			double std = _stdFitnessHistory.get(i);
			stdAbove.add(new double[] { i, (mean + std) });
			stdBelow.add(new double[] { i, (mean - std) });
		}
		LinePlot plot = Plot2D.linePlot(_meanFitnessHistory,
				java.awt.Color.BLUE, "Average Fitness", "Generation",
				"Average Fitness");

		plot.addData(stdAbove, java.awt.Color.MAGENTA, "None");
		plot.addData(stdBelow, java.awt.Color.MAGENTA, "None");

		return plot;
	}
	
	/**
	 * Plots the average fitness trend.
	 * @return
	 */
	public LinePlot plotMeanFitness()
	{
		List<Double> x = new ArrayList<Double>(_meanFitnessHistory.size());
		for(int i=0;i<_meanFitnessHistory.size();i++){
			x.add(new Double(i));
		}
		LinePlot plot = Plot2D.linePlot(x, _meanFitnessHistory, java.awt.Color.BLUE, "Average Fitness", "Generation", "Fitness");
		return plot;
	}

	/**
	 * Saves the current population to an XML file.
	 * 
	 * @param filename
	 *            an XML file name
	 * @throws IOException
	 *             if an I/O error occurs while writing the XML file
	 */
	public void save(String filename) throws IOException
	{
		Document document = XMLUtil.createDocument();
		Element docElm = document.createElement(GAXML.POPULATION_ELM);
		docElm.setAttribute(GAXML.GENERATION_ATTR, String.valueOf(_generation));
		document.appendChild(docElm);
		Iterator<Individual<T>> iter = iterator();
		while (iter.hasNext()) {
			Individual<T> individual = iter.next();
			docElm.appendChild(individual.toXML(individual, document));
		}
		String xml = XMLUtil.toString(document);
		FileWriter fw = new FileWriter(filename);
		fw.write(xml);
		fw.close();
	}

	/**
	 * Loads a population from an XML file.
	 * 
	 * @param filename
	 *            an XML file name
	 * @throws IOException
	 *             if an I/O error occurs while reading the XML file
	 * @throws SAXException
	 *             if an error occurs while parsing the XML
	 */
	public void load(String filename) throws IOException, SAXException
	{
		Individual<T> defaultInd = new Individual<T>(_operators.generate(), 0,
				_operators);
		Document document = XMLUtil.parse(new File(filename));
		Element docElm = document.getDocumentElement();
		_generation = Integer.parseInt(docElm.getAttribute(GAXML.GENERATION_ATTR));
		NodeList individuals = docElm.getChildNodes();
		_population.clear();
		for (int i = 0; i < individuals.getLength(); i++) {
			Node node = individuals.item(i);
			if (node instanceof Element) {
				Element elm = (Element) node;
				_population.add(defaultInd.fromXML(elm));
			}
		}
	}

	/**
	 * Creates an instance of the evolutionary search algorithm.
	 * 
	 * @param f
	 *            the fitness function to use to determine the fitness score of
	 *            each chromosome.
	 * @param operators
	 *            genetic operators for the templated type
	 * @param populationSize
	 *            the size of each generation. Valid values are all positive
	 *            integers.
	 * @throws IllegalArgumentException
	 *             if any of its arguments are not positive.
	 */
	public GeneticAlgorithm(FitnessFunction<T> f,
			GeneticOperators<T> operators, int populationSize)
			throws IllegalArgumentException
	{
		if (populationSize < 1) {
			throw new IllegalArgumentException(
					"The population size must be positive. The given size is "
							+ populationSize);
		}

		_f = f;
		_operators = operators;
		_populationSize = populationSize;
		_meanFitnessHistory = new ArrayList<Double>(10000);
		_stdFitnessHistory = new ArrayList<Double>(10000);
		init();
	}

	/**
	 * An example of how GeneticAlgorithm can be used.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		// Create an instance of an anonymous subclass of FitnessFunction
		// This fitness function has higher output for vectors that are similar
		// to the vector truth
		FitnessFunction<double[]> ff = new FitnessFunction<double[]>() {
			double[] truth = new double[] { 0, 0.4, -0.6 };

			@Override
			public double evaluate(Individual<double[]> individual)
			{
				return Math.exp(-VectorOps.distance(individual.getGenome(),
						truth));
			}
		};

		// Create a genetic algorithm with double array representations for
		// chromosomes,
		// an instance of double array genetic operators, and
		// a population size of 200
		GeneticAlgorithm<double[]> ga = new GeneticAlgorithm<double[]>(ff,
				new DoubleArrayGeneticOperators(3, DEFAULT_CROSSOVER_RATE,
						DEFAULT_MUTATION_RATE), 200);

		// Evolve for 10 generations
		ga.evolve(10);

		// Test writing a population to an XML file
		System.out.println("Writing population to file ...");
		ga.save("test_pop.xml");
		// Test reading a population from an XML file
		System.out.println("Reading population from file ...");
		ga.load("test_pop.xml");
		// Evolve the population for 10 more generations
		ga.evolve(10);
		// Write the population to a file again
		ga.save("test_pop.xml");
	}
}