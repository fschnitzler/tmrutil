package tmrutil.learning.ga;

import java.util.Arrays;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tmrutil.stats.Random;

/**
 * Genetic operators for chromosomes represented by an array of
 * <code>int</code> elements.
 * 
 * @author Timothy Mann
 * 
 */
public class IntArrayGeneticOperators implements GeneticOperators<int[]>
{
	
	public static final String INT_ARRAY_ELM = GAXML.NS + ":INT_ARRAY";
	public static final String INT_ELM = GAXML.NS + ":INT";
	public static final String VALUE_ATTR = GAXML.NS + ":VALUE";

	/**
	 * The number of elements of the double array.
	 */
	private int _dim;
	/**
	 * The probability of crossover.
	 */
	private double _cr;
	/**
	 * The probability of mutation.
	 */
	private double _mr;
	
	private int _maxVal;
	private int _minVal;
	
	/**
	 * Constructs an object that handles genetic operations on
	 * <code>int</code> arrays.
	 * 
	 * @param dim
	 *            the number of elements in a valid genome
	 * @param minVal the minimum value of an integer in a valid genome
	 * 
	 * @param maxVal the maximum value of an integer in a valid genome
	 * 
	 * @param crossoverRate
	 *            a value in [0,1] indicating the crossover rate
	 * @param mutationRate
	 *            a value in [0,1] indicating the mutation rate
	 */
	public IntArrayGeneticOperators(int dim, int minVal, int maxVal, double crossoverRate, double mutationRate)
	{
		if(dim < 1){
			throw new IllegalArgumentException("Cannot construct genetic operators for arrays with less than 1 dimension.");
		}
		
		_dim = dim;
		
		
		if(_minVal > _maxVal){
			throw new IllegalArgumentException("The minimum value is greater than the maximum value specified.");
		}
		_minVal = minVal;
		_maxVal = maxVal;
		
		if(crossoverRate < 0 || crossoverRate > 1){
			throw new IllegalArgumentException("The crossover rate must be within interval [0, 1].");
		}
		if(mutationRate < 0 || mutationRate > 1){
			throw new IllegalArgumentException("The mutation rate must be within interval [0,1].");
		}
		
		_cr = crossoverRate;
		_mr = mutationRate;
	}
	
	public IntArrayGeneticOperators(int dim, int minVal, int maxVal)
	{
		this(dim, minVal, maxVal, DEFAULT_CROSSOVER_RATE, DEFAULT_MUTATION_RATE);
	}

	@Override
	public Element toXML(int[] t, Document document)
	{
		Element element = document.createElement(INT_ARRAY_ELM);
		for (int i = 0; i < t.length; i++) {
			Element intElm = document.createElement(INT_ELM);
			intElm.setAttribute(VALUE_ATTR, String.valueOf(t[i]));
			element.appendChild(intElm);
		}
		return element;
	}

	@Override
	public int[] fromXML(Element element) throws IllegalArgumentException
	{
		int[] t = new int[_dim];
		NodeList children = element.getChildNodes();
		int ind = 0;
		try {
			for (int i = 0; i < children.getLength(); i++) {
				Node node = children.item(i);
				if (node instanceof Element) {
					Element intElm = (Element) node;
					t[ind++] = Integer.parseInt(intElm.getAttribute(VALUE_ATTR));
				}
			}
		} catch (IndexOutOfBoundsException ex) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		if (ind != _dim) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		return t;
	}

	@Override
	public int[] crossover(int[] p1, int[] p2)
	{
		if(p1.length != _dim || p2.length != _dim){
			throw new IllegalArgumentException("The length of both parent genomes must be " + _dim + ".");
		}
		int[] child = new int[p1.length];
		if (tmrutil.stats.Random.uniform() < _cr) {
			for(int i=0;i<child.length;i++){
				if(Random.nextBoolean()){
					child[i] = p1[i];
				}else{
					child[i] = p2[i];
				}
			}
		} else {
			System.arraycopy(p1, 0, child, 0, p1.length);
		}
		return child;
	}

	@Override
	public int[] mutate(int[] p)
	{
		if(p.length != _dim){
			throw new IllegalArgumentException("The length of the parent genome must be " + _dim + ".");
		}
		int[] child = Arrays.copyOf(p, p.length);
		for (int i = 0; i < p.length; i++) {
			if (tmrutil.stats.Random.uniform() < _mr) {
				child[i] = generateValue();
			}
		}
		return child;
	}

	@Override
	public int[] generate()
	{
		int[] individual = new int[_dim];
		for(int i=0;i<individual.length;i++){
			individual[i] = generateValue();
		}
		return individual;
	}
	
	private int generateValue()
	{
		return Random.nextInt(_maxVal - _minVal) + _minVal;
	}

}
