package tmrutil.learning.ga;

import java.util.Arrays;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.util.Interval;

/**
 * Genetic operators for chromosomes represented by an array of
 * <code>double</code> elements.
 * 
 * @author Timothy Mann
 * 
 */
public class DoubleArrayGeneticOperators implements GeneticOperators<double[]>
{
	public static final String DOUBLE_ARRAY_ELM = GAXML.NS + ":DOUBLE_ARRAY";
	public static final String DOUBLE_ELM = GAXML.NS + ":DOUBLE";
	public static final String VALUE_ATTR = GAXML.NS + ":VALUE";

	/**
	 * The number of elements of the double array.
	 */
	private int _dim;
	/**
	 * The probability of crossover.
	 */
	private double _cr;
	/**
	 * The probability of mutation.
	 */
	private double _mr;

	/**
	 * Determines the minimum and maximum generated weights.
	 */
	private Interval _bounds;

	/**
	 * Constructs an object that handles genetic operations on
	 * <code>double</code> arrays.
	 * 
	 * @param dim
	 *            the number of elements in a valid genome
	 * @param bounds
	 *            an interval determining the minimum and maximum values when a
	 *            <code>double</code> is generated
	 * @param crossoverRate
	 *            a value in [0,1] indicating the crossover rate
	 * @param mutationRate
	 *            a value in [0,1] indicating the mutation rate
	 */
	public DoubleArrayGeneticOperators(int dim, Interval bounds,
			double crossoverRate, double mutationRate)
	{
		if(dim < 1){
			throw new IllegalArgumentException("The number of elements in a valid genome must be greater than 0.");
		}
		if(crossoverRate < 0 || crossoverRate > 1){
			throw new IllegalArgumentException("The crossover rate must be within interval [0, 1].");
		}
		if(mutationRate < 0 || mutationRate > 1){
			throw new IllegalArgumentException("The mutation rate must be within interval [0,1].");
		}
		_dim = dim;
		_cr = crossoverRate;
		_mr = mutationRate;
		_bounds = new Interval(bounds);
	}

	/**
	 * Constructs an object that handles genetic operations on
	 * <code>double</code> arrays. The crossover and mutation rates are set to
	 * default values.
	 * 
	 * @param dim
	 *            the number of elements in a valid genome
	 * @param bounds
	 *            an interval determining the minimum and maximum values when a
	 *            <code>double</code> is generated
	 */
	public DoubleArrayGeneticOperators(int dim, Interval bounds)
	{
		this(dim, bounds, DEFAULT_CROSSOVER_RATE, DEFAULT_MUTATION_RATE);
	}

	/**
	 * Constructs an instance which handles genetic operations on
	 * <code>double</code> arrays.
	 * 
	 * @param dim
	 *            the number of elements in each genome
	 * @param crossoverRate
	 *            a value between 0 and 1 indicating the crossover rate
	 * @param mutationRate
	 *            a value between 0 and 1 indicating the mutation rate
	 */
	public DoubleArrayGeneticOperators(int dim, double crossoverRate,
			double mutationRate)
	{
		this(dim, new Interval(-1, 1), crossoverRate, mutationRate);
	}

	/**
	 * Constructs an instance which handles genetic operations on
	 * <code>double</code> arrays. The default crossover and mutation rates are
	 * used.
	 * 
	 * @param dim
	 *            the number of elements in each genome
	 */
	public DoubleArrayGeneticOperators(int dim)
	{
		this(dim, DEFAULT_CROSSOVER_RATE, DEFAULT_MUTATION_RATE);
	}

	@Override
	public double[] crossover(double[] p1, double[] p2)
	{
		if(p1.length != _dim || p2.length != _dim){
			throw new IllegalArgumentException("The length of both parent genomes must be " + _dim + ".");
		}
		double[] child = new double[p1.length];
		if (tmrutil.stats.Random.uniform() < _cr) {
			for(int i=0;i<child.length;i++){
				if(Random.nextBoolean()){
					child[i] = p1[i];
				}else{
					child[i] = p2[i];
				}
			}
		} else {
			System.arraycopy(p1, 0, child, 0, p1.length);
		}
		return child;
	}

	@Override
	public double[] generate()
	{
		return VectorOps.random(_dim, _bounds.getMin(), _bounds.getMax());
	}

	@Override
	public double[] mutate(double[] p)
	{
		if(p.length != _dim){
			throw new IllegalArgumentException("The length of the parent genome must be " + _dim + ".");
		}
		double[] child = Arrays.copyOf(p, p.length);
		for (int i = 0; i < p.length; i++) {
			if (tmrutil.stats.Random.uniform() < _mr) {
				child[i] = Random.uniform(-1, 1);
				// child[i] += tmrutil.stats.Random.normal(0, 0.2);
			}
		}
		return child;
	}

	@Override
	public Element toXML(double[] t, Document document)
	{
		Element element = document.createElement(DOUBLE_ARRAY_ELM);
		for (int i = 0; i < t.length; i++) {
			Element doubleElm = document.createElement(DOUBLE_ELM);
			doubleElm.setAttribute(VALUE_ATTR, String.valueOf(t[i]));
			element.appendChild(doubleElm);
		}
		return element;
	}

	@Override
	public double[] fromXML(Element element) throws IllegalArgumentException
	{
		double[] t = new double[_dim];
		NodeList children = element.getChildNodes();
		int ind = 0;
		try {
			for (int i = 0; i < children.getLength(); i++) {
				Node node = children.item(i);
				if (node instanceof Element) {
					Element doubleElm = (Element) node;
					t[ind++] = Double.parseDouble(doubleElm.getAttribute(VALUE_ATTR));
				}
			}
		} catch (IndexOutOfBoundsException ex) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		if (ind != _dim) {
			throw new IllegalArgumentException(
					"Element does not represent an instance of type : "
							+ getClass());
		}
		return t;
	}

}
