package tmrutil.learning;

import tmrutil.util.Factory;

/**
 * An incremental function approximator can update its approximation on-line
 * with one sample at a time.
 * 
 * @author Timothy A. Mann
 * 
 * @param <D>
 *            the domain type
 * @param <R>
 *            the range type
 */
public interface IncrementalFunctionApproximator<D, R> extends
		FunctionApproximator<D, R>, Factory<IncrementalFunctionApproximator<D,R>>
{
	/**
	 * Updates the function approximator's estimate given a single labeled pair.
	 * 
	 * @param input
	 *            an input sample
	 * @param target
	 *            the input's label
	 * @param learningRate
	 *            the learning rate (or step size) which determines how much of
	 *            the error is accounted for when updating
	 *            
	 * @return an estimate of the error before training
	 */
	public R train(D input, R target, double learningRate);
}
