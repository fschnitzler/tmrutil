/*
 * Perceptron.java
 */

package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import tmrutil.math.RealFunction;
import tmrutil.math.VectorOps;
import tmrutil.util.Pair;

/**
 * A perceptron is a simple linear classifier that takes an n-dimensional input
 * and produces an m-dimensional output. Perceptrons can be either single or
 * multilayered.
 */
public abstract class Perceptron extends NNet
{
	private static final long serialVersionUID = -7890886986367348353L;

	/** The default value of the bias term. */
	public static final double DEFAULT_BIAS = 1.0;

	/** The number of bias elements. */
	public static final int BIAS = 1;

	/**
	 * The activation function.
	 */
	protected RealFunction _f;

	/**
	 * Constructs a perceptron with an input and output size.
	 * 
	 * @param f
	 *            a function
	 * @param inputSize
	 *            the number of dimensions of the input vector.
	 * @param outputSize
	 *            the number of dimensions of the output vector.
	 * @throws IllegalArgumentException
	 *             if either <code>inputSize</code> or <code>outputSize</code>
	 *             is less than 1.
	 */
	public Perceptron(RealFunction f, int inputSize, int outputSize)
			throws IllegalArgumentException
	{
		super(inputSize, outputSize);
		_f = f;
	}

	public int getInputSizeWBias()
	{
		return getInputSize() + 1;
	}

	/**
	 * Takes an input vector and extends it by one element. The value of the
	 * last element is set to <code>DEFAULT_BIAS</code>.
	 */
	public static final double[] addBias(double[] input)
	{
		double[] inputB = Arrays.copyOf(input, input.length + 1);
		inputB[inputB.length - 1] = DEFAULT_BIAS;
		return inputB;
	}

	/**
	 * Calculates the error of the perceptron at a specified input.
	 * 
	 * @param input
	 *            an input vector
	 * @param target
	 *            the target output of this perceptron given <code>input</code>
	 * @throws IllegalArgumentException
	 *             if the size of the input vector or target vector is not the
	 *             expected size.
	 */
	public double[] error(double[] input, double[] target)
			throws IllegalArgumentException
	{
		if (target.length != getOutputSize()) {
			throw new IllegalArgumentException(
					"The dimension of the target vector does not match the output size: target.length = "
							+ target.length
							+ " AND output size is "
							+ getOutputSize());
		}
		double[] out = evaluate(input);
		return VectorOps.subtract(target, out);
	}

	/**
	 * Trains the perceptron given a set of input-target pairs.
	 * 
	 * @param inputs
	 *            a set of inputs
	 * @param targets
	 *            a set of target values corresponding to each input
	 * @param iterations
	 *            the number of iterations to train for
	 * @param learningRate
	 *            the learning rate parameter (a value in the range (0, 1]).
	 * @throws IllegalArgumentException
	 *             if the number of inputs does not match the number of targets,
	 *             iterations is not positive, or the learning rate is not
	 *             positive and less than or equal to 1.
	 * @return a list of absolute error for each epoch
	 */
	public List<double[]> train(List<double[]> inputs, List<double[]> targets,
			int iterations, double learningRate)
			throws IllegalArgumentException
	{
		if(inputs.size() != targets.size()){
			throw new IllegalArgumentException("Number of inputs does not equal number of targets.");
		}
		List<double[]> errorList = new ArrayList<double[]>(iterations);
		for (int i = 0; i < iterations; i++) {
			double[] error = new double[getOutputSize()];
			for (int j = 0; j < inputs.size(); j++) {
				error = VectorOps.add(error, train(inputs.get(j),
						targets.get(j), learningRate), error);
			}
			error = VectorOps.divide(error, inputs.size(), error);
			errorList.add(error);
		}
		return errorList;
	}

	/**
	 * Resets the weights of this perceptron with random weights.
	 */
	public abstract void reset();

	/**
	 * Dumps weights to the console. This method is used for debugging.
	 */
	public abstract void dumpWeights();

}