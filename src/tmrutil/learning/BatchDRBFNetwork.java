package tmrutil.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.math3.analysis.MultivariateMatrixFunction;
import org.apache.commons.math3.analysis.differentiation.DerivativeStructure;
import org.apache.commons.math3.analysis.differentiation.JacobianFunction;
import org.apache.commons.math3.analysis.differentiation.MultivariateDifferentiableFunction;
import org.apache.commons.math3.analysis.differentiation.MultivariateDifferentiableVectorFunction;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.MultiDimensionMismatchException;
import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointVectorValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.vector.ModelFunction;
import org.apache.commons.math3.optim.nonlinear.vector.ModelFunctionJacobian;
import org.apache.commons.math3.optim.nonlinear.vector.Target;
import org.apache.commons.math3.optim.nonlinear.vector.Weight;
import org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer;

import tmrutil.graphics.DataProperties;
import tmrutil.graphics.DataProperties.DisplayStyle;
import tmrutil.graphics.plot2d.LinePlot;
import tmrutil.math.Function;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.stats.StdEstimator;
import tmrutil.stats.VarianceEstimator;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * An implementation of a radial basis function network with dynamic bases and
 * batch training.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BatchDRBFNetwork implements
		BatchFunctionApproximator<double[], Double> {

	public static final int MAX_ITERATIONS = 200;
	public static final int MAX_EVALS = (int) Math.pow(2, 16);

	private class RBFObjectiveFunction implements
			MultivariateDifferentiableVectorFunction {
		private List<Pair<double[], Double>> _samples;
		private int _dimension;
		private int _numBases;

		private double[] _out;
		private DerivativeStructure[] _dsOut;

		public RBFObjectiveFunction(int dimension, int numBases,
				List<Pair<double[], Double>> samples) {
			_dimension = dimension;
			_numBases = numBases;
			_samples = samples;

			_out = new double[samples.size()];
			_dsOut = new DerivativeStructure[samples.size()];
		}

		@Override
		public double[] value(double[] params) throws IllegalArgumentException {
			RBFNet net = fromParams(params);
			for (int i = 0; i < _samples.size(); i++) {
				Pair<double[], Double> sample = _samples.get(i);
				_out[i] = net.evaluate(sample.getA());
			}
			return _out;
		}

		@Override
		public DerivativeStructure[] value(DerivativeStructure[] params)
				throws MathIllegalArgumentException {
			int numParams = (_numBases + 1) /* # of output weights */
					+ (_numBases * (2 * _dimension)) /* # of basis parameters */;
			if (params.length != numParams) {
				Integer[] wrong = new Integer[] { params.length };
				Integer[] expected = new Integer[] { numParams };
				throw new MultiDimensionMismatchException(wrong, expected);
			}

			DerivativeStructure[] yhat = _dsOut;

			for (int s = 0; s < _samples.size(); s++) {
				Pair<double[], Double> sample = _samples.get(s);
				double[] x = sample.getA();

				DerivativeStructure dsSample = null;
				for (int i = 0; i < _numBases; i++) {
					int si = (_numBases + 1) + (i * 2 * _dimension);

					int muOffset = si;
					int isigmaOffset = si + _dimension;
					DerivativeStructure basisI = basis(x, params, muOffset,
							isigmaOffset);
					DerivativeStructure dsi = basisI.multiply(params[i]);

					if (dsSample == null) {
						dsSample = dsi;
					} else {
						dsSample = dsSample.add(dsi);
					}
				}

				// Add the bias
				DerivativeStructure dsOutput = dsSample.add(params[_numBases]);

				yhat[s] = dsOutput;
			}

			return yhat;
		}

		public DerivativeStructure basis(double[] x,
				DerivativeStructure[] params, int muOffset, int isigmaOffset) {
			DerivativeStructure inner = null;
			for (int i = 0; i < _dimension; i++) {
				DerivativeStructure diff = params[muOffset + i].subtract(x[i]);
				DerivativeStructure dsi = diff.multiply(
						params[isigmaOffset + i]).multiply(diff);
				if (inner == null) {
					inner = dsi;
				} else {
					inner = inner.add(dsi);
				}
			}
			DerivativeStructure dsOut = inner.negate().exp();
			return dsOut;
		}

		public RBFNet fromParams(double[] params) {
			return new RBFNet(_dimension, _numBases, params);
		}

	}

	public static class RBFConvChecker implements
			ConvergenceChecker<PointVectorValuePair> {

		@Override
		public boolean converged(int iteration, PointVectorValuePair previous,
				PointVectorValuePair current) {
			if (iteration >= MAX_ITERATIONS) {
				return true;
			} else {
				return false;
			}
		}

	}

	private class PBasis implements Function<double[], Double> {
		private double[] _params;
		private int _muOffset;
		private int _isigmaOffset;
		private int _dimension;

		public PBasis(double[] params, int muOffset, int isigmaOffset,
				int dimension) {
			_params = params;
			_muOffset = muOffset;
			_isigmaOffset = isigmaOffset;
			_dimension = dimension;
		}

		public Double evaluate(double[] x) throws IllegalArgumentException {
			double inner = 0;
			for (int d = 0; d < _dimension; d++) {
				double diff = x[d] - _params[_muOffset + d];
				inner += diff * _params[_isigmaOffset + d]
						* _params[_isigmaOffset] * diff;
			}
			return Math.exp(-inner);
		}

		public String toString() {
			StringBuffer sb = new StringBuffer(getClass().getSimpleName()
					+ "[center=(");
			for (int d = 0; d < _dimension; d++) {
				if (d == 0) {
					sb.append(String.valueOf(_params[_muOffset + d]));
				} else {
					sb.append(", " + String.valueOf(_params[_muOffset + d]));
				}
			}
			sb.append("), sigma=(");
			for (int d = 0; d < _dimension; d++) {
				if (d == 0) {
					sb.append(String.valueOf(_params[_isigmaOffset + d]));
				} else {
					sb.append(", " + String.valueOf(_params[_isigmaOffset + d]));
				}
			}
			sb.append(")]");
			return sb.toString();
		}
	}

	public class RBFNet implements Function<double[], Double> {
		private double[] _params;
		private int _dimension;
		private List<PBasis> _bases;
		private double[] _h;

		public RBFNet(int dimension, int numBases, double[] params) {
			_dimension = dimension;

			_params = params;
			_h = new double[numBases + 1];

			_bases = new ArrayList<PBasis>(numBases);
			for (int i = 0; i < numBases; i++) {
				int si = (numBases + 1) + (i * 2 * dimension);

				int muOffset = si;
				int isigmaOffset = si + dimension;
				PBasis b = new PBasis(params, muOffset, isigmaOffset, dimension);
				_bases.add(b);
			}
		}

		@Override
		public Double evaluate(double[] x) throws IllegalArgumentException {
			double[] h = hidAct(x);

			double sum = 0;
			for (int i = 0; i < h.length; i++) {
				sum += _params[i] * h[i];
			}
			return sum;
		}

		public double[] hidAct(double[] x) {
			double[] h = _h;
			for (int i = 0; i < _bases.size(); i++) {
				PBasis b = _bases.get(i);
				h[i] = b.evaluate(x);
			}
			h[_bases.size()] = 1;
			return h;
		}

		/**
		 * Computes the error gradient for this RBF network at a particular
		 * training point.
		 * 
		 * @param x
		 *            the input vector
		 * @param y
		 *            the target value for <code>x</code>
		 * @param result
		 *            the vector to store the error gradient result. The length
		 *            of the vector must equal the number of parameters of this
		 *            RBF network
		 * @param addToResult
		 *            if true then the error gradient is added to the values
		 *            already stored in the result vector; otherwise the values
		 *            in the result vector are overwritten by the error gradient
		 *            computed for the specified training observation
		 *            <code>(x,y)</code>
		 * @return a reference to the result vector
		 */
		public double[] errorGradient(double[] x, Double y, double[] result,
				boolean addToResult) {
			if (result.length != _params.length) {
				throw new IllegalArgumentException(
						"Result vector does not match the number of parameters.");
			}
			double[] g = result;

			double[] h = hidAct(x);
			double yhat = 0;
			for (int i = 0; i < h.length; i++) {
				yhat += _params[i] * h[i];
			}
			double error = y - yhat;

			// Compute the gradient for the output weights
			for (int i = 0; i < h.length; i++) {
				double eg = error * -h[i];
				if (addToResult) {
					g[i] += eg;
				} else {
					g[i] = eg;
				}
			}

			for (int i = 0; i < _bases.size(); i++) {
				double wi = _params[i];
				double basisOutput = _bases.get(i).evaluate(x);
				int offset = (_bases.size() + 1) + (i * 2 * _dimension);

				for (int d = 0; d < _dimension; d++) {
					double mud = _params[offset + d];
					double isigmad = _params[offset + _dimension + d];
					double diff = x[d] - mud;

					double pre = error * -wi * basisOutput;

					// Compute the gradient for the basis centers
					double egmu = pre * (2 * (isigmad * isigmad) * diff);

					// Compute the gradient for the basis shape parameters
					double egisigma = pre * 2 * (diff * diff) * isigmad;

					if (addToResult) {
						g[offset + d] += egmu;
						g[offset + _dimension + d] += egisigma;
					} else {
						g[offset + d] = egmu;
						g[offset + _dimension + d] = egisigma;
					}
				}
			}

			return g;
		}

		public double meanSquaredError(
				Collection<Pair<double[], Double>> samples) {
			int numSamples = samples.size();
			double se = 0;
			for (Pair<double[], Double> sample : samples) {
				double yhat = evaluate(sample.getA());
				double error = sample.getB() - yhat;
				se += error * error;
			}
			return se / numSamples;
		}
	}

	private List<Interval> _inputSpace;
	private RBFNet _net;
	private boolean _trained;
	private int _dimension;
	private int _numBases;

	private double _mean;
	private double _std;

	/**
	 * Default value for the basis shape parameters.
	 */
	private double _defaultSigma;
	
	/**
	 * Regularization parameter for output weights.
	 */
	private double _lambda;

	public BatchDRBFNetwork(List<Interval> inputSpace, int numBases, double defaultSigma,
			double lambda) {
		_inputSpace = inputSpace;
		_dimension = inputSpace.size();
		_numBases = numBases;
		_defaultSigma = defaultSigma;
		_lambda = lambda;

		_trained = false;
	}

	@Override
	public void reset() {
		_trained = false;
	}

	@Override
	public Double evaluate(double[] x) throws IllegalArgumentException {
		if (!_trained) {
			return 0.0;
		} else {
			return _std * _net.evaluate(x) + _mean;
		}
	}

	public int numberOfParams() {
		return (_numBases + 1) + (_numBases * 2 * _dimension);
	}

	@Override
	public void train(Collection<Pair<double[], Double>> trainingSet) {

		List<Pair<double[], Double>> tset = normalizeData(trainingSet);

		double maxLearningRate = 1;
		double minLearningRate = 0.001;
		double learningRate = maxLearningRate;

		int numIterations = MAX_ITERATIONS;
		double[] params = new double[numberOfParams()];
		initializeParameters(params, tset);
		double[] tmpParams = new double[numberOfParams()];

		RBFNet rbf = new RBFNet(_dimension, _numBases, params);
		double minMSE = rbf.meanSquaredError(tset);
		for (int i = 0; i < numIterations; i++) {

			double[] gradient = new double[numberOfParams()];
			for (Pair<double[], Double> sample : tset) {
				double[] x = sample.getA();
				Double y = sample.getB();
				rbf.errorGradient(x, y, gradient, true);
			}

			double lr = learningRate / tset.size();

			for (int j = 0; j < params.length; j++) {
				if (j < _numBases) {
					tmpParams[j] = params[j] - lr * gradient[j] - _lambda
							* (params[j] * params[j]);
				} else {
					tmpParams[j] = params[j] - (lr/4) * gradient[j];
				}

				if (Double.isNaN(tmpParams[j])
						|| Double.isInfinite(tmpParams[j])) {
					throw new TrainingException("Detected " + tmpParams[j]
							+ " while training.");
				}
			}

			// Adjust the learning rate
			RBFNet tmpRBF = new RBFNet(_dimension, _numBases, tmpParams);
			double mse = tmpRBF.meanSquaredError(tset);
			if (mse < minMSE) {
				System.arraycopy(tmpParams, 0, params, 0, params.length);
				learningRate = Math.min(maxLearningRate, 2 * learningRate);
				minMSE = mse;
			} else {
				learningRate = Math.max(minLearningRate, 0.5 * learningRate);
			}
		}
		_net = new RBFNet(_dimension, _numBases, params);
		_trained = true;
	}

	private List<Pair<double[], Double>> normalizeData(
			Collection<Pair<double[], Double>> trainingSet) {
		_mean = mean(trainingSet);
		_std = std(trainingSet);

		List<Pair<double[], Double>> ntset = new ArrayList<Pair<double[], Double>>(
				trainingSet.size());
		for (Pair<double[], Double> sample : trainingSet) {
			double y = (sample.getB() - _mean) / _std;
			ntset.add(new Pair<double[], Double>(sample.getA(), y));
		}
		return ntset;
	}

	private double mean(Collection<Pair<double[], Double>> trainingSet) {
		double bias = 0;
		for (Pair<double[], Double> sample : trainingSet) {
			bias += sample.getB();
		}
		return bias / trainingSet.size();
	}

	private double std(Collection<Pair<double[], Double>> trainingSet) {
		StdEstimator sest = new StdEstimator();
		for (Pair<double[], Double> sample : trainingSet) {
			sest.add(sample.getB());
		}
		return sest.estimate();
	}

	private void initializeParameters(double[] params,
			Collection<Pair<double[], Double>> trainingData) {
		for (int i = 0; i < _numBases; i++) {
			params[i] = Random.normal();

			int offset = _numBases + 1 + (i * 2 * _dimension);
			double[] mu = Interval.random(_inputSpace);
			for (int j = 0; j < _dimension; j++) {
				params[offset + j] = mu[j];
				params[offset + _dimension + j] = 1/_defaultSigma;
			}
		}
		params[_numBases] = 0;
	}

	public static void main(String[] args) {
		int numSamples = 100;
		int numEvals = 500;
		int numBases = 8;
		double defaultSigma = 0.1;
		double lambda = 0.0;

		List<double[]> samplePoints = new ArrayList<double[]>(numSamples);
		Collection<Pair<double[], Double>> samples = new ArrayList<Pair<double[], Double>>();
		for (int i = 0; i < numSamples; i++) {
			double[] x = new double[] { Random.uniform() };
			double y = Math.sin(8 * x[0]) + 20 + Random.normal()/6;
			Pair<double[], Double> sample = new Pair<double[], Double>(x, y);
			samples.add(sample);

			samplePoints.add(new double[] { x[0], y });
		}

		List<Interval> inputSpace = new ArrayList<Interval>();
		inputSpace.add(new Interval(0, 1));
		BatchDRBFNetwork rbf = new BatchDRBFNetwork(inputSpace, numBases, defaultSigma,
				lambda);
		rbf.train(samples);

		double[] x = VectorOps.range(0, 1, numEvals);
		double[] y = new double[x.length];
		double[] yhat = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			y[i] = Math.sin(8 * x[i]) + 20;
			yhat[i] = rbf.evaluate(new double[] { x[i] });
		}

		LinePlot plot = tmrutil.graphics.plot2d.Plot2D.linePlot(x, y,
				java.awt.Color.BLUE, "RBF Training", "X", "Y");
		plot.addData(x, yhat, java.awt.Color.RED, "");

		DataProperties dp = new DataProperties(samplePoints,
				DisplayStyle.POINT, java.awt.Color.RED, "");
		plot.addData(dp);
	}

	@Override
	public BatchFunctionApproximator<double[], Double> newInstance() {
		return new BatchDRBFNetwork(_inputSpace, _numBases, _defaultSigma, _lambda);
	}

}
