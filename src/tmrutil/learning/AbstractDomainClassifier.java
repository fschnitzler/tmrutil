package tmrutil.learning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import tmrutil.math.Function;
import tmrutil.util.Pair;

/**
 * A wrapper class that enables classifiers to process input samples from
 * arbitrary domains.
 * 
 * @author Timothy A. Mann
 * 
 * @param <D1>
 *            the desired input domain
 * @param <D2>
 *            the domain of the underlying classifier
 * @param <C>
 *            the class label type
 */
public class AbstractDomainClassifier<D1, D2, C> implements Classifier<D1, C> {

	private Function<D1, D2> _featureExtractor;
	private Classifier<D2, C> _c;

	public AbstractDomainClassifier(Function<D1, D2> featureExtractor,
			Classifier<D2, C> c) {
		_featureExtractor = featureExtractor;
		_c = c;
	}

	@Override
	public C classify(D1 instance) {
		D2 x = _featureExtractor.evaluate(instance);
		return _c.classify(x);
	}

	@Override
	public double[] scores(D1 instance) {
		D2 x = _featureExtractor.evaluate(instance);
		return _c.scores(x);
	}

	@Override
	public void train(Collection<Pair<D1, C>> trainingSet) {
		List<Pair<D2, C>> tset = new ArrayList<Pair<D2, C>>(trainingSet.size());
		for (Pair<D1, C> sample : trainingSet) {
			D2 x = _featureExtractor.evaluate(sample.getA());
			C c = sample.getB();
			tset.add(new Pair<D2, C>(x, c));
		}
		_c.train(tset);
	}

	@Override
	public int numClasses() {
		return _c.numClasses();
	}

	@Override
	public AbstractDomainClassifier<D1,D2,C> newInstance() {
		return new AbstractDomainClassifier<D1,D2,C>(_featureExtractor, _c.newInstance());
	}
}
