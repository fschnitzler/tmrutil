package tmrutil.learning;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import tmrutil.util.Pair;

/**
 * Classification data represents a set of vectors pair with discrete integer
 * labels.
 * 
 * @author Timothy A. Mann
 *
 */
public class ClassificationData extends AbstractLabeledData<Integer> {

	public ClassificationData(List<Pair<RealVector, Integer>> data) {
		super(data);
	}
	
	/**
	 * Returns a new {@link RegressionData} instance where the input vectors
	 * only contain the columns specified by <code>variableIndices</code>.
	 * 
	 * @param variableIndices
	 *            a list of indices of variables to include in the new
	 *            regression data set
	 * @return a new regression data instance with input vectors only containing
	 *         the variables specified by <code>variableIndices</code>
	 */
	public ClassificationData subsetOfVariables(List<Integer> variableIndices) {
		List<Pair<RealVector, Integer>> newPairs = new ArrayList<Pair<RealVector, Integer>>(
				_data.size());
		for (Pair<RealVector, Integer> pair : _data) {
			RealVector nv = extractSubset(pair.getA(), variableIndices);
			Pair<RealVector, Integer> newPair = new Pair<RealVector, Integer>(nv,
					pair.getB());
			newPairs.add(newPair);
		}
		return new ClassificationData(newPairs);
	}

	/**
	 * Creates a RegressionData instance from this instance removing any
	 * variables from the input vectors that have zero variance.
	 * 
	 * @return a RegressionData instance with zero variance variables removed
	 */
	public ClassificationData removeVariablesWithNoVariance() {
		List<Integer> activeSet = variablesWithNontrivialVariance();
		return subsetOfVariables(activeSet);
	}

	/**
	 * Returns a new data set by projecting the input vectors of
	 * this regression data set using the specified linear transformation.
	 * 
	 * @param linTran
	 *            a linear transformation (where the columns are the same
	 *            dimension as the input vectors and the new dimension will be
	 *            equal to the number of columns)
	 * @return a new data set
	 */
	public ClassificationData project(RealMatrix linTran) {
		List<Pair<RealVector, Integer>> data = new ArrayList<Pair<RealVector, Integer>>();
		for (Pair<RealVector, Integer> pair : _data) {
			RealVector newX = project(pair.getA(), linTran);
			Pair<RealVector, Integer> newPair = new Pair<RealVector, Integer>(
					newX, pair.getB());
			data.add(newPair);
		}
		return new ClassificationData(data);
	}
}
