package tmrutil.learning;

import java.util.Arrays;

/**
 * Translates and scales the input and output to an underlying neural network
 * given mean and standard deviation vectors for the input and output data.
 * 
 * @author Timothy A. Mann
 * 
 */
public class StaticNNetFilter extends NNet
{
	private NNet _nnet;
	
	private double[] _zInput;
	private double[] _zTarget;

	private double[] _inputMean;
	private double[] _inputStd;

	private double[] _outputMean;
	private double[] _outputStd;

	/**
	 * Constructs a static neural network filter that translates and scales both
	 * the input and output.
	 * 
	 * @param nnet
	 *            the underlying neural network
	 * @param inputMean
	 *            the mean to subtract from input samples before passing them to
	 *            the underlying neural network
	 * @param inputStd
	 *            the standard deviation to scale the samples by before passing
	 *            them to the underlying neural network
	 * @param outputMean
	 *            the mean to subtract from target values before training and
	 *            add to the outputs of the underlying neural network
	 * @param outputStd
	 *            the standard deviation to scale target values by before
	 *            training and to divide the outputs of the underlying neural
	 *            network
	 */
	public StaticNNetFilter(NNet nnet, double[] inputMean, double[] inputStd,
			double[] outputMean, double[] outputStd)
	{
		super(nnet.getInputSize(), nnet.getOutputSize());
		_nnet = nnet;

		if(inputMean.length != nnet.getInputSize()){
			throw new IllegalArgumentException("The input mean vector does not match the neural network input size.");
		}
		if(inputStd.length != nnet.getInputSize()){
			throw new IllegalArgumentException("The input standard deviation vector does not match the neural network input size.");
		}
		if(outputMean.length != nnet.getOutputSize()){
			throw new IllegalArgumentException("The output mean vector does not match the neural network input size.");
		}
		if(outputStd.length != nnet.getOutputSize()){
			throw new IllegalArgumentException("The output standard deviation vector does not match the neural network output size.");
		}
		
		_inputMean = Arrays.copyOf(inputMean, inputMean.length);
		_inputStd = Arrays.copyOf(inputStd, inputStd.length);

		_outputMean = Arrays.copyOf(outputMean, outputMean.length);
		_outputStd = Arrays.copyOf(outputStd, outputStd.length);
		
		_zInput = new double[nnet.getInputSize()];
		_zTarget = new double[nnet.getOutputSize()];
	}

	@Override
	public int numberOfWeights()
	{
		return _nnet.numberOfWeights();
	}

	@Override
	public void setWeights(double[] weights) throws IllegalArgumentException
	{
		_nnet.setWeights(weights);
	}

	@Override
	public double[] train(double[] input, double[] target, double learningRate)
	{
		_zInput = zscore(input, _inputMean, _inputStd, _zInput);
		_zTarget = zscore(target, _outputMean, _outputStd, _zTarget);
		
		return _nnet.train(_zInput, _zTarget, learningRate);
	}

	@Override
	public double[] evaluate(double[] x, double[] result)
			throws IllegalArgumentException
	{
		_zInput = zscore(x, _inputMean, _inputStd, _zInput);
		double[] output = _nnet.evaluate(_zInput);
		return unzscore(output, _outputMean, _outputStd, output);
	}

	private double[] zscore(double[] x, double[] mean, double[] std, double[] result)
	{
		for(int i=0;i<x.length;i++){
			result[i] = (x[i] - mean[i]) / std[i];
		}
		return result;
	}
	
	private double[] unzscore(double[] x, double[] mean, double[] std, double[] result)
	{
		for(int i=0;i<x.length;i++){
			result[i] = (x[i] * std[i]) + mean[i];
		}
		return result;
	}

	@Override
	public StaticNNetFilter newInstance() {
		return new StaticNNetFilter(_nnet.newInstance(), _inputMean, _inputStd, _outputMean, _outputStd);
	}
}
