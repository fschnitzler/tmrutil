package tmrutil.learning;


import java.util.Collection;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;

import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * Simple batch linear function approximation.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BatchLinearApproximator implements
		BatchFunctionApproximator<RealVector, Double> {

	private static final long serialVersionUID = 1328180767316357356L;
	private int _inputSize;
	private RealVector _w;
	private Interval _outputBounds;

	public BatchLinearApproximator(int inputSize) {
		this(inputSize, null);
	}

	public BatchLinearApproximator(int inputSize, Interval outputBounds) {
		_inputSize = inputSize;
		_outputBounds = outputBounds;
	}
	
	/**
	 * Returns the number of features expected in a valid input vector.
	 * @return the number of features of an input vector
	 */
	public int getInputSize()
	{
		return _inputSize;
	}

	@Override
	public void reset() {
		_w = new ArrayRealVector(_inputSize+1);
	}

	@Override
	public Double evaluate(RealVector x) throws IllegalArgumentException {
		
		if(_w == null){
			return 0.0;
		}
		
		RealVector vx = new ArrayRealVector(addBias(x));
		double response = _w.dotProduct(vx);

		if (_outputBounds == null) {
			return response;
		} else {
			return _outputBounds.clip(response);
		}
	}
	
	private double[] addBias(RealVector vx){
		double[] xbias = new double[vx.getDimension()+1];
		double[] xdata = vx.toArray();
		System.arraycopy(xdata, 0, xbias, 1, xdata.length);
		xbias[0] = 1;
		return xbias;
	}

	@Override
	public void train(Collection<Pair<RealVector, Double>> trainingSet) {
		RealMatrix amat = new Array2DRowRealMatrix(trainingSet.size(), _inputSize+1);
		RealVector bvec = new ArrayRealVector(trainingSet.size());
		
		int r = 0;
		for(Pair<RealVector,Double> pair : trainingSet){
			amat.setRow(r, addBias(pair.getA()));
			bvec.setEntry(r, pair.getB());
			r++;
		}
		SingularValueDecomposition svd = new SingularValueDecomposition(amat);
		DecompositionSolver solver = svd.getSolver();
		_w = solver.solve(bvec);
		
		
		/*
		double[][] sampleMat = new double[trainingSet.size()][];
		double[] target = new double[trainingSet.size()];
		int i = 0;
		for (Pair<RealVector, Double> sample : trainingSet) {
			target[i] = sample.getB();
			double[] v = sample.getA().toArray();
			sampleMat[i] = v;
			i++;
		}

		OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
		regression.newSampleData(target, sampleMat);
		double[] w = regression.estimateRegressionParameters();
		
		_w = new ArrayRealVector(w);
		*/
	}
	
	public double[] residuals(Collection<Pair<RealVector,Double>> samples)
	{
		double[] r = new double[samples.size()];
		int i=0;
		for(Pair<RealVector,Double> sample : samples){
			r[i] = sample.getB() - evaluate(sample.getA());
			i++;
		}
		return r;
	}

	@Override
	public BatchLinearApproximator newInstance() {
		return new BatchLinearApproximator(_inputSize, _outputBounds);
	}

}
