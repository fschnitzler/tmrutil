package tmrutil.learning;

import java.util.Arrays;
import tmrutil.math.RealFunction;
import tmrutil.math.RealVectorFunction;
import tmrutil.math.VectorOps;

/**
 * A superclass for feed-forward or recurrent neural networks.
 * 
 * @author Timothy Mann
 */
public abstract class NNet extends RealVectorFunction implements
		IncrementalFunctionApproximator<double[], double[]> {

	private static final long serialVersionUID = -8528340861806219634L;

	public NNet(int inputSize, int outputSize) {
		super(inputSize, outputSize);
	}

	/**
	 * Trains the neural network using a single input-target pair.
	 * 
	 * @param input
	 *            an input vector
	 * @param target
	 *            a target vector
	 * @param learningRate
	 *            the learning rate (a value in the range (0, 1])
	 * @return the training error
	 */
	public abstract double[] train(double[] input, double[] target,
			double learningRate);

	/**
	 * Sets the weights for each layer of this perceptron using
	 * <code>weights</code>.
	 * 
	 * @param weights
	 *            an array of weights
	 * @throws IllegalArgumentException
	 *             if <code>weights.length != this.numberOfWeights()</code>
	 */
	public abstract void setWeights(double[] weights)
			throws IllegalArgumentException;

	/**
	 * Calculate the number of weights required for this perceptron.
	 */
	public abstract int numberOfWeights();

	/**
	 * Computes the derivative of this function (numerically) at an input value
	 * <code>x</code> with respect to a specified input index.
	 * 
	 * @param x
	 *            an input vector
	 * @param inDim
	 *            index of a component in the input vector
	 * @param outDim
	 *            index of a component in the output vector
	 * @return the derivative at <code>x</code> with respect to
	 *         <code>x[inDim]</code>
	 */
	public double differentiate(double[] x, int inDim, int outDim) {
		double deltaX = RealFunction.EPSILON;
		double d = 0, dnext = 0;
		double[] xtmp1 = Arrays.copyOf(x, x.length);
		double[] xtmp2 = Arrays.copyOf(x, x.length);
		do {
			xtmp1[inDim] = x[inDim] + deltaX;
			xtmp2[inDim] = x[inDim] - deltaX;
			d = (evaluate(xtmp1)[outDim] - evaluate(xtmp2)[outDim])
					/ (2 * deltaX);
			deltaX = deltaX / 2;
			xtmp1[inDim] = x[inDim] + deltaX;
			xtmp2[inDim] = x[inDim] - deltaX;
			dnext = (evaluate(xtmp1)[outDim] - evaluate(xtmp2)[outDim])
					/ (2 * deltaX);
		} while (d > (dnext + deltaX));
		return d;
	}

	/**
	 * Resets the weights in this neural network to random values in the
	 * interval [-1, 1].
	 */
	public void reset() {
		double[] weights = VectorOps.random(numberOfWeights(), -1, 1);
		setWeights(weights);
	}

	/**
	 * Constructs a new instance of this neural network using the same arguments
	 * that were used to construct this neural network instance.
	 */
	public abstract NNet newInstance();
}
