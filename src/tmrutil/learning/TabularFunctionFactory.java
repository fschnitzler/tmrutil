package tmrutil.learning;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import tmrutil.util.Factory;
import tmrutil.util.Pair;

/**
 * Tabular Function factory is a convenience class that constructs "fake"
 * function approximators. These function approximators memorize there inputs
 * and have no generalization. This class is generally used for testing
 * purposes.
 * 
 * @author Timothy A. Mann
 * 
 * @param <X>
 *            the input type
 */
public class TabularFunctionFactory<X> implements
		Factory<BatchFunctionApproximator<X, Double>> {

	public static class MapFunction<X> implements
			BatchFunctionApproximator<X, Double> {
		private Map<X, Double> _values;

		public MapFunction() {
			_values = new HashMap<X, Double>();
		}

		@Override
		public void reset() {
			_values.clear();
		}

		@Override
		public Double evaluate(X x) throws IllegalArgumentException {
			Double v = _values.get(x);
			if (v == null) {
				return 0.0;
			} else {
				return v;
			}
		}

		@Override
		public void train(Collection<Pair<X, Double>> trainingSet) {
			for (Pair<X, Double> sample : trainingSet) {
				_values.put(sample.getA(), sample.getB());
			}
		}

		@Override
		public MapFunction<X> newInstance() {
			return new MapFunction<X>();
		}

	}

	@Override
	public MapFunction<X> newInstance() {
		return new MapFunction<X>();
	}

}
