package tmrutil.learning;

import java.util.ArrayList;
import java.util.List;
import tmrutil.graphics.DataProperties;
import tmrutil.graphics.plot2d.LinePlot;
import tmrutil.math.RealFunction;
import tmrutil.math.VectorOps;
import tmrutil.math.functions.BellCurve;
import tmrutil.math.functions.Compose;
import tmrutil.math.functions.ConstantScale;
import tmrutil.math.functions.NaturalLog;
import tmrutil.math.functions.OneOverX;
import tmrutil.math.functions.Sin;
import tmrutil.math.functions.XSquared;
import tmrutil.stats.Random;

/**
 * A simple function fitter uses gradient descent to fit a given function to
 * data. Given a function f, the algorithm learns parameters a, b, c, and d and
 * evaluates the function as <br/>&nbsp;&nbsp;<code>a * f(b * x + c) + d</code>
 * 
 * @author Timothy A. Mann
 * 
 */
public class SimpleFunctionFitter implements
		IncrementalFunctionApproximator<Double, Double>
{
	private static final long serialVersionUID = -7337341959696721607L;

	private RealFunction _f;
	private double _scaleY;
	private double _biasY;
	private double _scaleX;
	private double _biasX;
	
	private int _numSamplesPerUpdate;
	private int _numSamples;
	private List<Double> _xSamples;
	private List<Double> _ySamples;

	/**
	 * Constructs a simple function fitter with a specified function to fit to data.
	 * @param f a differentiable function to fit to data
	 * @param numSamplesPerUpdate the number of samples to use at each update
	 */
	public SimpleFunctionFitter(RealFunction f, int numSamplesPerUpdate)
	{
		_f = f;
		_numSamplesPerUpdate = numSamplesPerUpdate;
		
		_xSamples = new ArrayList<Double>(numSamplesPerUpdate);
		_ySamples = new ArrayList<Double>(numSamplesPerUpdate);
		
		reset();
	}

	@Override
	public void reset()
	{
		_scaleY = 1;
		_biasY = 0;
		_scaleX = 1;
		_biasX = 0;
		
		_numSamples = 0;
		_xSamples.clear();
		_ySamples.clear();
	}

	@Override
	public Double evaluate(Double x) throws IllegalArgumentException
	{
		double input = _scaleX * x + _biasX;
		double output = _scaleY * _f.evaluate(input) + _biasY;
		return output;
	}

	@Override
	public Double train(Double input, Double target, double learningRate)
	{
		double output = evaluate(input);
		double error = target - output;
		
		// Add the sample
		if(_numSamples < _numSamplesPerUpdate){
			_xSamples.add(input);
			_ySamples.add(target);
			_numSamples++;
		}
		
		// Perform the update
		if(_numSamples == _numSamplesPerUpdate){
			double scaleDy = 0, scaleDx = 0, biasDy = 0, biasDx = 0;
			double pre = (1.0/_numSamples);
			for(int i=0;i<_numSamples;i++){
				double x = _xSamples.get(i);
				double y = _ySamples.get(i);
				double err = y - evaluate(x);
				scaleDy += pre * err * _f.evaluate(_scaleX * input + _biasX);
				biasDy += pre * err;
				scaleDx += pre * err * (_scaleY * _f.differentiate(_scaleX * input + _biasX) * input);
				biasDx += pre * err * (_scaleY * _f.differentiate(_scaleX * input + _biasX));
			}
			//_scaleY = _scaleY + (learningRate * scaleDy);
			//_biasY = _biasY + (learningRate * biasDy);
			_scaleX = _scaleX + (learningRate * scaleDx);
			//_biasX = _biasX + (learningRate * biasDx);
			
			_xSamples.clear();
			_ySamples.clear();
			_numSamples = 0;
		}
		
		return error;
	}
	
	@Override
	public String toString()
	{
		return _scaleY + " f( " + _scaleX + " x + " + _biasX + " ) + " + _biasY;
	}
	
	public static void main(String[] args){
		double scaleY = 1;
		double biasY = 0;
		double scaleX = 4;
		double biasX = 0;
		
		double minX = -1;
		double maxX = 2;
		
		RealFunction coreFunction = new Sin();
		RealFunction secretFunction = new Compose(new ConstantScale(scaleY, biasY), new Compose(coreFunction, new ConstantScale(scaleX, biasX)));
		
		int numSamplesPerUpdate = 20;
		SimpleFunctionFitter fitter = new SimpleFunctionFitter(coreFunction, numSamplesPerUpdate);
		double sigma = 0.5;
		int numTrainingSamples = 100000;
		double learningRate = 0.05;
		
		for(int i = 0; i < numTrainingSamples; i++){
			double input = Random.uniform(minX, maxX);
			double target = secretFunction.evaluate(input) + Random.normal(0, sigma);
			
			fitter.train(input, target, learningRate);
		}
		
		double[] xrange = VectorOps.range(minX, maxX, 100);
		double[][] trueData = new double[xrange.length][2];
		double[] yhat = new double[xrange.length];
		for(int i=0;i<xrange.length;i++){
			trueData[i][0] = xrange[i];
			trueData[i][1] = secretFunction.evaluate(xrange[i]);
			yhat[i] = fitter.evaluate(xrange[i]);
		}
		
		LinePlot lplot = tmrutil.graphics.plot2d.Plot2D.scatterPlot(trueData, java.awt.Color.RED, "", "", "");
		lplot.addData(xrange, yhat, java.awt.Color.BLUE, "");
		
		System.out.println(fitter);
	}

}
