package tmrutil.physics;

import java.util.List;
import tmrutil.math.Function;
import tmrutil.math.RealFunction;
import tmrutil.math.RealVectorFunction;
import tmrutil.util.ArrayOps;

/**
 * Implements a Runge-Kutta 4th order numerical ODE solver.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RungeKutta
{
	/**
	 * Numerically solves an ODE for the next value given the current value, an
	 * integration time step, and the four slope approximations.
	 * 
	 * @param x
	 *            current value
	 * @param h
	 *            integration time step
	 * @param k1
	 * @param k2
	 * @param k3
	 * @param k4
	 * @return the next value
	 */
	public static final double solve(double x, double h, double k1, double k2,
			double k3, double k4)
	{
		return x + (h / 6) * (k1 + 2 * k2 + 2 * k3 + k4);
	}

	/**
	 * Numerically solves an ODE for the next value given the current value, the
	 * current time, an integration time step, and an ODE.
	 * 
	 * @param x
	 *            the current value
	 * @param t
	 *            the current time
	 * @param h
	 *            integration time step
	 * @param ode
	 *            an ordinary differential equation which takes a 2-dimensional
	 *            input vector where the first component represents time and the
	 *            second is the value
	 * @return the next value
	 */
	public static final double solve(double x, double t, double h,
			Function<double[], Double> ode)
	{
		double k1 = ode.evaluate(new double[] { t, x });
		double k2 = ode.evaluate(new double[] { t + (0.5 * h),
				x + (0.5 * h * k1) });
		double k3 = ode.evaluate(new double[] { t + (0.5 * h),
				x + (0.5 * h * k2) });
		double k4 = ode.evaluate(new double[] { t + h, x + h * k3 });

		return solve(x, h, k1, k2, k3, k4);
	}

	/**
	 * Numerically solves a set of ODEs for the next values.
	 * 
	 * @param x
	 *            a vector of current values
	 * @param t
	 *            the current time
	 * @param h
	 *            integration time step
	 * @param odes
	 *            a list of ordinary differential equations
	 * @return a vector of next values
	 */
	public static final double[] solve(double[] x, double t, double h,
			List<Function<double[], Double>> odes)
	{
		if (x.length != odes.size()) {
			throw new IllegalArgumentException(
					"Error while numerically solving ODE : The number of variables does not match the number of ODEs.");
		}
		double[] output = new double[x.length];
		for (int i = 0; i < x.length; i++) {
			output[i] = solve(x[i], t, h, odes.get(i));
		}
		return output;
	}
}
