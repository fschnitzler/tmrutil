package tmrutil.physics;

import java.util.LinkedList;
import java.util.List;

/**
 * A simulation of a physical world.
 * 
 * @author Timothy Mann
 * 
 */
public class World
{

	public static int THREE_DIMENSIONS = 3;
	public static int TWO_DIMENSIONS = 2;

	/**
	 * A list of all bodies in this physical world.
	 */
	private List<Body> _bodies;

	/**
	 * The simulation time in seconds.
	 */
	private double _time;
	
	/**
	 * The gravity vector.
	 */
	private double[] _gravity;

	/**
	 * Constructs a simulation world with the specified gravity vector.
	 * 
	 * @param gravity
	 */
	public World(double[] gravity) throws IllegalArgumentException
	{
		if (gravity.length != THREE_DIMENSIONS) {
			throw new IllegalArgumentException(
					"The gravity vector must have 3-dimensions");
		}
		_time = 0.0;
		_gravity = gravity;
		_bodies = new LinkedList<Body>();
	}

	/**
	 * Steps the simulation <code>t</code> seconds into the future.
	 * 
	 * @param t
	 *            a number of seconds
	 */
	public void step(double t)
	{

	}
	
}
