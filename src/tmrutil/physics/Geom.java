package tmrutil.physics;

/**
 * A simple 3-dimensional shape.
 * @author Timothy Mann
 *
 */
public abstract class Geom
{
	public abstract double intersects(Geom geom);
}
