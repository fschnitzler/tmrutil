package tmrutil.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import tmrutil.math.DimensionMismatchException;

public class ListUtil
{
	/**
	 * Constructs a list from the elements of the passed array.
	 * @param ts an array of elements
	 * @return a list containing the elements of <code>ts</code>
	 */
	public static final <T> List<T> toList(T[] ts)
	{
		List<T> tlist = new ArrayList<T>(ts.length);
		Collections.addAll(tlist, ts);
		return tlist;
	}
	
	/**
	 * Constructs a list from an array of doubles.
	 * 
	 * @param v
	 *            an array of doubles
	 * @return a list
	 */
	public static final List<Double> toList(double[] v)
	{
		List<Double> a = new ArrayList<Double>(v.length);
		for (int i = 0; i < v.length; i++) {
			a.add(v[i]);
		}
		return a;
	}
	
	/**
	 * Constructs a list from an array of integers.
	 * @param v an array of integers
	 * @return a list
	 */
	public static final List<Integer> toList(int[] v)
	{
		List<Integer> a = new ArrayList<Integer>(v.length);
		for(int i=0;i<v.length;i++){
			a.add(v[i]);
		}
		return a;
	}
	
	/**
	 * Constructs a list from a matrix. Each row of the matrix is added as an element to the list.
	 * @param m a matrix of points
	 * @return a list containing the rows of <code>m</code> as elements
	 */
	public static final List<double[]> toList(double[][] m)
	{
		List<double[]> list = new ArrayList<double[]>(m.length);
		for(int i=0;i<m.length;i++){
			list.add(m[i]);
		}
		return list;
	}

	/**
	 * Constructs an array from a list of doubles.
	 * 
	 * @param v
	 *            a list of doubles
	 * @return an array
	 */
	public static final double[] toArray(List<? extends Number> v)
	{
		double[] a = new double[v.size()];
		for (int i = 0; i < a.length; i++) {
			a[i] = v.get(i).doubleValue();
		}
		return a;
	}

	/**
	 * Creates an 1 x N matrix where <code>v</code> is a vector of length N. The
	 * first column of the returned matrix contains the values 0,1,...,N and the
	 * second column contains the values from <code>v</code>.
	 * 
	 * @param v
	 *            a list of double of length N
	 * @return an 1 x N matrix
	 */
	public static final double[][] toRowVector(List<? extends Number> v)
	{
		double[][] m = new double[1][v.size()];
		for (int i = 0; i < v.size(); i++) {
			m[0][i] = v.get(i).doubleValue();
		}
		return m;
	}

	/**
	 * Creates a matrix from the points stored in <code>v</code>. Each point in
	 * the list <code>v</code> becomes a row of the new matrix.
	 * 
	 * @param v a list of points
	 * @return a matrix
	 */
	public static final double[][] toMatrix(List<double[]> v)
	{
		double[][] m = new double[v.size()][];
		int index = 0;
		for (double[] p : v) {
			m[index] = Arrays.copyOf(p, p.length);
			index++;
		}
		return m;
	}

	/**
	 * Creates an N x 2 matrix where <code>x</code> and <code>y</code> are
	 * vectors of length N. The values of <code>x</code> are stored in the first
	 * column and the values of <code>y</code> are stored in the second column.
	 * 
	 * @param x
	 *            a list of doubles of length N
	 * @param y
	 *            a list of doubles of length N
	 * @return an N x 2 matrix
	 * @throws DimensionMismatchException
	 *             if <code>x.size() != y.size()</code>
	 */
	public static final double[][] toMatrix(List<? extends Number> x, List<? extends Number> y)
			throws DimensionMismatchException
	{
		if (x.size() != y.size()) {
			throw new DimensionMismatchException("x.size() != y.size() : "
					+ x.size() + " != " + y.size());
		}
		double[][] m = new double[x.size()][2];
		for (int i = 0; i < m.length; i++) {
			m[i][0] = x.get(i).doubleValue();
			m[i][1] = y.get(i).doubleValue();
		}
		return m;
	}

	/**
	 * Constructs a list of doubles containing the elements of the specified
	 * list of integers.
	 * 
	 * @param intList
	 *            a list of integers
	 * @return a list of doubles
	 */
	public static final List<Double> toDoubleList(List<? extends Number> intList)
	{
		List<Double> doubleList = new ArrayList<Double>(intList.size());
		for (int i = 0; i < intList.size(); i++) {
			double val = intList.get(i).doubleValue();
			doubleList.add(val);
		}
		return doubleList;
	}

	/**
	 * Constructs a list of integers containing the elements of the specified
	 * list of doubles (rounded to integers).
	 * 
	 * @param doubleList
	 *            a list of doubles
	 * @return a list of integers
	 */
	public static final List<Integer> toIntegerList(List<? extends Number> doubleList)
	{
		List<Integer> intList = new ArrayList<Integer>(doubleList.size());
		for (int i = 0; i < doubleList.size(); i++) {
			double val = doubleList.get(i).doubleValue();
			intList.add((int) val);
		}
		return intList;
	}
	
	/**
	 * Returns the pair in the list with the maximum double value.
	 * @param list a list of pairs where the second element in each pair is a double value
	 * @return the pair in the list with the maximum double value or null if the list is empty
	 */
	public static final <T> Pair<T,Double> maxElm(List<Pair<T,Double>> list)
	{
		Pair<T,Double> maxElm = null;
		for(Pair<T,Double> elm : list){
			if(maxElm == null || maxElm.getB() < elm.getB()){
				maxElm = elm;
			}
		}
		return maxElm;
	}
	
	/**
	 * Returns the pair in the list with the minimum double value.
	 * @param list a list of pairs where the second element in each pair is a double value
	 * @return the pair in the list with the minimum double value or null if the list is empty
	 */
	public static final <T> Pair<T,Double> minElm(List<Pair<T,Double>> list)
	{
		Pair<T,Double> minElm = null;
		for(Pair<T,Double> elm : list){
			if(minElm == null || minElm.getB() > elm.getB()){
				minElm = elm;
			}
		}
		return minElm;
	}

	/**
	 * Returns the top <code>percent</code> percent elements from a list of
	 * {@link Double} values.
	 * 
	 * @param x
	 *            a list of Double values
	 * @param percent
	 *            a percentage value between 0 and 100
	 * @return a list of the top values
	 */
	public static final List<Double> topPercent(List<Double> x, double percent)
	{
		double ratio = percent / 100;
		if (x.size() == 0) {
			return x;
		} else {
			int newSize = (int) (x.size() * ratio);
			Double[] xa = new Double[x.size()];
			xa = x.toArray(xa);
			int[] indices = Sort.sortIndex(xa, new GenericComparator<Double>(),
					false);
			List<Double> newList = new ArrayList<Double>(newSize);
			for (int i = 0; i < newSize; i++) {
				newList.add(xa[indices[i]]);
			}
			return newList;
		}
	}
	
	/**
	 * Constructs a new list with the integers [0, n-1].
	 * @param n a positive integer
	 * @return a new list containing integers [0, n-1]
	 */
	public static final List<Integer> range(int n)
	{
		List<Integer> newList = new ArrayList<Integer>(n);
		for(int i=0;i<n;i++){
			newList.add(i);
		}
		return newList;
	}
}
