package tmrutil.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;

/**
 * An implementation of a binary heap where values must be unique, but
 * priorities may not be unique.
 * 
 * @author Timothy A. Mann
 * 
 * @param <P>
 *            the priority type
 * @param <V>
 *            the value type
 */
public class UniqueValueHeap<P, V> implements Heap<P, V>
{
	/** A reference to the comparator used for priorities. */
	private Comparator<P> _priorityCmp;
	/** A reference to the comparator used for values. */
	private Comparator<V> _valCmp;

	/** A reference to the underlying heap. */
	List<Pair<P, V>> _elements;
	/** Map from values to priorities. */
	private Map<V, P> _valueToPriorityMap;

	public UniqueValueHeap(int capacity, Comparator<P> priorityCmp,
			Comparator<V> valCmp)
	{
		if (priorityCmp == null) {
			throw new NullPointerException("Encountered null key comparator.");
		}
		_priorityCmp = priorityCmp;
		if (valCmp == null) {
			throw new NullPointerException("Encountered null value comparator.");
		}
		_valCmp = valCmp;

		_elements = new ArrayList<Pair<P, V>>(capacity);
		_valueToPriorityMap = new TreeMap<V, P>(valCmp);
	}

	@Override
	public void insert(P priority, V value)
	{
		if (priority == null) {
			throw new NullPointerException("Encountered null key.");
		}
		if (value == null) {
			throw new NullPointerException("Encountered null value.");
		}

		if (_valueToPriorityMap.containsKey(value)) {
			remove(value);
		}

		// Construct the pair to be added
		Pair<P, V> pair = new Pair<P, V>(priority, value);

		// Add the pair to the end of the list
		_elements.add(pair);

		int pairIndex = _elements.size() - 1;
		int parentIndex = parentOf(pairIndex);

		while (pairIndex != 0 && isPriorityLess(parentIndex, pairIndex)) {
			swap(pairIndex, parentIndex);
			pairIndex = parentIndex;
			parentIndex = parentOf(pairIndex);
		}

		// Update the value to key map
		_valueToPriorityMap.put(value, priority);
	}

	/**
	 * Increases a values priority if the specified priority is larger than the
	 * priority already in the heap. If the value is not found in the heap, then
	 * it is added.
	 * 
	 * @param priority the new priority
	 * @param value the value
	 */
	public void increase(P priority, V value)
	{
		if (_valueToPriorityMap.containsKey(value)) {
			P oldPriority = _valueToPriorityMap.get(value);
			if (_priorityCmp.compare(priority, oldPriority) > 0) {
				insert(priority, value);
			}
		} else {
			insert(priority, value);
		}
	}

	@Override
	public Pair<P, V> peek()
	{
		return _elements.get(0);
	}

	@Override
	public P peekPriority()
	{
		return peek().getA();
	}

	@Override
	public V peekValue()
	{
		return peek().getB();
	}

	@Override
	public Pair<P, V> pop()
	{
		Pair<P, V> oldRoot = _elements.get(0);

		// Swap the first and last element
		swap(0, _elements.size() - 1);
		// Remove the old root
		_elements.remove(_elements.size() - 1);
		_valueToPriorityMap.remove(oldRoot.getB());

		// Update the heap
		int parentIndex = 0;
		int leftIndex = leftChildOf(parentIndex);
		int rightIndex = rightChildOf(parentIndex);

		while ((leftIndex < size() && isPriorityLess(parentIndex, leftIndex))
				|| (rightIndex < size() && isPriorityLess(parentIndex,
						rightIndex))) {
			if (rightIndex < size() && isPriorityLess(leftIndex, rightIndex)) {
				swap(parentIndex, rightIndex);
				parentIndex = rightIndex;
			} else {
				swap(parentIndex, leftIndex);
				parentIndex = leftIndex;
			}
			leftIndex = leftChildOf(parentIndex);
			rightIndex = rightChildOf(parentIndex);
		}

		return oldRoot;
	}

	/**
	 * Returns true if the priority of the priority-value pair indexed by
	 * <code>indexA</code> is strictly less than the priority of the
	 * priority-value pair indexed by <code>indexB</code>; otherwise false is
	 * returned.
	 * 
	 * @param indexA
	 *            an index
	 * @param indexB
	 *            an index
	 * @return true if the priority indexed by <code>indexA</code> is less than
	 *         the priority indexed by <code>indexB</code>; otherwise false
	 */
	private boolean isPriorityLess(int indexA, int indexB)
	{
		return _priorityCmp.compare(_elements.get(indexA).getA(),
				_elements.get(indexB).getA()) < 0;
	}

	@Override
	public P popPriority()
	{
		return pop().getA();
	}

	@Override
	public V popValue()
	{
		return pop().getB();
	}

	@Override
	public int size()
	{
		return _elements.size();
	}

	@Override
	public boolean remove(V value)
	{
		if (_valueToPriorityMap.containsKey(value)) {
			int index = -1;
			for (int i = 0; i < _elements.size(); i++) {
				if (_valCmp.compare(value, _elements.get(i).getB()) == 0) {
					index = i;
					break;
				}
			}

			if (index < 0) {
				throw new DebugException("The index should never be negative.");
			}

			// Swap this with the last index
			int lastI = _elements.size() - 1;
			swap(index, lastI);
			// Remove the last element
			_elements.remove(lastI);
			_valueToPriorityMap.remove(value);

			// Update the heap
			UniqueValueHeap<P, V> heap = new UniqueValueHeap<P, V>(size(),
					_priorityCmp, _valCmp);
			for (Pair<P, V> elm : _elements) {
				heap.insert(elm.getA(), elm.getB());
			}
			_elements = heap._elements;

			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void clear()
	{
		_elements.clear();
		_valueToPriorityMap.clear();
	}

	/**
	 * Swaps the elements at the specified indices.
	 * 
	 * @param indexA
	 *            an element index
	 * @param indexB
	 *            an element index
	 */
	private void swap(int indexA, int indexB)
	{
		Pair<P, V> pairA = _elements.get(indexA);
		Pair<P, V> pairB = _elements.get(indexB);
		_elements.set(indexB, pairA);
		_elements.set(indexA, pairB);
	}

	private int leftChildOf(int parentIndex)
	{
		return 2 * parentIndex + 1;
	}

	private int rightChildOf(int parentIndex)
	{
		return 2 * parentIndex + 2;
	}

	private int parentOf(int childIndex)
	{
		return (childIndex - 1) / 2;
	}
}
