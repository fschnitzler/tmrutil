package tmrutil.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tmrutil.math.MatrixOps;
import tmrutil.xml.FromXML;
import tmrutil.xml.ToXML;
import tmrutil.xml.XMLUtil;

/**
 * This class provides an XML format for storing the values of matrices and
 * scalar values. This class also provides the ability to store and load the
 * data in a file.
 * <p/>
 * 
 * Operations on this class are thread safe.
 * 
 * @author Timothy Mann
 * 
 */
public class MemoryDataStore implements ToXML<MemoryDataStore>, FromXML<MemoryDataStore>
{

	public static final String NEWLINE = System.getProperty("line.separator");
	public static final String DEFAULT_DESCRIPTION = "N/A";

	public static final String DOC_ELM = "TMRUTIL_DATA_STORE";
	public static final String MATRIX_ELM = "TMRUTIL_MATRIX_ELM";
	public static final String SCALAR_ELM = "TMRUTIL_SCALAR_ELM";
	public static final String ID_ATTR = "ID";
	public static final String VALUE_ATTR = "VALUE";
	public static final String DESCRIPTION_ATTR = "DESCRIPTION";
	public static final String NUM_ROWS_ATTR = "NUM_ROWS";
	public static final String NUM_COLS_ATTR = "NUM_COLS";

	/** A set of bound identifiers. */
	private Set<String> _identifiers;
	/** Binds identifiers to matrices and descriptions. */
	private Map<String, Pair<String, double[][]>> _matrices;
	/** Binds identifiers to scalars and descriptions. */
	private Map<String, Pair<String, Double>> _scalars;

	/**
	 * Constructs an empty data store.
	 */
	public MemoryDataStore()
	{
		_identifiers = new TreeSet<String>();
		_matrices = new HashMap<String, Pair<String, double[][]>>();
		_scalars = new HashMap<String, Pair<String, Double>>();
	}

	/**
	 * Returns a matrix given a valid identifier.
	 * 
	 * @param identifier
	 *            an identifier
	 * @return a matrix
	 * @throws NoSuchElementException
	 *             if the identifier is not bound to a matrix
	 */
	public synchronized double[][] getMatrix(String identifier)
			throws NoSuchElementException
	{
		if (_matrices.containsKey(identifier)) {
			return _matrices.get(identifier).getB();
		} else {
			throw new NoSuchElementException(
					"No matrix is bound to identifier (" + identifier + ").");
		}
	}

	/**
	 * Returns a scalar given a valid identifier.
	 * 
	 * @param identifier
	 *            an identifier
	 * @return a scalar
	 * @throws NoSuchElementException
	 *             if the identifier is not bound to a scalar
	 */
	public synchronized double getScalar(String identifier)
			throws NoSuchElementException
	{
		if (_scalars.containsKey(identifier)) {
			return _scalars.get(identifier).getB();
		} else {
			throw new NoSuchElementException(
					"No scalar is bound to identifier (" + identifier + ").");
		}
	}

	/**
	 * Returns a description of a matrix given an identifier.
	 * 
	 * @param identifier
	 *            an identifier
	 * @return a description of a matrix
	 * @throws NoSuchElementException
	 *             if the identifier is not bound to a matrix
	 */
	public synchronized String getMatrixDescription(String identifier)
			throws NoSuchElementException
	{
		if (_matrices.containsKey(identifier)) {
			return _matrices.get(identifier).getA();
		} else {
			throw new NoSuchElementException(
					"No matrix is bound to identifier (" + identifier + ").");
		}
	}

	/**
	 * Returns a description of a scalar value given an identifier.
	 * 
	 * @param identifier
	 *            an identifier
	 * @return a description of a scalar value
	 * @throws NoSuchElementException
	 *             if the identifier is not bound to a scalar
	 */
	public synchronized String getScalarDescription(String identifier)
			throws NoSuchElementException
	{
		if (_scalars.containsKey(identifier)) {
			return _scalars.get(identifier).getA();
		} else {
			throw new NoSuchElementException(
					"No scalar is bound to identifier (" + identifier + ").");
		}
	}

	/**
	 * Binds a matrix and description to an identifier. If the identifier is
	 * already bound to either a matrix or a scalar then the previous binding is
	 * replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param matrix
	 *            a matrix
	 * @param description
	 *            a description of the matrix data
	 */
	public synchronized void bind(String identifier, double[][] matrix,
			String description)
	{
		if (_identifiers.contains(identifier)) {
			if (_scalars.containsKey(identifier)) {
				_scalars.remove(identifier);
			}
		} else {
			_identifiers.add(identifier);
		}
		_matrices.put(identifier, new Pair<String, double[][]>(description,
				matrix));
	}

	/**
	 * Binds a matrix to an identifier. The default description is applied. If
	 * the identifier is already bound to either a matrix or a scalar then the
	 * previous binding is replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param matrix
	 *            a matrix
	 */
	public void bind(String identifier, double[][] matrix)
	{
		bind(identifier, matrix, DEFAULT_DESCRIPTION);
	}

	/**
	 * Binds a vector and description to an identifier. If the identifier is
	 * already bound to either a matrix or a scalar then the previous binding is
	 * replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param vector
	 *            a vector
	 * @param description
	 *            a description of the vector
	 */
	public void bind(String identifier, double[] vector, String description)
	{
		bind(identifier, new double[][] { vector }, description);
	}

	/**
	 * Bind an integer vector and description to an identifier. If the
	 * identifier is already bound to either a matrix or scalar, then the
	 * previous binding is overwritten.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param vector
	 *            an integer vector
	 * @param description
	 *            a description of the vector
	 */
	public void bind(String identifier, int[] vector, String description)
	{
		double[] dvector = new double[vector.length];
		for (int i = 0; i < vector.length; i++) {
			dvector[i] = vector[i];
		}
		bind(identifier, dvector, description);
	}

	/**
	 * Binds a vector to an identifier. The default description is applied. If
	 * the identifier is already bound to either a matrix or a scalar, then the
	 * previous binding is replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param vector
	 *            a vector
	 */
	public void bind(String identifier, int[] vector)
	{
		bind(identifier, vector, DEFAULT_DESCRIPTION);
	}

	/**
	 * Binds a vector and description to an identifier. If the identifier is
	 * already bound to either a matrix or a scalar then the previous binding is
	 * replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param vector
	 *            a vector
	 * @param description
	 *            a description of the vector
	 */
	public void bind(String identifier, List<Double> vector, String description)
	{
		bind(identifier, ListUtil.toArray(vector), description);
	}

	/**
	 * Binds a vector to an identifier. The default description is applied. If
	 * the identifier is already bound to either a matrix or a scalar then the
	 * previous binding is replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param vector
	 *            a vector
	 */
	public void bind(String identifier, double[] vector)
	{
		bind(identifier, vector, DEFAULT_DESCRIPTION);
	}

	/**
	 * Binds a vector to an identifier. The default description is applied. If
	 * the identifier is already bound to either a matrix or a scalar then the
	 * previous binding is replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param vector
	 *            a vector
	 */
	public void bind(String identifier, List<Double> vector)
	{
		bind(identifier, ListUtil.toArray(vector));
	}

	/**
	 * Binds a scalar and description to an identifier. If the identifier is
	 * already bound to either a matrix or a scalar then the previous binding is
	 * replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param scalar
	 *            a scalar value
	 * @param description
	 *            a description of the scalar data
	 */
	public synchronized void bind(String identifier, double scalar,
			String description)
	{
		if (_identifiers.contains(identifier)) {
			if (_matrices.containsKey(identifier)) {
				_matrices.remove(identifier);
			}
		} else {
			_identifiers.add(identifier);
		}
		_scalars.put(identifier, new Pair<String, Double>(description, scalar));
	}

	/**
	 * Binds a scalar to an identifier. The default description is applied. If
	 * the identifier is already bound to either a matrix or a scalar then the
	 * previous binding is replaced.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param scalar
	 *            a scalar value
	 */
	public void bind(String identifier, double scalar)
	{
		bind(identifier, scalar, DEFAULT_DESCRIPTION);
	}

	/**
	 * Appends a row to an existing bound matrix or creates a new bound matrix
	 * with one row if the identifier is not taken. This also updates the
	 * description of the matrix.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param row
	 *            a matrix row
	 * @param description
	 *            a description of the matrix
	 */
	public synchronized void appendRow(String identifier, double[] row,
			String description)
	{
		Pair<String, double[][]> pair = _matrices.get(identifier);
		if (pair == null) {
			bind(identifier, row, description);
		} else {
			double[][] m = pair.getB();
			_matrices.put(identifier, new Pair<String, double[][]>(description,
					MatrixOps.appendRow(m, row)));
		}
	}

	/**
	 * Appends a row to an existing bound matrix or creates a new bound matrix
	 * with one row if the identifier is not taken. If the matrix already has a
	 * description then the existing description is used; otherwise the default
	 * description is applied.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param row
	 *            a matrix row
	 */
	public synchronized void appendRow(String identifier, double[] row)
	{
		String description = DEFAULT_DESCRIPTION;
		if (_matrices.containsKey(identifier)) {
			description = _matrices.get(identifier).getA();
		}
		appendRow(identifier, row, description);
	}

	/**
	 * Appends a row to an existing bound matrix or creates a new bound matrix
	 * with one row if the identifier is not taken. This also updates the
	 * description of the matrix.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param row
	 *            a matrix row
	 * @param description
	 *            a description of the matrix
	 */
	public void appendRow(String identifier, List<Double> row,
			String description)
	{
		appendRow(identifier, ListUtil.toArray(row), description);
	}

	/**
	 * Appends a row to an existing bound matrix or creates a new bound matrix
	 * with one row if the identifier is not taken. If the matrix already has a
	 * description then the existing description is used; otherwise the default
	 * description is applied.
	 * 
	 * @param identifier
	 *            an identifier
	 * @param row
	 *            a matrix row
	 */
	public void appendRow(String identifier, List<Double> row)
	{
		appendRow(identifier, ListUtil.toArray(row));
	}

	/**
	 * Appends a subsampled row to an existing bound matrix or creates a new
	 * bound matrix from a subsample of the given row if the identifier is not
	 * taken.
	 * 
	 * @param identifier an identifier
	 * @param row a row to be subsampled 
	 * @param sampleFrequency the number of elements between samples
	 */
	public void appendSubsampledRow(String identifier, double[] row,
			int sampleFrequency)
	{
		double[] subsampledRow = subsample(row, sampleFrequency);
		appendRow(identifier, subsampledRow);
		bind(identifier + "_sample_frequency", sampleFrequency);
	}
	
	/**
	 * Appends a subsampled row to an existing bound matrix or creates a new
	 * bound matrix from a subsample of the given row if the identifier is not
	 * taken.
	 * 
	 * @param identifier an identifier
	 * @param row a row to be subsampled 
	 * @param sampleFrequency the number of elements between samples
	 */
	public void appendSubsampledRow(String identifier, List<Double> row, int sampleFrequency)
	{
		appendSubsampledRow(identifier, ListUtil.toArray(row), sampleFrequency);
	}

	/**
	 * A helper function for subsampling data.
	 * 
	 * @param completeData
	 *            the complete array of data
	 * @param sampleFrequency
	 *            the number of elements between samples
	 * @return a smaller array achieved by subsampling from the complete data
	 */
	public static final double[] subsample(double[] completeData,
			int sampleFrequency)
	{
		double[] subsampledData = new double[completeData.length
				/ sampleFrequency];
		for (int i = 0; i < subsampledData.length; i++) {
			int cind = sampleFrequency * i;
			subsampledData[i] = completeData[cind];
		}
		return subsampledData;
	}

	@Override
	public synchronized Element toXML(MemoryDataStore t, Document document)
	{
		Element docElm = document.createElement(DOC_ELM);
		document.appendChild(docElm);

		// Add the matrices to the document
		Set<String> identifiers = _matrices.keySet();
		for (String id : identifiers) {
			Pair<String, double[][]> pair = _matrices.get(id);
			Element matrixElm = document.createElement(MATRIX_ELM);
			matrixElm.setAttribute(ID_ATTR, id);
			matrixElm.setAttribute(DESCRIPTION_ATTR, pair.getA());
			StringBuffer matStr = new StringBuffer();
			double[][] mat = pair.getB();
			int numRows = MatrixOps.getNumRows(mat);
			int numCols = MatrixOps.getNumCols(mat);
			matrixElm.setAttribute(NUM_ROWS_ATTR, String.valueOf(numRows));
			matrixElm.setAttribute(NUM_COLS_ATTR, String.valueOf(numCols));
			for (int r = 0; r < numRows; r++) {
				for (int c = 0; c < numCols; c++) {
					matStr.append(String.valueOf(mat[r][c]) + " ");
				}
				matStr.append("\n");
			}
			matrixElm.setTextContent(matStr.toString());
			docElm.appendChild(matrixElm);
		}

		// Add the scalars to the document
		identifiers = _scalars.keySet();
		for (String id : identifiers) {
			Pair<String, Double> pair = _scalars.get(id);
			Element scalarElm = document.createElement(SCALAR_ELM);
			scalarElm.setAttribute(ID_ATTR, id);
			scalarElm.setAttribute(VALUE_ATTR, String.valueOf(pair.getB()));
			scalarElm.setAttribute(DESCRIPTION_ATTR, pair.getA());
			docElm.appendChild(scalarElm);
		}

		return docElm;
	}

	@Override
	public synchronized MemoryDataStore fromXML(Element element)
			throws IllegalArgumentException
	{
		NodeList matrixElms = element.getElementsByTagName(MATRIX_ELM);
		for (int i = 0; i < matrixElms.getLength(); i++) {
			Element matrixElm = (Element) matrixElms.item(i);
			String id = matrixElm.getAttribute(ID_ATTR);
			String description = matrixElm.getAttribute(DESCRIPTION_ATTR);
			int numRows = Integer.parseInt(matrixElm.getAttribute(NUM_ROWS_ATTR));
			int numCols = Integer.parseInt(matrixElm.getAttribute(NUM_COLS_ATTR));
			double[][] matrix = new double[numRows][numCols];
			String[] vals = matrixElm.getTextContent().split("\\s+");
			int ind = 0;
			for (int r = 0; r < numRows; r++) {
				for (int c = 0; c < numCols; c++) {
					if (vals[ind].isEmpty()) {
						ind++;
					} else {
						matrix[r][c] = Double.parseDouble(vals[ind++]);
					}
				}
			}
			bind(id, matrix, description);
		}

		NodeList scalarElms = element.getElementsByTagName(SCALAR_ELM);
		for (int i = 0; i < scalarElms.getLength(); i++) {
			Element scalarElm = (Element) scalarElms.item(i);
			bind(scalarElm.getAttribute(ID_ATTR),
					Double.parseDouble(scalarElm.getAttribute(VALUE_ATTR)),
					scalarElm.getAttribute(DESCRIPTION_ATTR));
		}

		return this;
	}

	/**
	 * Saves the variables stored in this data store object to a GNU Octave
	 * readable text data file.
	 * 
	 * @param filename
	 *            the name of the file to write to
	 * @throws IOException
	 *             thrown to indicate that an I/O exception occurred while
	 *             writing to the text file
	 */
	public synchronized void saveAsOctaveTxt(String filename)
			throws IOException
	{
		Map<String, Double> scalars = new HashMap<String, Double>();
		Map<String, double[][]> matrices = new HashMap<String, double[][]>();
		Set<String> scalarKeys = _scalars.keySet();
		for (String key : scalarKeys) {
			scalars.put(key, _scalars.get(key).getB());
		}
		Set<String> matrixKeys = _matrices.keySet();
		for (String key : matrixKeys) {
			matrices.put(key, _matrices.get(key).getB());
		}
		OctaveUtil.save(filename, matrices, scalars);
	}

	/**
	 * Saves the variables stored in this data store to a Python script.
	 * 
	 * @param filename
	 *            the name of the file to write to
	 * @throws IOException
	 *             thrown to indicate that an I/O error occurred while writing
	 *             to the text/python file
	 */
	public synchronized void saveAsPython(String filename) throws IOException
	{
		FileWriter fwriter = new FileWriter(filename);

		// Get the current date and time
		DateFormat df = DateFormat.getDateInstance();
		String dateStr = df.format(new Date());

		// Add the python header information
		fwriter.write("#" + NEWLINE);
		fwriter.write("# File      : " + filename + NEWLINE);
		fwriter.write("# Generated : " + getClass().getName() + NEWLINE);
		fwriter.write("# Date      : " + dateStr);
		fwriter.write("#" + NEWLINE);
		fwriter.write("# This python script is a data file." + NEWLINE);
		fwriter.write("#" + NEWLINE);

		// Save the scalars first
		Set<String> svarNames = _scalars.keySet();
		for (String varName : svarNames) {
			String description = this.getScalarDescription(varName);
			if (description != null && !description.equals("")) {
				fwriter.write("\"\"\"" + NEWLINE);
				fwriter.write(description);
				fwriter.write("\"\"\"" + NEWLINE);
			}
			double val = this.getScalar(varName);
			fwriter.write(varName + " = " + val + NEWLINE);
		}

		// Save the matrices second
		Set<String> mvarNames = _matrices.keySet();
		for (String varName : mvarNames) {
			String description = this.getMatrixDescription(varName);
			if (description != null && !description.equals("")) {
				fwriter.write("\"\"\"" + NEWLINE);
				fwriter.write(description + NEWLINE);
				fwriter.write("\"\"\"" + NEWLINE);
			}
			double[][] mat = this.getMatrix(varName);
			fwriter.write(varName + " = [");
			for (int r = 0; r < mat.length; r++) {
				fwriter.write("[");
				for (int c = 0; c < mat[r].length; c++) {
					if (c < mat[r].length - 1) {
						fwriter.write(mat[r][c] + ", ");
					} else {
						fwriter.write(String.valueOf(mat[r][c]));
					}
				}
				fwriter.write("]");
			}
			fwriter.write("]" + NEWLINE);
		}

		// Close the file
		fwriter.close();
	}

	/**
	 * Saves the variables stored in this data store object to a Matlab data
	 * file.
	 * 
	 * @param filename
	 *            the name of the file to write to
	 * @throws IOException
	 *             thrown to indicate that an I/O exception occurred while
	 *             writing to the text file
	 */
	public synchronized void saveAsMatlabFile(String filename)
			throws IOException
	{
		Map<String, Double> scalars = new HashMap<String, Double>();
		Map<String, double[][]> matrices = new HashMap<String, double[][]>();
		Set<String> scalarKeys = _scalars.keySet();
		for (String key : scalarKeys) {
			scalars.put(key, _scalars.get(key).getB());
		}
		Set<String> matrixKeys = _matrices.keySet();
		for (String key : matrixKeys) {
			matrices.put(key, _matrices.get(key).getB());
		}
		MatlabUtil.save(filename, matrices, scalars);
	}

	/**
	 * Saves the variables stored in this data store object to an XML file.
	 * 
	 * @param filename
	 *            the name of the file to write to
	 * @throws IOException
	 *             thrown to indicate that an I/O exception occurred while
	 *             writing to the text file
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 */
	public synchronized void save(String filename) throws IOException,
			TransformerFactoryConfigurationError, TransformerException
	{
		Document doc = XMLUtil.createDocument();
		toXML(this, doc);
		Source source = new DOMSource(doc);
		Result result = new StreamResult(new File(filename));
		Transformer xformer = TransformerFactory.newInstance().newTransformer();
		xformer.transform(source, result);
	}

	/**
	 * Loads data stored in an XML file into this data store object.
	 * 
	 * @param filename
	 *            the name of the file to read from
	 * @throws IOException
	 *             thrown to indicate that an I/O exception occurred while
	 *             reading from the text file
	 * @throws SAXException
	 */
	public synchronized void load(String filename) throws IOException,
			SAXException
	{
		Document doc = XMLUtil.parse(filename);
		fromXML(doc.getDocumentElement());
	}

	public static void main(String[] args)
	{
		MemoryDataStore dstore1 = new MemoryDataStore();
		dstore1.bind("m1", MatrixOps.random(5, 10), "A random matrix.");
		dstore1.bind("m2", MatrixOps.random(2, 2), "A small random matrix.");
		System.out.println(MatrixOps.toString(dstore1.getMatrix("m2")));

		Element elm = dstore1.toXML(dstore1, XMLUtil.createDocument());
		MemoryDataStore dstore2 = new MemoryDataStore();
		dstore2 = dstore2.fromXML(elm);
		System.out.println(MatrixOps.toString(dstore2.getMatrix("m2")));
	}
}
