package tmrutil.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

/**
 * A data structure for containing the top X objects. The top X objects are
 * determined by the natural ordering values associated with each stored object.
 * 
 * @author Timothy Mann
 * 
 * @param <K>
 *            a naturally ordered type
 * @param <V>
 *            the type of object to store
 */
public class TopXList<K extends Comparable<K>, V>
{
	/**
	 * The maximum number of elements that can be stored in this list.
	 */
	private int _max;

	/**
	 * A sorted map to store the top items in.
	 */
	private TreeMap<K, V> _topItems;

	/**
	 * Constructs a top X list with a maximum size.
	 * @param max the maximum number of items that can be stored in this data structure
	 * @throws IllegalArgumentException if <code>max &lt; 1</code>
	 */
	public TopXList(int max)
		throws IllegalArgumentException
	{
		if (max < 1) {
			throw new IllegalArgumentException("The maximum size of a "
					+ getClass().getName() + " must be positive");
		}
		_max = max;
		_topItems = new TreeMap<K,V>();
	}

	/**
	 * Adds an object with a score to the top X list.
	 * @param score the score to compare with other objects
	 * @param item the item to be stored
	 * @return true if the item was added; otherwise, false
	 */
	public boolean add(K score, V item)
	{
		if(_topItems.size() < _max){
			_topItems.put(score, item);
			return true;
		}else{
			K key = _topItems.firstKey();
			if(score.compareTo(key) > 0){
				_topItems.remove(key);
				_topItems.put(score, item);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns a collection of the top X elements.
	 * @return a collection of the top X elements
	 */
	public Collection<V> getTopX()
	{
		return _topItems.values();
	}
	
	/**
	 * Returns a list of the top X elements.
	 * @return a list of the top X elements
	 */
	public List<V> getTopXList()
	{
		Set<K> keys = _topItems.keySet();
		Iterator<K> iter = keys.iterator();
		List<V> topList = new ArrayList<V>(keys.size());
		while(iter.hasNext()){
			topList.add(_topItems.get(iter.next()));
		}
		return topList;
	}
}
