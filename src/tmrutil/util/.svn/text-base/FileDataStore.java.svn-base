package tmrutil.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import tmrutil.math.MatrixOps;

/**
 * Implements incremental storage of scalar and matrix data to a set of files in
 * a specified directory.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FileDataStore implements DataStore
{
	public static final String VALID_ID_REGEX = "[\\p{Alpha}_][\\p{Alnum}_]*";
	public static final String BIND_FILE = "bindings";
	public static final String SCALAR_FILE = "scalars";
	public static final String MATRIX_FILE = "matrices";
	public static final String MATRIX_DIR = "matrices_data";
	public static final String DESC_FILE = "description.txt";
	public static final String TMP_EXT = ".tmp";
	public static final String PATH_SEPARATOR = System.getProperty("file.separator");

	public static final char SPLIT_CHAR = ':';
	public static final String SPLIT_STR = String.valueOf(SPLIT_CHAR);
	public static final String SCALAR_STR = "1";
	public static final String MATRIX_STR = "0";
	public static final String ANY_STR = "\\p{Graph}+";

	private File _dataDir;
	private File _bindFile;
	private File _scalarFile;
	private File _matrixFile;
	private File _descFile;
	private File _matDir;

	private Set<String> _ids;
	private Map<String, Boolean> _isScalar;
	private Map<String, Integer> _matrixNumRows;
	private Map<String, Integer> _matrixNumCols;

	private boolean _readOnly;

	public FileDataStore(File dataDir) throws IOException
	{
		this(dataDir, false);
	}

	public FileDataStore(File dataDir, boolean readOnly) throws IOException
	{
		if (dataDir == null) {
			throw new NullPointerException(
					"Cannot construct data store with null file.");
		}
		_readOnly = readOnly;

		_dataDir = dataDir;
		if (!_dataDir.exists()) {
			if (_readOnly) {
				throw new IllegalStateException(
						"The specified path \""
								+ dataDir.getAbsolutePath()
								+ "\" does not exist and cannot be created in read only mode.");
			}
			_dataDir.mkdirs();
		} else if (!_dataDir.isDirectory()) {
			throw new IOException("The specified path \""
					+ dataDir.getAbsolutePath() + "\" is not a directory.");
		}
		String absPath = _dataDir.getAbsolutePath();
		
		_descFile = new File(absPath + PATH_SEPARATOR + DESC_FILE);
		
		_bindFile = new File(absPath + PATH_SEPARATOR + BIND_FILE);
		if (!_bindFile.exists()) {
			if (_readOnly) {
				throw new IllegalStateException(
						"The bindings file does not exist and cannot be created in read only mode.");
			}
			_bindFile.createNewFile();
		}
		_scalarFile = new File(absPath + PATH_SEPARATOR + SCALAR_FILE);
		if (!_scalarFile.exists()) {
			if (_readOnly) {
				throw new IllegalStateException(
						"The scalars file does not exist and cannot be created in read only mode.");
			}
			_scalarFile.createNewFile();
		}
		_matrixFile = new File(absPath + PATH_SEPARATOR + MATRIX_FILE);
		if (!_matrixFile.exists()) {
			if (_readOnly) {
				throw new IllegalStateException(
						"The matrices file does not exist and cannot be created in read only mode.");
			}
			_matrixFile.createNewFile();
		}
		_matDir = new File(absPath + PATH_SEPARATOR + MATRIX_DIR);
		if (!_matDir.exists()) {
			if (_readOnly) {
				throw new IllegalStateException(
						"The matrix data directory does not exist and cannot be created in read only mode.");
			}
			_matDir.mkdir();
		} else if (!_matDir.isDirectory()) {
			throw new IOException("The matrix data path \""
					+ _matDir.getAbsolutePath() + "\" is not a directory.");
		}

		_ids = new HashSet<String>();
		_isScalar = new HashMap<String, Boolean>();
		populateIdentifiers();

		_matrixNumRows = new HashMap<String, Integer>();
		_matrixNumCols = new HashMap<String, Integer>();
		populateMatrixSizes();
	}

	private void populateIdentifiers() throws IOException
	{
		FileReader fread = new FileReader(_bindFile);
		BufferedReader bread = new BufferedReader(fread);
		String line = null;
		while ((line = bread.readLine()) != null) {
			String[] tokens = line.split(SPLIT_STR);
			String identifier = tokens[0];
			boolean isScalar = tokens[1].equals(SCALAR_STR);
			_ids.add(identifier);
			_isScalar.put(identifier, isScalar);
		}
		bread.close();
	}

	private void populateMatrixSizes() throws IOException
	{
		FileReader fread = new FileReader(_matrixFile);
		BufferedReader bread = new BufferedReader(fread);
		String line = null;
		while ((line = bread.readLine()) != null) {
			String[] tokens = line.split(SPLIT_STR);
			String identifier = tokens[0];
			int numRows = Integer.parseInt(tokens[1]);
			int numCols = Integer.parseInt(tokens[2]);

			_matrixNumRows.put(identifier, numRows);
			_matrixNumCols.put(identifier, numCols);
		}
	}

	private File matrixDataFile(String identifier)
	{
		return new File(_matDir.getAbsolutePath() + PATH_SEPARATOR + identifier);
	}

	@Override
	public boolean isValid(String identifier)
	{
		return identifier.matches(VALID_ID_REGEX);
	}

	@Override
	public Set<String> boundIdentifiers()
	{
		return new HashSet<String>(_ids);
	}
	
	/**
	 * Returns a description of this data storage or the empty string if none exists.
	 * @return a description of this data storage
	 */
	public String description()
	{
		try{
			FileReader freader = new FileReader(_descFile);
			BufferedReader breader = new BufferedReader(freader);
			StringBuilder desc = new StringBuilder();
			String line = breader.readLine();
			while(line != null){
				desc.append(line + TextFileUtil.NEWLINE);
				line = breader.readLine();
			}
			return desc.toString();
		}catch(IOException ex){
			return "";
		}
	}
	
	/**
	 * Sets the current description of this data store overwriting any previous description.
	 * @param description a description of this data store
	 * @throws IOException if an I/O error occurs while writing the description to a file
	 */
	public void setDescription(String description) throws IOException
	{
		if(_readOnly){
			throw new IllegalStateException("Cannot modify description in read-only mode.");
		}
		FileWriter fwriter = new FileWriter(_descFile, false);
		BufferedWriter bwriter = new BufferedWriter(fwriter);
		bwriter.write(description);
		bwriter.close();
	}
	
	/**
	 * Appends <code>moreDescription</code> to the current description.
	 * @param moreDescription string that will augment the current description
	 * @throws IOException if an I/O error occurs while writing the description to a file
	 */
	public void appendDescription(String moreDescription) throws IOException
	{
		if(_readOnly){
			throw new IllegalStateException("Cannot modify description in read-only mode.");
		}
		FileWriter fwriter = new FileWriter(_descFile, true);
		BufferedWriter bwriter = new BufferedWriter(fwriter);
		bwriter.write(moreDescription);
		bwriter.close();
	}

	@Override
	public void clear()
	{
		Set<String> ids = boundIdentifiers();
		for (String id : ids) {
			remove(id);
		}
		try{
			setDescription("");
		}catch(IOException ex){ /* Don't do anything. */ }
	}

	@Override
	public boolean isBound(String identifier)
	{
		return _ids.contains(identifier);
	}

	@Override
	public boolean isScalar(String identifier)
	{
		Boolean response = _isScalar.get(identifier);
		if (response == null) {
			return false;
		} else {
			return response.booleanValue();
		}
	}

	@Override
	public boolean isMatrix(String identifier)
	{
		if (isBound(identifier) && !isScalar(identifier)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns true if this file data store instance is in read only mode. In
	 * read only mode the file data store can only be inspected and cannot be
	 * modified.
	 * 
	 * @return true if in read only mode; otherwise false
	 */
	public boolean readOnly()
	{
		return _readOnly;
	}

	@Override
	public boolean remove(String identifier)
	{
		if (_readOnly) {
			throw new IllegalStateException(
					"Cannot remove bindings while in read only mode.");
		}
		if (isBound(identifier)) {
			if (isScalar(identifier)) {
				return removeScalar(identifier);
			} else {
				return removeMatrix(identifier);
			}
		} else {
			return false;
		}
	}

	private boolean removeScalar(String identifier)
	{
		String deleteBindingStr = identifier + SPLIT_STR + SCALAR_STR;
		String deleteValueStr = identifier + SPLIT_STR + ANY_STR;

		try {
			File tmpBindFile = new File(_bindFile.getAbsolutePath() + TMP_EXT);
			TextFileUtil.deleteLines(_bindFile, tmpBindFile, deleteBindingStr);
			File tmpScalarFile = new File(_scalarFile.getAbsolutePath()
					+ TMP_EXT);
			TextFileUtil.deleteLines(_scalarFile, tmpScalarFile, deleteValueStr);

			_ids.remove(identifier);
			_isScalar.remove(identifier);
		} catch (IOException ex) {
			return false;
		}

		return true;
	}

	private boolean removeMatrix(String identifier)
	{
		String deleteBindingStr = identifier + SPLIT_STR + MATRIX_STR;
		String deleteMatrixStr = identifier + SPLIT_STR + ANY_STR;

		try {
			File tmpBindFile = new File(_bindFile.getAbsoluteFile() + TMP_EXT);
			TextFileUtil.deleteLines(_bindFile, tmpBindFile, deleteBindingStr);

			File tmpMatrixFile = new File(_matrixFile.getAbsolutePath()
					+ TMP_EXT);
			TextFileUtil.deleteLines(_matrixFile, tmpMatrixFile,
					deleteMatrixStr);

			File matrixDataFile = matrixDataFile(identifier);
			matrixDataFile.delete();

			_ids.remove(identifier);
			_isScalar.remove(identifier);

			_matrixNumRows.remove(identifier);
			_matrixNumCols.remove(identifier);
		} catch (IOException ex) {
			return false;
		}

		return true;
	}

	@Override
	public void bind(String identifier, double dscalar)
	{
		if (_readOnly) {
			throw new IllegalStateException(
					"Cannot bind data while in read only mode.");
		}
		if (identifier == null) {
			throw new NullPointerException(
					"Cannot create binding with null identifier.");
		}
		if (!isValid(identifier)) {
			throw new IllegalArgumentException("String \"" + identifier
					+ "\" is not a valid identifier.");
		}

		if (isBound(identifier)) {
			remove(identifier);
		}

		try {
			BufferedWriter bindWriter = new BufferedWriter(new FileWriter(
					_bindFile, true));
			bindWriter.append(identifier + SPLIT_STR + SCALAR_STR
					+ TextFileUtil.NEWLINE);
			bindWriter.close();

			BufferedWriter scalarWriter = new BufferedWriter(new FileWriter(
					_scalarFile, true));
			scalarWriter.append(identifier + SPLIT_STR + dscalar
					+ TextFileUtil.NEWLINE);
			scalarWriter.close();

			_ids.add(identifier);
			_isScalar.put(identifier, true);

		} catch (IOException ex) {
			throw new DebugException(
					"An error occurred while binding a scalar to an identifier.",
					ex);
		}
	}

	@Override
	public void bind(String identifier, Number nscalar)
	{
		bind(identifier, nscalar.doubleValue());
	}

	@Override
	public void bind(String identifier, int[] ivector)
	{
		bind(identifier, new int[][] { ivector });

	}

	@Override
	public void bind(String identifier, float[] fvector)
	{
		bind(identifier, new float[][] { fvector });
	}

	@Override
	public void bind(String identifier, double[] dvector)
	{
		bind(identifier, new double[][] { dvector });
	}

	@Override
	public void bind(String identifier, Collection<? extends Number> cvector)
	{
		double[] dvector = new double[cvector.size()];
		int i = 0;
		for (Number n : cvector) {
			dvector[i] = n.doubleValue();
			i++;
		}
		bind(identifier, dvector);
	}

	private static interface NumericArrayWrapper
	{
		public double get(int row, int col);

		public int numRows();

		public int numCols();
	}

	private static class IntArrayWrapper implements NumericArrayWrapper
	{
		private int[][] _mat;

		public IntArrayWrapper(int[][] mat)
		{
			_mat = mat;
		}

		@Override
		public double get(int row, int col)
		{
			return _mat[row][col];
		}

		@Override
		public int numRows()
		{
			return _mat.length;
		}

		@Override
		public int numCols()
		{
			return _mat[0].length;
		}
	}

	private static class FloatArrayWrapper implements NumericArrayWrapper
	{
		private float[][] _mat;

		public FloatArrayWrapper(float[][] mat)
		{
			_mat = mat;
		}

		@Override
		public double get(int row, int col)
		{
			return _mat[row][col];
		}

		@Override
		public int numRows()
		{
			return _mat.length;
		}

		@Override
		public int numCols()
		{
			return _mat[0].length;
		}
	}

	private static class DoubleArrayWrapper implements NumericArrayWrapper
	{
		private double[][] _mat;

		public DoubleArrayWrapper(double[][] mat)
		{
			_mat = mat;
		}

		@Override
		public double get(int row, int col)
		{
			return _mat[row][col];
		}

		@Override
		public int numRows()
		{
			return _mat.length;
		}

		@Override
		public int numCols()
		{
			return _mat[0].length;
		}
	}

	@Override
	public void bind(String identifier, int[][] imatrix)
	{
		bind(identifier, new IntArrayWrapper(imatrix));
	}

	@Override
	public void bind(String identifier, float[][] fmatrix)
	{
		bind(identifier, new FloatArrayWrapper(fmatrix));
	}

	@Override
	public void bind(String identifier, double[][] dmatrix)
	{
		bind(identifier, new DoubleArrayWrapper(dmatrix));
	}

	private void bind(String identifier, NumericArrayWrapper matrix)
	{
		if (_readOnly) {
			throw new IllegalStateException(
					"Cannot bind data while in read only mode.");
		}
		if (identifier == null) {
			throw new NullPointerException(
					"Cannot create binding with null identifier.");
		}
		if (!isValid(identifier)) {
			throw new IllegalArgumentException("String \"" + identifier
					+ "\" is not a valid identifier.");
		}

		if (isBound(identifier)) {
			remove(identifier);
		}

		try {
			BufferedWriter bindWriter = new BufferedWriter(new FileWriter(
					_bindFile, true));
			bindWriter.append(identifier + SPLIT_STR + MATRIX_STR
					+ TextFileUtil.NEWLINE);
			bindWriter.close();

			BufferedWriter matrixWriter = new BufferedWriter(new FileWriter(
					_matrixFile, true));
			matrixWriter.append(identifier + SPLIT_STR + matrix.numRows()
					+ SPLIT_STR + matrix.numCols() + TextFileUtil.NEWLINE);
			matrixWriter.close();

			// BufferedWriter matrixDataWriter = new BufferedWriter(
			// new FileWriter(matrixDataFile(identifier)));
			//
			// for (int r = 0; r < matrix.numRows(); r++) {
			// for (int c = 0; c < matrix.numCols(); c++) {
			// double value = matrix.get(r,c);
			// if (c != 0){
			// matrixDataWriter.append(SPLIT_CHAR);
			// }
			// if(value != 0){
			// matrixDataWriter.append(String.valueOf(value));
			// }
			// }
			// matrixDataWriter.append(TextFileUtil.NEWLINE);
			// }
			// matrixDataWriter.close();

			GZIPOutputStream gzstream = new GZIPOutputStream(
					new FileOutputStream(matrixDataFile(identifier)));
			DataOutputStream matrixDataStream = new DataOutputStream(gzstream);
			for (int r = 0; r < matrix.numRows(); r++) {
				for (int c = 0; c < matrix.numCols(); c++) {
					matrixDataStream.writeDouble(matrix.get(r, c));
				}
			}
			matrixDataStream.close();

			_ids.add(identifier);
			_isScalar.put(identifier, false);

			_matrixNumRows.put(identifier, matrix.numRows());
			_matrixNumCols.put(identifier, matrix.numCols());

		} catch (IOException ex) {
			throw new DebugException(
					"An error occurred while binding a matrix to an identifier.",
					ex);
		}
	}

	@Override
	public void appendRow(String identifier, int[] irow)
	{
		appendRow(identifier, new IntArrayWrapper(new int[][] { irow }));
	}

	@Override
	public void appendRow(String identifier, float[] frow)
	{
		appendRow(identifier, new FloatArrayWrapper(new float[][] { frow }));
	}

	@Override
	public void appendRow(String identifier, double[] drow)
	{
		appendRow(identifier, new DoubleArrayWrapper(new double[][] { drow }));
	}

	@Override
	public void appendRow(String identifier, Collection<? extends Number> crow)
	{
		double[] drow = new double[crow.size()];
		int i = 0;
		for (Number n : crow) {
			drow[i] = n.doubleValue();
			i++;
		}
		appendRow(identifier, drow);
	}

	private void appendRow(String identifier, NumericArrayWrapper row)
	{
		if (_readOnly) {
			throw new IllegalStateException(
					"Cannot append matrix data while in read only mode.");
		}
		if (isMatrix(identifier)) {

			try {
				GZIPOutputStream gzstream = new GZIPOutputStream(
						new FileOutputStream(matrixDataFile(identifier), true));
				DataOutputStream matrixDataStream = new DataOutputStream(
						gzstream);
				for (int r = 0; r < row.numRows(); r++) {
					for (int c = 0; c < row.numCols(); c++) {
						matrixDataStream.writeDouble(row.get(r, c));
					}
				}
				matrixDataStream.close();

				int nrows = matrixNumRows(identifier);
				int ncols = matrixNumCols(identifier);
				String oldEntry = identifier + SPLIT_STR + nrows + SPLIT_STR
						+ ncols;
				String newEntry = identifier + SPLIT_STR
						+ (nrows + row.numRows()) + SPLIT_STR + ncols;
				TextFileUtil.findReplace(oldEntry, newEntry,
						_matrixFile.getAbsolutePath());

				_matrixNumRows.put(identifier, nrows + row.numRows());
			} catch (IOException ex) {
				throw new DebugException(
						"An error occurred while appending a row to a bound matrix.",
						ex);
			}

		} else {
			bind(identifier, row);
		}
	}

	public int matrixNumRows(String identifier)
	{
		if (isMatrix(identifier)) {
			return _matrixNumRows.get(identifier);
		} else {
			throw new IllegalArgumentException("Identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}

	}

	public int matrixNumCols(String identifier)
	{
		if (isMatrix(identifier)) {
			return _matrixNumCols.get(identifier);
		} else {
			throw new IllegalArgumentException("Identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	public double getScalar(String identifier) throws IOException
	{
		if (isScalar(identifier)) {
			BufferedReader scalarReader = new BufferedReader(new FileReader(
					_scalarFile));
			String line = null;
			while ((line = scalarReader.readLine()) != null) {
				if (line.matches(identifier + SPLIT_STR + ANY_STR)) {
					String[] tokens = line.split(SPLIT_STR);
					return Double.parseDouble(tokens[1]);
				}
			}
			throw new DebugException(
					"Reach end of scalar file without finding identifier \""
							+ identifier + "\"");
		} else {
			throw new IllegalArgumentException("The identifier \"" + identifier
					+ "\" is not bound to a scalar.");
		}
	}

	public double[][] getMatrix(String identifier) throws IOException
	{
		if (isMatrix(identifier)) {
			int nrows = matrixNumRows(identifier);
			int ncols = matrixNumCols(identifier);
			double[][] matrix = new double[nrows][ncols];

			GZIPInputStream gzstream = new GZIPInputStream(new FileInputStream(
					matrixDataFile(identifier)));
			DataInputStream matrixDataStream = new DataInputStream(gzstream);
			for (int r = 0; r < nrows; r++) {
				for (int c = 0; c < ncols; c++) {
					matrix[r][c] = matrixDataStream.readDouble();
				}
			}
			matrixDataStream.close();

			return matrix;
		} else {
			throw new IllegalArgumentException("The identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	public double[][] getMatrixSubsampleColumns(String identifier,
			double proportion) throws IOException
	{
		if (proportion <= 0) {
			throw new IllegalArgumentException(
					"Subsample proportion must be in (0, 1].");
		}
		if (isMatrix(identifier)) {
			int nrows = matrixNumRows(identifier);
			int ncols = matrixNumCols(identifier);

			int subNCols = (int) (proportion * ncols);
			double[][] matrix = new double[nrows][subNCols];
			if (subNCols == 0) {
				return matrix;
			}
			int skip = ((ncols / subNCols) - 1);
			int remaining = ncols - ((subNCols - 1) * (skip + 1)) - 1;
			int doublesRead = 0;

			GZIPInputStream gzstream = new GZIPInputStream(new FileInputStream(
					matrixDataFile(identifier)));
			DataInputStream matrixDataStream = new DataInputStream(gzstream);
			for (int r = 0; r < nrows; r++) {
				for (int c = 0; c < subNCols; c++) {
					matrix[r][c] = matrixDataStream.readDouble();
					doublesRead++;
					if (c < subNCols - 1) {
						matrixDataStream.skip(skip * (Double.SIZE/8));
					} else {
						matrixDataStream.skip(remaining * (Double.SIZE/8));
					}
				}
			}
			matrixDataStream.close();

			return matrix;
		} else {
			throw new IllegalArgumentException("The identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	public double[] getMatrixRowSum(String identifier) throws IOException
	{
		if (isMatrix(identifier)) {
			int nrows = matrixNumRows(identifier);
			int ncols = matrixNumCols(identifier);
			double[] rowSum = new double[ncols];

			GZIPInputStream gzstream = new GZIPInputStream(new FileInputStream(
					matrixDataFile(identifier)));
			DataInputStream matrixDataStream = new DataInputStream(gzstream);
			for (int r = 0; r < nrows; r++) {
				for (int c = 0; c < ncols; c++) {
					rowSum[c] += matrixDataStream.readDouble();
				}
			}
			return rowSum;
		} else {
			throw new IllegalArgumentException("The identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	public double[] getMatrixRowMean(String identifier) throws IOException
	{
		double[] rowSum = getMatrixRowSum(identifier);
		double[] rowMean = rowSum;
		int nrows = matrixNumRows(identifier);
		for (int c = 0; c < rowSum.length; c++) {
			rowMean[c] = rowSum[c] / nrows;
		}
		return rowMean;
	}

	public double[] getMatrixRowVariance(String identifier) throws IOException
	{
		if (isMatrix(identifier)) {
			int nrows = matrixNumRows(identifier);
			int ncols = matrixNumCols(identifier);
			double[] rowSumSq = new double[ncols];
			double[] rowSum = new double[ncols];

			GZIPInputStream gzstream = new GZIPInputStream(new FileInputStream(
					matrixDataFile(identifier)));
			DataInputStream matrixDataStream = new DataInputStream(gzstream);
			for (int r = 0; r < nrows; r++) {
				for (int c = 0; c < ncols; c++) {
					double val = matrixDataStream.readDouble();
					rowSumSq[c] += (val * val);
					rowSum[c] += val;
				}
			}
			double[] rowVar = rowSumSq;
			double unbias = (nrows / (nrows - 1));
			for (int i = 0; i < rowVar.length; i++) {
				rowVar[i] = unbias
						* ((rowSumSq[i] / nrows) - Math.pow(rowSum[i] / nrows,
								2));
			}
			return rowVar;
		} else {
			throw new IllegalArgumentException("The identifier \"" + identifier
					+ "\" is not bound to a matrix.");
		}
	}

	public double[] getMatrixRowStd(String identifier) throws IOException
	{
		double[] rowVar = getMatrixRowVariance(identifier);
		double[] rowStd = rowVar;
		for (int i = 0; i < rowVar.length; i++) {
			rowStd[i] = Math.sqrt(rowVar[i]);
		}
		return rowStd;
	}

	public static void main(String[] args) throws IOException
	{
		File dataDir = new File(
				"/home/tim/Documents/Papers/Fall 2011/data/test_data/");
		FileDataStore dstore = new FileDataStore(dataDir);

		String scalar1 = "scalar1";
		dstore.bind(scalar1, 10);
		String scalar2 = "scalar2";
		dstore.bind(scalar2, -120);
		dstore.bind(scalar1, 700);

		int numRows = 10;
		int numCols = 20;
		String matID = "mat";
		double[][] mat = MatrixOps.random(numRows, numCols);
		dstore.bind(matID, mat);

		dstore.appendRow(matID, new double[numCols]);

		dstore.bind(matID, 0.3);

		dstore.bind(matID, mat);

		dstore.appendRow(matID, new double[numCols]);

		dstore.bind("test_mat", new double[][] { { 1, 0, 0, 1 }, { 0, 1, 0, 2 },
				{ 0, 0, 1, 3 } });

		double[][] mat2 = dstore.getMatrix("test_mat");
		System.out.println(MatrixOps.toString(mat2));

		double[][] mat3 = dstore.getMatrixSubsampleColumns("test_mat", 0.9);
		System.out.println(MatrixOps.toString(mat3));
	}
}
