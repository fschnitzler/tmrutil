package tmrutil.util;

/**
 * An immutable double floating point number in a bounded interval. This is a
 * convenience class that helps to enforce passing valid numerical arguments.
 * 
 * @author Timothy A. Mann
 * 
 */
public class BoundedDouble extends Number {

	private static final long serialVersionUID = 2785226252396160639L;

	private double _value;
	private Interval _interval;

	/**
	 * Constructs a new instance with value in the center of the given interval.
	 * 
	 * @param interval
	 *            a non-empty interval specifying the range of valid values
	 * @throws NullPointerException
	 *             if <code>interval == null</code>
	 */
	public BoundedDouble(Interval interval) {
		if (interval == null) {
			throw new NullPointerException("Interval cannot be null.");
		}
		_value = (interval.getMax() - interval.getMin()) / 2;
		_interval = interval;
	}

	/**
	 * Constructs {@link BoundedDouble} provided that the specified value is
	 * within the interval.
	 * 
	 * @param value
	 *            a value within the interval
	 * @param interval
	 *            an non-empty interval specifying the range of valid values
	 * @throws NullPointerException
	 *             if <code>interval == null</code>
	 * @throws IllegalArgumentException
	 *             if <code>value</code> is not in the provided interval
	 */
	public BoundedDouble(double value, Interval interval) {
		if (interval == null) {
			throw new NullPointerException("Interval cannot be null.");
		}
		if (!interval.contains(value)) {
			throw new IllegalArgumentException("Value " + value
					+ " is outside of interval " + interval);
		}
		_value = value;
		_interval = interval;
	}

	/**
	 * Constructs a new bounded double using this instances interval.
	 * 
	 * @param value
	 *            a value within the bounds of this instances interval
	 * @return a new bounded double instance with the specified value
	 * @throws IllegalArgumentException
	 *             if <code>value</code> is not in <code>this</code> instance's
	 *             interval
	 */
	public BoundedDouble newValue(double value) {
		return new BoundedDouble(value, _interval);
	}

	/**
	 * The interval expressing valid numbers.
	 * 
	 * @return an interval
	 */
	public Interval interval() {
		return _interval;
	}

	@Override
	public int intValue() {
		return (int) _value;
	}

	@Override
	public long longValue() {
		return (long) _value;
	}

	@Override
	public float floatValue() {
		return (float) _value;
	}

	@Override
	public double doubleValue() {
		return _value;
	}

}
