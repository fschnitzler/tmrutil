package tmrutil.util;

/**
 * Bundles three objects, of possibly different types, together.
 * @author Timothy Mann
 *
 */
public class ThreeTuple <A, B, C> extends Pair<A, B> {

	private static final long serialVersionUID = 5455053142427591832L;
	
	private C _c;
	
	/**
	 * Constructs a three-tuple from three objects.
	 * @param a
	 * @param b
	 * @param c
	 */
	public ThreeTuple(A a, B b, C c)
	{
		super(a,b);
		_c = c;
	}
	
	/**
	 * Returns the third object.
	 */
	public final C getC()
	{
		return _c;
	}
	
	/**
	 * Sets the third object.
	 */
	public final void setC(C c)
	{
		_c = c;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof ThreeTuple<?, ?, ?>) {
			ThreeTuple<?, ?, ?> tuple = (ThreeTuple<?, ?, ?>) obj;
			return (getA().equals(tuple.getA()) && getB().equals(tuple.getB()) && getC().equals(tuple.getC()));
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return getA().hashCode() + getB().hashCode() + getC().hashCode();
	}
	
	@Override
	public String toString()
	{
		return "[" + getA() + ", " + getB() + ", " + getC() + "]";
	}
}
