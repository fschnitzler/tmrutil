package tmrutil.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A {@link Map} that returns a default value if the specified key is not bound to any
 * value.
 * 
 * @author Timothy A. Mann
 * 
 * @param <K>
 *            the key type
 * @param <V>
 *            the value type
 */
public class DefaultValueMap<K, V> implements Map<K, V>
{
	private Map<K, V> _map;
	private V _defaultValue;

	public DefaultValueMap(V defaultValue)
	{
		this(new HashMap<K, V>(), defaultValue);
	}

	public DefaultValueMap(Map<K, V> map, V defaultValue)
	{
		_map = map;
		_defaultValue = defaultValue;
	}

	@Override
	public int size()
	{
		return _map.size();
	}

	@Override
	public boolean isEmpty()
	{
		return _map.isEmpty();
	}

	@Override
	public boolean containsKey(Object key)
	{
		return _map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value)
	{
		return _map.containsValue(value);
	}

	@Override
	public V get(Object key)
	{
		V v = _map.get(key);
		if (v == null) {
			return _defaultValue;
		} else {
			return v;
		}
	}

	@Override
	public V put(K key, V value)
	{
		return _map.put(key, value);
	}

	@Override
	public V remove(Object key)
	{
		return _map.remove(key);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m)
	{
		_map.putAll(m);
	}

	@Override
	public void clear()
	{
		_map.clear();
	}

	@Override
	public Set<K> keySet()
	{
		return _map.keySet();
	}

	@Override
	public Collection<V> values()
	{
		return _map.values();
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet()
	{
		return _map.entrySet();
	}

}
