package tmrutil.util;

import java.lang.reflect.TypeVariable;
import java.util.Comparator;

/**
 * A generic comparator from a comparable type. Using this class comparators can
 * easily be generated from any type with natural ordering.
 * 
 * @author Timothy Mann
 * 
 * @param <T>
 *            a comparable type
 */
public class GenericComparator<T extends Comparable<T>> implements
		Comparator<T>
{
	/**
	 * Compares two objects using their natural ordering.
	 */
	public int compare(T o1, T o2)
	{
		return o1.compareTo(o2);
	}

	/**
	 * Determines whether this comparator is equal to another. Two generic
	 * comparators are equal if they are both generic comparators and have the
	 * same type parameters.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof GenericComparator) {
			Class<?> tclass = this.getClass();
			Class<?> oclass = obj.getClass();
			TypeVariable<?>[] otp = oclass.getTypeParameters();
			TypeVariable<?>[] ttp = tclass.getTypeParameters();
			if (otp.length != ttp.length) {
				return false;
			}
			for (int i = 0; i < otp.length; i++) {
				if (!otp[i].equals(ttp[i])) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
