package tmrutil.util;

/**
 * Each instance of this class represents a unit of length. The primary purpose
 * of this class is to simplify the process of converting between units.
 * 
 * @author Timothy A. Mann
 * 
 */
public final class LengthUnit implements Unit<LengthUnit>
{

	public static final LengthUnit INCHES = new LengthUnit("Inches", 1);
	public static final LengthUnit FEET = new LengthUnit("Feet", 12);
	public static final LengthUnit YARDS = new LengthUnit("Yards", 36);
	public static final LengthUnit MILES = new LengthUnit("Miles", 63360);
	public static final LengthUnit MILLIMETERS = new LengthUnit("Millimeters",
			0.0393700787);
	public static final LengthUnit CENTIMETERS = new LengthUnit("Centimeters",
			0.393700787);
	public static final LengthUnit METERS = new LengthUnit("Meters", 39.3700787);
	public static final LengthUnit KILOMETERS = new LengthUnit("Kilometers",
			39370.0787);

	private String _unitLabel;
	private double _numInches;

	/**
	 * Constructs a length unit with a unit label and the number of inches in 1
	 * of the constructed unit type.
	 * 
	 * @param unitLabel
	 *            a string label for the unit type
	 * @param numInches
	 *            the number of inches in 1 of the constructed unit type
	 */
	private LengthUnit(String unitLabel, double numInches)
	{
		if (unitLabel == null) {
			throw new NullPointerException("Unit label cannot be null.");
		}
		if(numInches <= 0){
			throw new IllegalArgumentException("Units of length cannot be zero and so must be equal to a positive number of inches.");
		}
		_unitLabel = unitLabel;
		_numInches = numInches;
	}

	@Override
	public double convert(double val, LengthUnit newUnit)
	{
		if (this == newUnit) {
			return val;
		} else {
			double scale = _numInches / newUnit._numInches;
			return scale * val;
		}
	}

	@Override
	public String toString()
	{
		return _unitLabel;
	}
}
