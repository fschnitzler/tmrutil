package tmrutil.util;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class FileDataStoreMergeTool
{
	public static void usage()
	{
		System.out.println("usage : java " + FileDataStoreMergeTool.class.getSimpleName() + " <new-data-dir> <data-dir-1> <data-dir-2>");
	}
	
	public static void main(String[] args) throws IOException
	{
		if(args.length < 3){
			usage();
		}else{
			File newFile = new File(args[0]);
			File d1File = new File(args[1]);
			File d2File = new File(args[2]);
			
			FileDataStore newDstore = new FileDataStore(newFile);
			FileDataStore d1Dstore = new FileDataStore(d1File, true);
			FileDataStore d2Dstore = new FileDataStore(d2File, true);
			newDstore.merge(d1Dstore);
			Set<String> notMerged = newDstore.merge(d2Dstore);
			
			StringBuilder swarn = new StringBuilder("***Warning: Unable to merge the following scalar entries from the second data storage directory: ");
			StringBuilder mwarn = new StringBuilder("***Warning: Unable to merge the following matrix entries from the second data storage directory: ");
			for(String id : notMerged){
				if(d2Dstore.isScalar(id)){
					swarn.append(id + ", ");
				}else{
					mwarn.append(id + ", ");
				}
			}
			System.out.println(swarn.toString());
			System.out.println(mwarn.toString());
		}
	}
}
