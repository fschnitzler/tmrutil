package tmrutil.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import tmrutil.math.MatrixOps;

public class OctaveUtil
{
	private static enum Type {
		CELL("cell"), MATRIX("matrix"), RANGE("range"), SCALAR("scalar"), STRUCT(
				"struct");
		private String _type;

		private Type(String type)
		{
			_type = type;
		}

		public String value()
		{
			return _type;
		}
	};

	private static final String HEADER = "# Created by tmrutil.util.OctaveUtil";

	/**
	 * Writes a set of matrices and scalar values to a file that can be read by
	 * GNU Octave.
	 * 
	 * @param filename
	 *            the name of the file to save the variables to
	 * @param matrices
	 *            a map of variable names to matrices
	 * @param scalars
	 *            a map of variable names to scalar values
	 * @throws IOException
	 *             if an I/O error occurs while writing to <code>filename</code>
	 */
	public static void save(String filename, Map<String, double[][]> matrices,
			Map<String, Double> scalars) throws IOException
	{
		FileWriter fw = new FileWriter(filename);
		fw.write(HEADER + "\n");

		Iterator<String> varIter = matrices.keySet().iterator();
		while (varIter.hasNext()) {
			String varName = varIter.next();
			double[][] matrix = matrices.get(varName);
			int rows = MatrixOps.getNumRows(matrix);
			int cols = MatrixOps.getNumCols(matrix);
			fw.write("# name: " + varName + "\n");
			fw.write("# type: " + Type.MATRIX.value() + "\n");
			fw.write("# rows: " + rows + "\n");
			fw.write("# columns: " + cols + "\n");
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					fw.write(" " + matrix[i][j]);
				}
				fw.write("\n");
			}
		}

		varIter = scalars.keySet().iterator();
		while (varIter.hasNext()) {
			String varName = varIter.next();
			double scalar = scalars.get(varName);
			fw.write("# name: " + varName + "\n");
			fw.write("# type: " + Type.SCALAR.value() + "\n");
			fw.write(scalar + "\n");
		}

		fw.close();
	}
}
