package tmrutil.util;

/**
 * Thrown to indicate that a side affecting function was called in a way that
 * could potentially affect the correctness of the output.<P/>
 * 
 * For example, if one of the functions input arguments is also passed as an
 * argument to store the result of the functions computation this may interfere
 * with the correctness of the final output.
 * 
 * @author Timothy Mann
 * 
 */
public class UnsafeSideAffectException extends RuntimeException
{
	private static final long serialVersionUID = -801277255119513206L;

	public UnsafeSideAffectException(String message)
	{
		super(message);
	}
}
