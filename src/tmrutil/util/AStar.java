package tmrutil.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tmrutil.math.Metric;

/**
 * An implementation of the AStar algorithm.
 * @author Timothy A. Mann
 *
 */
public class AStar<N> {
	/**
	 * The adjacency matrix.
	 */
	private boolean[][] _amat;
	private List<N> _nodes;
	private Metric<N> _heuristic;
	
	public AStar(List<N> nodes, boolean[][] amat, Metric<N> heuristic)
	{
		_nodes = nodes;
		_amat = amat;
		
		_heuristic = heuristic;
	}
	
	public List<N> findPath(N start, N goal)
	{
		Set<N> openSet = new HashSet<N>();
		openSet.add(start);
		Set<N> closedSet = new HashSet<N>();
		Map<N,N> cameFrom = new HashMap<N,N>();
		
		Map<N,Double> gscore = new HashMap<N,Double>();
		gscore.put(start, 0.0);
		Map<N,Double> fscore = new HashMap<N,Double>();
		fscore.put(start, 0.0 + _heuristic.distance(start, goal));
		
		while(!openSet.isEmpty()){
			N current = findLowestF(openSet, fscore, goal);
			if(current.equals(goal)){
				return reconstructPath(cameFrom, goal);
			}
			
			openSet.remove(current);
			closedSet.add(current);
			
			int ci = _nodes.indexOf(current);
			for(int i=0;i<_nodes.size();i++){
				if(_amat[ci][i]){
					N neighbor = _nodes.get(i);
					double tgscore = gscore.get(current) + 1;
					if(closedSet.contains(neighbor)){
						if(tgscore >= gscore.get(neighbor)){
							continue;
						}
					}
					
					if(!openSet.contains(neighbor) || tgscore < gscore.get(neighbor)){
						cameFrom.put(neighbor, current);
						gscore.put(neighbor, tgscore);
						fscore.put(neighbor, tgscore + _heuristic.distance(neighbor, goal));
						openSet.add(neighbor);
					}
				}
			}
			
		}
		
		// Failed to find a path
		return new ArrayList<N>();
	}
	
	public List<N> reconstructPath(Map<N,N> cameFrom, N currentNode)
	{
		N prevNode = cameFrom.get(currentNode);
		if(prevNode != null){
			List<N> path = reconstructPath(cameFrom, prevNode);
			path.add(currentNode);
			return path;
		}else{
			List<N> path = new ArrayList<N>();
			path.add(currentNode);
			return path;
		}
	}
	
	public N findLowestF(Set<N> nodes, Map<N,Double> fscore, N goal)
	{
		double bestF = 0;
		N best = null;
		for(N node : nodes){
			double f = fscore.get(node);
			if(best == null || bestF > f){
				best = node;
				bestF = f;
			}
		}
		return best;
	}
}
