package tmrutil.util;

import java.util.Comparator;
import tmrutil.math.DimensionMismatchException;

public class IntArrayComparator implements Comparator<int[]>
{

	@Override
	public int compare(int[] o1, int[] o2)
	{
		if(o1.length == o2.length){
			for (int i = 0; i < o1.length; i++) {
				int ans = o1[i] - o2[i];
				if (ans != 0) {
					return ans;
				}
			}
			return 0;
		}
		throw new DimensionMismatchException("Lexicographical ordering is only defined for vectors of the same length.");
	}

}
