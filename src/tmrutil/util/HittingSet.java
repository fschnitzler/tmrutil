package tmrutil.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A utility class for finding the hitting set of a collection of sets over a
 * universe of elements.
 * 
 * @author Timothy A. Mann
 * 
 * @param X
 *            the element type of the hitting set
 */
public class HittingSet<X>
{
	private Set<X> _universe;
	private List<Set<X>> _subsets;

	/**
	 * Constructs a hitting set search instance.
	 * 
	 * @param universe
	 *            the universe of elements
	 * @param subsets
	 *            a collection of subsets of the universe
	 */
	public HittingSet(Set<X> universe, Collection<Set<X>> subsets)
	{
		_universe = new HashSet<X>(universe);
		_subsets = new ArrayList<Set<X>>(subsets.size());
		for (Set<X> sub : subsets) {
			if (_universe.containsAll(sub)) {
				_subsets.add(new HashSet<X>(sub));
			} else {
				throw new IllegalArgumentException(
						"Detected nonsubset of the universe in the collection of subsets.");
			}
		}
	}

	/**
	 * Returns the minimal hitting set for the collection of sets contained in
	 * this instance.
	 * 
	 * @return the minimal hitting set
	 */
	public Set<X> minimalHittingSet()
	{
		return allMinimalHittingSets().iterator().next();
	}

	public Collection<Set<X>> allMinimalHittingSets()
	{
		List<Set<X>> minHs = new ArrayList<Set<X>>();
		List<X> univ = new ArrayList<X>();
		univ.addAll(_universe);

		List<Set<Integer>> combos = new ArrayList<Set<Integer>>(univ.size());
		for (int i = 0; i < univ.size(); i++) {
			Set<Integer> s = new HashSet<Integer>();
			s.add(0);
			s.add(1);
			combos.add(s);
		}
		IntArrayEnumeration iaEnum = new IntArrayEnumeration(combos);

		Set<X> minH = null;
		for (int[] ia : iaEnum) {
			Set<X> s = new HashSet<X>();
			for (int i = 0; i < ia.length; i++) {
				if (ia[i] == 1) {
					s.add(univ.get(i));
				}
			}

			if (isHittingSet(s)) {
				if (minH == null || minH.size() > s.size()) {
					minHs.clear();
					minH = s;
					minHs.add(s);
				} else if (minH.size() == s.size()) {
					minHs.add(s);
				}
			}
		}
		return minHs;
	}

	/**
	 * Determines whether the specified set is a hitting set for the collection
	 * of sets contained in this instance.
	 * 
	 * @param s
	 *            a set
	 * @return true if <code>s</code> is a hitting set
	 */
	public boolean isHittingSet(Set<X> s)
	{
		if (_universe.containsAll(s)) {
			for (Set<X> sub : _subsets) {
				if (!intersect(s, sub)) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Determines whether there is any overlap between the sets <code>a</code>
	 * and <code>b</code>.
	 * 
	 * @param a
	 *            a set
	 * @param b
	 *            a set
	 * @return true if there is overlap between <code>a</code> and
	 *         <code>b</code>; otherwise false
	 */
	public boolean intersect(Set<X> a, Set<X> b)
	{
		for (X ax : a) {
			if (b.contains(ax)) {
				return true;
			}
		}
		return false;
	}

	public static final Set<Integer> newIntSet(int[] s)
	{
		Set<Integer> set = new HashSet<Integer>();
		for (int e : s) {
			set.add(e);
		}
		return set;
	}

	public static void main(String[] args)
	{
		Set<Integer> universe = new HashSet<Integer>();
		for (int i = 0; i < 5; i++) {
			universe.add(i);
		}
		Collection<Set<Integer>> subsets = new ArrayList<Set<Integer>>();
		Set<Integer> s1 = newIntSet(new int[] { 0, 1, 2, 3, 4 });
		subsets.add(s1);
		Set<Integer> s2 = newIntSet(new int[] { 0, 2 });
		subsets.add(s2);
		Set<Integer> s3 = newIntSet(new int[] { 1, 3 });
		subsets.add(s3);
		Set<Integer> s4 = newIntSet(new int[] { 1, 2, 4 });
		subsets.add(s4);

		HittingSet<Integer> hset = new HittingSet<Integer>(universe, subsets);
		Collection<Set<Integer>> minHSets = hset.allMinimalHittingSets();
		for (Set<Integer> minH : minHSets) {
			System.out.print("{");
			for (Integer e : minH) {
				System.out.print(e + " ");
			}
			System.out.println("}");
		}
	}
}
