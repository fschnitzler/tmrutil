package tmrutil.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * An LR Parser is a general parser for deterministic context free languages. It
 * parses the input string from left to right and produces a right-most
 * derivation for the input string.
 * 
 * @author Timothy A. Mann
 * 
 * @param <S> the state type
 */
public class LRParser<S>
{
	public enum Action {SHIFT, REDUCE, ACCEPT, REJECT};
	
	public interface Rule
	{
		public String getLeftHandSide();
		public String getRightHandSide();
	}
	
	private int _index;
	private List<Rule> _rules;
	private List<S> _states;
	private Stack<S> _stack;
	private Map<S,Map<String,Pair<Action,Rule>>> _actionTable;
	private Map<S,Rule> _gotoTable;
	
	public LRParser(List<Rule> rules, List<S> states, Map<S,Map<String,Pair<Action,Rule>>> actionTable, Map<S,Rule> gotoTable)
	{
		_rules = rules;
		_states = states;
		_actionTable = actionTable;
		_gotoTable = gotoTable;
		
		_stack = new Stack<S>();
	}
	
	public List<Rule> parse(String input) throws SyntaxException
	{
		List<Rule> output = new ArrayList<Rule>();
		boolean accept = false;
		_index = 0;
		
		while(!accept){
			
		}
		
		return output;
	}
	
	public void shift(S state)
	{
		_index++;
		_stack.push(state);
	}
	
	public void reduce(Rule rule)
	{
		
	}
}
