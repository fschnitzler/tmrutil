package tmrutil.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for tokenization.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Tokenizer
{
	/** A pattern of characters to ignore. */
	private Pattern _ignorePattern;
	/** A set of patterns to tokenize. */
	private Set<Pattern> _acceptPatterns;

	/**
	 * Constructs a tokenizer only accepts tokens with specified patterns.
	 * 
	 * @param ignoreRegex
	 * @param tokenPatterns
	 */
	public Tokenizer(String ignoreRegex, String... tokenRegexes)
	{
		_ignorePattern = Pattern.compile(ignoreRegex);
		_acceptPatterns = new HashSet<Pattern>();
		for (String tokenRegex : tokenRegexes) {
			Pattern tokenPattern = Pattern.compile(tokenRegex);
			_acceptPatterns.add(tokenPattern);
		}
	}

	public List<String> tokenize(String text) throws IllegalArgumentException
	{
		int textLength = text.length();
		List<String> tokens = new ArrayList<String>();
		Matcher ignoreMatcher = _ignorePattern.matcher(text);
		Set<Matcher> acceptMatchers = new HashSet<Matcher>();
		for (Pattern acceptPattern : _acceptPatterns) {
			acceptMatchers.add(acceptPattern.matcher(text));
		}

		for (int i = 0; i < text.length(); i++) {
			ignoreMatcher.region(i, textLength);
			// Skip over symbols that should be ignored
			if (ignoreMatcher.lookingAt()) {
				i = ignoreMatcher.end();
			}

			// Obtain the next token
			boolean unmatched = true;
			for (Matcher acceptMatcher : acceptMatchers) {
				acceptMatcher.region(i, textLength);
				if (acceptMatcher.lookingAt()) {
					unmatched = false;
					int end = acceptMatcher.end();
					tokens.add(text.substring(i, end));
					i = end - 1;
					break;
				}
			}

			if (unmatched) {
				throw new IllegalArgumentException(
						"The token encountered at index " + i
								+ " does not match any provided rule.");
			}
		}
		return tokens;
	}

	/**
	 * Returns a sublist of a list of tokens starting at the provided offset and
	 * ending at the first token to match the provided regular expression. If no
	 * tokens after the offset match the provided regular expression then the
	 * remainder of the list is returned.
	 * 
	 * @param tokens
	 *            a list of tokens
	 * @param offset
	 *            index of the first token of the sublist
	 * @param stopRegex
	 *            a regular expression such that the final element of the
	 *            sublist should be the first element at or after the offset
	 *            that matches
	 * @return a sublist of the list of tokens
	 */
	public static final List<String> tokenSublist(List<String> tokens,
			int offset, String stopRegex)
	{
		int end = -1;
		for (int i = offset; i < tokens.size(); i++) {
			if (tokens.get(i).matches(stopRegex)) {
				end = i + 1;
				break;
			}
		}
		if (end < 0) {
			end = tokens.size();
		}
		List<String> sublist = new ArrayList<String>(end - offset);
		for(int i=offset;i<end;i++){
			sublist.add(tokens.get(i));
		}
		return sublist;
	}
	
	public static final void dump(List<String> tokens)
	{
		System.out.println("[Token List]");
		System.out.println("============");
		for(String token : tokens){
			System.out.println("\t" + token);
		}
		System.out.println("=====END====");
	}

	public static void main(String[] args)
	{
		String text = "2X[0]^{13} + 2 X [1] X[2]^{2}";
		Tokenizer tokenizer = new Tokenizer("\\s+", "\\{", "\\}", "\\^", "\\+",
				"\\[", "\\]", "X", "\\d+");

		List<String> tokens = tokenizer.tokenize(text);

		System.out.println("Tokenized Tokens : ");
		System.out.println("===================");
		for (String token : tokens) {
			System.out.println("\t" + token);
		}
	}
}
