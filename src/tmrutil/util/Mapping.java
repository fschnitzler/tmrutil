package tmrutil.util;

/**
 * Represents a mapping from an element of a set A to an element of a set B.
 * @author Timothy Mann
 *
 * @param <A> type to map from
 * @param <B> type to map to
 */
public interface Mapping<A, B>
{
	/**
	 * Implements a mapping from objects of type <code>A</code> to objects of type <code>B</code>.
	 * @param x an element
	 * @return an element of type <code>B</code>
	 */
	public B map(A x);
}
