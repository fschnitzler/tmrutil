package tmrutil.xml;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The purpose of this class is to enable easy storage and retrieval of
 * arguments in a human readable/editable XML file.
 * 
 * @author Timothy Mann
 * 
 */
public class Args implements ToXML<Args>, FromXML<Args>
{
	private static final String ARGS_ELM = "ARGS";
	private static final String ARG_ELM = "ARG";
	private static final String ID_ATTR = "ID";
	private static final String VALUE_ATTR = "VALUE";

	private Map<String, String> _args;

	/**
	 * Constructs an empty set of arguments.
	 */
	public Args()
	{
		_args = new HashMap<String, String>();
	}

	/**
	 * Adds an argument to the arguments to be stored.
	 * 
	 * @param identifier
	 *            the identifier for the argument
	 * @param value
	 *            the value of the argument
	 */
	public void add(String identifier, String value)
	{
		_args.put(identifier, value);
	}

	/**
	 * Returns a map of argument identifiers to values.
	 * 
	 * @return a map of argument identifiers to values
	 */
	public Map<String, String> getArgsMap()
	{
		return _args;
	}

	/**
	 * Retrieves a value given an identifier. If there is no argument with a
	 * matching identifier then an exception is thrown.
	 * 
	 * @param identifier
	 *            an identifier for an argument
	 * @throws IllegalArgumentException
	 *             if no matching identifiers are present
	 */
	public String get(String identifier) throws IllegalArgumentException
	{
		if (!_args.containsKey(identifier)) {
			throw new IllegalArgumentException("The specified identifier(\""
					+ identifier + "\") was not found in the argument list.");
		}
		return _args.get(identifier);
	}

	/**
	 * Retrieves a value given an identifier and attempts to convert it to a
	 * boolean value. If there is no argument with a matching identifier then an
	 * exception is thrown.
	 * 
	 * @param identifier
	 *            an identifier for an argument
	 * @return a boolean value
	 * @throws IllegalArgumentException
	 *             if no matching identifiers are present
	 */
	public boolean getBoolean(String identifier)
			throws IllegalArgumentException
	{
		return Boolean.parseBoolean(get(identifier));
	}

	/**
	 * Retrieves a value given an identifier and attempts to convert it to an
	 * integer. If there is no argument with a matching identifier then an
	 * exception is thrown.
	 * 
	 * @param identifier
	 *            an identifier for an argument
	 * @throws IllegalArgumentException
	 *             if no matching identifiers are present
	 * @throws NumberFormatException
	 *             if the value of the argument cannot be converted to an
	 *             integer
	 */
	public int getInt(String identifier) throws IllegalArgumentException,
			NumberFormatException
	{
		return Integer.parseInt(get(identifier));
	}

	/**
	 * Retrieves a value given an identifier and attempts to convert it to a
	 * double. If there is no argument with a matching identifier then an
	 * exception is thrown.
	 * 
	 * @param identifier
	 *            an identifier for an argument
	 * @throws IllegalArgumentException
	 *             if no matching identifiers are present
	 * @throws NumberFormatException
	 *             if the value of the argument cannot be converted to a double
	 */
	public double getDouble(String identifier) throws IllegalArgumentException,
			NumberFormatException
	{
		return Double.parseDouble(get(identifier));
	}

	/**
	 * Saves the current set of arguments to an XML file.
	 * 
	 * @param filename
	 *            an XML file to save the arguments in
	 * @throws IOException
	 *             if an I/O error occurs while writing to the file
	 */
	public void save(String filename) throws IOException
	{
		Document document = XMLUtil.createDocument();
		Element docElm = toXML(this, document);
		document.appendChild(docElm);
		String xml = XMLUtil.toString(document);
		FileWriter fw = new FileWriter(filename);
		fw.write(xml);
		fw.close();
	}

	/**
	 * Loads a set of arguments from an XML file. The arguments from the file
	 * are added to the current set of arguments (if there are any). If any
	 * arguments from the XML file have the same identifier as existing
	 * arguments there values are replaced with the argument values from the XML
	 * file.
	 * 
	 * @param filename
	 *            an XML file to load the arguments from
	 * @throws IOException
	 *             if an I/O exception occurs while reading from the file
	 * @throws SAXException
	 *             if an error occurs while parsing the XML
	 */
	public void load(String filename) throws IOException, SAXException
	{
		Document document = null;
		try{
			InputStream resource = ClassLoader.getSystemResourceAsStream(filename);
			document = XMLUtil.parse(resource);
		}catch(IllegalArgumentException ex){
			document = XMLUtil.parse(filename);
		}
		Element docElm = document.getDocumentElement();
		Map<String, String> args = fromXML(docElm).getArgsMap();
		Iterator<String> keys = args.keySet().iterator();
		while (keys.hasNext()) {
			String id = keys.next();
			_args.put(id, args.get(id));
		}
	}

	@Override
	public Element toXML(Args t, Document document)
	{
		Element argsElm = document.createElement(ARGS_ELM);
		Iterator<String> keys = _args.keySet().iterator();
		while (keys.hasNext()) {
			Element argElm = document.createElement(ARG_ELM);
			String id = keys.next();
			String val = _args.get(id);
			argElm.setAttribute(ID_ATTR, id);
			argElm.setAttribute(VALUE_ATTR, val);
			argsElm.appendChild(argElm);
		}
		return argsElm;
	}

	@Override
	public Args fromXML(Element element) throws IllegalArgumentException
	{
		if (!element.getTagName().equals(ARGS_ELM)) {
			throw new IllegalArgumentException(
					"The specified element does not represent an element of type "
							+ ARGS_ELM);
		}
		Args args = new Args();
		NodeList nodes = element.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node instanceof Element) {
				Element argElm = (Element) node;
				args.add(argElm.getAttribute(ID_ATTR),
						argElm.getAttribute(VALUE_ATTR));
			}
		}
		return args;
	}

}
