package tmrutil.math;

/**
 * Represents a matrix with real valued components.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface Matrix
{
	/**
	 * Creates a new matrix given a two-dimensional array of entries and a
	 * specified number of rows and columns. The first index of the entries
	 * matrix corresponds to the rows of the matrix, while the second index
	 * corresponds to the columns of the matrix.
	 * 
	 * @param entries
	 *            a two-dimensional array of matrix entries
	 * @param numRows
	 *            the number of rows in the new matrix
	 * @param numCols
	 *            the number of columns of the new matrix
	 * @return a new matrix
	 */
	public Matrix create(double[][] entries, int numRows, int numColumns);

	/**
	 * Constructs a zeros matrix with the specified number of rows and columns.
	 * 
	 * @param numRows
	 *            the number of rows in the new matrix
	 * @param numCols
	 *            the number of columns in the new matrix
	 * @return a new matrix
	 */
	public Matrix zeros(int numRows, int numCols);

	/**
	 * Used by the matrix implementation to decide whether or not a scalar value
	 * is "close enough" to be considered 0.
	 * 
	 * @param val
	 *            a scalar value
	 * @return true if the value is considered 0; otherwise false
	 */
	public boolean isZero(double val);

	/**
	 * Returns true if the number of rows and columns of <code>this</code>
	 * matrix are the same. False is returned to indicate that the dimensions of
	 * the matrix are unequal.
	 * 
	 * @return true if the matrix dimensions are equal; otherwise false
	 */
	public boolean isSquare();

	/**
	 * Copies the elements of <code>rhs</code> matrix to <code>this</code>
	 * matrix and resizes the dimensions of <code>this</code>, if necessary.
	 * 
	 * @param rhs
	 *            a matrix
	 */
	public void copy(Matrix rhs);
	
	/**
	 * Copies the elements of <code>this</code> matrix into a new matrix.
	 * @return a copy of <code>this</code> matrix
	 */
	public Matrix copy();

	/**
	 * Adds <code>this</code> matrix to the <code>rhs</code> matrix and returns
	 * the resulting matrix.
	 * 
	 * @param rhs
	 *            a matrix with the same number of rows and columns as
	 *            <code>this</code> matrix
	 * @return a new matrix equal to <code>this + rhs</code>
	 * @throws DimensionMismatchException
	 *             if <code>rhs</code> does not have the same number of rows and
	 *             columns as <code>this</code> matrix
	 */
	public Matrix add(Matrix rhs) throws DimensionMismatchException;

	/**
	 * Adds the matrix <code>rhs</code> to <code>this</code> matrix. This is a
	 * SIDE AFFECTING method, meaning that it changes the elements of
	 * <code>this</code> matrix.
	 * 
	 * @param rhs
	 *            a matrix with the same number of rows and columns as
	 *            <code>this</code> matrix
	 * @throws DimensionMismatchException
	 *             if <code>rhs</code> does not have the same number of rows and
	 *             columns as <code>this</code> matrix
	 */
	public void addToThis(Matrix rhs) throws DimensionMismatchException;

	/**
	 * Subtracts <code>rhs</code> matrix from <code>this</code> matrix and
	 * returns the resulting matrix.
	 * 
	 * @param rhs
	 *            a matrix with the same number of rows and columns as
	 *            <code>this</code> matrix
	 * @return a new matrix equal to <code>this - rhs</code>
	 * @throws DimensionMismatchException
	 *             if <code>rhs</code> does not have the same number of rows and
	 *             columns as <code>this</code> matrix
	 */
	public Matrix subtract(Matrix rhs) throws DimensionMismatchException;

	/**
	 * Subtracts <code>rhs</code> matrix from <code>this</code> matrix. This is
	 * a SIDE AFFECTING method, meaning that it changes the elements of
	 * <code>this</code> matrix.
	 * 
	 * @param rhs
	 *            a matrix with the same number of rows and columns as
	 *            <code>this</code> matrix
	 * @throws DimensionMismatchException
	 *             if <code>rhs</code> does not have the same number of rows and
	 *             columsn as <code>this</code> matrix
	 */
	public void subtractFromThis(Matrix rhs) throws DimensionMismatchException;

	/**
	 * Multiplies each element of this matrix with a specified scalar value and
	 * returns the resulting matrix.
	 * 
	 * @param s
	 *            a scalar value
	 * @return a new matrix equal to <code>s * this</code>
	 */
	public Matrix multiply(double s);

	/**
	 * Multiplies each element of this matrix with a specified scalar value.
	 * This is a SIDE AFFECTING method, meaning that it changes the elements of
	 * <code>this</code> matrix.
	 * 
	 * @param s
	 *            a scalar value
	 */
	public void multiplyThisBy(double s);

	/**
	 * Multiplies <code>this</code> matrix by <code>rhs</code> matrix and
	 * returns the resulting matrix.
	 * 
	 * @param rhs
	 *            a matrix whose number of rows equals the number of
	 *            <code>this</code> matrix's columns
	 * @return a new matrix equal to <code>this * rhs</code>
	 * @throws DimensionMismatchException
	 *             if the number of columns of <code>this</code> matrix does not
	 *             match the number of rows of <code>rhs</code>
	 */
	public Matrix multiply(Matrix rhs) throws DimensionMismatchException;

	/**
	 * Multiplies <code>this</code> matrix by <code>rhs</code> matrix. This is a
	 * SIDE AFFECTING method, meaning that it changes the elements of
	 * <code>this</code> matrix.
	 * 
	 * @param rhs
	 *            a matrix whose number of rows equals the number of
	 *            <code>this</code> matrix
	 * @throws DimensionMismatchException
	 *             if the number of columns of <code>this</code> matrix does not
	 *             match the number of rows of <code>rhs</code>
	 */
	public void multiplyThisBy(Matrix rhs) throws DimensionMismatchException;

	/**
	 * Multiplies <code>this</code> matrix by <code>rhs</code> vector and
	 * returns the resulting vector.
	 * 
	 * @param rhs
	 *            a vector whose number of components equals the number of
	 *            <code>this</code> matrix's columns
	 * @return a new vector equal to <code>this * rhs</code>
	 * @throws DimensionMismatchException
	 *             if the number of columns of <code>this</code> matrix does not
	 *             match the number of components of <code>rhs</code>
	 */
	public Vector multiply(Vector rhs) throws DimensionMismatchException;

	/**
	 * Performs a component-wise multiplication between <code>this</code> matrix
	 * and <code>rhs</code> and returns the resulting matrix.
	 * 
	 * @param rhs
	 *            a matrix whose number of rows and columns are equal to the
	 *            number of rows and columns of <code>this</code>
	 * @return a new matrix whose components are the product of the
	 *         corresponding elements of <code>this</code> and <code>rhs</code>
	 * @throws DimensionMismatchException
	 *             if the number of rows and columns of <code>this</code> do not
	 *             equal the number of rows and columns of <code>rhs</code>
	 */
	public Matrix componentMultiply(Matrix rhs)
			throws DimensionMismatchException;

	/**
	 * Performs a component-wise multiplication between <code>this</code> matrix
	 * and <code>rhs</code> matrix. This is a SIDE AFFECTING method, meaning
	 * that it changes the elements of <code>this</code> matrix.
	 * 
	 * @param rhs
	 *            a matrix whose number of rows and columns are equal to the
	 *            number of rows and columns of <code>this</code>
	 * @throws DimensionMismatchException
	 *             if the number of rows and columns of <code>this</code> do not
	 *             equal the number of rows and columns of <code>rhs</code>
	 */
	public void componentMultiplyThisBy(Matrix rhs)
			throws DimensionMismatchException;

	/**
	 * Returns the transpose of <code>this</code> matrix.
	 * 
	 * @return the transpose of <code>this</code> matrix
	 */
	public Matrix transpose();

	/**
	 * Transposes this matrix. This is a SIDE AFFECTING method, meaning that it
	 * changes the elements of <code>this</code> matrix.
	 */
	public void transposeThis();

	/**
	 * Returns the value of the matrix entry at a specified row and column.
	 * 
	 * @param rowIndex
	 *            an integer in the range <code>[0, this.rows()]</code>
	 * @param colIndex
	 *            an integer in the range <code>[0, this.cols()]</code>
	 * @return the value of the matrix entry at the specified row and column
	 */
	public double get(int rowIndex, int colIndex);
	
	/**
	 * Returns a reshaped matrix with the specified number of rows and columns.
	 * 
	 * @param numRows
	 *            the number of rows in the new matrix
	 * @param numCols
	 *            the number of columns in the new matrix
	 * @return a new reshaped matrix
	 * @throws DimensionMismatchException
	 *             if
	 *             <code>numRows x numCols != this.rows() x this.cols()</code>
	 */
	public Matrix reshape(int numRows, int numCols)
			throws DimensionMismatchException;

	/**
	 * Returns the row at the specified index.
	 * 
	 * @param index
	 *            an integer in the range <code>[0, this.rows()]</code>
	 * @return a vector containing the elements at the specified row of
	 *         <code>this</code> matrix
	 */
	public Vector getRow(int index);

	/**
	 * Returns the column at the specified index.
	 * 
	 * @param index
	 *            an integer in the range <code>[0, this.cols()]</code>
	 * @return a vector containing the elements at the specified column of
	 *         <code>this</code> matrix
	 */
	public Vector getColumn(int index);

	/**
	 * Returns a matrix where each element is the negation of a corresponding
	 * element of <code>this</code> matrix.
	 * 
	 * @return a new matrix, which is the negation of <code>this</code>
	 */
	public Matrix negate();

	/**
	 * Negates <code>this</code> matrix. This is a SIDE AFFECTING method,
	 * meaning that it changes the elements of <code>this</code> matrix.
	 */
	public void negateThis();

	/**
	 * Returns the number of rows contained by this matrix.
	 * 
	 * @return a positive integer representing the number of rows of this matrix
	 */
	public int rows();

	/**
	 * Returns the number of columns contained by this matrix.
	 * 
	 * @return a positive integer representing the number of columns of this
	 *         matrix
	 */
	public int cols();

	/**
	 * Returns the data of this matrix as a vector.
	 * @return a vector
	 */
	public Vector flatten();
	
	/**
	 * Returns a 1-dimensional array containing the elements of this matrix.
	 * 
	 * @return a 1-dimensional array
	 */
	public double[] flatArray();

	/**
	 * Returns a 2-dimensional array containing the elements of this matrix.
	 * 
	 * @return a 2-dimensional array
	 */
	public double[][] toArray();
	
	/**
	 * Sets the value of a particular element in this matrix.
	 * @param row a row index
	 * @param col a column index
	 * @param value the new value
	 */
	public void set(int row, int col, double value);
	
	/**
	 * Creates a new matrix by concatenating the rows of <code>bottom</code> matrix to the rows of <code>this</code> matrix.
	 * @param bottom the matrix that will be concatenated to the bottom of <code>this</code> matrix
	 * @return a new concatenated matrix
	 */
	public Matrix concatRows(Matrix bottom) throws DimensionMismatchException;
	
	/**
	 * Creates a new matrix by concatenating the columns of <code>right</code> matrix to the columns of <code>this</code> matrix.
	 * @param right the matrix that will be concatenated to the right of <code>this</code> matrix
	 * @return a new concatenated matrix
	 */
	public Matrix concatCols(Matrix right) throws DimensionMismatchException;

	/**
	 * Computes the eigenvectors for this matrix and returns a matrix where each
	 * column is an eigenvector and the columns are sorted in
	 * decreasing order with respect to each eigenvector's corresponding eigenvalues.
	 * 
	 * @param num the number of eigenvectors to compute
	 * 
	 * @return a matrix where each column is an eigenvector
	 */
	public Matrix eigenvectors(int num);

	/**
	 * Computes the eigenvalues for this matrix and returns the eigenvalues sorted in decreasing order
	 * @param num the number of eigenvalues to compute
	 * @return a vector of eigenvalues
	 */
	public Vector eigenvalues(int num);
}
