/*
 * MatrixException.java
 */

package tmrutil.math;

/**
 * Thrown to indicate an error related to matrix math.
 */
public class MatrixException extends MathException
{
	private static final long serialVersionUID = 4567061669856522413L;

	public MatrixException(String message)
    {
	super(message);
    }
}