/*
 * Line2D.java
 */

package tmrutil.math.geometry;

/**
 * Represents a line in 2-dimensional space.
 */
public class Line2D extends Line
{
	private double _slope;
	private double _bias;

	/**
	 * Constructs a line from two points.
	 * 
	 * @param x0
	 *            an X-coordinate of point 0
	 * @param y0
	 *            a Y-coordinate of point 0
	 * @param x1
	 *            an X-coordinate of point 1
	 * @param y1
	 *            a Y-coordinate of point 1
	 */
	public Line2D(double x0, double y0, double x1, double y1)
		throws IllegalArgumentException
	{
		super(new double[] { x0, y0 }, new double[] { x1, y1 });
		if((x0 - x1) == 0){throw new IllegalArgumentException("Cannot construct a line from two points with the same X-coordinate value.");}
		
		if(x0 > x1){
			_slope = (y0 - y1)/(x0 - x1);
		}else{
			_slope = (y1 - y0)/(x1 - x0);
		}
		_bias = y0 - (x0 * _slope);
	}

	/**
	 * Returns the slope of this line.
	 * 
	 * @return the slope of this line
	 */
	public double getSlope()
	{
		return _slope;
	}

	/**
	 * Returns the bias of this line.
	 * 
	 * @return the bias of this line
	 * @return
	 */
	public double getBias()
	{
		return _bias;
	}
	
	/**
	 * Returns true if this line is strictly to the left of the point specified by (<code>x</code>, <code>y</code>).
	 * @param x an X-coordinate
	 * @param y a Y-coordinate
	 * @return true if this line is strictly to the left of the point specified by (<code>x</code>, <code>y</code>)
	 */
	public boolean isLeftOf(double x, double y)
	{
		double yHat = (x*_slope) + _bias;
		if(_slope > 0)
		{
			return (y < yHat);
		}
		if(_slope < 0){
			return (y > yHat);
		}
		return false;
	}

	/**
	 * Returns true if this line is strictly to the right of the point specified by (<code>x</code>, <code>y</code>). 
	 * @param x an X-coordinate
	 * @param y a Y-coordinate
	 * @return true if this line is strictly to the right of the point specified by (<code>x</code>, <code>y</code>)
	 */
	public boolean isRightOf(double x, double y)
	{
		double yHat = (x*_slope) + _bias;
		if(_slope > 0){
			return (y > yHat);
		}
		if(_slope < 0){
			return (y < yHat);
		}
		return false;
	}
}