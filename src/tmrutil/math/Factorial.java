package tmrutil.math;

public class Factorial {

	private Factorial() {

	}

	/**
	 * Computes the factorial of <code>n</code> iteratively.
	 * @param n
	 * @return <code>n!</code> the factorial of <code>n</code>
	 */
	public static final int fact(int n) {
		if (n <= 1) {
			return 1;
		} else {
			int f = 1;
			for(int i=2;i<=n;i++){
				f = f * i;
			}
			return f;
		}
	}
	
}
