package tmrutil.math;

/**
 * A set of useful side-affecting vector operations. Since the operations are
 * side-affecting care is taken so that no new objects are allocated during
 * calls to these methods. As a result they should all be faster than their
 * corresponding calls in {@link tmrutil.math.VectorOps}.
 * 
 * @author Timothy Mann
 * 
 */
public final class SAVectorOps
{

	/**
	 * Multiplies the scalar value <code>s</code> with the vector
	 * <code>aSA</code> modifying the values of <code>aSA</code>.
	 * 
	 * @param aSA
	 *            a vector which will be modified during multiplication.
	 * @param s
	 *            a scalar value
	 * @return the modified vector <code>aSA</code>
	 */
	public static final double[] multiply(double[] aSA, double s)
	{
		for (int i = 0; i < aSA.length; i++) {
			aSA[i] *= s;
		}
		return aSA;
	}

	/**
	 * Divides the vector <code>aSA</code> by the scalar value <code>s</code>
	 * modifying the values of <code>aSA</code>.
	 * 
	 * @param aSA
	 *            a vector which will be modified during division.
	 * @param s
	 *            a scalar value
	 * @return the modified vector <code>aSA</code>
	 */
	public static final double[] divide(double[] aSA, double s)
	{
		for (int i = 0; i < aSA.length; i++) {
			aSA[i] /= s;
		}
		return aSA;
	}

	/**
	 * Adds the scalar value <code>s</code> to the vector <code>aSA</code>
	 * modifying the values of <code>aSA</code>.
	 * 
	 * @param aSA
	 *            a vector which will be modified during addition
	 * @param s
	 *            a scalar value
	 * @return the modified vector <code>aSA</code>
	 */
	public static final double[] add(double[] aSA, double s)
	{
		for (int i = 0; i < aSA.length; i++) {
			aSA[i] += s;
		}
		return aSA;
	}

	/**
	 * Adds the vector <code>b</code> to the vector <code>aSA</code> modifying
	 * the values of <code>aSA</code>.
	 * 
	 * @param aSA
	 *            a vector which will be modified during addition
	 * @param b
	 *            a vector
	 * @return the modified vector <code>aSA</code>
	 * @throws DimensionMismatchException
	 *             if <code>aSA.length != b.length</code>
	 */
	public static final double[] add(double[] aSA, double[] b)
			throws DimensionMismatchException
	{
		if (aSA.length != b.length) {
			throw new DimensionMismatchException("The dimensions of aSA("
					+ aSA.length + ") and b(" + b.length
					+ ") must match in order to perform vector addition.");
		}
		for (int i = 0; i < aSA.length; i++) {
			aSA[i] += b[i];
		}
		return aSA;
	}

	/**
	 * Subtracts the vector <code>b</code> from the vector <code>aSA</code>
	 * modifying the values of <code>aSA</code>.
	 * 
	 * @param aSA
	 *            a vector which will be modified during subtraction
	 * @param b
	 *            a vector
	 * @return the modified vector <code>aSA</code>
	 * @throws DimensionMismatchException
	 */
	public static final double[] subtract(double[] aSA, double[] b)
			throws DimensionMismatchException
	{
		if (aSA.length != b.length) {
			throw new DimensionMismatchException("The dimensions of aSA("
					+ aSA.length + ") and b(" + b.length
					+ ") must match in order to perform vector subtraction.");
		}
		for (int i = 0; i < aSA.length; i++) {
			aSA[i] -= b[i];
		}
		return aSA;
	}

	/**
	 * Sets the components of the vector <code>aSA</code> to zero from index
	 * <code>begin</code> (inclusive) to index <code>end</code> (inclusive).
	 * 
	 * @param aSA
	 *            a vector which will be modified during this operation
	 * @param begin
	 *            the first index
	 * @param end
	 *            the last index
	 * @return the modified vector <code>aSA</code>
	 */
	public static final double[] zero(double[] aSA, int begin, int end)
	{
		for (int i = begin; i <= end; i++) {
			aSA[i] = 0;
		}
		return aSA;
	}

	/**
	 * This class cannot and should not be instantiated.
	 */
	private SAVectorOps()
	{
	}
}
