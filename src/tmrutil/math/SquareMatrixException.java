/*
 * SquareMatrixException.java
 */

package tmrutil.math;

/**
 * Thrown to indicate that a non-square matrix was passed to a function
 * which requires a square matrix.
 */
public class SquareMatrixException extends MatrixException
{
	private static final long serialVersionUID = 1526552925612316677L;
	
	public static final boolean isSquare(double[][] m)
	{
		int s = m.length;
		for(int i=0;i<s;i++){
			if(s != m[i].length){
				return false;
			}
		}
		return true;
	}

	public SquareMatrixException(String message)
    {
	super(message);
    }
}