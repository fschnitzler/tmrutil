/*
 * DimensionMismatchException.java
 */

package tmrutil.math;

/**
 * Thrown to indicate that an operation cannot be performed because the
 * dimensions of the operands are incompatible (with respect to an operation).
 */
public class DimensionMismatchException extends MathException
{
	private static final long serialVersionUID = -8324931659460489833L;

	public static final String VECTOR_DOT_PRODUCT_ERROR = "Cannot compute dot product between two vectors with different numbers of components.";
	public static final String VECTOR_DISTANCE_ERROR = "Cannot compute Euclidean distance between two vectors with different numbers of components.";
	public static final String VECTOR_ADD_ERROR = "Cannot add two vectors with different numbers of components.";
	public static final String VECTOR_SUBTRACT_ERROR = "Cannot subtract right hand side vector from left hand side vector with different number of components.";
	public static final String VECTOR_COMPWISE_MULT_ERROR = "Cannot perform component-wise multiplication between vectors with different numbers of components.";
	public static final String MATRIX_ADD_ERROR = "Cannot add two matrices with different number of rows or different number of columns.";
	public static final String MATRIX_SUBTRACT_ERROR = "Cannot subtract two matrices with different number of rows or different number of columns.";
	public static final String MATRIX_COMPWISE_MULT_ERROR = "Cannot perform component-wise matrix multiplication between two matrices with different number of rows and different number of columns.";
	public static final String MATRIX_MULT_ERROR = "Cannot perform matrix multiplication between two matrices where the left hand side matrix's number of columns does not match the right hand side matrix's number of rows.";
	public static final String VECTOR_MATRIX_MULT_ERROR = "Cannot multiply vector on the left hand side with matrix on the right hand side if the number of vector components does not match the number of matrix rows.";
	public static final String MATRIX_VECTOR_MULT_ERROR = "Cannot multiply matrix on the left hand side with vector on the right hand side if the number of vector components does not match the number of matrix columns";

	public static final String MATRIX_RESHAPE_ERROR = "Cannot reshape matrix to the specified number of rows and columns.";

	public static final String VECTOR_INFLATE_ERROR = "Cannot inflate vector to matrix because (rows x columns) is not equal to the number of vector components.";

	public static final String MATRIX_ROW_CONCAT_ERROR = "Matrices cannot be concatenated because the number of columns are unequal.";
	public static final String MATRIX_COL_CONCAT_ERROR = "Matrices cannot be concatenated because the number of rows are unequal.";

	public DimensionMismatchException(String message)
	{
		super(message);
	}
}