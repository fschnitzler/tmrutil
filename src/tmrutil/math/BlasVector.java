package tmrutil.math;

import java.util.Arrays;
import org.jblas.DoubleMatrix;

/**
 * A vector where the underlying implementation is based on JBlas vectors.
 * @author tim
 *
 */
public class BlasVector extends AbstractVector
{

	protected DoubleMatrix _components;
	
	public BlasVector(int size)
	{
		super(size);
		_components = DoubleMatrix.zeros(size,1);
	}
	
	public BlasVector(double[] components)
	{
		super(components.length);
		_components = new DoubleMatrix(new double[][]{components});
	}
	
	public BlasVector(BlasVector other)
	{
		super(other.size());
		double[] otherComps = other.toArray();
		double[] comps = Arrays.copyOf(otherComps, otherComps.length);
		_components = new DoubleMatrix(new double[][]{comps});
	}

	@Override
	public Vector create(double[] components)
	{
		BlasVector v = new BlasVector(components);
		return v;
	}

	@Override
	public void copy(Vector rhs)
	{
		double[] rhsComps = rhs.toArray();
		double[] components = Arrays.copyOf(rhsComps, rhsComps.length);
		_components = new DoubleMatrix(new double[][]{components});
	}

	@Override
	public double get(int index)
	{
		return _components.get(index, 0);
	}

	@Override
	public double dotProduct(Vector rhs) throws DimensionMismatchException
	{
		if(rhs instanceof BlasVector){
			if (size() != rhs.size()) {
				throw new DimensionMismatchException(DimensionMismatchException.VECTOR_DOT_PRODUCT_ERROR);
			}
			BlasVector blasRhs = (BlasVector)rhs;
			return _components.dot(blasRhs._components.transpose());
		}else{
			return super.dotProduct(rhs);
		}
	}
	
	@Override
	public Vector multiply(Matrix rhs) throws DimensionMismatchException
	{
		if(size() != rhs.rows()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_MATRIX_MULT_ERROR);
		}
		
		if(rhs instanceof BlasMatrix){
			BlasMatrix rhsBlas = (BlasMatrix)rhs;
			DoubleMatrix components = _components.mmul(rhsBlas._entries);
			return new BlasVector(components.toArray());
		}else{
			double[] components = new double[rhs.cols()];
			for(int c=0;c<rhs.cols();c++){
				double dp = 0;
				for(int r=0;r<rhs.rows();r++){
					dp += get(r) * rhs.get(r, c);
				}
				components[c] = dp;
			}
			return new BlasVector(components);
		}
	}

	@Override
	public Matrix outerProduct(Vector rhs)
	{
		if(rhs instanceof BlasVector){
			BlasVector blasRhs = (BlasVector)rhs;
			DoubleMatrix omat = _components.transpose().mmul(blasRhs._components);
			return new BlasMatrix(omat);
		}else{
			DoubleMatrix entries = DoubleMatrix.zeros(size(), rhs.size());
			for(int r=0;r<size();r++){
				for(int c=0;c<rhs.size();c++){
					double v = get(r) * rhs.get(c);
					entries.put(r, c, v);
				}
			}
			return new BlasMatrix(entries);
		}
	}

	@Override
	public double[] toArray()
	{
		return _components.toArray();
	}

	@Override
	public Vector copy()
	{
		return new BlasVector(this);
	}

	@Override
	public Matrix inflate(int numRows, int numCols)
			throws DimensionMismatchException
	{
		if((numRows * numCols) != size()){
			throw new DimensionMismatchException(DimensionMismatchException.VECTOR_INFLATE_ERROR);
		}
		DoubleMatrix newMat = new DoubleMatrix(numRows, numCols, toArray());
		return new BlasMatrix(newMat);
	}

}
