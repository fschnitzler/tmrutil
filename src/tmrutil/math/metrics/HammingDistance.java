package tmrutil.math.metrics;

import tmrutil.math.Metric;

/**
 * Implementation of the Hamming distance metric between two real valued vectors.
 * @author Timothy Mann
 *
 */
public class HammingDistance implements Metric<double[]>
{

	@Override
	public double distance(double[] x, double[] y)
	{
		double sum = 0;
		for(int i=0;i<x.length;i++){
			sum += Math.abs(x[i] - y[i]);
		}
		return sum;
	}

}
