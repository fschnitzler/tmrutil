package tmrutil.math.metrics;

import tmrutil.math.Metric;

/**
 * A Euclidan metric between points.
 * @author Timothy A. Mann
 *
 * @param <P> the point type
 */
public class EuclideanPointDistance<P extends java.awt.geom.Point2D> implements
		Metric<P> {

	@Override
	public double distance(P x, P y) {
		return x.distance(y);
	}

}
