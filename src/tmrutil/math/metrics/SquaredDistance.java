package tmrutil.math.metrics;

import tmrutil.math.DimensionMismatchException;
import tmrutil.math.Metric;

/**
 * Computes the squared Euclidean distance between two vectors.
 * @author Timothy A. Mann
 *
 */
public class SquaredDistance implements Metric<double[]>
{

	@Override
	public double distance(double[] x, double[] y)
	{
		if(x.length != y.length){
			throw new DimensionMismatchException("The dimension of x and y must match.");
		}
		double ss = 0;
		for(int i=0;i<x.length;i++){
			double diff = x[i] - y[i];
			ss += diff * diff;
		}
		return ss;
	}

}
