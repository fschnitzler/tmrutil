package tmrutil.math.functions;

import org.apache.commons.math3.linear.RealVector;

import tmrutil.math.Function;

/**
 * A basic linear function.
 * 
 * @author Timothy A. Mann
 * 
 */
public class LinearFunction implements Function<RealVector, Double> {

	private static final long serialVersionUID = -3777418323944974392L;

	private RealVector _weights;

	public LinearFunction(RealVector weights) {
		_weights = weights;
	}

	@Override
	public Double evaluate(RealVector x) throws IllegalArgumentException {
		return _weights.dotProduct(x);
	}

}
