package tmrutil.math.functions;

import tmrutil.math.Function;

/**
 * Gabor filters are useful linear filters that are similar to receptive fields
 * found in the primary visual cortex. Gabor filters have been used for edge
 * detection, texture boundry detection, etc. This class represents a 2D Gabor
 * filter.
 * <p/>
 * 
 * <code>g(x, y; lambda, theta, psi, sigma, gamma)</code><br/>
 * The filter takes input vectors with two components and has five parameters.
 * <OL>
 * <LI>lambda : represents the wavelength of the cosine factor</LI>
 * <LI>theta  : represents the orientation of the filter</LI>
 * <LI>psi    : represents the phase offset</LI>
 * <LI>sigma  : represents the Gaussian envelope (width of the Gaussian component)</LI>
 * <LI>gamma  : represents the aspect ratio (or how elliptic the filter is)</LI>
 * </OL>
 * 
 * @author Timothy A. Mann
 * 
 */
public class GaborFilter implements Function<double[], Double>
{
	/** The wave-length. */
	private double _lambda;
	/** The orientation of the filter. */
	private double _theta;
	/** The phase offset. */
	private double _psi;
	/** The Gaussian envelope width. */
	private double _sigma;
	/** The spatial aspect ratio. */
	private double _gamma;

	private double _cosTheta;
	private double _sinTheta;
	private double _sigmaSquared;
	private double _gammaSquared;

	/**
	 * Constructs a Gabor filter.
	 * 
	 * @param lambda
	 *            the wavelength
	 * @param theta
	 *            the orientation
	 * @param psi
	 *            the phase offset
	 * @param sigma
	 *            the Gaussian envelope (width or standard deviation of the
	 *            Gaussian component)
	 * @param gamma
	 *            the aspect ratio
	 */
	public GaborFilter(double lambda, double theta, double psi, double sigma,
			double gamma)
	{
		setWavelength(lambda);
		setOrientation(theta);
		setPhaseOffset(psi);
		setGaussianEnvelope(sigma);
		setAspectRatio(gamma);
	}

	@Override
	public Double evaluate(double[] x) throws IllegalArgumentException
	{
		if (x.length != 2) {
			throw new IllegalArgumentException(
					"Gabor filter only supports 2D vector");
		}
		double xPrime = x[0] * _cosTheta + x[1] * _sinTheta;
		double yPrime = -x[0] * _sinTheta + x[1] * _cosTheta;
		double xPrimeSquared = xPrime * xPrime;
		double yPrimeSquared = yPrime * yPrime;

		double first = Math.exp(-(xPrimeSquared + _gammaSquared * yPrimeSquared)
				/ (2 * _sigmaSquared));
		double second = Math.cos((2 * Math.PI) * (xPrime / _lambda) + _psi);

		return first * second;
	}

	/**
	 * Sets the wavelength of the cosine factor.
	 * 
	 * @param lambda
	 *            a positive scalar value
	 * @throws IllegalArgumentException
	 *             if <code>lambda &lt; 0</code>
	 */
	public void setWavelength(double lambda) throws IllegalArgumentException
	{
		if (lambda < 0) {
			throw new IllegalArgumentException(
					"The wavelength must be positive.");
		}
		_lambda = lambda;
	}

	/**
	 * Returns the wavelength of the cosine factor.
	 * 
	 * @return the wavelength of the cosine factor
	 */
	public double getWavelength()
	{
		return _lambda;
	}

	/**
	 * Sets the orientation of this Gabor filter in radians.
	 * 
	 * @param theta
	 *            an orientation in radians
	 */
	public void setOrientation(double theta)
	{
		_theta = theta;
		_cosTheta = Math.cos(theta);
		_sinTheta = Math.sin(theta);
	}

	/**
	 * Returns the orientation of this Gabor filter in radians.
	 * 
	 * @return an orientation in radians
	 */
	public double getOrientation()
	{
		return _theta;
	}

	/**
	 * Sets the phase offset.
	 * 
	 * @param psi
	 *            a scalar value representing a phase offset
	 */
	public void setPhaseOffset(double psi)
	{
		_psi = psi;
	}

	/**
	 * Returns the phase offset.
	 * 
	 * @return the phase offset
	 */
	public double getPhaseOffset()
	{
		return _psi;
	}

	/**
	 * Sets the Gaussian envolope (think standard deviation or width) of this
	 * Gabor filter.
	 * 
	 * @param sigma
	 *            a scalar value
	 * @throws IllegalArgumentException
	 *             if <code>sigma &lt; 0</code>
	 */
	public void setGaussianEnvelope(double sigma)
			throws IllegalArgumentException
	{
		if (sigma < 0) {
			throw new IllegalArgumentException(
					"The Gaussian envelope must be positive.");
		}
		_sigma = sigma;
		_sigmaSquared = sigma * sigma;
	}

	/**
	 * Returns the Gaussian envelope (think standard deviation or width) of this
	 * Gabor filter.
	 * 
	 * @return a scalar value representing the Gaussian envelope
	 */
	public double getGaussianEnvelope()
	{
		return _sigma;
	}

	/**
	 * Sets the aspect ratio (or how elliptic the filter is).
	 * 
	 * @param gamma
	 *            a positive scalar value
	 * @throws IllegalArgumentException
	 *             if <code>gamma &lt; 0</code>
	 */
	public void setAspectRatio(double gamma) throws IllegalArgumentException
	{
		if (gamma < 0) {
			throw new IllegalArgumentException(
					"The aspect ratio must be positive.");
		}
		_gamma = gamma;
		_gammaSquared = gamma * gamma;
	}

	/**
	 * Returns the aspect ratio (or how elliptic the filter is).
	 * 
	 * @return the aspect ratio
	 */
	public double getAspectRatio()
	{
		return _gamma;
	}

}
