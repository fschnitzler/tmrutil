package tmrutil.math.functions;

import tmrutil.math.KernelFunction;
import tmrutil.math.VectorOps;

/**
 * A radial basis kernel function.
 * 
 * @author Timothy A. Mann
 * 
 */
public class RadialBasisKernelFunction implements KernelFunction<double[]>
{
	/** Determines the width of the radial basis function. */
	private double _sigma;

	/**
	 * Constructs a radial basis kernel function with a specified width.
	 * 
	 * @param sigma
	 *            width of the radial basis
	 */
	public RadialBasisKernelFunction(double sigma)
	{
		_sigma = sigma;
	}

	@Override
	public double evaluate(double[] a, double[] b)
			throws IllegalArgumentException
	{
		double d2 = VectorOps.distanceSqd(a, b);
		return Math.exp(-d2 / (2 * _sigma * _sigma));
	}

}
