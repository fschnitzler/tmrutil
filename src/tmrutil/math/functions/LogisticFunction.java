package tmrutil.math.functions;

import tmrutil.math.RealFunction;

/**
 * The logistic function is a sinusoidal function often used as the activation
 * function for neural networks.
 * 
 * @author Timothy Mann
 * 
 */
public class LogisticFunction extends RealFunction
{
	private double _beta;

	/**
	 * Constructs a logistic function with a default shaping value (1.0).
	 */
	public LogisticFunction()
	{
		this(1.0);
	}

	/**
	 * Constructs a logistic function with a specified shaping parameter.
	 * 
	 * @param beta
	 *            a shaping value
	 */
	public LogisticFunction(double beta)
	{
		_beta = beta;
	}

	@Override
	public Double evaluate(Double x) throws IllegalArgumentException
	{
		return 1.0 / (1.0 + Math.exp(-_beta * x));
	}

	@Override
	public Double differentiate(Double x)
	{
		return (_beta * Math.exp(-_beta * x))
				/ Math.pow(1.0 + Math.exp(-_beta * x), 2);
	}

}
