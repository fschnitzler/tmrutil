package tmrutil.math.functions;

import tmrutil.math.RealFunction;

public class ConstantScale extends RealFunction
{
	private double _scalar;
	private double _bias;
	
	public ConstantScale(double scalar, double bias)
	{
		_scalar = scalar;
		_bias = bias;
	}

	@Override
	public Double evaluate(Double x) throws IllegalArgumentException
	{
		return _scalar * x + _bias;
	}
	
	@Override
	public Double differentiate(Double x)
	{
		return _scalar;
	}

}
