package tmrutil.math.functions;

import tmrutil.math.RealFunction;

public class Negative extends RealFunction
{

	@Override
	public Double evaluate(Double x)
	{
		return -x;
	}
	
	@Override
	public Double differentiate(Double x)
	{
		return new Double(-1);
	}

}
