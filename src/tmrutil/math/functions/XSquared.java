package tmrutil.math.functions;

import tmrutil.math.RealFunction;

public class XSquared extends RealFunction
{

	@Override
	public Double evaluate(Double x)
	{
		return x*x;
	}
	
	@Override
	public Double differentiate(Double x)
	{
		return 2*x;
	}

}
