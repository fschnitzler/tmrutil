package tmrutil.math.functions;

import tmrutil.math.RealFunction;

public class HyperbolicTangent extends RealFunction
{

	@Override
	public Double evaluate(Double x)
	{
		return Math.tanh(x);
	}
	
	@Override
	public Double differentiate(Double x)
	{
		return 1 - Math.pow(Math.tanh(x),2);
	}

}
