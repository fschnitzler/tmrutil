package tmrutil.math.functions;

import tmrutil.math.Function;

public class UnivariateLinearIntegerFunction implements Function<Integer, Integer> {

	private static final long serialVersionUID = -4608575260447958841L;

	private Integer _slope;
	private Integer _intercept;
	
	public UnivariateLinearIntegerFunction(Integer slope, Integer intercept)
	{
		_slope = slope;
		_intercept = intercept;
	}
	
	@Override
	public Integer evaluate(Integer x) throws IllegalArgumentException {
		Integer v = _slope * x + _intercept;
		return v;
	}

}
