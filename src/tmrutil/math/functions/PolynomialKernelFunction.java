package tmrutil.math.functions;

import tmrutil.math.KernelFunction;
import tmrutil.math.VectorOps;

public class PolynomialKernelFunction implements KernelFunction<double[]>
{

	private int _d;
	
	public PolynomialKernelFunction(int d)
	{
		_d = d;
	}
	
	@Override
	public double evaluate(double[] a, double[] b)
			throws IllegalArgumentException
	{
		if(a.length != b.length){
			throw new IllegalArgumentException("The dimensions of a and be must be the same.");
		}
		return Math.pow(VectorOps.dotProduct(a, b), _d);
	}

}
