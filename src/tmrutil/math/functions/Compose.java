package tmrutil.math.functions;

import tmrutil.math.RealFunction;

/**
 * Composes two functions <code>f</code> and <code>g</code> constructing a new function <code>f(g(x))</code>.
 * @author Timothy Mann
 */
public class Compose extends RealFunction
{
	private RealFunction _f;
	private RealFunction _g;

	/**
	 * Constructs the composition of two functions <code>f</code> and <code>g</code> resulting in <code>f(g(x))</code>.
	 * @param f a function
	 * @param g a function
	 */
	public Compose(RealFunction f, RealFunction g)
	{
		_f = f;
		_g = g;
	}
	
	@Override
	public Double evaluate(Double x)
	{
		return _f.evaluate(_g.evaluate(x));
	}
	
	@Override
	public Double differentiate(Double x)
	{
		// Use the chain rule : (d/dx)f(g(x)) = f'(g(x)) * g'(x)
		return _f.differentiate(_g.evaluate(x)) * _g.differentiate(x);
	}

}
