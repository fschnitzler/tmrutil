package tmrutil.math;

import java.io.Serializable;

/**
 * Represents a generic function.
 * @author Timothy Mann
 *
 * @param <D> the function domain type
 * @param <R> the function range type
 */
public interface Function<D, R> extends Serializable
{	
	/**
	 * Evaluates this function at the specified input <code>x</code>.
	 * @param x an input value
	 * @return an output value
	 * @throws IllegalArgumentException if <code>x</code> is invalid (i.e. not in the functions domain)
	 */
	public abstract R evaluate(D x) throws IllegalArgumentException;
}
