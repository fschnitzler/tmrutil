package tmrutil.math;

/**
 * Represents a similarity metric. A metric <em>d : T x T --&gt; R</em> over a set <code>T</code> containing <code>x, y, z</code> has four properties:
 * <OL>
 * <LI><code>d(x,y) &ge; 0</code> (Non-negative)</LI>
 * <LI><code>d(x,y) = 0</code> if and only if <code>x = y</code> (Identity of Indiscernibles)</LI>
 * <LI><code>d(x,y) = d(y,x)</code> (Symmetry)</LI>
 * <LI><code>d(x,z) &le; d(x,y) + d(y,z)</code> (Triangle Inequality)</LI>
 * </OL>
 * @author Timothy Mann
 *
 * @param <T> the type that the distance function is for
 */
public interface Metric<T>
{
	/**
	 * Returns a real number denoting the similarity/difference between two objects <code>x</code> and <code>y</code>.
	 * @param y an object
	 * @param x an object
	 * @return a real number denoting similarity/difference
	 */
	public double distance(T x, T y);
}
