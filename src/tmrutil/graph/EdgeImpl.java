package tmrutil.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of an undirected edge.
 * @author Timothy A. Mann
 *
 * @param <N> node type
 * @param <T> content type
 */
public class EdgeImpl<N extends Node<?,?>,T> implements Edge<N, T>
{
	private N _nodeA;
	private N _nodeB;
	private T _content;
	
	public EdgeImpl(N nodeA, N nodeB, T content)
	{
		_nodeA = nodeA;
		_nodeB = nodeB;
		_content = content;
	}

	@Override
	public Set<N> nodes()
	{
		Set<N> s = new HashSet<N>();
		s.add(_nodeA);
		s.add(_nodeB);
		return s;
	}

	@Override
	public T content()
	{
		return _content;
	}

}
