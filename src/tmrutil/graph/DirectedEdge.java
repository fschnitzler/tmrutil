package tmrutil.graph;

/**
 * A directed edge between two nodes in a graph.
 * @author Timothy A. Mann
 *
 * @param <N> the node type
 */
public interface DirectedEdge<N extends Node<?,?>,T> extends Edge<N,T>
{
	/**
	 * The node that this directed edge goes to.
	 * @return the end node
	 */
	public N to();
	
	/**
	 * The node that this directed edge comes from.
	 * @return the start node
	 */
	public N from();
}
