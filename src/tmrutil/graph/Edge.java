package tmrutil.graph;

import java.util.Set;

/**
 * Represents an edge between two nodes.
 * @author Timothy A. Mann
 *
 * @param <N> the node type
 * @param <T> the content type
 */
public interface Edge<N extends Node<?,?>,T>
{
	/**
	 * Returns a set containing the two nodes connected by this edge in arbitrary order.
	 * @return a set containing two connected nodes
	 */
	public Set<N> nodes();
	
	/**
	 * The content associated with this edge.
	 * @return content
	 */
	public T content();
}
