package tmrutil.optim;

import java.util.Collection;
import java.util.List;

import tmrutil.math.Function;
import tmrutil.util.Pair;

/**
 * Implements linear search.
 * 
 * @author Timothy A. Mann
 * 
 * @param <E>
 *            the element type
 */
public class LinearSearch<E> {

	private Optimization _opType;

	public LinearSearch(Optimization opType) {
		_opType = opType;
	}

	/**
	 * Returns a pair containing the element in the collection with the best
	 * score along with the best score.
	 * 
	 * @param elements
	 *            a collection of elements
	 * @param scorer
	 *            a scorer for the elements
	 * @return a pair containing the best element and its score
	 */
	public Pair<E, Double> search(Collection<E> elements,
			Function<E, Double> scorer) {
		Double bestScore = null;
		E bestElm = null;
		for (E elm : elements) {
			double score = scorer.evaluate(elm);
			if (bestScore == null || isBetter(bestScore, score)) {
				bestElm = elm;
				bestScore = score;
			}
		}
		return new Pair<E, Double>(bestElm, bestScore);
	}

	/**
	 * Returns the lowest index with the best score.
	 * 
	 * @param elements
	 *            a list of elements
	 * @param scorer
	 *            a scorer for the elements
	 * @return a pair containing the index of the best element and its score
	 */
	public Pair<Integer, Double> searchForIndex(List<E> elements,
			Function<E, Double> scorer) {
		Double bestScore = null;
		Integer bestIndex = null;
		for (int i = 0; i < elements.size(); i++) {
			E elm = elements.get(i);
			double score = scorer.evaluate(elm);
			if (bestScore == null || isBetter(bestScore, score)) {
				bestIndex = i;
				bestScore = score;
			}
		}
		return new Pair<Integer, Double>(bestIndex, bestScore);
	}

	/**
	 * Returns an index (from the given list of indices) with the best score. If
	 * multiple indices share the lowest score, this method could return anyone
	 * of those.
	 * 
	 * @param elements
	 *            a list of elements
	 * @param scorer
	 *            a scorer for the elements
	 * @param indices
	 *            the indices in elements to search
	 * @return a pair containing the index of the best element and its score
	 */
	public Pair<Integer, Double> searchForIndex(List<E> elements,
			Function<E, Double> scorer, Collection<Integer> indices) {
		Double bestScore = null;
		Integer bestIndex = null;
		for (Integer i : indices) {
			E elm = elements.get(i);
			double score = scorer.evaluate(elm);
			if (bestScore == null || isBetter(bestScore, score)) {
				bestIndex = i;
				bestScore = score;
			}
		}
		return new Pair<Integer, Double>(bestIndex, bestScore);
	}

	private boolean isBetter(double baseScore, double newScore) {
		if (_opType.equals(Optimization.MAXIMIZE)) {
			return newScore > baseScore;
		} else {
			return newScore < baseScore;
		}
	}
}
