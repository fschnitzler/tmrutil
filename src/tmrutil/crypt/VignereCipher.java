package tmrutil.crypt;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements the Vignere cipher and deciphering.
 * 
 * @author Timothy A. Mann
 * 
 */
public class VignereCipher implements
		Cipher<Iterable<Character>, List<Character>>,
		Decipher<List<Character>, Iterable<Character>>
{
	private static final int ALPHABET_SIZE = 26;

	private String _cipherKey;
	private List<CaesarCipher> _vignereTable;

	public VignereCipher(String cipherKey)
	{
		setCipherKey(cipherKey);
		_vignereTable = new ArrayList<CaesarCipher>(ALPHABET_SIZE);
		for (int i = 0; i < ALPHABET_SIZE; i++) {
			_vignereTable.add(new CaesarCipher(i));
		}
	}

	public void setCipherKey(String cipherKey)
	{
		_cipherKey = cipherKey.toLowerCase();
	}

	public List<Character> decipher(CharSequence ciphertext)
	{
		List<Character> clist = new ArrayList<Character>(ciphertext.length());
		for (int i = 0; i < ciphertext.length(); i++) {
			clist.add(ciphertext.charAt(i));
		}
		return decipher(clist);
	}

	@Override
	public List<Character> decipher(Iterable<Character> ciphertext)
	{
		List<Character> plaintext = new ArrayList<Character>();
		int keyIndex = 0;
		for (Character csymbol : ciphertext) {
			char keyLetter = _cipherKey.charAt(keyIndex);
			int alphaIndex = keyLetter - 'a';
			Character psymbol = _vignereTable.get(alphaIndex).decipherSymbol(
					csymbol);
			plaintext.add(psymbol);

			keyIndex++;
			if (keyIndex > _cipherKey.length() - 1) {
				keyIndex = 0;
			}
		}
		return plaintext;
	}

	public List<Character> encipher(CharSequence plaintext)
	{
		List<Character> plist = new ArrayList<Character>(plaintext.length());
		for (int i = 0; i < plaintext.length(); i++) {
			plist.add(plaintext.charAt(i));
		}
		return encipher(plist);
	}

	@Override
	public List<Character> encipher(Iterable<Character> plaintext)
	{
		List<Character> ciphertext = new ArrayList<Character>();
		int keyIndex = 0;
		for (Character psymbol : plaintext) {
			char keyLetter = _cipherKey.charAt(keyIndex);
			int alphaIndex = keyLetter - 'a';
			Character csymbol = _vignereTable.get(alphaIndex).encipherSymbol(
					psymbol);
			ciphertext.add(csymbol);

			keyIndex++;
			if (keyIndex > _cipherKey.length() - 1) {
				keyIndex = 0;
			}
		}
		return ciphertext;
	}

	public static void main(String[] args)
	{
		String cipherKey = "xfiewjls";
		String plaintext = "Thisisatestmessage";
		
		System.out.println("Key : " + cipherKey);
		System.out.println("Paintext   : " + plaintext);

		//CaesarCipher ccipher = new CaesarCipher(10);
		VignereCipher vcipher = new VignereCipher(cipherKey);
		List<Character> ciphertext = vcipher.encipher(plaintext);
		TextAnalysisImpl<Character> canalysis = new TextAnalysisImpl<Character>(ciphertext);
		System.out.println("Ciphertext : " + canalysis);
		
		List<Character> plaintext2 = vcipher.decipher(ciphertext);
		TextAnalysisImpl<Character> panalysis = new TextAnalysisImpl<Character>(plaintext2);
		System.out.println("Plaintext  : " + panalysis);

	}
}
