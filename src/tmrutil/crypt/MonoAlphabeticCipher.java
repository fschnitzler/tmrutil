package tmrutil.crypt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implements a mono-alphabetic cipher/decipher.
 * @author Timothy A. Mann
 *
 * @param <PTS> the plaintext symbol type
 * @param <CTS> the ciphertext symbol type
 */
public class MonoAlphabeticCipher<PTS,CTS> implements Cipher<Iterable<PTS>, List<CTS>>, Decipher<List<PTS>, Iterable<CTS>>
{
	private Map<PTS,CTS> _cipherKey;
	private Map<CTS,PTS> _decipherKey;
	
	public MonoAlphabeticCipher(Map<PTS,CTS> cipherKey)
	{
		setCipherKey(cipherKey);
	}
	
	public void setCipherKey(Map<PTS,CTS> cipherKey)
	{
		_cipherKey = cipherKey;
		_decipherKey = new HashMap<CTS,PTS>();
		Set<PTS> keys = cipherKey.keySet(); 
		for(PTS key : keys){
			CTS val = cipherKey.get(key);
			_decipherKey.put(val, key);
		}
	}
	
	public void setDecipherKey(Map<CTS,PTS> decipherKey)
	{
		_decipherKey = decipherKey;
		_cipherKey = new HashMap<PTS,CTS>();
		Set<CTS> keys = decipherKey.keySet();
		for(CTS key : keys){
			PTS val = decipherKey.get(key);
			_cipherKey.put(val, key);
		}
	}

	@Override
	public List<PTS> decipher(Iterable<CTS> ciphertext)
	{
		List<PTS> plaintext = new ArrayList<PTS>();
		
		for(CTS csymbol : ciphertext){
			PTS psymbol = decipherSymbol(csymbol);
			plaintext.add(psymbol);
		}
		
		return plaintext;
	}

	@Override
	public List<CTS> encipher(Iterable<PTS> plaintext)
	{
		List<CTS> ciphertext = new ArrayList<CTS>();
		
		for(PTS psymbol : plaintext){
			CTS csymbol = encipherSymbol(psymbol);
			ciphertext.add(csymbol);
		}
		return ciphertext;
	}
	
	public CTS encipherSymbol(PTS psymbol)
	{
		return _cipherKey.get(psymbol);
	}
	
	public PTS decipherSymbol(CTS csymbol)
	{
		return _decipherKey.get(csymbol);
	}

}
