package tmrutil.crypt;

/**
 * An interface for enciphering messages.
 * @author Timothy A. Mann
 *
 * @param <PT> the plaintext type
 * @param <CT> the ciphertext type
 */
public interface Cipher<PT,CT>
{
	/**
	 * Enciphers the plaintext producing a ciphertext.
	 * @param plaintext a plaintext message
	 * @return a ciphertext message based on the plaintext
	 */
	public CT encipher(PT plaintext);
}
