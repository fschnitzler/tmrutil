package tmrutil.threads;

import java.util.ArrayList;
import java.util.List;

/**
 * A task can be executed by a thread of a task manager.
 * 
 * @author Timothy Mann
 * 
 */
public abstract class Task
{

	private boolean _isDone;
	private boolean _isStopped;
	private boolean _isPaused;
	private List<TaskListener> _taskListeners;
	
	public Task()
	{
		_isDone = false;
		_isStopped = false;
		_isPaused = false;
		_taskListeners = new ArrayList<TaskListener>(2);
	}
	
	/**
	 * Add a task listener to this task.
	 */
	public void addTaskListener(TaskListener listener)
	{
		_taskListeners.add(listener);
	}
	
	/**
	 * Remove a task listener from this task.
	 */
	public void removeTaskListener(TaskListener listener)
	{
		_taskListeners.remove(listener);
	}

	/**
	 * Called by a task runner to execute this task.
	 */
	protected void trExec()
	{
		execute();
		_isStopped = true;
		_isDone = true;
		for(TaskListener l : _taskListeners)
		{
			l.taskFinished(this);
		}
	}

	/**
	 * Causes the task to execute. This should not be called directly. Only the
	 * task manager will make use of this method.
	 */
	public abstract void execute();

	/**
	 * Stops this task from completing.
	 */
	public void stop()
	{
		_isStopped = true;
	}

	/**
	 * Pauses this task's execution if it was running and is not finished.
	 * Otherwise nothing happens.
	 */
	public void pause()
	{
		if (!isDone() && !isPaused()) {
			_isPaused = true;
		}
	}

	/**
	 * Resumes this task's execution if it was paused and is not finished.
	 * Otherwise nothing happens.
	 */
	public void resume()
	{
		if (!isDone() && isPaused()) {
			_isPaused = false;
		}
	}

	/**
	 * Returns true if the execution of this task is paused. A task can only be
	 * paused if the task is not done executing.
	 * 
	 * @return true if the execution of this task is paused
	 */
	public boolean isPaused()
	{
		if (_isDone) {
			return false;
		}
		return _isPaused;
	}

	/**
	 * Returns true if a signal has been given to this task to stop execution.
	 * 
	 * @return true if a signal has been given to this task to stop execution
	 */
	public boolean isStopped()
	{
		return _isStopped;
	}

	/**
	 * Returns true if the execution of this task has completed.
	 * 
	 * @return true if the execution of this task has completed
	 */
	public boolean isDone()
	{
		return _isDone;
	}
}
