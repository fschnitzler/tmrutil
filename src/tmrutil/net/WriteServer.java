package tmrutil.net;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * A simple application that listens for clients and writes text stream data
 * from clients to a file.
 * 
 * @author Timothy Mann
 * 
 */
public class WriteServer
{
	protected static final int DEFAULT_PORT = 30231;
	
	/**
	 * A class denoting an end-of-file.
	 * 
	 * @author Timothy Mann
	 * 
	 */
	protected static final class EOF implements Serializable
	{
		private static final long serialVersionUID = -1174796686843643056L;
	}

	/**
	 * A class which starts a thread and writes a file with text data sent from
	 * a client.
	 * 
	 * @author Timothy Mann
	 * 
	 */
	private static class WriteJob implements Runnable
	{
		private Socket _socket;

		public WriteJob(Socket socket)
		{
			_socket = socket;
		}

		public void run()
		{
			try {
				InputStream is = _socket.getInputStream();
				ObjectInputStream ois = new ObjectInputStream(is);
				File file = (File) ois.readObject();
				FileWriter fw = new FileWriter(file);
				Object obj = null;
				do {
					obj = ois.readObject();
					if (obj instanceof String) {
						String str = (String) obj;
						fw.write(str);
					}
				} while (!(obj instanceof EOF));
				fw.close();
				is.close();
			} catch (ClassNotFoundException ex) {
				System.err.println("Client failed to follow correct protocol : "
						+ _socket.getInetAddress());
			} catch (IOException ex) {
				System.err.println("Failed to write data from : "
						+ _socket.getInetAddress());
			}
		}
	}

	/**
	 * The server socket that listens for clients.
	 */
	private ServerSocket _server;

	/**
	 * Creates a write server given a port number.
	 * 
	 * @param port
	 *            a port number to listen on
	 * @throws IOException
	 *             thrown to indicate an I/O exception occurred while starting
	 *             the server.
	 */
	public WriteServer(int port) throws IOException
	{
		_server = new ServerSocket(port);
	}

	/**
	 * Accepts a connection from a client and then starts a thread to handle the
	 * clients requests.
	 * 
	 * @throws IOException
	 *             to indicate an I/O error occurred while trying to connect
	 *             with a client
	 */
	public void accept() throws IOException
	{
		Socket socket = _server.accept();
		System.out.println("Accepted connection from : " + socket.getInetAddress() + " on port : " + socket.getPort());
		WriteJob wjob = new WriteJob(socket);
		Thread t = new Thread(wjob);
		t.start();
	}

	public static void main(String[] args)
	{
		try {
			WriteServer wserver = new WriteServer(DEFAULT_PORT);
			while (true) {
				wserver.accept();
			}
		} catch (IOException ex) {
			System.err.println("Failed to create a WriteServer at port : "
					+ DEFAULT_PORT);
			ex.printStackTrace();
		}
	}
}
