package tmrutil.kinematics;

import java.util.ArrayList;
import java.util.List;
import tmrutil.math.geometry.Point3D;
import tmrutil.util.Interval;
import tmrutil.util.ListUtil;

/**
 * Represents a 3-dimensional kinematic arm.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Arm3D
{
	/**
	 * A 3-dimensional link.
	 * @author Timothy A. Mann
	 *
	 */
	private static class Link3D
	{
		private double _length;
		private Interval _xLimits;
		private Interval _yLimits;

		public Link3D(double length, Interval xLimits, Interval yLimits)
		{
			_length = length;
			_xLimits = new Interval(xLimits);
			_yLimits = new Interval(yLimits);
		}

		public double getLength()
		{
			return _length;
		}

		public Interval getXLimits()
		{
			return new Interval(_xLimits);
		}

		public Interval getYLimits()
		{
			return new Interval(_yLimits);
		}
	}

	private Point3D _base;
	private List<Link3D> _links;
	private List<Double> _xThetas;
	private List<Double> _yThetas;

	/**
	 * Constructs an empty arm (no links) with its base at the origin.
	 */
	public Arm3D()
	{
		_base = new Point3D();
		_links = new ArrayList<Link3D>();
		_xThetas = new ArrayList<Double>();
		_yThetas = new ArrayList<Double>();
	}

	/**
	 * Returns the point where the base of the arm is located.
	 * 
	 * @return a 3-dimensional point
	 */
	public Point3D getBase()
	{
		return _base;
	}

	/**
	 * Adds a link to this arm connecting the link to the previous link or base
	 * point with a universal joint where the limits of that joint specified by
	 * <code>xLimits</code> and <code>yLimits</code>.
	 * 
	 * @param length
	 *            the length of the link
	 * @param xLimits
	 *            the X-limits of the links joint
	 * @param yLimits
	 *            the Y-limits of the links joint
	 */
	public void addLink(double length, Interval xLimits, Interval yLimits)
	{
		Link3D link = new Link3D(length, xLimits, yLimits);
		_links.add(link);
		_xThetas.add(xLimits.getMin());
		_yThetas.add(yLimits.getMin());
	}

	/**
	 * Sets the joint angles to specified values.
	 * @param xAngles the angles for the first axis of the joints
	 * @param yAngles the angles for the second axis of the joints
	 */
	public void setJointAngles(double[] xAngles, double[] yAngles)
	{
		if(xAngles.length != _xThetas.size()){
			throw new IllegalArgumentException(
			"Size of joint angle vector does not match the number of joints.");
		}
		if(yAngles.length != _yThetas.size()){
			throw new IllegalArgumentException(
			"Size of joint angle vector does not match the number of joints.");
		}
		for (int i = 0; i < _links.size(); i++) {
			Link3D link = _links.get(i);
			_xThetas.set(i, link.getXLimits().clip(xAngles[i]));
			_yThetas.set(i, link.getYLimits().clip(yAngles[i]));
		}
	}
	
	/**
	 * Returns the end-point of this arm given its current pose.
	 * @return the end-point of the arm
	 */
	public Point3D endPoint()
	{
		double[] xAngles = ListUtil.toArray(_xThetas);
		double[] yAngles = ListUtil.toArray(_yThetas);
		return endPoint(xAngles, yAngles);
	}
	
	/**
	 * Returns the end-point of this arm given a specified set of joint angles.
	 * @param xAngles the angles for the first axis of the joints
	 * @param yAngles the angles for the second axis of the joints
	 * @return the end-point of the arm
	 */
	public Point3D endPoint(double[] xAngles, double[] yAngles)
	{
		return linkEndPoint(size(), xAngles, yAngles);
	}
	
	/**
	 * Computes the end-point of the link denoted by <code>index</code>.
	 * @param index index of a link
	 * @param xAngles the angles for the first axis of the joints
	 * @param yAngles the angles for the second axis of the joints
	 * @return the end-point of the arm link specified by <code>index</code>
	 */
	public Point3D linkEndPoint(int index, double[] xAngles, double[] yAngles)
	{
		if(index < 0 || index > _links.size()){
			throw new IndexOutOfBoundsException("Invalid link index : " + index);
		}
		if(xAngles.length != _xThetas.size()){
			throw new IllegalArgumentException(
			"Size of joint angle vector does not match the number of joints.");
		}
		if(yAngles.length != _yThetas.size()){
			throw new IllegalArgumentException(
			"Size of joint angle vector does not match the number of joints.");
		}
		
		double x = _base.getX(), y = _base.getY(), z = _base.getZ();
		double xTheta = 0;
		double yTheta = 0;
		for(int i=0;i<index;i++){
			xTheta += xAngles[i];
			yTheta += yAngles[i];
			
			Link3D link = _links.get(i);
			
			double sinY = Math.sin(yTheta);
			double cosXcosY = Math.cos(xTheta) * Math.cos(yTheta);
			
			z += link.getLength() * sinY;
			x += link.getLength() * cosXcosY;
			y += link.getLength() * Math.sqrt(1 - sinY - cosXcosY);
		}
		return new Point3D(x, y, z);
	}

	/**
	 * Sets the arm's position so that each joint is set to the minimum angle
	 * within its valid range.
	 */
	public void reset()
	{
		for (int i = 0; i < _links.size(); i++) {
			Link3D link = _links.get(i);
			_xThetas.set(i, link.getXLimits().getMin());
			_yThetas.set(i, link.getYLimits().getMin());
		}
	}

	/**
	 * Sets the arm's position so that each joint is set to a random angle
	 * within its valid range.
	 */
	public void random()
	{
		for (int i = 0; i < _links.size(); i++) {
			Link3D link = _links.get(i);
			_xThetas.set(i, link.getXLimits().random());
			_yThetas.set(i, link.getYLimits().random());
		}
	}

	/**
	 * Returns the maximum possible length of the arm if each link is laid
	 * end-to-end.
	 * 
	 * @return the sum of the length of all links
	 */
	public double getMaxArmLength()
	{
		double sum = 0;
		for (int i = 0; i < _links.size(); i++) {
			sum += _links.get(i).getLength();
		}
		return sum;
	}

	/**
	 * Returns the number of links in this arm.
	 * 
	 * @return the number of links in this arm
	 */
	public int size()
	{
		return _links.size();
	}
}
