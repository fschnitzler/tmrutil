package tmrutil.kinematics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import tmrutil.graphics.Drawable;
import tmrutil.kinematics.Arm;
import tmrutil.learning.rl.LearningSystem;
import tmrutil.learning.rl.ReinforcementSignal;
import tmrutil.learning.rl.Task;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.stats.Statistics;
import tmrutil.util.DebugException;
import tmrutil.util.Pair;

/**
 * Implements a target reaching control task for a multi-link arm. The objective
 * of this task is to supply actions in the form of joint angle adjustments that
 * move the simulated arm's end-effector (or end-point) as close to the current
 * target position as possible. This task does not have a finishing condition.
 * 
 * The state description at each time step is a vector which depends on the
 * representation type selected when the task is constructed.
 * 
 * Actions are specified as vectors with the same number of components as their
 * are arm joints. Each component of the vector specifies either a positive or
 * negative change (in radians) of a specific arm joint. The first index (i.e.
 * 0) specifies the joint angle change for the base joint while increasing the
 * index specifies joint angle change for joints successively further up the
 * arm. If the specified action falls within the range of the underlying arms
 * joint angle limits then the new joint angles for the arm will be equal to the
 * previous joint angles plus the specified action vector. However, if the
 * action vector moves any joint outside of its acceptable range the resulting
 * joint angle will be clipped to its nearest extreme (minimum or maximum).
 * 
 * @author Timothy A. Mann
 * 
 */
public class TargetReachTask extends Task<double[], double[]> implements
		Drawable
{
	public enum Representation {
		JA, JA_DIFF, JA_DIFF_NORM, DIFF, ALT2, ALT4, CARTESIAN, POLAR, CARTESIAN_DIFF, POLAR_DIFF
	};

	public static class DefaultReinforcementSignal implements
			ReinforcementSignal3<double[], double[]>
	{
		Representation _repr;

		public DefaultReinforcementSignal(Representation repr)
		{
			_repr = repr;
		}

		@Override
		public double evaluate(double[] prevState, double[] action,
				double[] newState)
		{
			switch (_repr) {
			case JA:
				break;
			case JA_DIFF:
				break;
			case JA_DIFF_NORM:
				break;
			case DIFF:
				break;
			case ALT2:
				break;
			case ALT4:
				break;
			case CARTESIAN:
				return Math.pow(newState[0] - newState[2], 2)
						+ Math.pow(newState[1] - newState[3], 2);
			case POLAR:
				return Math.pow(newState[0] - newState[2], 2)
						+ Math.pow(newState[1] - newState[3], 2);
			case CARTESIAN_DIFF:
				return Math.pow(newState[0], 2) + Math.pow(newState[1], 2);
			case POLAR_DIFF:
				return Math.pow(newState[0], 2) + Math.pow(newState[1], 2);
			}
			throw new UnsupportedOperationException(
					"This reinforcement signal does not support the specified representation.");
		}
	}

	public static final int TARGET_X = 0;
	public static final int TARGET_Y = 1;
	public static final int ENDPOINT_X = 2;
	public static final int ENDPOINT_Y = 3;

	private Arm _arm;
	private Point2D _target;
	private Representation _repr;

	/** A reference to the reinforcement signal. */
	private ReinforcementSignal3<double[], double[]> _rsignal;
	private double[] _prevStateBuff;
	private double[] _actionBuff;

	private boolean _drawHistory;
	private List<double[]> _poseHistory;

	public TargetReachTask(Arm arm, Representation representation)
	{
		this(arm, representation,
				new DefaultReinforcementSignal(representation));
	}

	/**
	 * Constructs a target reach task for the specified arm. A copy of the arm
	 * is made.
	 * 
	 * @param arm
	 *            an arm
	 * @param representation
	 *            the sensory representation type to utilize
	 * @param rsignal
	 *            the reinforcement signal to use
	 */
	public TargetReachTask(Arm arm, Representation representation,
			ReinforcementSignal3<double[], double[]> rsignal)
	{
		_arm = new Arm(arm);
		_repr = representation;
		_rsignal = rsignal;
		_target = new Point2D.Double();
		setDrawHistory(false);
		_poseHistory = new ArrayList<double[]>();

		_prevStateBuff = new double[getState().length];
		_actionBuff = new double[arm.size()];
	}

	/**
	 * A copy constructor for the arm target reach task.
	 * 
	 * @param task
	 *            an instance of the arm target reach task
	 */
	public TargetReachTask(TargetReachTask task)
	{
		_arm = new Arm(task.getArm());
		_repr = task.getRepresentation();
		_rsignal = task._rsignal;
		_target = task.getTarget();
		setDrawHistory(task._drawHistory);
		_poseHistory = new ArrayList<double[]>(task._poseHistory);

		_prevStateBuff = new double[getState().length];
		_actionBuff = new double[task.getArm().size()];
	}

	/**
	 * Returns the representation type used by this target reach task.
	 * 
	 * @return the representation type used
	 */
	public Representation getRepresentation()
	{
		return _repr;
	}

	@Override
	public double evaluate()
	{
		return _rsignal.evaluate(_prevStateBuff, _actionBuff, getState());
	}

	/**
	 * Returns the Euclidean distance between the arm's end-effector and the
	 * target point.
	 * 
	 * @return distance between end-effector and target
	 */
	public Double targetDistance()
	{
		Point2D ep = _arm.endPoint();
		return ep.distance(_target);
	}

	@Override
	public void execute(double[] action)
	{
		System.arraycopy(getState(), 0, _prevStateBuff, 0,
				_prevStateBuff.length);
		System.arraycopy(action, 0, _actionBuff, 0, _actionBuff.length);

		double[] ja = _arm.getJointAngles();
		_arm.setJointAngles(VectorOps.add(ja, action));
		_poseHistory.add(_arm.getJointAngles());
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

	/**
	 * Returns a copy of the underlying arm with the current pose.
	 * 
	 * @return a copy of the underlying arm
	 */
	public Arm getArm()
	{
		return new Arm(_arm);
	}

	/**
	 * Returns a copy of the target point.
	 * 
	 * @return a copy of the target point
	 */
	public Point2D getTarget()
	{
		return new Point2D.Double(_target.getX(), _target.getY());
	}

	/**
	 * Set the target point to a specified point.
	 * 
	 * @param target
	 *            a point
	 */
	public void setTarget(Point2D target)
	{
		_target.setLocation(target);
	}

	@Override
	public double[] getState()
	{
		switch (_repr) {
		case JA:
			return getJAState();
		case JA_DIFF:
			return getJADiffState();
		case JA_DIFF_NORM:
			return getJADiffNormState();
		case DIFF:
			return getDiffState();
		case CARTESIAN:
			return getCartesianState();
		case CARTESIAN_DIFF:
			return getCartesianDiffState();
		case POLAR:
			return getPolarState();
		case POLAR_DIFF:
			return getPolarDiffState();
		case ALT4:
			return getAlt4State();
		case ALT2:
			return getAlt2State();
		default:
			throw new DebugException("Unsupported representation.");
		}
	}

	public double[] getCartesianState()
	{
		Point2D ep = _arm.endPoint();
		return new double[] { _target.getX(), _target.getY(), ep.getX(),
				ep.getY() };
	}

	public double[] getCartesianDiffState()
	{
		Point2D ep = _arm.endPoint();
		return new double[] { _target.getX() - ep.getX(),
				_target.getY() - ep.getY() };
	}

	public double[] getPolarState()
	{
		Point2D base = _arm.getBase();
		Point2D ep = _arm.endPoint();
		double targetD = _target.distance(base);
		double epD = ep.distance(base);
		double targetAngle = Math.atan2(_target.getY() - base.getY(),
				_target.getX() - base.getX());
		double epAngle = Math.atan2(ep.getY() - base.getY(), ep.getX()
				- base.getX());
		return new double[] { targetD, targetAngle, epD, epAngle };
	}

	public double[] getPolarDiffState()
	{
		Point2D base = _arm.getBase();
		Point2D ep = _arm.endPoint();
		double targetD = _target.distance(base);
		double epD = ep.distance(base);
		double targetAngle = Math.atan2(_target.getY() - base.getY(),
				_target.getX() - base.getX());
		double epAngle = Math.atan2(ep.getY() - base.getY(), ep.getX()
				- base.getX());
		return new double[] { targetD - epD, targetAngle - epAngle };
	}

	/**
	 * Returns a state description vector based on the Cartesian coordinates of
	 * the target, end-effector, and joint angles of the arm. The first four
	 * indices of the vector correspond to the target point and end-effector
	 * point. The remaining components provide the joint angles of the arm.
	 * 
	 * @return a state description vector
	 */
	public double[] getJAState()
	{
		double[] state = new double[4 + _arm.size()];
		Point2D ep = _arm.endPoint();
		state[TARGET_X] = _target.getX();
		state[TARGET_Y] = _target.getY();
		state[ENDPOINT_X] = ep.getX();
		state[ENDPOINT_Y] = ep.getY();

		double[] ja = _arm.getJointAngles();
		System.arraycopy(ja, 0, state, 4, ja.length);

		return state;
	}

	/**
	 * Returns a state description vector based on the signed difference between
	 * the Cartesian coordinates of the target and end-effector and the joint
	 * angles of the arm. The first two components are the signed differences
	 * between the target and end-effector points' X and Y coordinates. The
	 * remaining components describe the joint angles of the arm.
	 * 
	 * @return a state description vector
	 */
	public double[] getJADiffState()
	{
		double[] jaState = getJAState();
		double[] state = new double[jaState.length - 2];
		state[0] = jaState[TARGET_X] - jaState[ENDPOINT_X];
		state[1] = jaState[TARGET_Y] - jaState[ENDPOINT_Y];
		System.arraycopy(jaState, 4, state, 2, state.length - 2);
		return state;
	}

	/**
	 * Returns a state description vector based on the normalized signed
	 * difference between the Cartesian coordinates of the target and
	 * end-effector and the joint angles of the arm. The first two components
	 * are the normalized signed differences between the target and end-effector
	 * points' X and Y coordinates. The remaining components describe the joint
	 * angles of the arm.
	 * 
	 * @return a state description vector
	 */
	public double[] getJADiffNormState()
	{
		double[] jaDiffState = getJADiffState();
		double[] diff = { jaDiffState[0], jaDiffState[1] };
		diff = VectorOps.normalize(diff);
		jaDiffState[0] = diff[0];
		jaDiffState[1] = diff[1];
		return jaDiffState;
	}

	/**
	 * Returns a state description vector based on the signed difference between
	 * the Cartesian coordinates of the target and end-effector.
	 * 
	 * @return a state description vector
	 */
	public double[] getDiffState()
	{
		double[] jaDiffState = getJADiffState();
		return new double[] { jaDiffState[0], jaDiffState[1] };
	}

	/**
	 * Returns a state description vector based on the distance from the
	 * shoulder to target, distance from the shoulder to end-effector, angle
	 * between the x-axis, shoulder, and target, and the angle between the
	 * x-axis, shoulder, and end-effector.
	 * 
	 * @return a state description vector
	 */
	public double[] getAlt4State()
	{
		double[] state = new double[4];
		Arm arm = getArm();
		Point2D base = arm.getBase();
		Point2D ep = arm.endPoint();
		Point2D target = getTarget();

		state[0] = base.distance(ep);
		state[1] = base.distance(target);
		state[2] = Math.atan2(ep.getY() - base.getY(), ep.getX() - base.getX());
		state[3] = Math.atan2(target.getY() - base.getY(), target.getX()
				- base.getX());
		return state;
	}

	/**
	 * Returns a state description vector based on the difference between the
	 * distance from the shoulder to target and distance from the shoulder to
	 * end effector and the difference between the angle formed by the x-axis,
	 * shoulder, and target and the x-axis, shoulder, and end-effector.
	 * 
	 * @return a state description vector
	 */
	public double[] getAlt2State()
	{
		double[] alt4State = getAlt4State();
		return new double[] { alt4State[1] - alt4State[0],
				alt4State[3] - alt4State[2] };
	}

	@Override
	public void reset()
	{
		_poseHistory.clear();
		_arm.random();
		_poseHistory.add(_arm.getJointAngles());

		double[] randPose = new double[_arm.size()];
		for (int i = 0; i < randPose.length; i++) {
			randPose[i] = Random.uniform(_arm.getLinkLowerBound(i),
					_arm.getLinkUpperBound(i));
		}

		Point2D target = _arm.endPoint(randPose);
		_target.setLocation(target);
	}

	/**
	 * Allows the joint angles of the underlying arm to be set directly.
	 * 
	 * @param ja
	 *            a vector of joint angles
	 */
	public void setJointAngles(double[] ja)
	{
		_poseHistory.clear();
		_arm.setJointAngles(ja);
		_poseHistory.add(_arm.getJointAngles());
	}

	/**
	 * Returns the number of hinge joints of the underlying arm.
	 * 
	 * @return the number of hinge joints of the underlying arm.
	 */
	public int getArmSize()
	{
		return _arm.size();
	}

	/**
	 * Sets whether or not previous poses are rendered.
	 * 
	 * @param drawHistory
	 *            true to render previous arm poses; false only renders the
	 *            current pose
	 */
	public void setDrawHistory(boolean drawHistory)
	{
		_drawHistory = drawHistory;
	}

	/**
	 * Returns true if previous poses should be rendered; otherwise false (only
	 * the current pose is rendered).
	 * 
	 * @return true if previous poses should be rendered; otherwise false (only
	 *         the current pose is rendered)
	 */
	public boolean isDrawHistory()
	{
		return _drawHistory;
	}
	
	/**
	 * Constructs a task error map for an agent.
	 * @param agent a learning system
	 * @param sampleDensity the number of points to be checked for each joint
	 * @param numSamples the number of trials to run for each point
	 * @return a list containing pairs of target points and the final distance
	 */
	public List<Pair<Point2D,Double>> makeTaskErrorMap(LearningSystem<double[],double[]> agent, int sampleDensity, int numSamples)
	{
		List<Pair<Point2D,Double>> errorMap = new ArrayList<Pair<Point2D,Double>>();
		List<Point2D> validValues = Arm.generateReachableGrid(_arm, sampleDensity);
		
		int numSteps = 100;
		
		Point2D targetBackup = getTarget();
		for(Point2D target : validValues){
			TargetReachTaskObserver observer = new TargetReachTaskObserver();
			for(int episode = 0; episode < numSamples; episode++){
				reset();
				setTarget(target);
				Task.runEpisode(this, agent, numSteps, false, observer, false);
			}
			errorMap.add(new Pair<Point2D,Double>(target, Statistics.mean(observer.getFinalDistances())));
		}
		setTarget(targetBackup);
		return errorMap;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		AffineTransform xform = new AffineTransform();
		xform.translate(width / 2, height / 2);
		double scale = Math.min(width, height) / 5.0;
		xform.scale(scale, -scale);
		g.transform(xform);

		g.setStroke(new BasicStroke((float) (1 / scale)));

		if (isDrawHistory()) {
			int cinc = 255 / _poseHistory.size();
			Point2D currEP = _arm.endPoint(_poseHistory.get(0));

			Path2D path = new Path2D.Double();
			path.moveTo(currEP.getX(), currEP.getY());

			for (int i = 0; i < _poseHistory.size(); i++) {
				int cval = 255 - (i * cinc);
				drawPose(g, _arm, _poseHistory.get(i), new Color(cval, cval,
						cval));
				currEP = _arm.endPoint(_poseHistory.get(i));
				path.lineTo(currEP.getX(), currEP.getY());
			}
			g.setColor(Color.BLUE);
			g.draw(path);
		} else {
			drawPose(g, _arm, _arm.getJointAngles(), Color.BLACK);
		}

		double twidth = 0.1;
		double theight = 0.1;
		double x = (_target.getX() - _arm.getBase().getX()) - (twidth / 2);
		double y = (_target.getY() - _arm.getBase().getY()) - (theight / 2);
		Ellipse2D target = new Ellipse2D.Double(x, y, twidth, theight);
		g.setColor(Color.RED);
		g.fill(target);
		g.setColor(Color.BLACK);
		g.draw(target);
	}

	/**
	 * Draws an arm with a specified pose and color.
	 * 
	 * @param g
	 *            a graphics 2d object
	 * @param arm
	 *            an arm
	 * @param pose
	 *            a pose
	 * @param color
	 *            a color
	 */
	private void drawPose(Graphics2D g, Arm arm, double[] pose, Color color)
	{
		Point2D base = arm.getBase();
		double x = base.getX();
		double y = base.getY();
		double theta = 0;
		g.setColor(color);

		for (int i = 0; i < arm.size(); i++) {
			double length = arm.getLinkLength(i);
			theta += pose[i];
			double newX = x + length * Math.cos(theta);
			double newY = y + length * Math.sin(theta);
			Line2D line = new Line2D.Double(x, y, newX, newY);
			g.draw(line);
			x = newX;
			y = newY;
		}
	}

}
