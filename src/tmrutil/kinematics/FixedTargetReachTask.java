package tmrutil.kinematics;

import java.awt.geom.Point2D;
import tmrutil.kinematics.TargetReachTask.Representation;
import tmrutil.learning.rl.ReinforcementSignal;

/**
 * Implements the target reaching task where the target is a fixed point instead
 * of randomly positioned at the beginning of each episode.
 * 
 * @author Timothy A. Mann
 * 
 */
public class FixedTargetReachTask extends TargetReachTask
{
	
	public FixedTargetReachTask(Arm arm, Representation representation)
	{
		super(arm, representation);
	}

	public FixedTargetReachTask(Arm arm, Representation representation,
			ReinforcementSignal3<double[], double[]> rsignal)
	{
		super(arm, representation, rsignal);
	}

	@Override
	public void reset()
	{
		Point2D backupTarget = getTarget();
		super.reset();
		setTarget(backupTarget);
	}
}
