package tmrutil.kinematics;

import tmrutil.math.MatrixOps;
import tmrutil.math.VectorOps;

/**
 * Represents a 2-link planar, dynamic arm that is controlled in muscle space.
 * @author Timothy A. Mann
 */
public class DynamicArm
{
	private static final int NUM_JOINTS = 2;
	private static final double DEFAULT_TIME_STEP = 0.01;
	
	/**
	 * The current time of the simulation in seconds.
	 */
	private double _time;
	/** The duration of a time step. */
	private double _timeStep;
	
	/** The upper link of this arm. */
	private DynamicLink2D _upper;
	/** The lower link of this arm. */
	private DynamicLink2D _lower;
	
	/** The joint angles of the shoulder (index 0) and elbow (index 1). */
	private double[] _thetaP;
	/** The joint angle velocity of the shoulder (index 0) and elbow (index 1). */
	private double[] _thetaV;
	/** The joint angle acceleration of the shoulder (index 0) and elbow (index 1). */
	private double[] _thetaA;
	
	/** The moment of inertia matrix. */
	private double[][] _I;
	/** The centrifugal and Coriollis matrix. */
	private double[][] _C;
	
	/**
	 * Moment arms matrix.
	 */
	private double[][] _A;
	
	/** The current muscle lengths. */
	private double[] _L;
	/** The current muscle rates of change. */
	private double[] _LDot;
	/** The rest lengths of each muscle when the applied command is 0. */
	private double[] _L0;
	/** Determines muscle commands' proportion to change in muscle rest length. */
	private double[] _R;
	/** Each muscle's spring constant. */
	private double[] _Q;
	/** Each muscle's nonlinear damping constant. */
	private double[] _D;
	
	/**
	 * Constructs a 2-link planar, dynamic arm.
	 * @param upper the upper link of the constructed arm
	 * @param lower the lower link of the constructed arm
	 */
	public DynamicArm(DynamicLink2D upper, DynamicLink2D lower)
	{
		_time = 0;
		_timeStep = DEFAULT_TIME_STEP;
		
		_upper = upper;
		_lower = lower;
		
		_thetaP = new double[NUM_JOINTS];
		_thetaV = new double[NUM_JOINTS];
		_thetaA = new double[NUM_JOINTS];
		
		_I = new double[2][2];
		_C = new double[2][2];
		
		_A = new double[][]{{1, -1, 0, 0, 1, -1},{0, 0, 1, -1, 1, -1}};
	}
	
	private void step(double[] u)
	{
		_time += _timeStep;
		double[] torque = muscleTorque(u);
		
	}
	
	private double[] torque()
	{
		double[] term1 = MatrixOps.multiply(I(_thetaP), _thetaA);
		double[] term2 = MatrixOps.multiply(C(_thetaP, _thetaV), _thetaV);
		return VectorOps.add(term1, term2);
	}
	
	/**
	 * Constructs the moment of inertia matrix.
	 * @param thetaP the joint angles of this arm
	 * @return the moment of inertia matrix
	 */
	private double[][] I(double[] thetaP)
	{
		double I1 = _upper.getInertia();
		double I2 = _lower.getInertia();
		double M2 = _lower.getMass();
		double L1 = _upper.getLength();
		double LG2 = _lower.getCenterOfGravity();
		
		double theta2 = thetaP[1];
		
		_I[0][0] = I1 + I2 + M2 * (L1 * L1) + 2 * M2 * L1 * LG2 * Math.cos(theta2); _I[0][1] = I2 + M2 * (L1 * L1) * LG2 * Math.cos(theta2);
		_I[1][0] = I2 + M2 * (L1 * L1) * LG2 * Math.cos(theta2); _I[1][1] = I2;
		
		return _I;
	}
	
	/**
	 * Constructs the centrifugal and Coriollis matrix.
	 * @param thetaP the joint angles of this arm
	 * @param thetaV the angular velocities of the joint angles of this arm
	 * @return a matrix which represents the centrifugal and Coriollis effects on this arm
	 */
	private double[][] C(double[] thetaP, double[] thetaV)
	{
		double M2 = _lower.getMass();
		double L1 = _upper.getLength();
		double LG2 = _lower.getCenterOfGravity();
		double D1 = _upper.getJointFriction();
		double D2 = _lower.getJointFriction();
		
		double theta2 = thetaP[1];
		
		double thetaV1 = thetaV[0];
		double thetaV2 = thetaV[1];
		
		double pre = M2 * L1 * LG2 * Math.sin(theta2);
		
		_C[0][0] = pre * -2 * thetaV2 - D1; _C[0][1] = pre * -thetaV2;
		_C[1][0] = pre * thetaV1; _C[1][1] = - D2;
		
		return _C;
	}
	
	/**
	 * A threshold linear function used for modeling muscle forces.
	 * @param val a scalar value
	 * @return if <code>val >= 0</code>, then <code>val</code>; otherwise 0
	 */
	private double f(double val)
	{
		if(val < 0){
			return 0;
		}else{
			return val;
		}
	}
	
	/**
	 * Computes the tension produced by a single muscle in this dynamic arm.
	 * @param l the current muscle length
	 * @param lDot the rate of change of the muscle
	 * @param u the command applied to the muscle
	 * @param r 
	 * @param l0 the rest length of the muscle if the applied command is 0
	 * @param Q a spring constant
	 * @param D gain on the nonlinear damping factor
	 * @return torque produced by a muscle
	 */
	private double muscleTension(double l, double lDot, double u, double r, double l0, double Q, double D)
	{
		double lr = l0 - r * u;
		return f(Q * (l - lr) + D * Math.pow(lDot, 1.0/5));
	}
	
	private double[] muscleTorque(double[] u)
	{
		double[] uhat = new double[u.length];
		for(int i=0;i<u.length;i++){
			uhat[i] = muscleTension(_L[i], _LDot[i], u[i], _R[i], _L0[i], _Q[i], _D[i]);
		}
		return MatrixOps.multiply(_A, uhat);
	}
}
