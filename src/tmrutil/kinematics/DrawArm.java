package tmrutil.kinematics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import tmrutil.graphics.Drawable;
import tmrutil.graphics.DrawableComponent;

public class DrawArm implements Drawable
{
	private Arm _arm;
	private List<double[]> _jointAngleSequence;
	private Point2D _target;
	private boolean _drawHistory;
	private boolean _drawLinksAsRectangles;

	public DrawArm(Arm arm, List<double[]> jointAngleSequence, Point2D target,
			boolean drawHistory, boolean drawLinksAsRectangles)
	{
		_arm = arm;
		_jointAngleSequence = jointAngleSequence;
		_target = target;
		_drawHistory = drawHistory;
		_drawLinksAsRectangles = drawLinksAsRectangles;
	}

	/**
	 * If draw history is on, then the entire sequence of arm poses will be
	 * rendered. If draw history is false, then only the last pose in the
	 * sequence will be drawn.
	 * 
	 * @return true if draw history is on; otherwise false
	 */
	public boolean isDrawHistoryOn()
	{
		return _drawHistory;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		AffineTransform xform = new AffineTransform();
		xform.translate(width / 2, height / 2);
		double scale = Math.min(width, height) / 5.0;
		xform.scale(scale, -scale);
		g.transform(xform);

		g.setStroke(new BasicStroke((float) (1 / scale)));

		if (_drawHistory) {
			int cinc = 255 / _jointAngleSequence.size();
			Point2D currEP = _arm.endPoint(_jointAngleSequence.get(0));

			Path2D path = new Path2D.Double();
			path.moveTo(currEP.getX(), currEP.getY());

			for (int i = 0; i < _jointAngleSequence.size(); i++) {
				int cval = 255 - (i * cinc);
				drawPose(g, _arm, _jointAngleSequence.get(i), new Color(cval,
						cval, cval));
				currEP = _arm.endPoint(_jointAngleSequence.get(i));
				path.lineTo(currEP.getX(), currEP.getY());
			}
			g.setColor(Color.BLUE);
			g.draw(path);
		} else {
			drawPose(g, _arm,
					_jointAngleSequence.get(_jointAngleSequence.size() - 1),
					Color.BLACK);
		}

		double twidth = 0.1;
		double theight = 0.1;
		double x = (_target.getX() - _arm.getBase().getX()) - (twidth / 2);
		double y = (_target.getY() - _arm.getBase().getY()) - (theight / 2);
		Ellipse2D target = new Ellipse2D.Double(x, y, twidth, theight);
		g.setColor(Color.RED);
		g.fill(target);
		g.setColor(Color.BLACK);
		g.draw(target);
	}

	/**
	 * Draws an arm with a specified pose and color.
	 * 
	 * @param g
	 *            a graphics 2d object
	 * @param arm
	 *            an arm
	 * @param pose
	 *            a pose
	 * @param color
	 *            a color
	 */
	private void drawPose(Graphics2D g, Arm arm, double[] pose, Color color)
	{
		Point2D base = arm.getBase();
		double x = base.getX();
		double y = base.getY();
		double theta = 0;
		g.setColor(color);

		double rheight = _arm.getMaxArmLength() / 20;

			for (int i = 0; i < arm.size(); i++) {
			double length = arm.getLinkLength(i);
			theta += pose[i];
			double newX = x + length * Math.cos(theta);
			double newY = y + length * Math.sin(theta);
			if (_drawLinksAsRectangles) {
				Rectangle2D rect = new Rectangle2D.Double(0,0,length,rheight);
				AffineTransform oldXform = g.getTransform();
				AffineTransform newXform = new AffineTransform();
				newXform.concatenate(oldXform);
				newXform.translate(x, y - rheight/2);
				newXform.rotate(theta, 0, +rheight/2);
				
				g.setTransform(newXform);
				g.draw(rect);
				g.setTransform(oldXform);
			} else {
				Line2D line = new Line2D.Double(x, y, newX, newY);
				g.draw(line);
			}
			x = newX;
			y = newY;
		}
	}
	
	public static void main(String[] args)
	{
		Arm arm = new Arm();
		arm.addLink(1, -Math.PI/2, Math.PI/2);
		arm.addLink(1, -Math.PI/2, Math.PI/2);
		arm.addLink(1, -Math.PI/2, Math.PI/2);
		arm.addLink(1, -Math.PI/2, Math.PI/2);
		
		arm.random();
		double[] jas = arm.getJointAngles();
		List<double[]> jaSequence = new ArrayList<double[]>();
		jaSequence.add(jas);
		
		Point2D target = new Point2D.Double(0.6, 0.6);
		
		DrawArm darm = new DrawArm(arm, jaSequence, target, false, true);
		
		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		DrawableComponent dc = new DrawableComponent(darm);
		frame.add(dc);
		frame.setVisible(true);
	}

}
