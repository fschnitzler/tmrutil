package tmrutil.kinematics;

import java.util.ArrayList;
import java.util.List;
import tmrutil.learning.rl.TaskObserver;
import tmrutil.learning.rl.tasks.SumOfReinforcementsTaskObserver;

/**
 * An observer for collecting statistics while a target reaching task is running.
 * @author Timothy A. Mann
 *
 */
public class TargetReachTaskObserver extends
		SumOfReinforcementsTaskObserver<double[], double[], TargetReachTask>
{
	private List<Double> _finalDistances;

	public TargetReachTaskObserver()
	{
		super();
		_finalDistances = new ArrayList<Double>();
	}

	@Override
	public void observeEpisodeEnd(TargetReachTask task)
	{
		super.observeEpisodeEnd(task);
		_finalDistances.add(task.targetDistance());
	}

	@Override
	public void reset()
	{
		super.reset();
		_finalDistances.clear();
	}

	/**
	 * A list of the distance between the target and end-effector at the end of
	 * each observed episode.
	 * 
	 * @return a list of final (end-effector, target) distances
	 */
	public List<Double> getFinalDistances()
	{
		return new ArrayList<Double>(_finalDistances);
	}

}
