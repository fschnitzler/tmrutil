package tmrutil.kinematics;

import java.awt.Shape;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import tmrutil.math.VectorOps;
import tmrutil.stats.Random;
import tmrutil.util.Interval;
import tmrutil.util.Pair;

/**
 * A simulated arm built from a chain of links connected by hinge joints. Each
 * link is connected to the previous by a hinge joint. The first link is
 * connected to the environment at a base point by a hinge joint. The total
 * number of links is the same as the number of joints.
 * 
 * @author Timothy A. Mann
 * 
 */
public class Arm
{
	/** The location of this arm's base. */
	private Point2D _base;
	/** The links of this arm. */
	private List<Link2D> _links;
	/** The joint angles of this arm's links. */
	private List<Double> _jointAngles;

	/**
	 * Constructs an empty arm.
	 */
	public Arm()
	{
		_base = new Point2D.Double();
		_links = new ArrayList<Link2D>();
		_jointAngles = new ArrayList<Double>();
	}

	/**
	 * Copy constructor. Makes a deep copy of all arm instance variables.
	 * 
	 * @param arm
	 *            an arm instance
	 */
	public Arm(Arm arm)
	{
		_base = new Point2D.Double(arm._base.getX(), arm._base.getY());
		_links = new ArrayList<Link2D>();
		_jointAngles = new ArrayList<Double>();

		for (int i = 0; i < arm._links.size(); i++) {
			_links.add(new Link2D(arm._links.get(i)));
			_jointAngles.add(arm._jointAngles.get(i).doubleValue());
		}
	}

	/**
	 * Adds a link to the end of the arm and sets its joint angle to the lower
	 * bound.
	 * 
	 * @param length
	 *            the length of the link
	 * @param lowerBound
	 *            the lower bound on the joint angle
	 * @param upperBound
	 *            the upper bound on the joint angle
	 * @throws IllegalArgumentException
	 *             if the lower bound is greater than the upper bound or either
	 *             the lower bound or upper bound are outside of [-pi, pi].
	 */
	public void addLink(double length, double lowerBound, double upperBound)
			throws IllegalArgumentException
	{
		if (lowerBound > upperBound) {
			throw new IllegalArgumentException(
					"Lower bound cannot be greater than upper bound");
		}
		if (lowerBound < -Math.PI || lowerBound > Math.PI
				|| upperBound < -Math.PI || upperBound > Math.PI) {
			throw new IllegalArgumentException(
					"Upper and lower bound must be in the range [-pi, pi]");
		}
		_links.add(new Link2D(length, new Interval(lowerBound, upperBound)));
		_jointAngles.add(lowerBound);
	}

	/**
	 * Returns a copy of the joint angles for each link in the arm.
	 * 
	 * @return a vector of joint angles
	 */
	public double[] getJointAngles()
	{
		double[] ja = new double[size()];
		for (int i = 0; i < ja.length; i++) {
			ja[i] = _jointAngles.get(i);
		}
		return ja;
	}

	/**
	 * Returns a copy of the normalized joint angle for each link in the arm. A
	 * normalized joint angle is a value in the interval [0,1] where 0
	 * represents the joint angle being at the lower bound and 1 represents the
	 * joint angle being at the upper bound.
	 * 
	 * @return a vector of normalized joint angles
	 */
	public double[] getNormalizedJointAngles()
	{
		double[] ja = new double[size()];
		for (int i = 0; i < ja.length; i++) {
			Link2D link = _links.get(i);
			double lower = link.getLowerBound();
			double upper = link.getUpperBound();
			double diff = upper - lower;
			ja[i] = (_jointAngles.get(i) - lower) / diff;
		}
		return ja;
	}

	/**
	 * Sets the base point for this arm.
	 * 
	 * @param base
	 *            a point
	 */
	public void setBase(Point2D base)
	{
		if (base == null) {
			throw new NullPointerException(
					"An arm cannot have a null base point.");
		}
		_base = new Point2D.Double(base.getX(), base.getY());
	}

	/**
	 * Returns a copy of the location of the base of this arm.
	 * 
	 * @return a point
	 */
	public Point2D getBase()
	{
		return new Point2D.Double(_base.getX(), _base.getY());
	}

	/**
	 * Returns the end point of this arm given the current set of joint angles.
	 * 
	 * @return the end point of this arm
	 */
	public Point2D endPoint()
	{
		double x = _base.getX();
		double y = _base.getY();

		double theta = 0;
		for (int i = 0; i < _jointAngles.size(); i++) {
			theta += _jointAngles.get(i);
			x += _links.get(i).getLength() * Math.cos(theta);
			y += _links.get(i).getLength() * Math.sin(theta);
		}
		return new Point2D.Double(x, y);
	}

	/**
	 * Returns the end point of this arm given a set of joint angles.
	 * 
	 * @param jointAngles
	 *            a vector of joint angles
	 * @return the end point of this arm
	 */
	public Point2D endPoint(double[] jointAngles)
	{
		double x = _base.getX();
		double y = _base.getY();

		double theta = 0;
		for (int i = 0; i < jointAngles.length; i++) {
			theta += jointAngles[i];
			x += _links.get(i).getLength() * Math.cos(theta);
			y += _links.get(i).getLength() * Math.sin(theta);
		}
		return new Point2D.Double(x, y);
	}

	/**
	 * Returns the end point of a specified link of this arm.
	 * 
	 * @param i
	 *            the index of the ith link
	 * @return the end point of the ith link
	 */
	public Point2D getLinkEndPoint(int i)
	{
		double x = _base.getX();
		double y = _base.getY();

		double theta = 0;
		for (int j = 0; j <= i; j++) {
			theta += _jointAngles.get(j);
			x += _links.get(j).getLength() * Math.cos(theta);
			y += _links.get(j).getLength() * Math.sin(theta);
		}
		return new Point2D.Double(x, y);
	}

	/**
	 * Resets the joint angles in the arm to the lower bound for each link.
	 */
	public void reset()
	{
		for (int i = 0; i < _jointAngles.size(); i++) {
			_jointAngles.set(i, _links.get(i).getLowerBound());
		}
	}

	/**
	 * Resets the joint angles of the arm to random (but valid) values for each
	 * link.
	 */
	public void random()
	{
		for (int i = 0; i < _jointAngles.size(); i++) {
			_jointAngles.set(i, Random.uniform(_links.get(i).getLowerBound(),
					_links.get(i).getUpperBound()));
		}
	}

	/**
	 * Returns the number of links in this arm.
	 * 
	 * @return the number of links in this arm
	 */
	public int size()
	{
		return _links.size();
	}

	/**
	 * Returns the length of the ith link of this arm.
	 * 
	 * @param i
	 *            the index of a link
	 * @return the length of the ith link of this arm
	 */
	public double getLinkLength(int i)
	{
		return _links.get(i).getLength();
	}

	/**
	 * Returns the maximum possible length from its base to its end-point if the
	 * arm is completely straight.
	 * 
	 * @return maximum arm length from base to end-point
	 */
	public double getMaxArmLength()
	{
		double sum = 0;
		for (int i = 0; i < _links.size(); i++) {
			sum += _links.get(i).getLength();
		}
		return sum;
	}

	/**
	 * Returns the upper bound in radians for the ith link's joint.
	 * 
	 * @param i
	 *            the index of a link
	 * @return the upper bound in radians for the ith link's joint
	 */
	public double getLinkUpperBound(int i)
	{
		return _links.get(i).getUpperBound();
	}

	/**
	 * Returns the lower bound in radians for the ith link's joint.
	 * 
	 * @param i
	 *            the index of a link
	 * @return the lower bound in radians for the ith link's joint
	 */
	public double getLinkLowerBound(int i)
	{
		return _links.get(i).getLowerBound();
	}

	/**
	 * Returns the joint angle of the ith joint of this arm.
	 * 
	 * @param i
	 *            the index of a joint
	 * @return the joint angle of the ith joint of this arm
	 */
	public double getJointAngle(int i)
	{
		return _jointAngles.get(i);
	}

	/**
	 * Sets all of the joint angles in this arm to the joint angles in a vector.
	 * 
	 * @param jointAngles
	 *            a vector of joint angles
	 * @throws IllegalArgumentException
	 *             if the size of the joint angle vector does not match the
	 *             number of joints of this arm
	 */
	public void setJointAngles(double[] jointAngles)
			throws IllegalArgumentException
	{
		if (jointAngles.length != _jointAngles.size()) {
			throw new IllegalArgumentException(
					"Size of joint angle vector does not match the number of joints.");
		}
		for (int i = 0; i < jointAngles.length; i++) {
			double theta = jointAngles[i];
			Link2D link = _links.get(i);
			if (theta < link.getLowerBound()) {
				theta = link.getLowerBound();
			}
			if (theta > link.getUpperBound()) {
				theta = link.getUpperBound();
			}
			_jointAngles.set(i, theta);
		}
	}

	/**
	 * Sets the bounds of a joint in the arm.
	 * 
	 * @param index
	 *            the index of a joint
	 * @param bounds
	 *            the desired bounds for the joint
	 */
	public void setJointBounds(int index, Interval bounds)
	{
		Link2D link = _links.get(index);
		link.setBounds(bounds);
	}

	/**
	 * Gets an array of intervals where each interval in the array specifies the
	 * boundaries of each joint angle.
	 * 
	 * @return an array of joint angle boundaries
	 */
	public Interval[] getJointBounds()
	{
		Interval[] bounds = new Interval[size()];
		for (int i = 0; i < _links.size(); i++) {
			Link2D link = _links.get(i);
			bounds[i] = new Interval(link.getLowerBound(), link.getUpperBound());
		}
		return bounds;
	}

	/**
	 * Returns a reference to the list of links that make up this kinematic
	 * chain.
	 * 
	 * @return a list of links
	 */
	private List<Link2D> getLinks()
	{
		return _links;
	}

	/**
	 * Constructs an arm for the pre-birth condition.
	 * 
	 * @return an arm
	 */
	public static final Arm buildPrenatalArm()
	{
		double upperArmLength = 1;
		double forearmLength = 1;
		double link1LowerBound = Math.toRadians(-50); // (-70); // -Math.PI / 2;
		double link1UpperBound = Math.toRadians(25); // (45); // -Math.PI / 4;
		double link2LowerBound = Math.toRadians(90); // (70); // 3 * Math.PI /
		// 5;
		double link2UpperBound = Math.toRadians(145); // (145); // 4 * Math.PI /
		// 5;

		Arm arm = new Arm();
		arm.addLink(upperArmLength, link1LowerBound, link1UpperBound);
		arm.addLink(forearmLength, link2LowerBound, link2UpperBound);
		return arm;
	}

	public static final double POSTNATAL_SHOULDER_MIN = Math.toRadians(-140);
	public static final double POSTNATAL_SHOULDER_MAX = Math.toRadians(90);
	public static final double POSTNATAL_ELBOW_MIN = Math.toRadians(0);
	public static final double POSTNATAL_ELBOW_MAX = Math.toRadians(145);

	/**
	 * Constructs an arm for the post-birth condition.
	 * 
	 * @return an arm
	 */
	public static final Arm buildPostnatalArm()
	{
		double upperArmLength = 1;
		double forearmLength = 1;
		double link1LowerBound = POSTNATAL_SHOULDER_MIN; // -Math.PI / 2;
		double link1UpperBound = POSTNATAL_SHOULDER_MAX; // Math.PI / 5;
		double link2LowerBound = POSTNATAL_ELBOW_MIN; // 0;
		double link2UpperBound = POSTNATAL_ELBOW_MAX; // 4 * Math.PI / 5;

		Arm arm = new Arm();
		arm.addLink(upperArmLength, link1LowerBound, link1UpperBound);
		arm.addLink(forearmLength, link2LowerBound, link2UpperBound);
		return arm;
	}

	/**
	 * Constructs a multi-link arm with a maximum length of 2.0 units. The
	 * number of links is equal to <code>numLinks</code>. The upper and lower
	 * bounds on each joint are set to -Pi/2 and Pi/2, respectively.
	 * 
	 * @param numLinks
	 *            the number of links in the tentacle (must be positive)
	 * @return a tentacle-like arm
	 */
	public static final Arm buildTentacle(int numLinks)
	{
		return buildTentacle(numLinks, -Math.PI / 2, Math.PI / 2);
	}

	/**
	 * Constructs a multi-link arm with a maximum length of 2.0 units. The
	 * number of links is equal to <code>numLinks</code>. The upper and lower
	 * bounds are the same for each link's joint.
	 * 
	 * @param numLinks
	 *            the number of links in the tentacle (must be positive)
	 * @param lowerLimit
	 *            the lower bound (in radians) on the tentacle's joint angles
	 * @param upperLimit
	 *            the upper limit (in radians) on the tentacle's joint angles
	 * @return a tentacle-like arm
	 */
	public static final Arm buildTentacle(int numLinks, double lowerLimit,
			double upperLimit)
	{
		if (numLinks < 1) {
			throw new IllegalArgumentException(
					"The number of links must be greater than 0.");
		}

		double linkLength = 2.0 / numLinks;
		double linkLowerBound = lowerLimit;
		double linkUpperBound = upperLimit;

		Arm arm = new Arm();

		for (int i = 0; i < numLinks; i++) {
			arm.addLink(linkLength, linkLowerBound, linkUpperBound);
		}

		return arm;
	}

	/**
	 * Constructs a new arm with the same properties as the one specified except
	 * that the links are modified by the specified values in
	 * <code>linkDeltas</code>.
	 * 
	 * @param arm
	 *            an arm
	 * @param linkDeltas
	 *            an array of lengths to modify each link of the specified arm
	 *            by
	 * @return a new arm with link lengths modified by the values from
	 *         <code>linkDeltas</code>
	 * @throws IllegalArgumentException
	 *             if <code>arm.size() != linkDeltas.length</code>
	 */
	public static final Arm changeArmLinks(Arm arm, double[] linkDeltas)
			throws IllegalArgumentException
	{
		if (arm.size() != linkDeltas.length) {
			throw new IllegalArgumentException(
					"The number of elements in linkDeltas ("
							+ linkDeltas.length
							+ ") does not match the number of links in the specified arm ("
							+ arm.size() + ").");
		}
		Arm deltaArm = new Arm(arm);
		for (int i = 0; i < deltaArm.size(); i++) {
			Link2D deltaLink = deltaArm._links.get(i);
			deltaLink.setLength(deltaLink.getLength() + linkDeltas[i]);
		}
		return deltaArm;
	}

	/**
	 * Constructs a list of reachable points by an arm given its bounds
	 * 
	 * @param arm
	 *            an arm instance
	 * @param ns
	 *            number of samples to at each joint
	 * @return a list of reachable points
	 */
	public static final List<Point2D> generateReachableGrid(Arm arm, int ns)
	{
		int numThetas = arm.size();
		List<Point2D> reachableGrid = new ArrayList<Point2D>();
		int[] currentNs = new int[numThetas];

		List<Link2D> links = arm.getLinks();
		double[] thetas = new double[numThetas];
		for (int i = 0; i < thetas.length; i++) {
			thetas[i] = links.get(i).getLowerBound();
		}

		boolean finished = false;
		while (!finished) {
			for (int i = numThetas - 1; i >= 0; i--) {
				int cns = currentNs[i];
				Link2D link = links.get(i);
				if (cns <= ns) {
					double inc = (link.getUpperBound() - link.getLowerBound())
							/ ns;
					thetas[i] = (cns * inc) + link.getLowerBound();
					currentNs[i] = cns + 1;
					break;
				} else {
					thetas[i] = link.getLowerBound();
					currentNs[i] = 0;
					if (i == 0) {
						finished = true;
					}
				}
			}
			reachableGrid.add(arm.endPoint(thetas));
		}

		return reachableGrid;
	}

	/**
	 * Attempts to determine whether or not an arm collides with a specified
	 * point. Collisions are detected by dividing the arms total motion into
	 * <code>inc</code> motions and checking whether the point is contained in
	 * polygons constructed from the position of an arm link over a single small
	 * interval.
	 * 
	 * @param base
	 *            the position of the base of the arm
	 * @param linkLengths
	 *            the lengths of each arm link
	 * @param thetaStart
	 *            the initial angles of the arms joints
	 * @param thetaEnd
	 *            the final angles of the arms joints
	 * @param point
	 *            the point to check for collision with
	 * @param inc
	 *            the number of intermediate steps to check between the initial
	 *            and final arm joint angles
	 * @return true if a collision was detected; otherwise false
	 */
	public static final Pair<Boolean, List<Shape>> collisionDetected(
			Point2D base, double[] linkLengths, double[] thetaStart,
			double[] thetaEnd, Point2D point, int inc)
	{
		boolean collision = false;
		List<Shape> shapes = new ArrayList<Shape>();
		point.setLocation(point.getX() - base.getX(), point.getY()
				- base.getY());
		for (int i = 1; i <= inc; i++) {
			Point2D base1 = new Point2D.Double(0.0, 0.0);
			Point2D base2 = new Point2D.Double(0.0, 0.0);

			double theta1Base = 0;
			double theta2Base = 0;
			for (int linkIndex = 0; linkIndex < thetaStart.length; linkIndex++) {
				double thetaInc = (thetaEnd[linkIndex] - thetaStart[linkIndex])
						/ inc;
				double theta1 = theta1Base + thetaStart[linkIndex]
						+ ((i - 1) * thetaInc);
				double theta2 = theta2Base + thetaStart[linkIndex]
						+ (i * thetaInc);

				double x11 = base1.getX();
				double y11 = base1.getY();
				double x12 = base1.getX()
						+ (linkLengths[linkIndex] * Math.sin(theta1));
				double y12 = base1.getY()
						+ (linkLengths[linkIndex] * -Math.cos(theta1));

				double x21 = base2.getX();
				double y21 = base2.getY();
				double x22 = base2.getX()
						+ (linkLengths[linkIndex] * Math.sin(theta2));
				double y22 = base2.getY()
						+ (linkLengths[linkIndex] * -Math.cos(theta2));

				Path2D path = new Path2D.Double();
				path.moveTo(x11, y11);
				path.lineTo(x12, y12);
				path.lineTo(x22, y22);
				path.lineTo(x21, y21);
				path.closePath();
				shapes.add(path);

				if (path.contains(point)) {
					collision = true;
				}

				base1.setLocation(x12, y12);
				base2.setLocation(x22, y22);
				theta1Base = theta1;
				theta2Base = theta2;
			}
		}

		return new Pair<Boolean, List<Shape>>(collision, shapes);
	}

	/**
	 * Generates actions for an agent to control an arm with multiple joints.
	 * 
	 * @param actionMagnitude
	 *            the magnitude of the actions
	 * @param numArmJoints
	 *            the number of joints in the arm
	 * @return a collection of action vectors
	 */
	public static final Collection<double[]> generateActions(
			double actionMagnitude, int numArmJoints)
	{
		List<double[]> actions = new ArrayList<double[]>(2 * numArmJoints);
		for (int r = 0; r < 2 * numArmJoints; r++) {
			double[] action = new double[numArmJoints];
			for (int c = 0; c < numArmJoints; c++) {
				action[c] = 0;
				if (r / 2 == c) {
					if (r % 2 == 0) {
						action[c] = actionMagnitude;
					} else {
						action[c] = -actionMagnitude;
					}
				}
			}
			actions.add(action);
		}
		return actions;
	}

	/**
	 * Generate actions for an agent to control an arm with multiple joints.
	 * 
	 * @param actionMagnitude
	 *            the magnitude of the actions
	 * @param numArmJoints
	 *            the number of joints in the arm
	 * @return a collection of action vectors
	 */
	public static final Collection<double[]> generateActionsWithCombinations(
			double actionMagnitude, int numArmJoints)
	{
		double[] options = { -actionMagnitude, 0, actionMagnitude };
		int numActions = (int) Math.pow(options.length, numArmJoints);
		List<double[]> actions = new ArrayList<double[]>(numActions);

		int[] counters = new int[numArmJoints];
		double[] action = VectorOps.multiply(options[0],
				VectorOps.ones(numArmJoints));
		for (int i = 0; i < numActions; i++) {
			actions.add(Arrays.copyOf(action, action.length));

			if (i < numActions - 1) {
				int cindex = 0;
				counters[cindex]++;
				while (counters[cindex] == options.length) {
					counters[cindex] = 0;
					cindex++;
					counters[cindex]++;
				}

				for (int j = 0; j < numArmJoints; j++) {
					action[j] = options[counters[j]];
				}
			}
		}

		return actions;
	}
}
