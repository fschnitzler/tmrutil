/*
 * Data.java
 */

/* Package */
package tmrutil.graphics;

/* Imports */
import java.awt.Color;
import java.util.List;

/**
 * Represents a data set to be displayed graphically.
 */
public class DataProperties
{
	private static final int DEFAULT_WIDTH = 1;
	
	public enum DisplayStyle {
		LINE, POINT, DOTTED, DASHED, TRIANGLE_LINE, SQUARE_LINE, CIRCLE_LINE
	};

	/** A description of the data. */
	private String _description;

	/** The style to use in plotting the points. */
	private DisplayStyle _style;

	/** The width of lines. */
	private int _lineWidth;
	/** The width of points. */
	private int _pointWidth;
	
	/** The color to plot the data with. */
	private Color _color;

	/** The actual data to be plotted. */
	private List<double[]> _data;

	/**
	 * Constructs a data set.
	 * @param data data vectors
	 * @param style the style to display the data with
	 * @param color the color to display the data with
	 * @param description a description of the data
	 */
	public DataProperties(List<double[]> data, DisplayStyle style, Color color,
			String description)
	{
		_data = data;
		_style = style;
		_color = color;
		_description = description;
		_lineWidth = DEFAULT_WIDTH;
		_pointWidth = DEFAULT_WIDTH;
	}
	
	/**
	 * Returns the color that will be used to paint the data set in a plot.
	 * 
	 * @return a color
	 */
	public Color getColor()
	{
		return _color;
	}

	/**
	 * Sets the color that will be used to paint the data set in a plot.
	 * 
	 * @param color
	 *            a color
	 */
	public void setColor(Color color)
	{
		_color = color;
	}

	/**
	 * Returns the style used to display the data.
	 * @return the style used to display the data
	 */
	public DisplayStyle getStyle()
	{
		return _style;
	}

	/**
	 * Sets the style used to display the data.
	 * @param style a style
	 */
	public void setStyle(DisplayStyle style)
	{
		_style = style;
	}

	public List<double[]> getData()
	{
		return _data;
	}

	public void setData(List<double[]> data)
	{
		_data = data;
	}
	
	/**
	 * Returns the line width.
	 * @return the line width
	 */
	public int getLineWidth()
	{
		return _lineWidth;
	}
	
	/**
	 * Sets the line width to a specified value.
	 * @param width a line width
	 * @throws IllegalArgumentException if the specified width is non-positive
	 */
	public void setLineWidth(int width) throws IllegalArgumentException
	{
		if(width < 1){throw new IllegalArgumentException("The line width must be greater than 0.");}
		_lineWidth = width;
	}
	
	/**
	 * Returns the point width.
	 * @return the point width
	 */
	public int getPointWidth()
	{
		return _pointWidth;
	}
	
	/**
	 * Sets the point width to a specified value.
	 * @param width a point width
	 * @throws IllegalArgumentException if the specified width is non-positive
	 */
	public void setPointWidth(int width) throws IllegalArgumentException
	{
		if(width < 1){throw new IllegalArgumentException("The point width must be greater than 0.");}
		_pointWidth = width;
	}

	/**
	 * Returns a description of the data.
	 * @return a description of the data
	 */
	public String getDescription()
	{
		return _description;
	}

	/**
	 * Sets a description of the data.
	 * @param description a description of the data
	 */
	public void setDescription(String description)
	{
		_description = description;
	}
}