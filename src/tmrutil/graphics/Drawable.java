package tmrutil.graphics;

import java.awt.Graphics2D;

/**
 * An interface for objects that can draw a graphical representation using a
 * graphics context object.
 * 
 * @author Timothy A. Mann
 * 
 */
public interface Drawable
{
	/**
	 * Draws a graphical representation of this object using the provided
	 * graphics context and assuming the specified pixel width and height.
	 * 
	 * @param g
	 *            a graphics context
	 * @param width
	 *            the pixel width
	 * @param height
	 *            the pixel height
	 */
	public void draw(Graphics2D g, int width, int height);
}
