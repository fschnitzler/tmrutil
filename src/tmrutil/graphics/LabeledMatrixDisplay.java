package tmrutil.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tmrutil.math.MatrixOps;
import tmrutil.stats.Statistics;

public class LabeledMatrixDisplay extends JComponent
{
	private static final long serialVersionUID = 4509270682762905618L;
	
	public enum Normalization {FULL, ROW, COLUMN};

	private static class Element extends JComponent
	{
		private static final long serialVersionUID = -825867690657898987L;
	
		private double[][] _mat;
		private int _i;
		private int _j;
		private DecimalFormat _df;
		private Normalization _normalization;
		
		public Element(double[][] mat, int i, int j, Normalization normalization)
		{
			_df = new DecimalFormat("0.0000;-0.0000");
			_mat = mat;
			_i = i;
			_j = j;
			_normalization = normalization;
		}
		
		@Override
		public void paintComponent(Graphics g)
		{
			setToolTipText(_df.format(_mat[_i][_j]));
			double min = 0;
			double max = 0;
			if(_normalization.equals(Normalization.FULL)){
				min = Statistics.min(_mat);
				max = Statistics.max(_mat);
			}
			if(_normalization.equals(Normalization.ROW)){
				min = Statistics.min(_mat[_i]);
				max = Statistics.max(_mat[_i]);
			}
			if(_normalization.equals(Normalization.COLUMN)){
				double[] col = MatrixOps.getColumn(_mat, _j);
				min = Statistics.min(col);
				max = Statistics.max(col);
			}
			double diff = max - min;
			
			double val = (_mat[_i][_j] - min) / diff;
			
			Color prev = g.getColor();
			Color color = new Color((float) val, (float) val, (float) val);
			g.setColor(color);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(prev);
		}
	}

	private static class VerticalJLabel extends JLabel
	{
		private static final long serialVersionUID = -3201952899125582639L;

		public VerticalJLabel(String label)
		{
			super(label);
			setVerticalAlignment(JLabel.BOTTOM);
			Dimension d = getPreferredSize();
			Dimension nd = new Dimension(d.height, d.width);
			setPreferredSize(nd);
			setMinimumSize(nd);
		}

		@Override
		public void paintComponent(Graphics g)
		{

			Graphics2D g2 = (Graphics2D) g;
			String text = getText();
			FontRenderContext frc = g2.getFontRenderContext();
			Font font = g2.getFont();
			Rectangle2D frect = font.getStringBounds(text, frc);
			g2.rotate(-Math.PI / 2);
			g2.drawString(text, -getWidth() - (int) frect.getWidth() + (int)(2*frect.getWidth()/text.length()),
					(int) frect.getHeight());
		}
	}

	private JLabel[] _rowLabels;
	private VerticalJLabel[] _colLabels;
	private Element[][] _matrix;

	public LabeledMatrixDisplay(String[] rowLabels, String[] colLabels,
			double[][] matrix, Normalization normalization)
	{
		_rowLabels = new JLabel[rowLabels.length];
		_colLabels = new VerticalJLabel[colLabels.length];
		_matrix = new Element[rowLabels.length][colLabels.length];

		for (int row = 0; row < rowLabels.length; row++) {
			_rowLabels[row] = new JLabel(rowLabels[row]);
		}
		for (int col = 0; col < colLabels.length; col++) {
			_colLabels[col] = new VerticalJLabel(colLabels[col]);
		}

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 0.1;
		gbc.weighty = 0.1;
		for (int r = 0; r < _rowLabels.length; r++) {
			gbc.gridx = 0;
			gbc.gridy = r;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			layout.setConstraints(_rowLabels[r], gbc);
			add(_rowLabels[r], gbc);
			for (int c = 0; c < _colLabels.length; c++) {
				_matrix[r][c] = new Element(matrix, r, c, normalization);
				gbc.gridx = c + 1;
				gbc.gridy = r;
				gbc.fill = GridBagConstraints.BOTH;
				layout.setConstraints(_matrix[r][c], gbc);
				add(_matrix[r][c], gbc);
			}
		}
		JPanel empty = new JPanel();
		add(empty);
		for (int c = 0; c < _colLabels.length; c++) {
			gbc.gridx = c + 1;
			gbc.gridy = _rowLabels.length;
			gbc.fill = GridBagConstraints.VERTICAL;
			layout.setConstraints(_colLabels[c], gbc);
			add(_colLabels[c], gbc);
		}

		setLayout(layout);
	}

	public void toPNG(String filename) throws IOException
	{
		BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D)image.getGraphics();
		paint(g2);
		ImageIO.write(image, "png", new File(filename));
	}

	public static void main(String[] args)
	{
		javax.swing.JFrame frame = new javax.swing.JFrame("M Test");
		frame.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
		String[] rlabels = { "row 1", "row 2" };
		String[] clabels = { "col 1", "col 2", "col 3" };
		frame.add(new LabeledMatrixDisplay(rlabels, clabels, MatrixOps.random(
				rlabels.length, clabels.length), Normalization.FULL));
		frame.setVisible(true);
		frame.pack();
	}

}
