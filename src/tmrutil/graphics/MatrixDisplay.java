/*
 * MatrixDisplay.java
 */

/* Package */
package tmrutil.graphics;

import tmrutil.stats.Statistics;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;

public class MatrixDisplay implements Drawable, MouseMotionListener
{
	private static final long serialVersionUID = 2951719957203863177L;

	private double[][] _a;
	private int _numRows;
	private int _numCols;

	public MatrixDisplay(double[][] a)
	{
		setMatrix(a);
	}
	
	public void setMatrix(double[][] a)
	{
		_a = a;
		_numRows = a.length;
		_numCols = a[0].length;
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		
		double theight = (height /(double) _numRows);
		double twidth = (width /(double) _numCols);

		Color old = g.getColor();
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, width, height);
		g2.setColor(old);
		double[] aArray = new double[_numRows * _numCols];
		for(int r=0;r<_numRows;r++){
			for(int c=0;c<_numCols;c++){
				aArray[r * _numCols + c] = _a[r][c];
			}
		}
		
		double min = Statistics.min(aArray); // Statistics.lowerQuartile(aArray);
		double max = Statistics.max(aArray); // Statistics.upperQuartile(aArray);
		double range = max - min;
		if (range == 0) {
			range = 1;
		}
		for (int i = 0; i < _a.length; i++) {
			for (int j = 0; j < _a[i].length; j++) {
				double val = (_a[i][j] - min) / range;
				if(val > 1){val = 1;}
				if(val < 0){val = 0;}
				
				Rectangle2D rect = new Rectangle2D.Double(j * twidth, i * theight, twidth, theight);
				Color c = new Color((float)val, (float)val, (float)val);
				g2.setColor(c);
				g2.fill(rect);
			}
		}
		
		g2.dispose();
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		e.consume();
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		Point p = e.getPoint();
		Component comp = e.getComponent();
		if(comp instanceof JComponent){
			JComponent jcomp = (JComponent)comp;
			double theight = (comp.getHeight()/(double)_numRows);
			double twidth = (comp.getWidth()/(double)_numCols);
			
			int c = (int)Math.floor(p.getX() / twidth);
			int r = (int)Math.floor(p.getY() / theight);
			
			if(r >= 0 && r < _numRows && c >= 0 && c < _numCols){
				double val = _a[r][c];
				jcomp.setToolTipText(String.valueOf(val));
			}
		}
		
		e.consume();		
	}

}