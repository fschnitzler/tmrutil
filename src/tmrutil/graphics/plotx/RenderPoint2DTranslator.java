package tmrutil.graphics.plotx;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class RenderPoint2DTranslator implements
		RenderCoordinateTranslator<Point2D>
{

	private Rectangle _pixelSpace;
	private Rectangle2D _coordSpace;
	
	public RenderPoint2DTranslator(Rectangle pixelSpace)
	{
		this(pixelSpace, new Rectangle2D.Double(0, 0, 1, 1));
	}
	
	public RenderPoint2DTranslator(Rectangle pixelSpace, Rectangle2D coordSpace)
	{
		_pixelSpace = new Rectangle(pixelSpace.x, pixelSpace.y, pixelSpace.width, pixelSpace.height);
		_coordSpace = new Rectangle2D.Double(coordSpace.getX(), coordSpace.getY(), coordSpace.getWidth(), coordSpace.getHeight()); 
	}
	
	public void setPixelSpace(Rectangle pixelSpace)
	{
		_pixelSpace = pixelSpace;
	}
	
	public void setCoordSpace(Rectangle2D coordSpace)
	{
		_coordSpace = coordSpace;
	}
	
	@Override
	public Point translate(Point2D coord)
	{
		int x = 0, y = 0;
		
		double xOff = _coordSpace.getX() * _pixelSpace.getWidth();
		double yOff = _coordSpace.getY() * _pixelSpace.getHeight();
		double width = _coordSpace.getWidth() * _pixelSpace.getWidth();
		double height = _coordSpace.getHeight() * _pixelSpace.getHeight();
		
		x = (int)(xOff + ((coord.getX() - _coordSpace.getX())/_coordSpace.getWidth()) * width);
		y = (int)(_pixelSpace.getHeight() - (yOff + ((coord.getY() - _coordSpace.getY())/_coordSpace.getHeight()) * height));
		
		return new Point(x, y);
	}
	
	public Rectangle translate(Rectangle2D rect)
	{
		Point2D llp = new Point2D.Double(rect.getX(), rect.getY());
		Point2D urp = new Point2D.Double(rect.getX() + rect.getWidth(), rect.getY() + rect.getHeight());
		Point llpi = translate(llp);
		Point urpi = translate(urp);
		
		return new Rectangle(llpi.x, urpi.y, urpi.x - llpi.x, llpi.y - urpi.y);
	}

}
