package tmrutil.graphics.plotx;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import tmrutil.util.DebugException;

/**
 * A scatter object is used to render data points that are not connected by a
 * line.
 * 
 * @author Timothy A. Mann
 * 
 */
public class ScatterObject implements PlotObject
{
	public enum MarkerType {
		CIRCLE, DIAMOND, SQUARE, TRIANGLE, X
	};

	private static final int MARKER_PIXEL_SIZE = 6;
	private static final int HALF_MARKER_PIXEL_SIZE = MARKER_PIXEL_SIZE / 2;

	private List<Point2D> _points;
	private MarkerType _shape;
	private Color _lineColor;
	private Stroke _lineStyle;
	private Axis _axis;

	/**
	 * Constructs a scatter object.
	 * 
	 * @param points
	 * @param shape
	 * @param lineColor
	 * @param lineStyle
	 * @param axis
	 */
	public ScatterObject(List<Point2D> points, MarkerType shape,
			Color lineColor, Stroke lineStyle, Axis axis)
	{
		_points = new ArrayList<Point2D>(points);
		_shape = shape;
		_lineColor = lineColor;
		_lineStyle = lineStyle;
		_axis = axis;
	}

	@Override
	public boolean belongsToAxis()
	{
		return _axis != null;
	}

	@Override
	public void draw(Graphics2D g, Rectangle2D axisRect,
			Rectangle2D componentRect)
	{
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setColor(_lineColor);
		g2.setStroke(_lineStyle);

		if (_axis != null) {
			Rectangle clipRect = Axis.noAxisTransformDataToPixel(axisRect,
					axisRect, componentRect);
			g2.setClip(clipRect);
		}

		// Draw the line
		for (int i = 0; i < _points.size(); i++) {
			Point2D newPoint = _points.get(i);

			Point pixelNewPoint = null;
			if (_axis != null) {
				pixelNewPoint = _axis.transformDataToPixel(newPoint, axisRect,
						componentRect);
			} else {
				pixelNewPoint = Axis.noAxisTransformDataToPixel(newPoint,
						axisRect, componentRect);
			}

			switch(_shape){
			case CIRCLE:
				renderCircle(g2, pixelNewPoint);
				break;
			case DIAMOND:
				renderDiamond(g2, pixelNewPoint);
				break;
			case SQUARE:
				renderSquare(g2, pixelNewPoint);
				break;
			case TRIANGLE:
				throw new DebugException("Triangle currently unsupported!");
			case X:
				renderX(g2, pixelNewPoint);
			}
		}

		g2.dispose();
	}

	public void renderCircle(Graphics2D g, Point p)
	{
		g.drawOval(p.x - HALF_MARKER_PIXEL_SIZE, p.y - HALF_MARKER_PIXEL_SIZE,
				MARKER_PIXEL_SIZE, MARKER_PIXEL_SIZE);
	}

	public void renderDiamond(Graphics2D g, Point p)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		
		AffineTransform xform = new AffineTransform();
		xform.rotate(Math.PI/4, p.x, p.y);
		g2.transform(xform);
		
		g2.drawRect(p.x - HALF_MARKER_PIXEL_SIZE, p.y - HALF_MARKER_PIXEL_SIZE,
				MARKER_PIXEL_SIZE, MARKER_PIXEL_SIZE);
		
		g2.dispose();
	}

	public void renderSquare(Graphics2D g, Point p)
	{
		g.drawRect(p.x - HALF_MARKER_PIXEL_SIZE, p.y - HALF_MARKER_PIXEL_SIZE,
				MARKER_PIXEL_SIZE, MARKER_PIXEL_SIZE);
	}

	public void renderX(Graphics2D g, Point p)
	{
		Point topLeft = new Point(p.x - HALF_MARKER_PIXEL_SIZE, p.y - HALF_MARKER_PIXEL_SIZE);
		Point topRight = new Point(p.x + HALF_MARKER_PIXEL_SIZE, p.y - HALF_MARKER_PIXEL_SIZE);
		Point bottomLeft = new Point(p.x - HALF_MARKER_PIXEL_SIZE, p.y + HALF_MARKER_PIXEL_SIZE);
		Point bottomRight = new Point(p.x + HALF_MARKER_PIXEL_SIZE, p.y + HALF_MARKER_PIXEL_SIZE);
		
		g.drawLine(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
		g.drawLine(topRight.x, topRight.y, bottomLeft.x, bottomLeft.y);
	}

	@Override
	public Rectangle2D getDataBounds()
	{
		double minx = 0, miny = 0, maxx = 0, maxy = 0;

		for (int i = 0; i < _points.size(); i++) {
			Point2D p = _points.get(i);
			double x = p.getX();
			double y = p.getY();
			if (minx > x || i == 0) {
				minx = x;
			}
			if (maxx < x || i == 0) {
				maxx = x;
			}
			if (miny > y || i == 0) {
				miny = y;
			}
			if (maxy < y || i == 0) {
				maxy = y;
			}
		}

		Rectangle2D dataBounds = new Rectangle2D.Double(minx, miny,
				maxx - minx, maxy - miny);
		return dataBounds;
	}

}
