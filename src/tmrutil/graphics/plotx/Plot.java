package tmrutil.graphics.plotx;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import tmrutil.graphics.DrawableComponent;
import tmrutil.math.VectorOps;

/**
 * Represents a simple 2D plot.
 * @author Timothy A. Mann
 *
 */
public class Plot extends AbstractPlot
{
	/**
	 * A reference to the axis used by this plot.
	 */
	private Axis _axis;
	
	/**
	 * Constructs an empty plot.
	 */
	public Plot()
	{
		super(new Rectangle2D.Double(0.1, 0.1, 0.88, 0.8));
		Rectangle2D axisRect = getAxisRect();
		RectangleObject rectObj = new RectangleObject(axisRect);
		add(rectObj);
		_axis = new Axis();
	}
	
	@Override
	public void setTitle(String title)
	{
		PlotProperties properties = getPlotProperties();
		Rectangle2D axisRect = getAxisRect();
		
		TextObject titleObject = new TextObject(title, properties.getTitleFont(),
				properties.getTitleColor(), new Point2D.Double(0.5, axisRect.getY() + axisRect.getHeight() + 0.02),
				TextObject.Justify.BOTTOM_CENTER);
		add(titleObject);
	}
	
	@Override
	public void setXLabel(String label)
	{
		PlotProperties properties = getPlotProperties();
		Rectangle2D axisRect = getAxisRect();
		
		TextObject xlabelObject = new TextObject(label, properties.getXLabelFont(), properties.getXLabelColor(), new Point2D.Double(axisRect.getX() + (axisRect.getWidth()/2), axisRect.getY() - 0.02), TextObject.Justify.TOP_CENTER);
		add(xlabelObject);
	}
	
	@Override
	public void setYLabel(String label)
	{
		PlotProperties properties = getPlotProperties();
		Rectangle2D axisRect = getAxisRect();
		
		TextObject ylabelObject = new TextObject(label, properties.getYLabelFont(), properties.getXLabelColor(), new Point2D.Double(axisRect.getX() - 0.02, axisRect.getY() + (axisRect.getHeight()/2)), TextObject.Justify.BOTTOM_CENTER, -Math.PI/2, null);
		add(ylabelObject);
	}
	
	/**
	 * Sets the rendered limits of the X-axis.
	 * @param xmin the minimum visible point on the X-axis
	 * @param xmax the maximum visible point on the X-axis
	 */
	public void setXLim(double xmin, double xmax)
	{
		_axis.setXLim(xmin, xmax);
	}
	
	/**
	 * Sets the rendered limits on the Y-axis.
	 * @param ymin the minimum visible point on the Y-axis
	 * @param ymax the maximum visible point on the Y-axis
	 */
	public void setYLim(double ymin, double ymax)
	{
		_axis.setYLim(ymin, ymax);
	}
	
	/**
	 * Resizes the axis to fit the data.
	 */
	public void autoResize()
	{
		Rectangle2D rect = null;
		for(PlotObject plotObject : this){
			if(plotObject.belongsToAxis()){
				Rectangle2D bounds = plotObject.getDataBounds();
				if(rect == null){
					rect = bounds;
				}else{
					if(!rect.contains(bounds)){
						rect = rect.createUnion(bounds);
					}
				}
			}
		}
		
		double x1 = rect.getX(), x2 = rect.getX() + rect.getWidth();
		double y1 = rect.getY(), y2 = rect.getY() + rect.getHeight();
		setXLim(Math.min(x1, x2), Math.max(x1, x2));
		setYLim(Math.min(y1, y2), Math.max(y1, y2));
	}
	
	/**
	 * Adds a line element to this plot with a specified color and line style.
	 * @param x a vector of X-coordinates
	 * @param y a vector of Y-coordinates corresponding to each element of <code>x</code>
	 * @param lineColor the color used to render the line
	 * @param lineStyle the line style used to draw the line
	 */
	public void addLine(double[] x, double[] y, Color lineColor, Stroke lineStyle)
	{
		List<Point2D> points = new ArrayList<Point2D>(x.length);
		for(int i=0;i<x.length;i++){
			points.add(new Point2D.Double(x[i],y[i]));
		}
		LineObject lineObject = new LineObject(points, lineColor, lineStyle, _axis);
		add(lineObject);
		
		autoResize();
	}
	
	/**
	 * Adds a line element to this plot with a specified color and line style.
	 * @param points a list of points
	 * @param lineColor the color used to render the line
	 * @param lineStyle the line style used to draw the line
	 */
	public void addLine(List<Point2D> points, Color lineColor, Stroke lineStyle)
	{
		LineObject lineObject = new LineObject(points, lineColor, lineStyle, _axis);
		add(lineObject);
		
		autoResize();
	}
	
	/**
	 * Adds data to be rendered in the plot axes where data points are not connected by a line.
	 * @param points a set of data points
	 * @param shape the shape of the marker at each point
	 * @param lineColor the color of the line used to draw the marker at each point
	 * @param lineStyle the style of the line used to draw the marker at each point
	 */
	public void addScatter(List<Point2D> points, ScatterObject.MarkerType shape, Color lineColor, Stroke lineStyle)
	{
		ScatterObject scatterObject = new ScatterObject(points, shape, lineColor, lineStyle, _axis);
		add(scatterObject);
		
		autoResize();
	}

	
	public static void main(String[] args)
	{
		double[] x = VectorOps.range(-4, 4, 1000);
		double[] y = new double[x.length];
		List<Point2D> points = new ArrayList<Point2D>(x.length);
		for(int i=0;i<x.length;i++){
			y[i] = Math.sin(x[i]);
			if(i % 50 == 0){
				points.add(new Point2D.Double(x[i], Math.cos(x[i])));
			}
		}
		
		JFrame frame = new JFrame();
		frame.setSize(640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Plot plot = new Plot();
		plot.setTitle("Test Plot Title");
		plot.setXLabel("X");
		plot.setYLabel("Y");
		plot.addLine(x, y, Color.RED, new BasicStroke(2.0f));
		plot.addScatter(points, ScatterObject.MarkerType.X, Color.BLUE, new BasicStroke(2.0f));
		DrawableComponent dc = new DrawableComponent(plot);
		frame.add(dc);
		frame.setVisible(true);
	}
}
