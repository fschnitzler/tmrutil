package tmrutil.graphics.plotx;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * A line or curve to be rendered on a plot.
 * 
 * @author Timothy A. Mann
 * 
 */
public class LineObject implements PlotObject
{
	private List<Point2D> _points;
	private Color _lineColor;
	private Stroke _lineStyle;
	private Axis _axis;

	/**
	 * Constructs a line object from a list of points, color, line style, and a
	 * null axis.
	 * 
	 * @param points
	 *            a list of points
	 * @param lineColor
	 *            the line color used to render the line
	 * @param lineStyle
	 *            the axis used to determine the line position
	 */
	public LineObject(List<Point2D> points, Color lineColor, Stroke lineStyle)
	{
		this(points, lineColor, lineStyle, null);
	}

	/**
	 * Constructs a line object from a list of points, color, line style, and
	 * axis.
	 * 
	 * @param points
	 *            a list of points
	 * @param lineColor
	 *            the color used to render the line
	 * @param lineStyle
	 *            a style used to draw the line
	 * @param axis
	 *            the axis used to determine line position
	 */
	public LineObject(List<Point2D> points, Color lineColor, Stroke lineStyle,
			Axis axis)
	{
		_points = new ArrayList<Point2D>(points);
		_lineColor = lineColor;
		_lineStyle = lineStyle;
		_axis = axis;
	}

	@Override
	public boolean belongsToAxis()
	{
		return _axis != null;
	}

	@Override
	public Rectangle2D getDataBounds()
	{
		double minx = 0, miny = 0, maxx = 0, maxy = 0;

		for (int i = 0; i < _points.size(); i++) {
			Point2D p = _points.get(i);
			double x = p.getX();
			double y = p.getY();
			if (minx > x || i == 0) {
				minx = x;
			}
			if (maxx < x || i == 0) {
				maxx = x;
			}
			if (miny > y || i == 0) {
				miny = y;
			}
			if (maxy < y || i == 0) {
				maxy = y;
			}
		}

		Rectangle2D dataBounds = new Rectangle2D.Double(minx, miny,
				maxx - minx, maxy - miny);
		return dataBounds;
	}

	@Override
	public void draw(Graphics2D g, Rectangle2D axisRect,
			Rectangle2D componentRect)
	{
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setColor(_lineColor);
		g2.setStroke(_lineStyle);

		if (_axis != null) {
			Rectangle clipRect = Axis.noAxisTransformDataToPixel(axisRect,
					axisRect, componentRect);
			g2.setClip(clipRect);
		}

		// Draw the line
		for (int i = 1; i < _points.size(); i++) {
			Point2D oldPoint = _points.get(i - 1);
			Point2D newPoint = _points.get(i);

			Point pixelOldPoint = null;
			Point pixelNewPoint = null;
			if (_axis != null) {
				pixelOldPoint = _axis.transformDataToPixel(oldPoint, axisRect,
						componentRect);
				pixelNewPoint = _axis.transformDataToPixel(newPoint, axisRect,
						componentRect);
			} else {
				pixelOldPoint = Axis.noAxisTransformDataToPixel(oldPoint,
						axisRect, componentRect);
				pixelNewPoint = Axis.noAxisTransformDataToPixel(newPoint,
						axisRect, componentRect);
			}

			g2.drawLine(pixelOldPoint.x, pixelOldPoint.y, pixelNewPoint.x,
					pixelNewPoint.y);
		}

		g2.dispose();
	}

}
