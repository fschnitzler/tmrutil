package tmrutil.graphics.plotx;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * A plot object is a line, string, shape, etc. that is rendered on a plot.
 * @author Timothy A. Mann
 *
 */
public interface PlotObject
{
	/**
	 * Renders the specific plot object.
	 * @param g a graphics object for rendering
	 * @param axisRect the rectangular region of the plot where the axis is drawn in normalized coordinates
	 * @param componentRect the rectangular region where the component is drawn in pixel coordinates
	 */
	public void draw(Graphics2D g, Rectangle2D axisRect, Rectangle2D componentRect);
	
	/**
	 * True if the underlying plot object belongs to an axis object.
	 * @return true if this object belongs to an axis object; otherwise false
	 */
	public boolean belongsToAxis();
	
	/**
	 * Returns the bounds of the data points or normalized region of the plot.
	 * @return a rectangle
	 */
	public Rectangle2D getDataBounds();
}
