package tmrutil.graphics.plotx;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Represents a text object that is rendered on a plot. The coordinates used by
 * text objects are normalized, meaning that the X and Y coordinates scale
 * between 0 and 1.
 * 
 * @author Timothy A. Mann
 * 
 */
public class TextObject implements PlotObject
{
	public enum Justify {TOP_LEFT, TOP_CENTER, TOP_RIGHT, MID_LEFT, MID_CENTER, MID_RIGHT, BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT};
	
	private String _text;
	private Font _font;
	private Color _color;
	private Point2D _location;
	private Justify _justified;
	private Axis _axis;
	private double _angle;

	/**
	 * Constructs a text object.
	 * 
	 * @param text
	 *            the text to display
	 * @param font
	 *            the font used to render the text
	 * @param location
	 *            the location of the text in normalized coordinates
	 * @param justified determines how the text is positioned horizontally with respect to the location
	 */
	public TextObject(String text, Font font, Color color, Point2D location, Justify justified)
	{
		this(text, font, color, location, justified, 0, null);
	}
	
	/**
	 * Constructs a text object.
	 * 
	 * @param text
	 *            the text to display
	 * @param font
	 *            the font used to render the text
	 * @param location
	 *            the location of the text in normalized coordinates
	 * @param justified determines how the text is positioned horizontally with respect to the location
	 * @param angle an angle in Radians to rotate the text object by
	 * @param axis the axis used to determine text position
	 */
	public TextObject(String text, Font font, Color color, Point2D location, Justify justified, double angle, Axis axis)
	{
		_text = text;
		_font = font;
		_color = color;
		_location = location;
		_justified = justified;
		_axis = axis;
		_angle = angle;
	}

	/**
	 * Returns the text rendered by this text object.
	 * @return a string
	 */
	public String getText()
	{
		return _text;
	}

	/**
	 * Sets the text that is rendered by this text object.
	 * @param text a string
	 */
	public void setText(String text)
	{
		_text = text;
	}

	public Font getFont()
	{
		return _font;
	}

	/**
	 * Sets the font used by this text object.
	 * @param font a font
	 */
	public void setFont(Font font)
	{
		_font = font;
	}

	public Point2D getLocation()
	{
		return _location;
	}

	/**
	 * The location where the text object should be rendered.
	 * @param location a point
	 */
	public void setLocation(Point2D location)
	{
		_location = location;
	}
	
	/**
	 * Returns the angle that this text object is rendered at (in Radians)
	 * @return an angle in Radians
	 */
	public double getAngle()
	{
		return _angle;
	}
	
	/**
	 * Sets the angle that this text object is rendered at (in Radians)
	 * @param angle an angle in Radians
	 */
	public void setAngle(double angle)
	{
		_angle = angle;
	}
	
	@Override
	public boolean belongsToAxis()
	{
		return _axis != null;
	}
	
	@Override
	public Rectangle2D getDataBounds()
	{
		return new Rectangle2D.Double(_location.getX(), _location.getY(), 0, 0);
	}

	@Override
	public void draw(Graphics2D g, Rectangle2D axisRect, Rectangle2D componentRect)
	{
		if (_text != null) {
			// Create a new graphics object
			Graphics2D g2 = (Graphics2D)g.create();
			// Set the font
			g2.setFont(_font);
			g2.setColor(_color);
			
			// Find the location to draw the string
			Point point = null;
			if(_axis != null){
				Rectangle clipRect = Axis.noAxisTransformDataToPixel(axisRect, axisRect, componentRect);
				g2.setClip(clipRect);
				point = _axis.transformDataToPixel(_location, axisRect, componentRect);
			}else{
				point = Axis.noAxisTransformDataToPixel(_location, axisRect, componentRect);
			}
			FontMetrics fm = g2.getFontMetrics();
			Rectangle2D textBounds = fm.getStringBounds(_text, g2);
			
			int x = 0, y = 0;
			switch(_justified){
			case BOTTOM_LEFT:
				x = point.x;
				y = point.y;
				break;
			case BOTTOM_CENTER:
				x = (int)(point.x - (textBounds.getWidth()/2));
				y = point.y;
				break;
			case BOTTOM_RIGHT:
				x = (int)(point.x - textBounds.getWidth());
				y = point.y;
				break;
			case MID_LEFT:
				x = point.x;
				y = (int)(point.y + (textBounds.getHeight()/2));
				break;
			case MID_CENTER:
				x = (int)(point.x - (textBounds.getWidth()/2));
				y = (int)(point.y + (textBounds.getHeight()/2));
				break;
			case MID_RIGHT:
				x = (int)(point.x - textBounds.getWidth());
				y = (int)(point.y + (textBounds.getHeight()/2));
				break;
			case TOP_LEFT:
				x = point.x;
				y = (int)(point.y + textBounds.getHeight());
				break;
			case TOP_CENTER:
				x = (int)(point.x - (textBounds.getWidth()/2));
				y = (int)(point.y + textBounds.getHeight());
				break;
			case TOP_RIGHT:
				x = (int)(point.x - textBounds.getWidth());
				y = (int)(point.y + textBounds.getHeight());
				break;
			}
			
			// Create an affine transform to translate the strings angle
			AffineTransform xform = new AffineTransform();
			xform.rotate(_angle, point.x, point.y);
			// Transform the graphics object
			g2.transform(xform);
			
			// Draw the string
			g2.drawString(_text, x, y);
			
			// Dispose of the new graphics object
			g2.dispose();
		}
	}

}
