package tmrutil.graphics.plotx;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import tmrutil.graphics.Drawable;

/**
 * Represents an abstract plot.
 * 
 * @author Timothy A. Mann
 * 
 */
public abstract class AbstractPlot implements Drawable, Iterable<PlotObject>
{

	/**
	 * A list of plot objects.
	 */
	private List<PlotObject> _plotObjects;
	
	/**
	 * A set of plot properties.
	 */
	private PlotProperties _plotProperties;

	/**
	 * The offset of the axis from the bottom left corner of the plot.
	 */
	private Rectangle2D _axisRect;

	/**
	 * Constructs an empty abstract plot instance.
	 */
	public AbstractPlot(Rectangle2D axisRect)
	{
		_axisRect = axisRect;
		_plotProperties = new PlotProperties();
		_plotObjects = new ArrayList<PlotObject>();
	}

	/**
	 * Add a plot object to this abstract plot.
	 * 
	 * @param plotObject
	 *            a plot object
	 */
	public void add(PlotObject plotObject)
	{
		_plotObjects.add(plotObject);
	}
	
	@Override
	public Iterator<PlotObject> iterator()
	{
		return _plotObjects.iterator();
	}

	@Override
	public void draw(Graphics2D g, int componentPixelWidth,
			int componentPixelHeight)
	{
		Rectangle2D componentRect = new Rectangle2D.Double(0, 0,
				componentPixelWidth, componentPixelHeight);
		for (int i = 0; i < _plotObjects.size(); i++) {
			PlotObject plotObject = _plotObjects.get(i);
			plotObject.draw(g, _axisRect, componentRect);
		}
	}
	
	/**
	 * Returns the plot properties.
	 * @return a plot properties instance
	 */
	protected PlotProperties getPlotProperties()
	{
		return _plotProperties;
	}

	/**
	 * Returns a reference to the rectangle where the axis is displayed. The
	 * rectangle is expressed in normalized coordinates.
	 * 
	 * @return a rectangle
	 */
	protected Rectangle2D getAxisRect()
	{
		return _axisRect;
	}

	/**
	 * Sets the title string of this plot.
	 * @param title a string
	 */
	public abstract void setTitle(String title);
	
	/**
	 * Sets the label of the X-axis of this plot.
	 * @param label a string
	 */
	public abstract void setXLabel(String label);
	
	/**
	 * Sets the label of the Y-axis of this plot.
	 * @param label a string
	 */
	public abstract void setYLabel(String label);

}
