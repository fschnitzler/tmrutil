package tmrutil.graphics.plotx;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;

/**
 * A rectangular object drawn to a plot.
 * @author Timothy A. Mann
 *
 */
public class RectangleObject implements PlotObject
{
	private Rectangle2D _rect;
	private Stroke _lineStyle;
	private Color _lineColor;
	private Color _fillColor;
	private Axis _axis;
	
	/**
	 * Constructs an unfilled rectangular plot object with a solid black outline.
	 * @param rect a rectangle specified in normalized coordinates
	 */
	public RectangleObject(Rectangle2D rect)
	{
		this(rect, new BasicStroke(), Color.BLACK);
	}
	
	/**
	 * Constructs an unfilled rectangular plot object with black outline.
	 * @param rect a rectangle specified in normalized coordinates
	 * @param lineStyle the stroke used to draw the rectangle border
	 */
	public RectangleObject(Rectangle2D rect, Stroke lineStyle)
	{
		this(rect, lineStyle, Color.BLACK);
	}
	
	/**
	 * Constructs an unfilled rectangular plot object with a specified line color.
	 * @param rect a rectangle specified in normalized coordinates
	 * @param lineStyle the stroke used to draw the rectangle border
	 * @param lineColor the line color (null will be completely transparent)
	 */
	public RectangleObject(Rectangle2D rect, Stroke lineStyle, Color lineColor)
	{
		this(rect, lineStyle, lineColor, null, null);
	}
	
	/**
	 * Constructs a rectangular plot object.
	 * @param rect a rectangle specified in normalized coordinates
	 * @param lineStyle the stroke used to draw the rectangle border
	 * @param lineColor the line color (null will be completely transparent)
	 * @param fillColor the fill color (null will be completely transparent)
	 * @param axis a reference to an axis instance
	 */
	public RectangleObject(Rectangle2D rect, Stroke lineStyle, Color lineColor, Color fillColor, Axis axis)
	{
		_rect = rect;
		if(lineStyle != null){
			_lineStyle = lineStyle;
		}else{
			_lineStyle = new BasicStroke();
		}
		_lineColor = lineColor;
		_fillColor = fillColor;
		_axis = axis;
	}
	
	@Override
	public boolean belongsToAxis()
	{
		return _axis != null;
	}
	
	@Override
	public Rectangle2D getDataBounds()
	{
		return _rect;
	}
	
	@Override
	public void draw(Graphics2D g, Rectangle2D axisRect,
			Rectangle2D componentRect)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		
		Rectangle rect = null;
		if(_axis != null){
			Rectangle clipRect = Axis.noAxisTransformDataToPixel(axisRect, axisRect, componentRect);
			g2.setClip(clipRect);
			rect = _axis.transformDataToPixel(_rect, axisRect, componentRect);
		}else{
			rect = Axis.noAxisTransformDataToPixel(_rect, axisRect, componentRect);
		}
		
		if(_fillColor != null){
			g2.setColor(_fillColor);
			g2.fill(rect);
		}
		if(_lineColor != null){
			g2.setColor(_lineColor);
			g2.setStroke(_lineStyle);
			g2.draw(rect);
		}
		
		g2.dispose();
	}

}
