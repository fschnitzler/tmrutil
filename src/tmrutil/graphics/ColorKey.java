package tmrutil.graphics;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import tmrutil.util.Interval;

/**
 * Used to draw a key in a figure depicting how the colors in that figure correspond to scalar values.
 * @author Timothy A. Mann
 *
 */
public class ColorKey implements Drawable
{
	private ColorMap _colorMap;
	private Interval _range;
	private Rectangle2D _keyRect;
	
	/**
	 * Constructs a color key given a color map and the interval over which the color map is valid.
	 * @param colorMap a color map (mapping scalar values to colors)
	 * @param range the interval over which this key is valid
	 * @param a rectangle specified in relative coordinates
	 */
	public ColorKey(ColorMap colorMap, Interval range, Rectangle2D keyRect)
	{
		_colorMap = colorMap;
		_range = new Interval(range);
		_keyRect = new Rectangle2D.Double(keyRect.getX(), keyRect.getY(), keyRect.getWidth(), keyRect.getHeight());
	}

	@Override
	public void draw(Graphics2D g, int width, int height)
	{
		Graphics2D g2 = (Graphics2D)g.create();
		Rectangle2D drawRect = new Rectangle2D.Double(_keyRect.getX() * width, _keyRect.getY() * height, _keyRect.getWidth() * width, _keyRect.getHeight() * height);
		
		g2.setPaint(_colorMap.getPaint(new Point2D.Double(0,drawRect.getY() + drawRect.getHeight()), new Point2D.Double(0,drawRect.getY())));
		g2.fill(drawRect);
		
		g2.setColor(Color.BLACK);
		g2.draw(drawRect);
		
		g2.setClip(drawRect);
		
		String max = String.valueOf(_range.getMax());
		String min = String.valueOf(_range.getMin());
		FontMetrics fm = g2.getFontMetrics();
		Rectangle2D maxBounds = fm.getStringBounds(max, g2);
		Rectangle2D minBounds = fm.getStringBounds(min, g2);
		
		double rectMid = drawRect.getX() + (drawRect.getWidth()/2);
		g2.drawString(max, (int)(rectMid - (maxBounds.getWidth()/2)), (int)(drawRect.getY() + maxBounds.getHeight()));
		g2.drawString(min, (int)(rectMid - (minBounds.getWidth()/2)), (int)(drawRect.getY() + drawRect.getHeight() - minBounds.getHeight()));
		
		g2.dispose();
	}

}
