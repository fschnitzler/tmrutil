package tmrutil.graphics.plot2d;

import java.awt.Graphics;
import java.util.List;
import tmrutil.graphics.ContinuousAxis;
import tmrutil.graphics.LabeledAxis;

/**
 * A utility class for plotting bar graphs.
 * @author Timothy Mann
 *
 */
public class BarGraph extends Plot2D
{

	private static final long serialVersionUID = 8919222065302973395L;

	private List<String> _labels; 
	private List<Double> _values;
	
	public BarGraph(List<String> labels, List<Double> values)
	{
		_labels = labels;
		_values = values;
		
		_xaxis = new LabeledAxis("", labels);
		_yaxis = new ContinuousAxis("");
	}
	
	@Override
	public void paintPlot(Graphics g, int width, int height)
	{
		// TODO Auto-generated method stub

	}

}
