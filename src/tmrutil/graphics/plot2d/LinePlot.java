/*
 * LinePlot.java
 */

/* Package */
package tmrutil.graphics.plot2d;

/* Imports */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import tmrutil.graphics.ContinuousAxis;
import tmrutil.graphics.DataProperties;

/**
 * Allows 2D graphical plots of points in a continuous plane. While capable of
 * generating scatter plots the default is to display data with connecting
 * lines.
 */
public class LinePlot extends Plot2D
{

	private static final long serialVersionUID = 5439398964271795746L;

	/** The number of pixels wide to render a point. */
	private static final int POINT_SIZE = 4;

	/**
	 * The amount of of additional space to provide in the Y-axis when
	 * automatically resizing.
	 */
	private static final double Y_ADJUSTMENT = 0.1;

	/** A list of data sets. */
	private List<DataProperties> _data;

	/** True if the axes should auto resize to the data. */
	private boolean _autoResize;

	/**
	 * Constructs an empty 2D plot over a continuous region.
	 */
	public LinePlot()
	{
		super();
		_data = new ArrayList<DataProperties>();
		_xaxis = new ContinuousAxis("");
		_yaxis = new ContinuousAxis("");
		setAutoResize(true);
	}

	private void updateAxes()
	{
		double xmin = Double.MAX_VALUE, xmax = Double.MIN_VALUE, ymin = Double.MAX_VALUE, ymax = Double.MIN_VALUE;
		for (DataProperties data : _data) {
			List<double[]> ds = data.getData();
			for (int i = 0; i < ds.size(); i++) {
				double[] d = ds.get(i);
				if (!Double.isInfinite(d[0]) && !Double.isNaN(d[0])) {
					if (d[0] < xmin) {
						xmin = d[0];
					}
					if (d[0] > xmax) {
						xmax = d[0];
					}
				}
				if (!Double.isInfinite(d[1]) && !Double.isNaN(d[1])) {
					if (d[1] < ymin) {
						ymin = d[1];
					}
					if (d[1] > ymax) {
						ymax = d[1];
					}
				}
			}
		}
		setXAxis(xmin, xmax);
		double ydiff = ymax - ymin;
		if(ydiff > 0){
			setYAxis(ymin - Y_ADJUSTMENT * ydiff, ymax + Y_ADJUSTMENT * ydiff);
		}else{
			setYAxis(0, 1);
		}
		repaint();
	}

	public void setXAxis(double min, double max)
	{
		((ContinuousAxis) _xaxis).setRange(min, max);
	}

	public void setYAxis(double min, double max)
	{
		((ContinuousAxis) _yaxis).setRange(min, max);
	}

	public void addData(DataProperties data)
	{
		synchronized (_data) {
			if (!_data.contains(data)) {
				_data.add(data);
			}
		}
		if (_autoResize) {
			updateAxes();
		}
	}

	public void addData(List<double[]> data, Color color, String description)
	{
		addData(new DataProperties(data, DataProperties.DisplayStyle.LINE,
				color, description));
	}

	public void addData(List<double[]> data, DataProperties.DisplayStyle style,
			Color color, String description)
	{
		addData(new DataProperties(data, style, color, description));
	}

	public void addData(double[] x, double[] y, Color color, String description)
	{
		List<double[]> data = new ArrayList<double[]>(x.length);
		for (int i = 0; i < x.length; i++) {
			data.add(new double[] { x[i], y[i] });
		}
		addData(data, color, description);
	}

	public boolean isAutoResize()
	{
		return _autoResize;
	}

	public void setAutoResize(boolean autoResize)
	{
		_autoResize = autoResize;
	}

	@Override
	public void paintPlot(Graphics g2, int width, int height)
	{
		ContinuousAxis xaxis = (ContinuousAxis) _xaxis;
		ContinuousAxis yaxis = (ContinuousAxis) _yaxis;
		DecimalFormat df = DEFAULT_DECIMAL_FORMAT;

		// Paint the title
		int titleWidth = (int) (width * TITLE_X);
		int titleHeight = (int) (height * TITLE_Y);
		String title = getTitle();
		FontMetrics fm = g2.getFontMetrics();
		Rectangle2D titleRect = fm.getStringBounds(title, g2);
		g2.setColor(Color.BLACK);
		g2.drawString(title, (int) (titleWidth / 2 - titleRect.getWidth() / 2),
				(int) titleRect.getHeight());

		// Paint the axes
		// X axis first
		int xAxisX = (int) ((1 - XAXIS_X) * width);
		int xAxisY = (int) ((1 - XAXIS_Y) * height);
		int xAxisWidth = width - xAxisX;
		String xlabel = _xaxis.getLabel();
		Rectangle2D xlabelRect = fm.getStringBounds(xlabel, g2);
		g2.drawString(xlabel,
				xAxisX + (int) (xAxisWidth / 2 - xlabelRect.getWidth() / 2),
				height);
		List<Double> ticks = xaxis.getTicks();
		for (int i = 0; i < ticks.size(); i++) {
			int x = xaxis.compute2DCoord(ticks.get(i), xAxisWidth, xAxisX,
					false);
			int y = xAxisY;
			String tick = df.format(ticks.get(i));
			Rectangle2D tickRect = fm.getStringBounds(tick, g2);
			g2.drawString(tick, (int) (x - (tickRect.getWidth() / 2)),
					(int) (y + tickRect.getHeight()));
		}

		// Y axis
		int yAxisY = titleHeight;
		int yAxisWidth = (int) (width * YAXIS_X);
		int yAxisHeight = height - titleHeight - (int) (height * XAXIS_Y);
		String ylabel = _yaxis.getLabel();
		Rectangle2D ylabelRect = fm.getStringBounds(ylabel, g2);
		try {
			Graphics2D g2tmp = (Graphics2D) g2.create();
			g2tmp.transform(AffineTransform.getRotateInstance(-Math.PI / 2));
			g2tmp.drawString(ylabel, -(yAxisHeight + titleHeight)
					+ (int) (yAxisHeight / 2 - ylabelRect.getWidth() / 2),
					(int) (ylabelRect.getHeight()));
			g2tmp.dispose();
		} catch (ClassCastException ex) {
			System.err.println("Warning : Cannot write vertical text.");
		}
		ticks = yaxis.getTicks();
		for (int i = 0; i < ticks.size(); i++) {
			int x = yAxisWidth;
			int y = yaxis.compute2DCoord(ticks.get(i), yAxisHeight,
					titleHeight, true);
			String tick = df.format(ticks.get(i));
			Rectangle2D tickRect = fm.getStringBounds(tick, g2);
			g2.drawString(tick, (int) (x - tickRect.getWidth()),
					(int) (y + tickRect.getHeight() / 2));
		}

		g2.setClip(xAxisX, yAxisY, xAxisWidth, yAxisHeight);

		// Paint the plot
		for (DataProperties dataSet : _data) {
			g2.setColor(dataSet.getColor());
			DataProperties.DisplayStyle style = dataSet.getStyle();
			if (style == DataProperties.DisplayStyle.LINE) {
				List<double[]> ds = dataSet.getData();
				for (int i = 1; i < ds.size(); i++) {
					double[] p1 = ds.get(i - 1);
					double[] p2 = ds.get(i);
					int x1 = xaxis.compute2DCoord(p1[0], xAxisWidth, xAxisX,
							false);
					int y1 = yaxis.compute2DCoord(p1[1], yAxisHeight, yAxisY,
							true);
					int x2 = xaxis.compute2DCoord(p2[0], xAxisWidth, xAxisX,
							false);
					int y2 = yaxis.compute2DCoord(p2[1], yAxisHeight, yAxisY,
							true);
					g2.drawLine(x1, y1, x2, y2);
				}
			}
			if (style == DataProperties.DisplayStyle.DASHED) {
				List<double[]> ds = dataSet.getData();
				Graphics2D g2tmp = (Graphics2D) g2.create();
				g2tmp.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
						BasicStroke.JOIN_ROUND, 5.0f, new float[] { 5.0f },
						0.0f));
				for (int i = 1; i < ds.size(); i++) {
					double[] p1 = ds.get(i - 1);
					double[] p2 = ds.get(i);
					int x1 = xaxis.compute2DCoord(p1[0], xAxisWidth, xAxisX,
							false);
					int y1 = yaxis.compute2DCoord(p1[1], yAxisHeight, yAxisY,
							true);
					int x2 = xaxis.compute2DCoord(p2[0], xAxisWidth, xAxisX,
							false);
					int y2 = yaxis.compute2DCoord(p2[1], yAxisHeight, yAxisY,
							true);
					g2tmp.drawLine(x1, y1, x2, y2);
				}
				g2tmp.dispose();
			}
			if (style == DataProperties.DisplayStyle.POINT) {
				List<double[]> ds = dataSet.getData();
				for (int i = 0; i < ds.size(); i++) {
					double[] p = ds.get(i);
					int x = xaxis.compute2DCoord(p[0], xAxisWidth, xAxisX,
							false);
					int y = yaxis.compute2DCoord(p[1], yAxisHeight, yAxisY,
							true);
					g2.fillOval(x - (POINT_SIZE / 2), y - (POINT_SIZE / 2),
							POINT_SIZE, POINT_SIZE);
				}
			}
		}

		g2.setColor(Color.BLACK);
		g2.drawRect(xAxisX, yAxisY, xAxisWidth - 1, yAxisHeight - 1);
	}
}