package tmrutil.graphics;

import java.awt.GridLayout;
import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

/**
 * A range iterator that also constructs a progress bar that tracks the progress
 * of a for loop.
 * 
 * @author Timothy Mann
 * 
 */
public class ProgressIterator extends JProgressBar implements Iterator<Integer>
{
	private static final long serialVersionUID = 5237228914337133391L;
	private static final int DEFAULT_WIDTH = 400;
	private static final int DEFAULT_HEIGHT = 80;

	private int _beginIndex;
	private int _currentIndex;
	private int _endIndex;
	private int _inc;

	/**
	 * Constructs an iterator associated with a progress bar.
	 * 
	 * @param beginIndex
	 *            the first index
	 * @param endIndex
	 *            the comparison value
	 * @param inc
	 *            the value to increment by
	 * @throws IllegalArgumentException
	 *             to indicate that the iterator will never terminate
	 */
	public ProgressIterator(int beginIndex, int endIndex, int inc)
			throws IllegalArgumentException
	{
		super(SwingConstants.HORIZONTAL, 0, 100);
		if ((beginIndex < endIndex && inc <= 0)
				|| (beginIndex > endIndex && inc >= 0)) {
			throw new IllegalArgumentException(
					"The increment argument ("
							+ inc
							+ ") is either 0 or has the wrong sign so that adding that value to the beginning index ("
							+ beginIndex + ") will never reach the end index ("
							+ endIndex + ").");
		}
		_beginIndex = beginIndex;
		_currentIndex = _beginIndex;
		_endIndex = endIndex;
		_inc = inc;

		setStringPainted(true);
		setValue(0);
	}

	@Override
	public boolean hasNext()
	{
		if ((_inc > 0 && (_currentIndex < _endIndex))
				|| (_inc < 0 && (_currentIndex > _endIndex))) {
			return true;
		} else {
			setValue(100);
			return false;
		}
	}

	@Override
	public Integer next()
	{
		if (hasNext()) {
			int tmp = _currentIndex;
			_currentIndex += _inc;
			int value = (int) (100 * ((_currentIndex - _beginIndex) / (double) (_endIndex - _beginIndex)));
			if(value > 100){value = 100;}
			if(value < 0){value = 0;}
			setValue(value);
			return tmp;
		} else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException(
				"Removing elements from a progress iterator is not supported.");
	}

	/**
	 * Resets the iterator and progress bar back to the initial conditions.
	 */
	public void reset()
	{
		_currentIndex = _beginIndex;
		setValue(0);
	}

	public static final ProgressIterator iterator(int beginIndex,
			int endIndex, int inc)
	{
		ProgressIterator piter = new ProgressIterator(beginIndex, endIndex, inc);
		JFrame frame = new JFrame("Progress Iterator : "
				+ DateFormat.getInstance().format(new Date()));
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		frame.add(piter);
		frame.setVisible(true);
		return piter;
	}

	public static final ProgressIterator iterator(ProgressIterator parent,
			int beginIndex, int endIndex, int inc)
	{
		ProgressIterator child = new ProgressIterator(beginIndex, endIndex, inc);
		JFrame frame = new JFrame("ProgressIterator : "
				+ DateFormat.getInstance().format(new Date()));
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(DEFAULT_WIDTH, 2 * DEFAULT_HEIGHT);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2, 1));
		panel.add(parent);
		panel.add(child);
		frame.add(panel);
		frame.setVisible(true);
		return child;
	}

	public static void main(String[] args)
	{
		ProgressIterator parent = new ProgressIterator(0, 15, 1);
		Iterator<Integer> child = ProgressIterator.iterator(parent, 0, 15, 1);
		while (parent.hasNext()) {
			int i = parent.next();
			while (child.hasNext()) {
				child.next();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ex) {
				}
			}
			((ProgressIterator)child).reset();
		}
	}
}
